package com.cbs.tablemate_version_2.interfaces;

/*********************************************************************
 * Created by Barani on 31-05-2016 in TableMateNew
 ***********************************************************************/
public interface MenuItemQtyChangeListener {

    void incrementQuantity(String p_itemId, int adapterPosition);

    void decrementQuantity(String p_itemId, int adapterPosition);

    void show_open_customization(String p_itemId);

    void update_nc(String p_itemId);
}

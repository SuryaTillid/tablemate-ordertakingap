package com.cbs.tablemate_version_2.interfaces;

/*********************************************************************
 * Created by Barani on 14-12-2017 in TableMateNew
 ***********************************************************************/
public interface ChangeColorListener {
    void changeColor(int position);
}

package com.cbs.tablemate_version_2.interfaces;

/*********************************************************************
 * Created by Barani on 03-11-2016 in TableMateNew
 ***********************************************************************/
public interface GetCustomizedUserData {
    void getCustomizedUserDataListener();
}

package com.cbs.tablemate_version_2.interfaces;

/*********************************************************************
 * Created by Barani on 26-09-2017 in TableMateNew
 ***********************************************************************/
public interface AddPaymentValuesListener {
    void setPaymentValues(int position, String amount, String card_num, String payType, boolean selected);
}

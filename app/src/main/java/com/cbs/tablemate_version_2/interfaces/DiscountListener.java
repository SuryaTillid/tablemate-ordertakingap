package com.cbs.tablemate_version_2.interfaces;

/*********************************************************************
 * Created by Barani on 16-02-2017 in TableMateNew
 ***********************************************************************/
public interface DiscountListener {
    void onDiscountTypeChanged(int DiscountPosition, int position);

    void onDiscountClick(int position);
}

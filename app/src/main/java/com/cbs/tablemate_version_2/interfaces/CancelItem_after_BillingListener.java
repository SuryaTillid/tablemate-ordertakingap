package com.cbs.tablemate_version_2.interfaces;

/*********************************************************************
 * Created by Barani on 05-05-2017 in TableMateNew
 ***********************************************************************/
public interface CancelItem_after_BillingListener {
    void cancelItem_fromListListener(int position, String comment);

    void deleteItem_fromListListener(int position, boolean isChecked, String comment, String other_reason);
}


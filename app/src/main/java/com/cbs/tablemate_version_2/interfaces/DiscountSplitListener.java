package com.cbs.tablemate_version_2.interfaces;

/*********************************************************************
 * Created by Barani on 15-10-2018 in TableMateNew
 ***********************************************************************/
public interface DiscountSplitListener {
    void onDiscountAdded(String discountType, String discountAmount, int position, boolean isChecked);
}

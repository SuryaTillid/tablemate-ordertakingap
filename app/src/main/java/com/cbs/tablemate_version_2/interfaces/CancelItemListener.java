package com.cbs.tablemate_version_2.interfaces;

/*********************************************************************
 * Created by Barani on 04-07-2016 in TableMateNew
 ***********************************************************************/
public interface CancelItemListener {
    void cancelItem_fromListListener(String order_id);

    void cancelItem_fromListListenerModified(String menu_name, String custom_note, String menu_item_id, String custom_data, String nc_order);
}

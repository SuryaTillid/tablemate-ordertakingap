package com.cbs.tablemate_version_2.interfaces;

/*********************************************************************
 * Created by Barani on 16-09-2016 in TableMateNew
 ***********************************************************************/
public interface GetDeliveryStatusListener {
    void getDeliveryStatus_for_menuItemListener(String order_id);
}

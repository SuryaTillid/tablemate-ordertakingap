package com.cbs.tablemate_version_2.interfaces;

/*********************************************************************
 * Created by Barani on 08-10-2018 in TableMateNew
 ***********************************************************************/
public interface ModifyItemListener {
    void update_ModifyItem_fromListListener(int position, boolean isCheck, String value);

    void modifyItem_clickListener(int position);
}

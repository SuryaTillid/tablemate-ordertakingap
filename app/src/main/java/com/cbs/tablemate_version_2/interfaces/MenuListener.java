package com.cbs.tablemate_version_2.interfaces;

/*********************************************************************
 * Created by Barani on 24-05-2016 in TableMateNew
 ***********************************************************************/
public interface MenuListener {
    void onMenuClick(String p_category);

    void onFilterClick(String p_filterType, String p_filterID, String p_filterColor);
}

package com.cbs.tablemate_version_2.interfaces;

/*********************************************************************
 * Created by Barani on 20-08-2018 in TableMateNew
 ***********************************************************************/
public interface TableClickListener {
    void OnTableClick(int childPosition);
}

package com.cbs.tablemate_version_2.UIWidgets;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;

import com.cbs.tablemate_version_2.R;

/*********************************************************************
 * Created by Barani on 14-02-2017 in TableMateNew-2.0
 ***********************************************************************/
public class MyCustomDialog extends ProgressDialog {
    String textMessage;
    private TextView Text_Message;

    public MyCustomDialog(Context context, String p_message) {
        super(context);
        textMessage = p_message;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_dialog);
        Text_Message = findViewById(R.id.Text_Message);
        if (!TextUtils.isEmpty(textMessage)) {
            Text_Message.setText(textMessage);
            Text_Message.setTextColor(Color.parseColor("#ff3300"));
            Text_Message.setTextSize(18);
        }
    }
}

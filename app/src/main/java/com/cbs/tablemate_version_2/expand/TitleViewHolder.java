package com.cbs.tablemate_version_2.expand;

import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.TextView;

import com.cbs.tablemate_version_2.R;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

/*********************************************************************
 * Created by Barani on 10-08-2018 in TableMateNew
 ***********************************************************************/
public class TitleViewHolder extends GroupViewHolder {
    private TextView tierName;

    public TitleViewHolder(View itemView) {
        super(itemView);
        tierName = itemView.findViewById(R.id.list_item_tier_name);
    }

    public void setTierTitle(ExpandableGroup tierTitle) {
        //if (tierTitle instanceof Tier) {
        tierName.setText(tierTitle.getTitle());
        //}
    }

    @Override
    public void expand() {
        animateExpand();
    }

    @Override
    public void collapse() {
        animateCollapse();
    }

    private void animateExpand() {
        RotateAnimation rotate = new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        //arrow.setAnimation(rotate);
    }

    private void animateCollapse() {
        RotateAnimation rotate = new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        //arrow.setAnimation(rotate);
    }
}
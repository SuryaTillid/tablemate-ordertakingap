package com.cbs.tablemate_version_2.expand;

import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.models.Table;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

/*********************************************************************
 * Created by Barani on 10-08-2018 in TableMateNew
 ***********************************************************************/
public class TablesViewHolder extends ChildViewHolder {

    FrameLayout frameGridView;
    TextView txt_Table_message;
    private ImageView img;
    private TextView tableId;
    private TextView tableSize;

    public TablesViewHolder(View itemView) {
        super(itemView);
        img = itemView.findViewById(R.id.imgTable);
        tableId = itemView.findViewById(R.id.txtTableId);
        tableSize = itemView.findViewById(R.id.txtTableSize);
        frameGridView = itemView.findViewById(R.id.frameGridView);
        txt_Table_message = itemView.findViewById(R.id.txtTableText);
    }

    public void setTierName(Table t_obj) {
        txt_Table_message.setVisibility(View.GONE);
        if (t_obj != null) {
            checkHoldTable(t_obj, img, txt_Table_message);
            tableId.setText(t_obj.getTable_name());
            tableSize.setText("Seats:" + t_obj.getSize());
            if (t_obj.isSelected()) {
                itemView.setBackground(itemView.getContext().getResources().getDrawable(R.drawable.edit_background));
            } else {
                itemView.setBackground(null);
            }
        } else {
            frameGridView.setVisibility(View.GONE);
        }
    }

    private void checkHoldTable(Table t_obj, ImageView img, TextView txt_Table_message) {

        String bill_id = t_obj.getBill_id();
        AppLog.write("BILL--", "--" + bill_id);
        AppLog.write("TableStatus--", "--" + t_obj.getStatus());

       /* if(!TextUtils.isEmpty(bill_id)&&order_type.equalsIgnoreCase("dine-in")&&!("Joined".equalsIgnoreCase(t_obj.getStatus())))
        {
            img.setImageResource(R.drawable.ic_locked_table);
            ColorFilter filter = new LightingColorFilter(Color.parseColor("#ffc002"),Color.parseColor("#ffc002"));
            img.setColorFilter(filter);
        }
        else
        {*/
        if ("0".equalsIgnoreCase(t_obj.getStatus())) {
            img.setImageResource(R.drawable.ic_free_table);
            ColorFilter filter = new LightingColorFilter(Color.parseColor("#008000"),
                    Color.parseColor("#008000"));
            img.setColorFilter(filter);
            txt_Table_message.setVisibility(View.GONE);
        } else if ("1".equalsIgnoreCase(t_obj.getStatus())) {
            img.setImageResource(R.drawable.ic_locked_table);
            ColorFilter filter = new LightingColorFilter(Color.RED, Color.RED);
            img.setColorFilter(filter);
            txt_Table_message.setVisibility(View.GONE);
        } else if ("3".equalsIgnoreCase(t_obj.getStatus())) {
            img.setImageResource(R.drawable.ic_locked_table);
            ColorFilter filter = new LightingColorFilter(Color.parseColor("#ffc002"), Color.parseColor("#ffc002"));
            img.setColorFilter(filter);
            txt_Table_message.setVisibility(View.GONE);
        } else if ("2".equalsIgnoreCase(t_obj.getStatus())) {
            img.setImageResource(R.drawable.ic_join_table);
            ColorFilter filter = new LightingColorFilter(Color.BLUE, Color.BLUE);
            img.setColorFilter(filter);
            img.setEnabled(false);
            t_obj.setSelected(false);
            txt_Table_message.setVisibility(View.VISIBLE);
            txt_Table_message.setText("Root : " + t_obj.getRoot_table_id());
        }
        //}
    }
}

package com.cbs.tablemate_version_2.utilities;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.AsyncTask;

import com.cbs.tablemate_version_2.configuration.AppLog;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.UUID;

/*********************************************************************
 * Created by Barani on 18-04-2017 in TableMateNew
 ***********************************************************************/
public class MyBluetoothConnector {
    private static final String TAG = "MY BLUETOOTH";
    private static final String SPP_UUID = "00001101-0000-1000-8000-00805F9B34FB";
    private BluetoothSocket mSocket;
    private OutputStream mOutputStream;
    private ConnectTask mConnectTask;
    //byte[] format = {29, 33, 35 };
    private BluetoothConnectionListener mListener;
    private boolean mIsConnecting = false;

    public MyBluetoothConnector(BluetoothConnectionListener listener) {
        mListener = listener;
    }

    public boolean isConnecting() {
        return mIsConnecting;
    }

    public boolean isConnected() {
        return mSocket != null;
    }

    public void connect(BluetoothDevice device) throws ConnectionException {
        if (mIsConnecting && mConnectTask != null) {
            throw new ConnectionException("Connection in progress");
        }

        if (mSocket != null) {
            throw new ConnectionException("Socket already connected");
        }
        (mConnectTask = new ConnectTask(device)).execute();
    }

    public void disconnect() throws ConnectionException {
        if (mSocket == null) {
            throw new ConnectionException("Socket is not connected");
        }

        try {
            mSocket.close();
            mSocket = null;
            mListener.onDisconnected();
        } catch (IOException e) {
            throw new ConnectionException(e.getMessage());
        }
    }

    public void cancel() throws ConnectionException {
        if (mIsConnecting && mConnectTask != null) {
            mConnectTask.cancel(true);
        } else {
            throw new ConnectionException("No connection is in progress");
        }
    }

    public void sendData(StringBuilder msg) throws ConnectionException {
        if (mSocket == null) {
            throw new ConnectionException("Socket is not connected, try to call connect() first");
        }

        try {
            String s = msg.toString();
            byte[] b = s.getBytes();
            //mOutputStream.write(format);
            mOutputStream.write(b);
            mOutputStream.flush();
            AppLog.write(TAG, "----------------------------------" + b);
        } catch (Exception e) {
            throw new ConnectionException(e.getMessage());
        }
    }

    public interface BluetoothConnectionListener {
        void onStartConnecting();

        void onConnectionCancelled();

        void onConnectionSuccess();

        void onConnectionFailed(String error);

        void onDisconnected();
    }

    public class ConnectTask extends AsyncTask<URL, Integer, Long> {
        BluetoothDevice device;
        String error = "";

        public ConnectTask(BluetoothDevice device) {
            this.device = device;
        }

        protected void onCancelled() {
            mIsConnecting = false;
            mListener.onConnectionCancelled();
        }

        protected void onPreExecute() {
            mListener.onStartConnecting();
            mIsConnecting = true;
        }

        protected Long doInBackground(URL... urls) {
            long result = 0;
            try {
                mSocket = device.createRfcommSocketToServiceRecord(UUID.fromString(SPP_UUID));
                mSocket.connect();
                mOutputStream = mSocket.getOutputStream();
                result = 1;
            } catch (IOException e) {
                e.printStackTrace();
                error = e.getMessage();
            }
            return result;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(Long result) {
            mIsConnecting = false;

            if (mSocket != null && result == 1) {
                mListener.onConnectionSuccess();
            } else {
                mListener.onConnectionFailed("Connection failed " + error);
            }
        }
    }
}


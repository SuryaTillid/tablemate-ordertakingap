package com.cbs.tablemate_version_2.utilities;

import com.cbs.tablemate_version_2.configuration.AppLog;

import java.util.ArrayList;
import java.util.List;

/*********************************************************************
 * Created by Barani on 25-06-2017 in TableMateNew
 ***********************************************************************/
public class StringUtils {
    public static String center(String s, int size) {
        return center(s, size, ' ');
    }

    public static String center(String s, int size, char pad) {
        if (s == null || size <= s.length())
            return s;

        StringBuilder sb = new StringBuilder(size);
        for (int i = 0; i < (size - s.length()) / 2; i++) {
            sb.append(pad);
        }
        sb.append(s);
        while (sb.length() < size) {
            sb.append(pad);
        }
        return sb.toString();
    }

    public static List<String> splitEqually(String text, int size) {
        // Give the list the right capacity to start with. You could use an array
        // instead if you wanted.
        AppLog.write("Text.Length--", "" + text.length());
        List<String> ret = new ArrayList<String>((text.length() + size - 1) / size);

        for (int start = 0; start < text.length(); start += size) {
            ret.add(text.substring(start, Math.min(text.length(), start + size)));
        }
        AppLog.write("Text.Length-----", "" + ret);
        return ret;
    }
}
package com.cbs.tablemate_version_2.utilities;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;

import com.cbs.tablemate_version_2.models.CustomizationItemsType;
import com.cbs.tablemate_version_2.models.Item;

import java.io.InputStream;
import java.io.OutputStream;

/*********************************************************************
 * Created by Barani on 14-02-2017 in TableMateNew
 ***********************************************************************/
public class Utility extends Activity {
    public static int customCounter = 0;

    public static int convertToInteger(String itemQty) {
        if (TextUtils.isEmpty(itemQty)) {
            return 0;
        } else {
            try {
                return Integer.parseInt(itemQty);
            } catch (Exception ex) {
                ex.printStackTrace();
                return 0;
            }
        }
    }

    public static double convertToDouble(String p_total) {
        if (TextUtils.isEmpty(p_total)) {
            return 0.0;
        } else {
            try {
                return Double.parseDouble(p_total);
            } catch (Exception ex) {
                ex.printStackTrace();
                return 0.0;
            }
        }
    }

    public static double convertToFloat(String p_total) {
        if (TextUtils.isEmpty(p_total)) {
            return 0.0;
        } else {
            try {
                return Float.parseFloat(p_total);
            } catch (Exception ex) {
                ex.printStackTrace();
                return 0.0;
            }
        }
    }


    public static double getItemPriceNC(Item item, String orderType, String NcOrder) {
        String item_price;
        if (orderType.equalsIgnoreCase("2")) {
            item_price = item.getAlternate_price();
            Log.e("item_price1111", "" + item_price);
        } else {
            item_price = item.getPrice();

        }
        double price = 0.0;
        if (item.getCustomised_item().equals("1")) {
            if (null != item.getCustomType() || null != item.getCustomPortion() || null != item.getCustomAddOn()) {
                if (item.getCustomType().size() > 0) {
                    for (CustomizationItemsType type : item.getCustomType()) {

                        if (!NcOrder.isEmpty() && NcOrder.equals("1")) {
                            price += Utility.convertToDouble("0.0");
                        } else {
                            price += Utility.convertToDouble(type.getPrice());
                        }

                        Log.e("price", "" + price);
                    }
                }
                if (null != item.getCustomPortion() && !NcOrder.isEmpty() && NcOrder.equals("1")) {
                    price += Utility.convertToDouble("0.0");
                }

                if (null != item.getCustomPortion() && NcOrder.isEmpty()) {
                    price += Utility.convertToFloat(item.getCustomPortion().getPortion_price());

                    Log.e("item_pricfsdfsdfe", "" + price);
                }
                if (null != item.getCustomAddOn()) {
                    price += Utility.convertToDouble(item.getCustomAddOn().getChargeblePrice());
                }
            } else {
                price = Utility.convertToFloat(item_price);

            }
        } else {
            price = Utility.convertToFloat(item_price);

        }
        return price;
    }

    public static double getItemPrice(Item item, String orderType) {
        String item_price;
        if (orderType.equalsIgnoreCase("2")) {
            item_price = item.getAlternate_price();
            Log.e("item_price1111", "" + item_price);

        } else {
            item_price = item.getPrice();

        }


        double price = 0.0;
        if (item.getCustomised_item().equals("1")) {
            if (null != item.getCustomType() || null != item.getCustomPortion() || null != item.getCustomAddOn()) {
                if (item.getCustomType().size() > 0) {
                    for (CustomizationItemsType type : item.getCustomType()) {
                        price += Utility.convertToDouble(type.getPrice());
                    }
                }
                if (null != item.getCustomPortion()) {
                    price += Utility.convertToDouble(item.getCustomPortion().getPortion_price());
                }
                if (null != item.getCustomAddOn()) {
                    price += Utility.convertToDouble(item.getCustomAddOn().getChargeblePrice());
                }
            } else {
                price = Utility.convertToFloat(item_price);
            }
        } else {
            price = Utility.convertToFloat(item_price);
        }

        Log.e("item_price", "" + price);
        return price;
    }


    public static String getItemName(Item item) {
        String itemName = item.getName();
        if (item.getCustomised_item().equals("1")) {
            if (null != item.getCustomType() || null != item.getCustomPortion() || null != item.getCustomAddOn()) {
                if (item.getCustomType().size() > 0) {

                    for (CustomizationItemsType type : item.getCustomType()) {
                        itemName += " + " + type.getCustomisation_name();
                    }
                    // itemName += " + " + item.getCustomType().getCustomisation_name();
                }
                if (null != item.getCustomPortion()) {
                    itemName += " + " + item.getCustomPortion().getPortion_name();
                }
                if (null != item.getCustomAddOn()) {
                    itemName += " + " + item.getCustomAddOn().getStockName();
                }
            }
        }
        return itemName;
    }

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }
}


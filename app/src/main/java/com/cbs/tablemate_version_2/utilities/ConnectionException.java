package com.cbs.tablemate_version_2.utilities;

/*********************************************************************
 * Created by Barani on 18-04-2017 in TableMateNew
 ***********************************************************************/
public class ConnectionException extends Exception {
    private static final long serialVersionUID = 1L;

    String error = "Error";

    public ConnectionException(String msg) {
        super(msg);

        error = msg;
    }

    public String getError() {
        return error;
    }
}

package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 15-07-2016 in TableMateNew
 ***********************************************************************/
public class GetPaymentOptions {
    private String id;
    private String paymentmode;
    private String row_changed;
    private String row_uploaded;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPaymentmode() {
        return paymentmode;
    }

    public void setPaymentmode(String paymentmode) {
        this.paymentmode = paymentmode;
    }

    public String getRow_changed() {
        return row_changed;
    }

    public void setRow_changed(String row_changed) {
        this.row_changed = row_changed;
    }

    public String getRow_uploaded() {
        return row_uploaded;
    }

    public void setRow_uploaded(String row_uploaded) {
        this.row_uploaded = row_uploaded;
    }
}

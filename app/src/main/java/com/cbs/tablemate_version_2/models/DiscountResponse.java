package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 04-08-2016 in TableMateNew
 ***********************************************************************/
public class DiscountResponse {
    public int success;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }
}

package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 11-07-2017 in TableMateNew
 ***********************************************************************/
public class Emp_BillSummary {

    private String table_result;

    private String nofbill;

    private String date;

    public String getTable_result() {
        return table_result;
    }

    public void setTable_result(String table_result) {
        this.table_result = table_result;
    }

    public String getNofbill() {
        return nofbill;
    }

    public void setNofbill(String nofbill) {
        this.nofbill = nofbill;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}

package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 28-03-2017 in TableMateNew
 ***********************************************************************/
public class DeliveryPartnerDetails {
    private String id;
    private String partner_name;
    private String contact_address;
    private String primary_contact_number;
    private String secondary_contact_number;
    private String payment_period_In_days;

    public DeliveryPartnerDetails(String id, String partner_name) {
        this.id = id;
        this.partner_name = partner_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPartner_name() {
        return partner_name;
    }

    public void setPartner_name(String partner_name) {
        this.partner_name = partner_name;
    }

    public String getContact_address() {
        return contact_address;
    }

    public void setContact_address(String contact_address) {
        this.contact_address = contact_address;
    }

    public String getPrimary_contact_number() {
        return primary_contact_number;
    }

    public void setPrimary_contact_number(String primary_contact_number) {
        this.primary_contact_number = primary_contact_number;
    }

    public String getSecondary_contact_number() {
        return secondary_contact_number;
    }

    public void setSecondary_contact_number(String secondary_contact_number) {
        this.secondary_contact_number = secondary_contact_number;
    }

    public String getPayment_period_In_days() {
        return payment_period_In_days;
    }

    public void setPayment_period_In_days(String payment_period_In_days) {
        this.payment_period_In_days = payment_period_In_days;
    }
}

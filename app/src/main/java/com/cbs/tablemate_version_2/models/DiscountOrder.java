package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 03-08-2016 in TableMateNew
 ***********************************************************************/
public class DiscountOrder {
    private Double discount_value;
    private Double item_price;
    private String order_id;
    private String discount_price;
    private String discount_type;
    private String id;
    private String mrp_price;
    private String menu_item_id;
    private String nc_order;
    private String modified_sale_price;
    private String sale_price;
    private String discount;
    private boolean isChecked;
    private String delete_comment;
    private String other_reason;

    public Double getDiscount_value() {
        return discount_value;
    }

    public void setDiscount_value(Double discount_value) {
        this.discount_value = discount_value;
    }

    public Double getItem_price() {
        return item_price;
    }

    public void setItem_price(Double item_price) {
        this.item_price = item_price;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getDiscount_price() {
        return discount_price;
    }

    public void setDiscount_price(String discount_price) {
        this.discount_price = discount_price;
    }

    public String getDiscount_type() {
        return discount_type;
    }

    public void setDiscount_type(String discount_type) {
        this.discount_type = discount_type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMrp_price() {
        return mrp_price;
    }

    public void setMrp_price(String mrp_price) {
        this.mrp_price = mrp_price;
    }

    public String getMenu_item_id() {
        return menu_item_id;
    }

    public void setMenu_item_id(String menu_item_id) {
        this.menu_item_id = menu_item_id;
    }

    public String getNc_order() {
        return nc_order;
    }

    public void setNc_order(String nc_order) {
        this.nc_order = nc_order;
    }

    public String getModified_sale_price() {
        return modified_sale_price;
    }

    public void setModified_sale_price(String modified_sale_price) {
        this.modified_sale_price = modified_sale_price;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getDelete_comment() {
        return delete_comment;
    }

    public void setDelete_comment(String delete_comment) {
        this.delete_comment = delete_comment;
    }

    public String getOther_reason() {
        return other_reason;
    }

    public void setOther_reason(String other_reason) {
        this.other_reason = other_reason;
    }

    public String getSale_price() {
        return sale_price;
    }

    public void setSale_price(String sale_price) {
        this.sale_price = sale_price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }
}
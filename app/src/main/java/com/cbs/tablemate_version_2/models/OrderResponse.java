package com.cbs.tablemate_version_2.models;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 12-06-2016 in TableMateNew
 ***********************************************************************/
public class OrderResponse {

    private String success;
    private String generated_kot;
    private ArrayList<ProcessedOrders> pro_orders = new ArrayList<>();
    private String waiter_id;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getGenerated_kot() {
        return generated_kot;
    }

    public void setGenerated_kot(String generated_kot) {
        this.generated_kot = generated_kot;
    }

    public ArrayList<ProcessedOrders> getPro_orders() {
        return pro_orders;
    }

    public void setPro_orders(ArrayList<ProcessedOrders> pro_orders) {
        this.pro_orders = pro_orders;
    }

    public String getWaiter_id() {
        return waiter_id;
    }

    public void setWaiter_id(String waiter_id) {
        this.waiter_id = waiter_id;
    }
}

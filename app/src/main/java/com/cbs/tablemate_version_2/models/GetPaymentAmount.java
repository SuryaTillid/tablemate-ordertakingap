package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 10-07-2017 in TableMateNew
 ***********************************************************************/
public class GetPaymentAmount {
    private String amount;
    private String mode;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }
}

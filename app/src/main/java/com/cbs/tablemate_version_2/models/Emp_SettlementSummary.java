package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 11-07-2017 in TableMateNew
 ***********************************************************************/
public class Emp_SettlementSummary {

    private String total;

    private String paymentMode;

    private String billTotal;

    private String date;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getBillTotal() {
        return billTotal;
    }

    public void setBillTotal(String billTotal) {
        this.billTotal = billTotal;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}

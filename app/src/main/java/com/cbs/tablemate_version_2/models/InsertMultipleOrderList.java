package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 26-04-2018 in TableMateNew
 ***********************************************************************/
public class InsertMultipleOrderList {

    private String menu_item_id;
    private String waiter_id;
    private String type;
    private String customer_id;
    private String quantity;
    private String custom_data;
    private String occupancy;
    private String customisation_comments;
    private String filter_type_id;
    private String sale_price;
    private String portion_size;
    private String mrp_price;
    private String discount;
    private String discount_type;

    public String getMenu_item_id() {
        return menu_item_id;
    }

    public void setMenu_item_id(String menu_item_id) {
        this.menu_item_id = menu_item_id;
    }

    public String getWaiter_id() {
        return waiter_id;
    }

    public void setWaiter_id(String waiter_id) {
        this.waiter_id = waiter_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getCustom_data() {
        return custom_data;
    }

    public void setCustom_data(String custom_data) {
        this.custom_data = custom_data;
    }

    public String getOccupancy() {
        return occupancy;
    }

    public void setOccupancy(String occupancy) {
        this.occupancy = occupancy;
    }

    public String getCustomisation_comments() {
        return customisation_comments;
    }

    public void setCustomisation_comments(String customisation_comments) {
        this.customisation_comments = customisation_comments;
    }

    public String getFilter_type_id() {
        return filter_type_id;
    }

    public void setFilter_type_id(String filter_type_id) {
        this.filter_type_id = filter_type_id;
    }

    public String getSale_price() {
        return sale_price;
    }

    public void setSale_price(String sale_price) {
        this.sale_price = sale_price;
    }

    public String getPortion_size() {
        return portion_size;
    }

    public void setPortion_size(String portion_size) {
        this.portion_size = portion_size;
    }

    public String getMrp_price() {
        return mrp_price;
    }

    public void setMrp_price(String mrp_price) {
        this.mrp_price = mrp_price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscount_type() {
        return discount_type;
    }

    public void setDiscount_type(String discount_type) {
        this.discount_type = discount_type;
    }
}

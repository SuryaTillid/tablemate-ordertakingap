package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 31-10-2017 in TableMateNew
 ***********************************************************************/
public class StoreName {
    private String id;
    private String name;
    private String phoneno;
    private String city;
    private String address;
    private String email;
    private String hotel_profile_id;
    private String bill_sequence;
    private String bill_sequence_food;
    private String bill_sequence_alcohol;
    private String row_changed;
    private String row_uploaded;
    private String last_sync;
    private String last_update;
    private String store_master_unique_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHotel_profile_id() {
        return hotel_profile_id;
    }

    public void setHotel_profile_id(String hotel_profile_id) {
        this.hotel_profile_id = hotel_profile_id;
    }

    public String getBill_sequence() {
        return bill_sequence;
    }

    public void setBill_sequence(String bill_sequence) {
        this.bill_sequence = bill_sequence;
    }

    public String getBill_sequence_food() {
        return bill_sequence_food;
    }

    public void setBill_sequence_food(String bill_sequence_food) {
        this.bill_sequence_food = bill_sequence_food;
    }

    public String getBill_sequence_alcohol() {
        return bill_sequence_alcohol;
    }

    public void setBill_sequence_alcohol(String bill_sequence_alcohol) {
        this.bill_sequence_alcohol = bill_sequence_alcohol;
    }

    public String getRow_changed() {
        return row_changed;
    }

    public void setRow_changed(String row_changed) {
        this.row_changed = row_changed;
    }

    public String getRow_uploaded() {
        return row_uploaded;
    }

    public void setRow_uploaded(String row_uploaded) {
        this.row_uploaded = row_uploaded;
    }

    public String getLast_sync() {
        return last_sync;
    }

    public void setLast_sync(String last_sync) {
        this.last_sync = last_sync;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public String getStore_master_unique_id() {
        return store_master_unique_id;
    }

    public void setStore_master_unique_id(String store_master_unique_id) {
        this.store_master_unique_id = store_master_unique_id;
    }
}

package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 14-12-2017 in TableMateNew
 ***********************************************************************/
public class CustomizationPreview {
    private String order_id;
    private String customisation_name;
    private String price;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getCustomisation_name() {
        return customisation_name;
    }

    public void setCustomisation_name(String customisation_name) {
        this.customisation_name = customisation_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}

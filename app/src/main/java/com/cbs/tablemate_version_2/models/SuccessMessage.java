package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 14-12-2017 in TableMateNew
 ***********************************************************************/
public class SuccessMessage {
    private String success;
    private String message;
    private String bill_id;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBill_id() {
        return bill_id;
    }

    public void setBill_id(String bill_id) {
        this.bill_id = bill_id;
    }
}

package com.cbs.tablemate_version_2.models;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 10-06-2016 in TableMateNew
 ***********************************************************************/
public class OrderDetails {
    private String tableID;
    private String customername;
    private String uname;
    private String customernumber;
    private String type;
    private String occupancy;
    private String customisation_comments;
    private String wtr_id;
    private String waiter_id_confirmation;
    private String order_type;
    private String generated_kot;
    private String delivery_partner;
    private String bill_id;
    private ArrayList<Order> orders = new ArrayList<>();

    public String getTableID() {
        return tableID;
    }

    public void setTableID(String tableID) {
        this.tableID = tableID;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getCustomernumber() {
        return customernumber;
    }

    public void setCustomernumber(String customernumber) {
        this.customernumber = customernumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOccupancy() {
        return occupancy;
    }

    public void setOccupancy(String occupancy) {
        this.occupancy = occupancy;
    }

    public String getCustomisation_comments() {
        return customisation_comments;
    }

    public void setCustomisation_comments(String customisation_comments) {
        this.customisation_comments = customisation_comments;
    }

    public String getWtr_id() {
        return wtr_id;
    }

    public void setWtr_id(String wtr_id) {
        this.wtr_id = wtr_id;
    }

    public String getWaiter_id_confirmation() {
        return waiter_id_confirmation;
    }

    public void setWaiter_id_confirmation(String waiter_id_confirmation) {
        this.waiter_id_confirmation = waiter_id_confirmation;
    }

    public ArrayList<Order> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<Order> orders) {
        this.orders = orders;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getGenerated_kot() {
        return generated_kot;
    }

    public void setGenerated_kot(String generated_kot) {
        this.generated_kot = generated_kot;
    }

    public String getDelivery_partner() {
        return delivery_partner;
    }

    public void setDelivery_partner(String delivery_partner) {
        this.delivery_partner = delivery_partner;
    }

    public String getBill_id() {
        return bill_id;
    }

    public void setBill_id(String bill_id) {
        this.bill_id = bill_id;
    }
}

package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 23-06-2016 in TableMateNew
 ***********************************************************************/
public class GetTotalModel {
    private String total;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}

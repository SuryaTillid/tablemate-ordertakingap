package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 09-07-2017 in TableMateNew
 ***********************************************************************/
public class Emp_SalesSummary {
    private String total;

    private String billGrandTotal;

    private String round_off;

    private String billtax;

    private String billtotal;

    private String discount;

    private String order_type;

    private String totalbill;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getBillGrandTotal() {
        return billGrandTotal;
    }

    public void setBillGrandTotal(String billGrandTotal) {
        this.billGrandTotal = billGrandTotal;
    }

    public String getRound_off() {
        return round_off;
    }

    public void setRound_off(String round_off) {
        this.round_off = round_off;
    }

    public String getBilltax() {
        return billtax;
    }

    public void setBilltax(String billtax) {
        this.billtax = billtax;
    }

    public String getBilltotal() {
        return billtotal;
    }

    public void setBilltotal(String billtotal) {
        this.billtotal = billtotal;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getTotalbill() {
        return totalbill;
    }

    public void setTotalbill(String totalbill) {
        this.totalbill = totalbill;
    }

}

package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 31-10-2017 in TableMateNew
 ***********************************************************************/
public class EmployeeTypeName {
    private String id;
    private String name;
    private String discount_precentage;
    private String row_changed;
    private String row_uploaded;
    private String last_sync;
    private String last_update;
    private String employee_types_unique_id;
    private boolean isSelected;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDiscount_precentage() {
        return discount_precentage;
    }

    public void setDiscount_precentage(String discount_precentage) {
        this.discount_precentage = discount_precentage;
    }

    public String getRow_changed() {
        return row_changed;
    }

    public void setRow_changed(String row_changed) {
        this.row_changed = row_changed;
    }

    public String getRow_uploaded() {
        return row_uploaded;
    }

    public void setRow_uploaded(String row_uploaded) {
        this.row_uploaded = row_uploaded;
    }

    public String getLast_sync() {
        return last_sync;
    }

    public void setLast_sync(String last_sync) {
        this.last_sync = last_sync;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public String getEmployee_types_unique_id() {
        return employee_types_unique_id;
    }

    public void setEmployee_types_unique_id(String employee_types_unique_id) {
        this.employee_types_unique_id = employee_types_unique_id;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}

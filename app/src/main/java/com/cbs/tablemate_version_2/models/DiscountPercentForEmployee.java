package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 12-02-2018 in TableMateNew
 ***********************************************************************/
public class DiscountPercentForEmployee {

    private String discount_precentage;

    public String getDiscount_percentage() {
        return discount_precentage;
    }

    public void setDiscount_percentage(String discount_percentage) {
        this.discount_precentage = discount_percentage;
    }
}

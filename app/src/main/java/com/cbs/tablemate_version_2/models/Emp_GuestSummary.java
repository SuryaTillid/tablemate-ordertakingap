package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 11-07-2017 in TableMateNew
 ***********************************************************************/
public class Emp_GuestSummary {
    private String averagetotal;

    private String date;

    private String occupancy;

    public String getAveragetotal() {
        return averagetotal;
    }

    public void setAveragetotal(String averagetotal) {
        this.averagetotal = averagetotal;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOccupancy() {
        return occupancy;
    }

    public void setOccupancy(String occupancy) {
        this.occupancy = occupancy;
    }
}

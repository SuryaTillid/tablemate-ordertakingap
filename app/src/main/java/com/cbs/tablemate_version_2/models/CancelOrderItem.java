package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 24-09-2018 in TableMateNew
 ***********************************************************************/
public class CancelOrderItem {
    private String order_id, cancel_comments, other_reason;

    public CancelOrderItem(String order_id, String cancel_comments, String other_reason) {
        this.order_id = order_id;
        this.cancel_comments = cancel_comments;
        this.other_reason = other_reason;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getCancel_comments() {
        return cancel_comments;
    }

    public void setCancel_comments(String cancel_comments) {
        this.cancel_comments = cancel_comments;
    }

    public String getOther_reason() {
        return other_reason;
    }

    public void setOther_reason(String other_reason) {
        this.other_reason = other_reason;
    }
}

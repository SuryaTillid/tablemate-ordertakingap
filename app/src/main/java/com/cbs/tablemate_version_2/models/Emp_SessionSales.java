package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 11-07-2017 in TableMateNew
 ***********************************************************************/
public class Emp_SessionSales {

    private String total;

    private String sale_price;

    private String round_off;

    private String nofbill;

    private String billtax;

    private String billtotal;

    private String mrp_price;

    private String gtotal;

    private String discount;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getSale_price() {
        return sale_price;
    }

    public void setSale_price(String sale_price) {
        this.sale_price = sale_price;
    }

    public String getRound_off() {
        return round_off;
    }

    public void setRound_off(String round_off) {
        this.round_off = round_off;
    }

    public String getNofbill() {
        return nofbill;
    }

    public void setNofbill(String nofbill) {
        this.nofbill = nofbill;
    }

    public String getBilltax() {
        return billtax;
    }

    public void setBilltax(String billtax) {
        this.billtax = billtax;
    }

    public String getBilltotal() {
        return billtotal;
    }

    public void setBilltotal(String billtotal) {
        this.billtotal = billtotal;
    }

    public String getMrp_price() {
        return mrp_price;
    }

    public void setMrp_price(String mrp_price) {
        this.mrp_price = mrp_price;
    }

    public String getGtotal() {
        return gtotal;
    }

    public void setGtotal(String gtotal) {
        this.gtotal = gtotal;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }
}

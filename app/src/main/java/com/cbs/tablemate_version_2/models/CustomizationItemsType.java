package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 29-07-2016 in TableMateNew
 ***********************************************************************/
public class CustomizationItemsType {
    private String id;
    private String customisation_name;
    private String customisation_type;
    private String price;
    private String quantity;
    private String stock_id;
    private boolean isSelected;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomisation_name() {
        return customisation_name;
    }

    public void setCustomisation_name(String customisation_name) {
        this.customisation_name = customisation_name;
    }

    public String getCustomisation_type() {
        return customisation_type;
    }

    public void setCustomisation_type(String customisation_type) {
        this.customisation_type = customisation_type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getStock_id() {
        return stock_id;
    }

    public void setStock_id(String stock_id) {
        this.stock_id = stock_id;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}

package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 03-08-2016 in TableMateNew
 ***********************************************************************/
public class DiscountModel {
    private String bill_id;
    private String order_id;
    private String discount;
    private String sale_price;
    private String discount_type;
    private Double gtotal;
    //private ArrayList<DiscountOrder> orderIds = new ArrayList<>();
    private String remark;
    private Double overalldiscount;

    public DiscountModel() {
    }

    public DiscountModel(String order_id, String discount, String sale_price, String discount_type) {
        this.order_id = order_id;
        this.discount = discount;
        this.sale_price = sale_price;
        this.discount_type = discount_type;
    }

    public String getBill_id() {
        return bill_id;
    }

    public void setBill_id(String bill_id) {
        this.bill_id = bill_id;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public Double getGtotal() {
        return gtotal;
    }

    public void setGtotal(Double gtotal) {
        this.gtotal = gtotal;
    }

    /* public ArrayList<DiscountOrder> getOrderIds() {
         return orderIds;
     }

     public void setOrderIds(ArrayList<DiscountOrder> orderIds) {
         this.orderIds = orderIds;
     }
 */
    public Double getOveralldiscount() {
        return overalldiscount;
    }

    public void setOveralldiscount(Double overalldiscount) {
        this.overalldiscount = overalldiscount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getSale_price() {
        return sale_price;
    }

    public void setSale_price(String sale_price) {
        this.sale_price = sale_price;
    }

    public String getDiscount_type() {
        return discount_type;
    }

    public void setDiscount_type(String discount_type) {
        this.discount_type = discount_type;
    }
}

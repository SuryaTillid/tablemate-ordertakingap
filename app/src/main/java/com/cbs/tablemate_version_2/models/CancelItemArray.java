package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 02-05-2017 in TableMateNew
 ***********************************************************************/
public class CancelItemArray {
    private String order_id;
    private String comments;
    private String delete_comment;
    private String other_reason;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getDelete_comment() {
        return delete_comment;
    }

    public void setDelete_comment(String delete_comment) {
        this.delete_comment = delete_comment;
    }

    public String getOther_reason() {
        return other_reason;
    }

    public void setOther_reason(String other_reason) {
        this.other_reason = other_reason;
    }
}

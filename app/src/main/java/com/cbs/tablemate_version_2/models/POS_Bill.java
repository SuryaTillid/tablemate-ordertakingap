
package com.cbs.tablemate_version_2.models;

public class POS_Bill {

    private String id;
    private String billTotal;
    private String billTax;
    private String billDiscount;
    private String billGrandTotal;
    private String round_off;
    private String employeeId;
    private String date;
    private String amountReceived;
    private String balanceGiven;
    private String paymentMode;
    private String bill_status;
    private String cardNumber;
    private String cardType;
    private String remark;
    private String balance;
    private String partial_amount;
    private String bill_split;
    private String overalldiscount;
    private String comments;
    private String table_id;
    private String store_id;
    private String store_order_id;
    private String bill_id;
    private String occupancy;
    private String row_changed;
    private String row_uploaded;
    private String last_sync;
    private String last_update;
    private String bill_master_unique_id;
    private String order_type;
    private String customer_id;
    private String delivery_partner_name;
    private String food_order_id;
    private String food_bill_id;
    private String alcohol_order_id;
    private String alcohol_bill_id;
    private String tip;
    private String child;
    private String gen_emp_id;
    private String post_room;
    private String delivery_charges;
    private String packaging_charges;
    private String other_charges;
    private String room_ref_no;
    private String bill_date;

    public POS_Bill(String bill_id, String grandTotal, String bill_status) {
        this.bill_id = bill_id;
        this.billGrandTotal = grandTotal;
        this.bill_status = bill_status;
    }

    public POS_Bill() {
    }

    public POS_Bill(String billId) {
        this.bill_id = billId;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The billTotal
     */
    public String getBillTotal() {
        return billTotal;
    }

    /**
     * @param billTotal The billTotal
     */
    public void setBillTotal(String billTotal) {
        this.billTotal = billTotal;
    }

    /**
     * @return The billTax
     */
    public String getBillTax() {
        return billTax;
    }

    /**
     * @param billTax The billTax
     */
    public void setBillTax(String billTax) {
        this.billTax = billTax;
    }

    /**
     * @return The billDiscount
     */
    public String getBillDiscount() {
        return billDiscount;
    }

    /**
     * @param billDiscount The billDiscount
     */
    public void setBillDiscount(String billDiscount) {
        this.billDiscount = billDiscount;
    }

    /**
     * @return The billGrandTotal
     */
    public String getBillGrandTotal() {
        return billGrandTotal;
    }

    /**
     * @param billGrandTotal The billGrandTotal
     */
    public void setBillGrandTotal(String billGrandTotal) {
        this.billGrandTotal = billGrandTotal;
    }

    /**
     * @return The employeeId
     */
    public String getEmployeeId() {
        return employeeId;
    }

    /**
     * @param employeeId The employeeId
     */
    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    /**
     * @return The date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return The amountReceived
     */
    public String getAmountReceived() {
        return amountReceived;
    }

    /**
     * @param amountReceived The amountReceived
     */
    public void setAmountReceived(String amountReceived) {
        this.amountReceived = amountReceived;
    }

    /**
     * @return The balanceGiven
     */
    public String getBalanceGiven() {
        return balanceGiven;
    }

    /**
     * @param balanceGiven The balanceGiven
     */
    public void setBalanceGiven(String balanceGiven) {
        this.balanceGiven = balanceGiven;
    }

    /**
     * @return The paymentMode
     */
    public String getPaymentMode() {
        return paymentMode;
    }

    /**
     * @param paymentMode The paymentMode
     */
    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getBill_master_unique_id() {
        return bill_master_unique_id;
    }

    public void setBill_master_unique_id(String bill_master_unique_id) {
        this.bill_master_unique_id = bill_master_unique_id;
    }

    public String getBill_status() {
        return bill_status;
    }

    public void setBill_status(String bill_status) {
        this.bill_status = bill_status;
    }

    public String getLast_sync() {
        return last_sync;
    }

    public void setLast_sync(String last_sync) {
        this.last_sync = last_sync;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    /**
     * @return The cardNumber
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * @param cardNumber The cardNumber
     */
    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    /**
     * @return The cardType
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * @param cardType The cardType
     */
    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    /**
     * @return The remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark The remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return The balance
     */
    public String getBalance() {
        return balance;
    }

    /**
     * @param balance The balance
     */
    public void setBalance(String balance) {
        this.balance = balance;
    }

    /**
     * @return The partial_amount
     */
    public String getPartial_amount() {
        return partial_amount;
    }

    /**
     * @param partial_amount The partial_amount
     */
    public void setPartial_amount(String partial_amount) {
        this.partial_amount = partial_amount;
    }

    /**
     * @return The bill_split
     */
    public String getBill_split() {
        return bill_split;
    }

    /**
     * @param bill_split The bill_split
     */
    public void setBill_split(String bill_split) {
        this.bill_split = bill_split;
    }

    /**
     * @return The overalldiscount
     */
    public String getOveralldiscount() {
        return overalldiscount;
    }

    /**
     * @param overalldiscount The overalldiscount
     */
    public void setOveralldiscount(String overalldiscount) {
        this.overalldiscount = overalldiscount;
    }

    /**
     * @return The comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * @param comments The comments
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * @return The table_id
     */
    public String getTable_id() {
        return table_id;
    }

    /**
     * @param table_id The table_id
     */
    public void setTable_id(String table_id) {
        this.table_id = table_id;
    }

    /**
     * @return The store_id
     */
    public String getStore_id() {
        return store_id;
    }

    /**
     * @param store_id The store_id
     */
    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    /**
     * @return The store_order_id
     */
    public String getStore_order_id() {
        return store_order_id;
    }

    /**
     * @param store_order_id The store_order_id
     */
    public void setStore_order_id(String store_order_id) {
        this.store_order_id = store_order_id;
    }

    /**
     * @return The bill_id
     */
    public String getBill_id() {
        return bill_id;
    }

    /**
     * @param bill_id The bill_id
     */
    public void setBill_id(String bill_id) {
        this.bill_id = bill_id;
    }

    /**
     * @return The row_changed
     */
    public String getRow_changed() {
        return row_changed;
    }

    /**
     * @param row_changed The row_changed
     */
    public void setRow_changed(String row_changed) {
        this.row_changed = row_changed;
    }

    /**
     * @return The row_uploaded
     */
    public String getRow_uploaded() {
        return row_uploaded;
    }

    /**
     * @param row_uploaded The row_uploaded
     */
    public void setRow_uploaded(String row_uploaded) {
        this.row_uploaded = row_uploaded;
    }

    public String getOccupancy() {
        return occupancy;
    }

    public void setOccupancy(String occupancy) {
        this.occupancy = occupancy;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getRound_off() {
        return round_off;
    }

    public void setRound_off(String round_off) {
        this.round_off = round_off;
    }

    public String getDelivery_partner_name() {
        return delivery_partner_name;
    }

    public void setDelivery_partner_name(String delivery_partner_name) {
        this.delivery_partner_name = delivery_partner_name;
    }

    public String getFood_order_id() {
        return food_order_id;
    }

    public void setFood_order_id(String food_order_id) {
        this.food_order_id = food_order_id;
    }

    public String getFood_bill_id() {
        return food_bill_id;
    }

    public void setFood_bill_id(String food_bill_id) {
        this.food_bill_id = food_bill_id;
    }

    public String getAlcohol_order_id() {
        return alcohol_order_id;
    }

    public void setAlcohol_order_id(String alcohol_order_id) {
        this.alcohol_order_id = alcohol_order_id;
    }

    public String getAlcohol_bill_id() {
        return alcohol_bill_id;
    }

    public void setAlcohol_bill_id(String alcohol_bill_id) {
        this.alcohol_bill_id = alcohol_bill_id;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public String getChild() {
        return child;
    }

    public void setChild(String child) {
        this.child = child;
    }

    public String getGen_emp_id() {
        return gen_emp_id;
    }

    public void setGen_emp_id(String gen_emp_id) {
        this.gen_emp_id = gen_emp_id;
    }

    public String getPost_room() {
        return post_room;
    }

    public void setPost_room(String post_room) {
        this.post_room = post_room;
    }

    public String getDelivery_charges() {
        return delivery_charges;
    }

    public void setDelivery_charges(String delivery_charges) {
        this.delivery_charges = delivery_charges;
    }

    public String getPackaging_charges() {
        return packaging_charges;
    }

    public void setPackaging_charges(String packaging_charges) {
        this.packaging_charges = packaging_charges;
    }

    public String getOther_charges() {
        return other_charges;
    }

    public void setOther_charges(String other_charges) {
        this.other_charges = other_charges;
    }

    public String getRoom_ref_no() {
        return room_ref_no;
    }

    public void setRoom_ref_no(String room_ref_no) {
        this.room_ref_no = room_ref_no;
    }

    public String getBill_date() {
        return bill_date;
    }

    public void setBill_date(String bill_date) {
        this.bill_date = bill_date;
    }
}

package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 26-09-2017 in TableMateNew
 ***********************************************************************/
public class PaymentOption {
    private String paymentType;
    private String amount;
    private String pin;
    private boolean isChecked;

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}

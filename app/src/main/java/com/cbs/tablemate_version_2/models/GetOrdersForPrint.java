package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 17-11-2016 in TableMateNew
 ***********************************************************************/
public class GetOrdersForPrint {
    private String id;
    private String table_id;
    private String date;
    private String status;
    private String menu_item_id;
    private String order_time;
    private String process_time;
    private String deliver_time;
    private String waiter_id;
    private String type;
    private String bill_status;
    private String processed_employee_id;
    private String customer_id;
    private String bill_id;
    private String quantity;
    private String kot;
    private String custom_data;
    private String process_start_time;
    private String discount;
    private String occupancy;
    private String deliver_status;
    private String process_end_time;
    private String custom_combo;
    private String wtr_id;
    private String customisation_comments;
    private String print_req;
    private String row_changed;
    private String row_uploaded;

    public String getBill_id() {
        return bill_id;
    }

    public void setBill_id(String bill_id) {
        this.bill_id = bill_id;
    }

    public String getBill_status() {
        return bill_status;
    }

    public void setBill_status(String bill_status) {
        this.bill_status = bill_status;
    }

    public String getCustom_combo() {
        return custom_combo;
    }

    public void setCustom_combo(String custom_combo) {
        this.custom_combo = custom_combo;
    }

    public String getCustom_data() {
        return custom_data;
    }

    public void setCustom_data(String custom_data) {
        this.custom_data = custom_data;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getCustomisation_comments() {
        return customisation_comments;
    }

    public void setCustomisation_comments(String customisation_comments) {
        this.customisation_comments = customisation_comments;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDeliver_status() {
        return deliver_status;
    }

    public void setDeliver_status(String deliver_status) {
        this.deliver_status = deliver_status;
    }

    public String getDeliver_time() {
        return deliver_time;
    }

    public void setDeliver_time(String deliver_time) {
        this.deliver_time = deliver_time;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKot() {
        return kot;
    }

    public void setKot(String kot) {
        this.kot = kot;
    }

    public String getMenu_item_id() {
        return menu_item_id;
    }

    public void setMenu_item_id(String menu_item_id) {
        this.menu_item_id = menu_item_id;
    }

    public String getOccupancy() {
        return occupancy;
    }

    public void setOccupancy(String occupancy) {
        this.occupancy = occupancy;
    }

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public String getPrint_req() {
        return print_req;
    }

    public void setPrint_req(String print_req) {
        this.print_req = print_req;
    }

    public String getProcess_end_time() {
        return process_end_time;
    }

    public void setProcess_end_time(String process_end_time) {
        this.process_end_time = process_end_time;
    }

    public String getProcess_start_time() {
        return process_start_time;
    }

    public void setProcess_start_time(String process_start_time) {
        this.process_start_time = process_start_time;
    }

    public String getProcess_time() {
        return process_time;
    }

    public void setProcess_time(String process_time) {
        this.process_time = process_time;
    }

    public String getProcessed_employee_id() {
        return processed_employee_id;
    }

    public void setProcessed_employee_id(String processed_employee_id) {
        this.processed_employee_id = processed_employee_id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getRow_changed() {
        return row_changed;
    }

    public void setRow_changed(String row_changed) {
        this.row_changed = row_changed;
    }

    public String getRow_uploaded() {
        return row_uploaded;
    }

    public void setRow_uploaded(String row_uploaded) {
        this.row_uploaded = row_uploaded;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTable_id() {
        return table_id;
    }

    public void setTable_id(String table_id) {
        this.table_id = table_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWaiter_id() {
        return waiter_id;
    }

    public void setWaiter_id(String waiter_id) {
        this.waiter_id = waiter_id;
    }

    public String getWtr_id() {
        return wtr_id;
    }

    public void setWtr_id(String wtr_id) {
        this.wtr_id = wtr_id;
    }
}

package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Baraneeswari on 06-05-2016 in TableMateNew
 ***********************************************************************/
public class Table {
    private String id;
    private String idd;

    private String success;
    private String saved_bill_id;
    private String table_id;
    private String rows;
    private String bill_id;
    private String coloumns;
    private String tables_flag;
    private String status;
    private String size;
    private String tier;
    private String time;
    private String date;
    private String customer_name;
    private String employeename;
    private String root_table;
    private String root_table_id;
    private String root_table_name;
    private String table_name;
    private String table_type;
    private String measurement;
    private String direction;
    private String[] join_tables;
    private String table_active_status;
    private String takeaway;
    private String delivery;
    private boolean isSelected;
    private String guest;
    private String no_of_guests;
    private String rooms;
    private String floor_name;

   /*  protected Table(Parcel in) {
        id = in.readString();
        saved_bill_id = in.readString();
        table_id = in.readString();
        rows = in.readString();
        bill_id = in.readString();
        coloumns = in.readString();
        tables_flag = in.readString();
        status = in.readString();
        size = in.readString();
        tier = in.readString();
        time = in.readString();
        date = in.readString();
        customer_name = in.readString();
        employeename = in.readString();
        root_table = in.readString();
        root_table_id = in.readString();
        table_name = in.readString();
        table_type = in.readString();
        measurement = in.readString();
        direction = in.readString();
        join_tables = in.createStringArray();
        table_active_status = in.readString();
        takeaway = in.readString();
        delivery = in.readString();
        isSelected = in.readByte() != 0;
        guest = in.readString();
        rooms = in.readString();
        primary_table_name = in.readString();
    }

   public static final Creator<Table> CREATOR = new Creator<Table>() {
        @Override
        public Table createFromParcel(Parcel in) {
            return new Table(in);
        }

        @Override
        public Table[] newArray(int size) {
            return new Table[size];
        }
    };*/

    public String getId() {
        return id;
    }

    public String getIdd() {
        return idd;
    }

    public void setIdd(String idd) {
        this.idd = idd;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getColoumns() {
        return coloumns;
    }

    public void setColoumns(String coloumns) {
        this.coloumns = coloumns;
    }

    public String getRows() {
        return rows;
    }

    public void setRows(String rows) {
        this.rows = rows;
    }

    public String getEmployeename() {
        return employeename;
    }

    public void setEmployeename(String employeename) {
        this.employeename = employeename;
    }

    public String getCustomername() {

        return customer_name;
    }

    public void setCustomername(String customername) {
        this.customer_name = customername;
    }

    public String getSize() {

        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTable_name() {
        return table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }

    public String getTable_type() {
        return table_type;
    }

    public void setTable_type(String table_type) {
        this.table_type = table_type;
    }

    public String getTable_active_status() {
        return table_active_status;
    }

    public void setTable_active_status(String table_active_status) {
        this.table_active_status = table_active_status;
    }

    public String getTakeaway() {
        return takeaway;
    }

    public void setTakeaway(String takeaway) {
        this.takeaway = takeaway;
    }

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String[] getJoin_tables() {
        return join_tables;
    }

    public void setJoin_tables(String[] join_tables) {
        this.join_tables = join_tables;
    }

    public String getMeasurement() {
        return measurement;
    }

    public void setMeasurement(String measurement) {
        this.measurement = measurement;
    }

    public String getRoot_table() {
        return root_table;
    }

    public void setRoot_table(String root_table) {
        this.root_table = root_table;
    }

    public String getRoot_table_id() {
        return root_table_id;
    }

    public void setRoot_table_id(String root_table_id) {
        this.root_table_id = root_table_id;
    }

    public String getTier() {
        return tier;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getGuest() {
        return guest;
    }

    public void setGuest(String guest) {
        this.guest = guest;
    }

    public String getRooms() {
        return rooms;
    }

    public void setRooms(String rooms) {
        this.rooms = rooms;
    }

    public String getBill_id() {
        return bill_id;
    }

    public void setBill_id(String bill_id) {
        this.bill_id = bill_id;
    }

    public String getTables_flag() {
        return tables_flag;
    }

    public void setTables_flag(String tables_flag) {
        this.tables_flag = tables_flag;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getFloor_name() {
        return floor_name;
    }

    public void setFloor_name(String floor_name) {
        this.floor_name = floor_name;
    }

    public String getTable_id() {
        return table_id;
    }

    public void setTable_id(String table_id) {
        this.table_id = table_id;
    }

    public String getSaved_bill_id() {
        return saved_bill_id;
    }

    public void setSaved_bill_id(String saved_bill_id) {
        this.saved_bill_id = saved_bill_id;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getRoot_table_name() {
        return root_table_name;
    }

    public void setRoot_table_name(String root_table_name) {
        this.root_table_name = root_table_name;
    }

    public String getNo_of_guests() {
        return no_of_guests;
    }

    public void setNo_of_guests(String no_of_guests) {
        this.no_of_guests = no_of_guests;
    }
    /*@Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(saved_bill_id);
        dest.writeString(rows);
        dest.writeString(bill_id);
        dest.writeString(coloumns);
        dest.writeString(tables_flag);
        dest.writeString(status);
        dest.writeString(size);
        dest.writeString(tier);
        dest.writeString(time);
        dest.writeString(date);
        dest.writeString(customer_name);
        dest.writeString(employeename);
        dest.writeString(root_table);
        dest.writeString(root_table_id);
        dest.writeString(table_name);
        dest.writeString(table_type);
        dest.writeString(measurement);
        dest.writeString(direction);
        dest.writeStringArray(join_tables);
        dest.writeString(table_active_status);
        dest.writeString(takeaway);
        dest.writeString(delivery);
        dest.writeByte((byte) (isSelected ? 1 : 0));
        dest.writeString(guest);
        dest.writeString(rooms);
        dest.writeString(floor_name);
        dest.writeString(table_id);
        dest.writeString(primary_table_name);
    }*/
}

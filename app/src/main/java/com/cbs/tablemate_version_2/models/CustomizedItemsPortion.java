package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 27-07-2016 in TableMateNew
 ***********************************************************************/
public class CustomizedItemsPortion {
    private String id;
    private String portion_name;
    private String portion_size;
    private String portion_price;
    private boolean isSelected;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPortion_name() {
        return portion_name;
    }

    public void setPortion_name(String portion_name) {
        this.portion_name = portion_name;
    }

    public String getPortion_size() {
        return portion_size;
    }

    public void setPortion_size(String portion_size) {
        this.portion_size = portion_size;
    }

    public String getPortion_price() {
        return portion_price;
    }

    public void setPortion_price(String portion_price) {
        this.portion_price = portion_price;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}

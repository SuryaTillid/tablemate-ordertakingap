package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 24-10-2016 in TableMateNew
 ***********************************************************************/
public class StockItem {
    private String id;
    private String stockName;
    private String stockQuantity;
    private String live_stock;
    private String sub_units;
    private int position;

    public StockItem(int position) {
        this.position = position;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLive_stock() {
        return live_stock;
    }

    public void setLive_stock(String live_stock) {
        this.live_stock = live_stock;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(String stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public String getSub_units() {
        return sub_units;
    }

    public void setSub_units(String sub_units) {
        this.sub_units = sub_units;
    }
}

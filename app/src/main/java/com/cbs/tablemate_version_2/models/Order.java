package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 10-06-2016 in TableMateNew
 ***********************************************************************/
public class Order {

    private String id;
    private String name;
    private String process_time;
    private String custom_data;
    private String rowId;
    private String price;
    private String total;
    private String custom_extra;
    private String custom_portion;
    private String custom_combo_data;
    private String customisation_comments;
    private String order_type;
    private String filter_type_id;
    private String sale_price;
    private String portion_price;
    private String portion_size;
    private String cus_details;
    private String qty;
    private String insert_multiple_orders;
    private String check_modify_price;
    private String discount;
    private String discount_type;
    private String menu_item_id;
    private String mrp_price;
    private String quantity;
    private String waiter_id;
    private String nc_order;
    private String employeeId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProcess_time() {
        return process_time;
    }

    public void setProcess_time(String process_time) {
        this.process_time = process_time;
    }

    public String getCustom_data() {
        return custom_data;
    }

    public void setCustom_data(String custom_data) {
        this.custom_data = custom_data;
    }

    public String getCustom_combo_data() {
        return custom_combo_data;
    }

    public void setCustom_combo_data(String custom_combo_data) {
        this.custom_combo_data = custom_combo_data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCustom_extra() {
        return custom_extra;
    }

    public void setCustom_extra(String custom_extra) {
        this.custom_extra = custom_extra;
    }

    public String getCustom_portion() {
        return custom_portion;
    }

    public void setCustom_portion(String custom_portion) {
        this.custom_portion = custom_portion;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCustomisation_comments() {
        return customisation_comments;
    }

    public void setCustomisation_comments(String customisation_comments) {
        this.customisation_comments = customisation_comments;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getFilter_type_id() {
        return filter_type_id;
    }

    public void setFilter_type_id(String filter_type_id) {
        this.filter_type_id = filter_type_id;
    }

    public String getSale_price() {
        return sale_price;
    }

    public void setSale_price(String sale_price) {
        this.sale_price = sale_price;
    }

    public String getPortion_size() {
        return portion_size;
    }

    public void setPortion_size(String portion_size) {
        this.portion_size = portion_size;
    }

    public String getCus_details() {
        return cus_details;
    }

    public void setCus_details(String cus_details) {
        this.cus_details = cus_details;
    }

    public String getPortion_price() {
        return portion_price;
    }

    public void setPortion_price(String portion_price) {
        this.portion_price = portion_price;
    }

    public String getInsert_multiple_orders() {
        return insert_multiple_orders;
    }

    public void setInsert_multiple_orders(String insert_multiple_orders) {
        this.insert_multiple_orders = insert_multiple_orders;
    }

    public String getCheck_modify_price() {
        return check_modify_price;
    }

    public void setCheck_modify_price(String check_modify_price) {
        this.check_modify_price = check_modify_price;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscount_type() {
        return discount_type;
    }

    public void setDiscount_type(String discount_type) {
        this.discount_type = discount_type;
    }

    public String getMenu_item_id() {
        return menu_item_id;
    }

    public void setMenu_item_id(String menu_item_id) {
        this.menu_item_id = menu_item_id;
    }

    public String getMrp_price() {
        return mrp_price;
    }

    public void setMrp_price(String mrp_price) {
        this.mrp_price = mrp_price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getWaiter_id() {
        return waiter_id;
    }

    public void setWaiter_id(String waiter_id) {
        this.waiter_id = waiter_id;
    }

    public String getNc_order() {
        return nc_order;
    }

    public void setNc_order(String nc_order) {
        this.nc_order = nc_order;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }
}

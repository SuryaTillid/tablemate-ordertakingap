package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 26-08-2016 in TableMateNew
 ***********************************************************************/
public class DeliveryTracking {
    private String menuItemName;
    private String progress;
    private String status;

    public DeliveryTracking() {
    }

    public DeliveryTracking(String menuItemName, String progress, String status) {
        this.menuItemName = menuItemName;
        this.progress = progress;
        this.status = status;
    }

    public String getMenuItemName() {
        return menuItemName;
    }

    public void setMenuItemName(String menuItemName) {
        this.menuItemName = menuItemName;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 28-08-2018 in TableMateNew
 ***********************************************************************/
public class FloorMaster {
    private String id;
    private String name;
    private String dimension;
    private String door;
    private String floor_master_unique_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    public String getDoor() {
        return door;
    }

    public void setDoor(String door) {
        this.door = door;
    }

    public String getFloor_master_unique_id() {
        return floor_master_unique_id;
    }

    public void setFloor_master_unique_id(String floor_master_unique_id) {
        this.floor_master_unique_id = floor_master_unique_id;
    }
}

package com.cbs.tablemate_version_2.models;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 14-06-2016 in TableMateNew
 ***********************************************************************/
public class LockTable {
    private String success;
    private String table_id;
    private String message;
    private ArrayList<String> join_tables = new ArrayList<>();
    private String result_table_id;
    private String saved_bill_id;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getTable_id() {
        return table_id;
    }

    public void setTable_id(String table_id) {
        this.table_id = table_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<String> getJoin_tables() {
        return join_tables;
    }

    public void setJoin_tables(ArrayList<String> join_tables) {
        this.join_tables = join_tables;
    }

    public String getResult_table_id() {
        return result_table_id;
    }

    public void setResult_table_id(String result_table_id) {
        this.result_table_id = result_table_id;
    }

    public String getSaved_bill_id() {
        return saved_bill_id;
    }

    public void setSaved_bill_id(String saved_bill_id) {
        this.saved_bill_id = saved_bill_id;
    }

}

/*
 * output ===> {"success":1,"table_id":"4","join_tables":["5"],"result_table_id":"5,6"}
 * */
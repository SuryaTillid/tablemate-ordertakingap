package com.cbs.tablemate_version_2.models;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 29-07-2016 in TableMateNew
 ***********************************************************************/
public class CustomizationModel {
    private ArrayList<CustomizationItemAddon> addons = new ArrayList<>();
    private ArrayList<CustomizationItemsType> itemsTypes = new ArrayList<>();
    private ArrayList<CustomizedItemsPortion> portions = new ArrayList<>();
    private ArrayList<EmployeeTypeName> employee = new ArrayList<>();
    private int id;

    public ArrayList<CustomizationItemAddon> getAddons() {
        return addons;
    }

    public void setAddons(ArrayList<CustomizationItemAddon> addons) {
        this.addons = addons;
    }

    public ArrayList<CustomizationItemsType> getItemsTypes() {
        return itemsTypes;
    }

    public void setItemsTypes(ArrayList<CustomizationItemsType> itemsTypes) {
        this.itemsTypes = itemsTypes;
    }

    public ArrayList<CustomizedItemsPortion> getPortions() {
        return portions;
    }

    public void setPortions(ArrayList<CustomizedItemsPortion> portions) {
        this.portions = portions;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<EmployeeTypeName> getEmployee() {
        return employee;
    }

    public void setEmployee(ArrayList<EmployeeTypeName> employee) {
        this.employee = employee;
    }
}

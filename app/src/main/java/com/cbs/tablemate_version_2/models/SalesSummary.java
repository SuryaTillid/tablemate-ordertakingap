package com.cbs.tablemate_version_2.models;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 22-02-2018 in TableMateNew
 ***********************************************************************/
public class SalesSummary {
    private ArrayList<Sales> bill_sales = new ArrayList<>();
    private ArrayList<Sales> customization_cost = new ArrayList<>();
    private ArrayList<Sales> guestsummary = new ArrayList<>();
    private ArrayList<Sales> payment = new ArrayList<>();

    public ArrayList<Sales> getBill_sales() {
        return bill_sales;
    }

    public void setBill_sales(ArrayList<Sales> bill_sales) {
        this.bill_sales = bill_sales;
    }

    public ArrayList<Sales> getCustomization_cost() {
        return customization_cost;
    }

    public void setCustomization_cost(ArrayList<Sales> customization_cost) {
        this.customization_cost = customization_cost;
    }

    public ArrayList<Sales> getGuestsummary() {
        return guestsummary;
    }

    public void setGuestsummary(ArrayList<Sales> guestsummary) {
        this.guestsummary = guestsummary;
    }

    public ArrayList<Sales> getPayment() {
        return payment;
    }

    public void setPayment(ArrayList<Sales> payment) {
        this.payment = payment;
    }
}

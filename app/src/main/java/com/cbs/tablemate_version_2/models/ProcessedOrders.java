package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 19-06-2017 in TableMateNew
 ***********************************************************************/
public class ProcessedOrders {

    private String id;
    private String menu_item_id;
    private String quantity;
    private String table_id;
    private String waiter_id;
    private String kot;
    private String custom_data;
    private String custom_combo;
    private String customisation_comments;
    private String order_type;
    private String filter_type_id;
    private String name;
    private String type;
    private String combo_item;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMenu_item_id() {
        return menu_item_id;
    }

    public void setMenu_item_id(String menu_item_id) {
        this.menu_item_id = menu_item_id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getTable_id() {
        return table_id;
    }

    public void setTable_id(String table_id) {
        this.table_id = table_id;
    }

    public String getWaiter_id() {
        return waiter_id;
    }

    public void setWaiter_id(String waiter_id) {
        this.waiter_id = waiter_id;
    }

    public String getKot() {
        return kot;
    }

    public void setKot(String kot) {
        this.kot = kot;
    }

    public String getCustom_data() {
        return custom_data;
    }

    public void setCustom_data(String custom_data) {
        this.custom_data = custom_data;
    }

    public String getCustom_combo() {
        return custom_combo;
    }

    public void setCustom_combo(String custom_combo) {
        this.custom_combo = custom_combo;
    }

    public String getCustomisation_comments() {
        return customisation_comments;
    }

    public void setCustomisation_comments(String customisation_comments) {
        this.customisation_comments = customisation_comments;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getFilter_type_id() {
        return filter_type_id;
    }

    public void setFilter_type_id(String filter_type_id) {
        this.filter_type_id = filter_type_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCombo_item() {
        return combo_item;
    }

    public void setCombo_item(String combo_item) {
        this.combo_item = combo_item;
    }
}

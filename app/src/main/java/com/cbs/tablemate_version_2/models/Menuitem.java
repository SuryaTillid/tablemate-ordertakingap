
package com.cbs.tablemate_version_2.models;

public class Menuitem {
    private String id;
    private String type;
    private String name;
    private String description;
    private String photo;
    private String price;
    private String delivery_cost;
    private String takeaway_cost;
    private String layout;
    private String filter_type_name;
    private String process_time;
    private String ingredients;
    private String profit_loss;
    private String disable;
    private String max_order_limit;
    private String dinein;
    private String takeaway;
    private String delivery;
    private String search_id;
    private String short_name;
    private String feedback_ratings;
    private String price_tax1;
    private String price_tax2;
    private String combo_item;
    private String exp_days;
    private String for_ck;
    private String row_changed;
    private String row_uploaded;
    private String cust_aval;
    private String cust_mandatory;
    private String food_dept_id;

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The photo
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * @param photo The photo
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    /**
     * @return The price
     */
    public String getPrice() {
        return price;
    }

    /**
     * @param price The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * @return The delivery_cost
     */
    public String getDelivery_cost() {
        return delivery_cost;
    }

    /**
     * @param delivery_cost The delivery_cost
     */
    public void setDelivery_cost(String delivery_cost) {
        this.delivery_cost = delivery_cost;
    }

    /**
     * @return The takeaway_cost
     */
    public String getTakeaway_cost() {
        return takeaway_cost;
    }

    /**
     * @param takeaway_cost The takeaway_cost
     */
    public void setTakeaway_cost(String takeaway_cost) {
        this.takeaway_cost = takeaway_cost;
    }

    /**
     * @return The layout
     */
    public String getLayout() {
        return layout;
    }

    /**
     * @param layout The layout
     */
    public void setLayout(String layout) {
        this.layout = layout;
    }

    /**
     * @return The filter_type_name
     */
    public String getFilter_type_name() {
        return filter_type_name;
    }

    /**
     * @param filter_type_name The filter_type_name
     */
    public void setFilter_type_name(String filter_type_name) {
        this.filter_type_name = filter_type_name;
    }

    /**
     * @return The process_time
     */
    public String getProcess_time() {
        return process_time;
    }

    /**
     * @param process_time The process_time
     */
    public void setProcess_time(String process_time) {
        this.process_time = process_time;
    }

    /**
     * @return The ingredients
     */
    public String getIngredients() {
        return ingredients;
    }

    /**
     * @param ingredients The ingredients
     */
    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    /**
     * @return The profit_loss
     */
    public String getProfit_loss() {
        return profit_loss;
    }

    /**
     * @param profit_loss The profit_loss
     */
    public void setProfit_loss(String profit_loss) {
        this.profit_loss = profit_loss;
    }

    /**
     * @return The disable
     */
    public String getDisable() {
        return disable;
    }

    /**
     * @param disable The disable
     */
    public void setDisable(String disable) {
        this.disable = disable;
    }

    /**
     * @return The max_order_limit
     */
    public String getMax_order_limit() {
        return max_order_limit;
    }

    /**
     * @param max_order_limit The max_order_limit
     */
    public void setMax_order_limit(String max_order_limit) {
        this.max_order_limit = max_order_limit;
    }

    /**
     * @return The dinein
     */
    public String getDinein() {
        return dinein;
    }

    /**
     * @param dinein The dinein
     */
    public void setDinein(String dinein) {
        this.dinein = dinein;
    }

    /**
     * @return The takeaway
     */
    public String getTakeaway() {
        return takeaway;
    }

    /**
     * @param takeaway The takeaway
     */
    public void setTakeaway(String takeaway) {
        this.takeaway = takeaway;
    }

    /**
     * @return The delivery
     */
    public String getDelivery() {
        return delivery;
    }

    /**
     * @param delivery The delivery
     */
    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    /**
     * @return The search_id
     */
    public String getSearch_id() {
        return search_id;
    }

    /**
     * @param search_id The search_id
     */
    public void setSearch_id(String search_id) {
        this.search_id = search_id;
    }

    /**
     * @return The short_name
     */
    public String getShort_name() {
        return short_name;
    }

    /**
     * @param short_name The short_name
     */
    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    /**
     * @return The feedback_ratings
     */
    public String getFeedback_ratings() {
        return feedback_ratings;
    }

    /**
     * @param feedback_ratings The feedback_ratings
     */
    public void setFeedback_ratings(String feedback_ratings) {
        this.feedback_ratings = feedback_ratings;
    }

    /**
     * @return The price_tax1
     */
    public String getPrice_tax1() {
        return price_tax1;
    }

    /**
     * @param price_tax1 The price_tax1
     */
    public void setPrice_tax1(String price_tax1) {
        this.price_tax1 = price_tax1;
    }

    /**
     * @return The price_tax2
     */
    public String getPrice_tax2() {
        return price_tax2;
    }

    /**
     * @param price_tax2 The price_tax2
     */
    public void setPrice_tax2(String price_tax2) {
        this.price_tax2 = price_tax2;
    }

    /**
     * @return The combo_item
     */
    public String getCombo_item() {
        return combo_item;
    }

    /**
     * @param combo_item The combo_item
     */
    public void setCombo_item(String combo_item) {
        this.combo_item = combo_item;
    }

    /**
     * @return The exp_days
     */
    public String getExp_days() {
        return exp_days;
    }

    /**
     * @param exp_days The exp_days
     */
    public void setExp_days(String exp_days) {
        this.exp_days = exp_days;
    }

    /**
     * @return The for_ck
     */
    public String getFor_ck() {
        return for_ck;
    }

    /**
     * @param for_ck The for_ck
     */
    public void setFor_ck(String for_ck) {
        this.for_ck = for_ck;
    }

    /**
     * @return The row_changed
     */
    public String getRow_changed() {
        return row_changed;
    }

    /**
     * @param row_changed The row_changed
     */
    public void setRow_changed(String row_changed) {
        this.row_changed = row_changed;
    }

    /**
     * @return The row_uploaded
     */
    public String getRow_uploaded() {
        return row_uploaded;
    }

    /**
     * @param row_uploaded The row_uploaded
     */
    public void setRow_uploaded(String row_uploaded) {
        this.row_uploaded = row_uploaded;
    }

    /**
     * @return The cust_aval
     */
    public String getCust_aval() {
        return cust_aval;
    }

    /**
     * @param cust_aval The cust_aval
     */
    public void setCust_aval(String cust_aval) {
        this.cust_aval = cust_aval;
    }

    /**
     * @return The cust_mandatory
     */
    public String getCust_mandatory() {
        return cust_mandatory;
    }

    /**
     * @param cust_mandatory The cust_mandatory
     */
    public void setCust_mandatory(String cust_mandatory) {
        this.cust_mandatory = cust_mandatory;
    }

    /**
     * @return The food_dept_id
     */
    public String getFood_dept_id() {
        return food_dept_id;
    }

    /**
     * @param food_dept_id The food_dept_id
     */
    public void setFood_dept_id(String food_dept_id) {
        this.food_dept_id = food_dept_id;
    }

}

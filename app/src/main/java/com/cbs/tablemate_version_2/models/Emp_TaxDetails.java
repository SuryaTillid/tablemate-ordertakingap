package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 11-07-2017 in TableMateNew
 ***********************************************************************/
public class Emp_TaxDetails {

    private String tax_name;

    private String tax_id;

    private String bill_id;

    private String taxvalue;

    public String getTax_name() {
        return tax_name;
    }

    public void setTax_name(String tax_name) {
        this.tax_name = tax_name;
    }

    public String getTax_id() {
        return tax_id;
    }

    public void setTax_id(String tax_id) {
        this.tax_id = tax_id;
    }

    public String getBill_id() {
        return bill_id;
    }

    public void setBill_id(String bill_id) {
        this.bill_id = bill_id;
    }

    public String getTaxvalue() {
        return taxvalue;
    }

    public void setTaxvalue(String taxvalue) {
        this.taxvalue = taxvalue;
    }
}

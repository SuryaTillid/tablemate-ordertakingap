package com.cbs.tablemate_version_2.models;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 30-05-2016 in TableMateNew
 ***********************************************************************/
public class DefaultCustomerDetails {

    private ArrayList<CustomerData> customer_list = new ArrayList<>();
    private ArrayList<Table> table_list = new ArrayList<>();

    public ArrayList<CustomerData> getCustomer_list() {
        return customer_list;
    }

    public void setCustomer_list(ArrayList<CustomerData> customer_list) {
        this.customer_list = customer_list;
    }

    public ArrayList<Table> getTable_list() {
        return table_list;
    }

    public void setTable_list(ArrayList<Table> table_list) {
        this.table_list = table_list;
    }
}
package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 14-06-2016 in TableMateNew
 ***********************************************************************/
public class CreateCustomerDetails {
    private String success;
    private String id;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 29-07-2016 in TableMateNew
 ***********************************************************************/
public class CustomizationItemAddon {

    private String stock_id;
    private String stockName;
    private String unitMasterName;
    private String sub_unit_name;
    private String unit_master_id;
    private String chargeblePrice;
    private String chargeable;
    private String addon;
    private String addon_quantity;
    private boolean isSelected;

    public String getStock_id() {
        return stock_id;
    }

    public void setStock_id(String stock_id) {
        this.stock_id = stock_id;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getUnitMasterName() {
        return unitMasterName;
    }

    public void setUnitMasterName(String unitMasterName) {
        this.unitMasterName = unitMasterName;
    }

    public String getSub_unit_name() {
        return sub_unit_name;
    }

    public void setSub_unit_name(String sub_unit_name) {
        this.sub_unit_name = sub_unit_name;
    }

    public String getUnit_master_id() {
        return unit_master_id;
    }

    public void setUnit_master_id(String unit_master_id) {
        this.unit_master_id = unit_master_id;
    }

    public String getChargeblePrice() {
        return chargeblePrice;
    }

    public void setChargeblePrice(String chargeblePrice) {
        this.chargeblePrice = chargeblePrice;
    }

    public String getChargeable() {
        return chargeable;
    }

    public void setChargeable(String chargeable) {
        this.chargeable = chargeable;
    }

    public String getAddon() {
        return addon;
    }

    public void setAddon(String addon) {
        this.addon = addon;
    }

    public String getAddon_quantity() {
        return addon_quantity;
    }

    public void setAddon_quantity(String addon_quantity) {
        this.addon_quantity = addon_quantity;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}

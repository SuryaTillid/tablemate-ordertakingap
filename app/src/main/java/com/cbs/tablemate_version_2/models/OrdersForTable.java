package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 20-06-2016 in TableMateNew
 ***********************************************************************/
public class OrdersForTable {

    private String order_id;
    private String deliver_status;
    private String table_id;
    private String menu_item_id;
    private String menu_item_name;
    private String sale_price;
    private String mrp_price;
    private String name;
    private String date;
    private String order_time;
    private String occupancy;
    private String status;
    private String bill_status;
    private String waiter_id;
    private String quantity;
    private String kot;
    private String custom_data;
    private String custom_combo;
    private String filter_type_id;
    private String process_time;
    private String price;
    private String portion_details;
    private String portion_size;
    private String customization_details;
    private String customisation_comments;
    private String orderQuantity;
    private String nc_order;
    private String tax;
    private String custom_details;
    private String other_reason;
    private String cancel_comments;
    private String delete_comment;
    private boolean isChecked;


    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getTable_id() {
        return table_id;
    }

    public void setTable_id(String table_id) {
        this.table_id = table_id;
    }

    public String getMenu_item_id() {
        return menu_item_id;
    }

    public void setMenu_item_id(String menu_item_id) {
        this.menu_item_id = menu_item_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBill_status() {
        return bill_status;
    }

    public void setBill_status(String bill_status) {
        this.bill_status = bill_status;
    }

    public String getWaiter_id() {
        return waiter_id;
    }

    public void setWaiter_id(String waiter_id) {
        this.waiter_id = waiter_id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getKot() {
        return kot;
    }

    public void setKot(String kot) {
        this.kot = kot;
    }

    public String getCustom_data() {
        return custom_data;
    }

    public void setCustom_data(String custom_data) {
        this.custom_data = custom_data;
    }

    public String getCustom_combo() {
        return custom_combo;
    }

    public void setCustom_combo(String custom_combo) {
        this.custom_combo = custom_combo;
    }

    public String getProcess_time() {
        return process_time;
    }

    public void setProcess_time(String process_time) {
        this.process_time = process_time;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDeliver_status() {
        return deliver_status;
    }

    public void setDeliver_status(String deliver_status) {
        this.deliver_status = deliver_status;
    }

    public String getOccupancy() {
        return occupancy;
    }

    public void setOccupancy(String occupancy) {
        this.occupancy = occupancy;
    }

    public String getFilter_type_id() {
        return filter_type_id;
    }

    public void setFilter_type_id(String filter_type_id) {
        this.filter_type_id = filter_type_id;
    }

    public String getPortion_details() {
        return portion_details;
    }

    public void setPortion_details(String portion_details) {
        this.portion_details = portion_details;
    }

    public String getCustomization_details() {
        return customization_details;
    }

    public void setCustomization_details(String customization_details) {
        this.customization_details = customization_details;
    }

    public String getCustomisation_comments() {
        return customisation_comments;
    }

    public void setCustomisation_comments(String customisation_comments) {
        this.customisation_comments = customisation_comments;
    }

    public String getMenu_item_name() {
        return menu_item_name;
    }

    public void setMenu_item_name(String menu_item_name) {
        this.menu_item_name = menu_item_name;
    }

    public String getSale_price() {
        return sale_price;
    }

    public void setSale_price(String sale_price) {
        this.sale_price = sale_price;
    }

    public String getMrp_price() {
        return mrp_price;
    }

    public void setMrp_price(String mrp_price) {
        this.mrp_price = mrp_price;
    }

    public String getPortion_size() {
        return portion_size;
    }

    public void setPortion_size(String portion_size) {
        this.portion_size = portion_size;
    }

    public String getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(String orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public String getNc_order() {
        return nc_order;
    }

    public void setNc_order(String nc_order) {
        this.nc_order = nc_order;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getCustom_details() {
        return custom_details;
    }

    public void setCustom_details(String custom_details) {
        this.custom_details = custom_details;
    }

    public String getOther_reason() {
        return other_reason;
    }

    public void setOther_reason(String other_reason) {
        this.other_reason = other_reason;
    }

    public String getCancel_comments() {
        return cancel_comments;
    }

    public void setCancel_comments(String cancel_comments) {
        this.cancel_comments = cancel_comments;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getDelete_comment() {
        return delete_comment;
    }

    public void setDelete_comment(String delete_comment) {
        this.delete_comment = delete_comment;
    }
}

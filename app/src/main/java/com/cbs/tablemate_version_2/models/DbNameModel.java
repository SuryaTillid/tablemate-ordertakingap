package com.cbs.tablemate_version_2.models;

import org.json.JSONObject;

/*********************************************************************
 * Created by Barani on 25-05-2016 in TableMateNew
 ***********************************************************************/
public class DbNameModel extends JSONObject {
    private String db_name;
    private String success;
    private String hotel_name;

    public String getDb_name() {
        return db_name;
    }

    public void setDb_name(String db_name) {
        this.db_name = db_name;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getHotel_name() {
        return hotel_name;
    }

    public void setHotel_name(String hotel_name) {
        this.hotel_name = hotel_name;
    }
}

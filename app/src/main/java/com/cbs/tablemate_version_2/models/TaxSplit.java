
package com.cbs.tablemate_version_2.models;

public class TaxSplit {

    private String tax_id;
    private String tax_name;
    private String tax_sum_api;

    /**
     * @return The tax_id
     */
    public String getTax_id() {
        return tax_id;
    }

    /**
     * @param tax_id The tax_id
     */
    public void setTax_id(String tax_id) {
        this.tax_id = tax_id;
    }

    /**
     * @return The tax_name
     */
    public String getTax_name() {
        return tax_name;
    }

    /**
     * @param tax_name The tax_name
     */
    public void setTax_name(String tax_name) {
        this.tax_name = tax_name;
    }

    /*public Float getTax_sum_api() {
        return tax_sum_api;
    }

    public void setTax_sum_api(Float tax_sum_api) {
        this.tax_sum_api = tax_sum_api;
    }*/

    public String getTax_sum_api() {
        return tax_sum_api;
    }

    public void setTax_sum_api(String tax_sum_api) {
        this.tax_sum_api = tax_sum_api;
    }
}
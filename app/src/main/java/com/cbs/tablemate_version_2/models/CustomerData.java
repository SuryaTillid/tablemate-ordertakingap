package com.cbs.tablemate_version_2.models;

/**
 * Created by Barani on 16-05-2016.
 */
public class CustomerData {
    private String id;
    private String membership_id;

    private String name;
    private String phone;
    private String address;
    private String address_secondary;
    private String email;
    private String password;
    private String verify_pass;
    private String date_of_birth;
    private String row_changed;
    private String row_uploaded;
    private String customer_conduct;
    private String alternate_contact;
    private String last_sync;
    private String last_update;
    private String number;
    private String guest;
    private String customer_master_unique_id;
    private String TIN_number;
    private String CST_number;
    private String PAN_number;
    private String LUXURY_taxnumber;

    public String getIdd() {
        return membership_id;
    }

    public void setIdd(String membership_id) {
        this.membership_id = membership_id;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVerify_pass() {
        return verify_pass;
    }

    public void setVerify_pass(String verify_pass) {
        this.verify_pass = verify_pass;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getRow_changed() {
        return row_changed;
    }

    public void setRow_changed(String row_changed) {
        this.row_changed = row_changed;
    }

    public String getRow_uploaded() {
        return row_uploaded;
    }

    public void setRow_uploaded(String row_uploaded) {
        this.row_uploaded = row_uploaded;
    }

    public String getCustomer_conduct() {
        return customer_conduct;
    }

    public void setCustomer_conduct(String customer_conduct) {
        this.customer_conduct = customer_conduct;
    }

    public String getCustomer_master_unique_id() {
        return customer_master_unique_id;
    }

    public void setCustomer_master_unique_id(String customer_master_unique_id) {
        this.customer_master_unique_id = customer_master_unique_id;
    }

    public String getLast_sync() {
        return last_sync;
    }

    public void setLast_sync(String last_sync) {
        this.last_sync = last_sync;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getGuest() {
        return guest;
    }

    public void setGuest(String guest) {
        this.guest = guest;
    }

    public String getTIN_number() {
        return TIN_number;
    }

    public void setTIN_number(String TIN_number) {
        this.TIN_number = TIN_number;
    }

    public String getCST_number() {
        return CST_number;
    }

    public void setCST_number(String CST_number) {
        this.CST_number = CST_number;
    }

    public String getPAN_number() {
        return PAN_number;
    }

    public void setPAN_number(String PAN_number) {
        this.PAN_number = PAN_number;
    }

    public String getLUXURY_taxnumber() {
        return LUXURY_taxnumber;
    }

    public void setLUXURY_taxnumber(String LUXURY_taxnumber) {
        this.LUXURY_taxnumber = LUXURY_taxnumber;
    }

    public String getAddress_secondary() {
        return address_secondary;
    }

    public void setAddress_secondary(String address_secondary) {
        this.address_secondary = address_secondary;
    }

    public String getAlternate_contact() {
        return alternate_contact;
    }

    public void setAlternate_contact(String alternate_contact) {
        this.alternate_contact = alternate_contact;
    }
}

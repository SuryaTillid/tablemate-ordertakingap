package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 08-10-2018 in TableMateNew
 ***********************************************************************/
public class ModifyOrder {

    private String modified_sale_price;
    private String order_id;

    public ModifyOrder(String order_id, String sale_price) {
        this.order_id = order_id;
        this.modified_sale_price = sale_price;
    }
}

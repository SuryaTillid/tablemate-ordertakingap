package com.cbs.tablemate_version_2.models;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 11-07-2017 in TableMateNew
 ***********************************************************************/
public class EmployeeSessionSummaryDetails {

    private ArrayList<Emp_SettlementSummary> listpayment = new ArrayList<>();

    private ArrayList<Emp_HoldBills> holdval = new ArrayList<>();

    private ArrayList<Emp_CancelledBills> cancelled = new ArrayList<>();

    private ArrayList<Emp_PendingBills> pendingval = new ArrayList<>();

    private ArrayList<Emp_SalesSummary> dinein = new ArrayList<>();

    private ArrayList<Emp_SessionSales> billdetail = new ArrayList<>();

    private ArrayList<Emp_BillSummary> billsummary = new ArrayList<>();

    private ArrayList<Emp_GuestSummary> guestsummary = new ArrayList<>();

    private ArrayList<Emp_TaxDetails> taxvalue = new ArrayList<>();

    public ArrayList<Emp_SettlementSummary> getListpayment() {
        return listpayment;
    }

    public void setListpayment(ArrayList<Emp_SettlementSummary> listpayment) {
        this.listpayment = listpayment;
    }

    public ArrayList<Emp_HoldBills> getHoldval() {
        return holdval;
    }

    public void setHoldval(ArrayList<Emp_HoldBills> holdval) {
        this.holdval = holdval;
    }

    public ArrayList<Emp_CancelledBills> getCancelled() {
        return cancelled;
    }

    public void setCancelled(ArrayList<Emp_CancelledBills> cancelled) {
        this.cancelled = cancelled;
    }

    public ArrayList<Emp_PendingBills> getPendingval() {
        return pendingval;
    }

    public void setPendingval(ArrayList<Emp_PendingBills> pendingval) {
        this.pendingval = pendingval;
    }

    public ArrayList<Emp_SalesSummary> getDinein() {
        return dinein;
    }

    public void setDinein(ArrayList<Emp_SalesSummary> dinein) {
        this.dinein = dinein;
    }

    public ArrayList<Emp_SessionSales> getBilldetail() {
        return billdetail;
    }

    public void setBilldetail(ArrayList<Emp_SessionSales> billdetail) {
        this.billdetail = billdetail;
    }

    public ArrayList<Emp_BillSummary> getBillsummary() {
        return billsummary;
    }

    public void setBillsummary(ArrayList<Emp_BillSummary> billsummary) {
        this.billsummary = billsummary;
    }

    public ArrayList<Emp_GuestSummary> getGuestsummary() {
        return guestsummary;
    }

    public void setGuestsummary(ArrayList<Emp_GuestSummary> guestsummary) {
        this.guestsummary = guestsummary;
    }

    public ArrayList<Emp_TaxDetails> getTaxvalue() {
        return taxvalue;
    }

    public void setTaxvalue(ArrayList<Emp_TaxDetails> taxvalue) {
        this.taxvalue = taxvalue;
    }
}
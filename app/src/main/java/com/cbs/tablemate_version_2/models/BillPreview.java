package com.cbs.tablemate_version_2.models;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 21-05-2016 in TableMateNew
 ***********************************************************************/
public class BillPreview {

    private String order_id;
    private String menu_item_id;
    private String menu_item_name;
    private String menu_item_price;
    private double price;
    private String billDiscount;
    private String orderQuantity;
    private String custom_data;
    private double tax;
    private String discount;
    private String bill_id;
    private String custom_combo;
    private String process_time;
    private String order_time;
    private String date;
    private String status;
    private String bill_status;
    private String waiter_id;
    private String table_id;
    private String kot;
    private String mrp_price;
    private double withoutTax_itemPrice;
    private String withoutTax_AllItemPrice;
    private double withoutTax_addonPrice;
    private double discountAmount;
    private String discountType;
    private String comments;
    private ArrayList<DiscountOrder> discountOrders = new ArrayList<>();

    /* "id": "2",
        "billTotal": "100.00",
        "billTax": "4.75",
        "billDiscount": "5.00",
        "round_off": "0.00",
        "billGrandTotal": "99.75",
        "employeeId": "1",
        "customer_id": "0",
        "date": "2018-10-08 13:42:14",
        "amountReceived": "0.00",
        "amountPaid": "0",
        "balanceGiven": null,
        "paymentMode": null,
        "bill_status": "1",
        "cardNumber": null,
        "cardType": null,
        "remark": "Demo",
        "balance": "0.00",
        "partial_amount": "0.00",
        "bill_split": "0",
        "overalldiscount": "0.00",
        "comments": null,
        "table_id": "2",
        "store_id": "3",
        "store_order_id": "2",
        "bill_id": "3_2",
        "occupancy": "1",
        "bill_date": "2018-10-07 23:59:59",
        "order_type": "1",
        "row_changed": "0",
        "row_uploaded": "0",
        "last_sync": "2018-10-08 15:18:01",
        "last_update": "2018-10-08 15:18:01",
        "room_ref_no": "",
        "bill_master_unique_id": null,
        "delivery_partner_name": "",
        "food_order_id": null,
        "food_bill_id": null,
        "alcohol_order_id": null,
        "alcohol_bill_id": null,
        "tip": "0",
        "child": "0",
        "gen_emp_id": "1",
        "post_room": "0",
        "delivery_charges": "0.00",
        "packaging_charges": "0.00",
        "other_charges": "0.00",
        "paid_data_time": null,
        "paid_employee_id": null*/

    public String getMenu_item_id() {
        return menu_item_id;
    }

    public void setMenu_item_id(String menu_item_id) {
        this.menu_item_id = menu_item_id;
    }

    public String getMenu_item_name() {
        return menu_item_name;
    }

    public void setMenu_item_name(String menu_item_name) {
        this.menu_item_name = menu_item_name;
    }

    public String getCustom_data() {
        return custom_data;
    }

    public void setCustom_data(String custom_data) {
        this.custom_data = custom_data;
    }

    public String getCustom_combo() {
        return custom_combo;
    }

    public void setCustom_combo(String custom_combo) {
        this.custom_combo = custom_combo;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(String orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(int tax) {
        this.tax = tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public double getWithoutTax_itemPrice() {
        return withoutTax_itemPrice;
    }

    public void setWithoutTax_itemPrice(double withoutTax_itemPrice) {
        this.withoutTax_itemPrice = withoutTax_itemPrice;
    }

    public String getWithoutTax_AllItemPrice() {
        return withoutTax_AllItemPrice;
    }

    public void setWithoutTax_AllItemPrice(String withoutTax_AllItemPrice) {
        this.withoutTax_AllItemPrice = withoutTax_AllItemPrice;
    }

    public double getWithoutTax_addonPrice() {
        return withoutTax_addonPrice;
    }

    public void setWithoutTax_addonPrice(double withoutTax_addonPrice) {
        this.withoutTax_addonPrice = withoutTax_addonPrice;
    }

    public double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getBill_id() {
        return bill_id;
    }

    public void setBill_id(String bill_id) {
        this.bill_id = bill_id;
    }

    public String getBill_status() {
        return bill_status;
    }

    public void setBill_status(String bill_status) {
        this.bill_status = bill_status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getKot() {
        return kot;
    }

    public void setKot(String kot) {
        this.kot = kot;
    }

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public String getProcess_time() {
        return process_time;
    }

    public void setProcess_time(String process_time) {
        this.process_time = process_time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTable_id() {
        return table_id;
    }

    public void setTable_id(String table_id) {
        this.table_id = table_id;
    }

    public String getWaiter_id() {
        return waiter_id;
    }

    public void setWaiter_id(String waiter_id) {
        this.waiter_id = waiter_id;
    }

    public String getMrp_price() {
        return mrp_price;
    }

    public void setMrp_price(String mrp_price) {
        this.mrp_price = mrp_price;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getBillDiscount() {
        return billDiscount;
    }

    public void setBillDiscount(String billDiscount) {
        this.billDiscount = billDiscount;
    }

    public String getMenu_item_price() {
        return menu_item_price;
    }

    public void setMenu_item_price(String menu_item_price) {
        this.menu_item_price = menu_item_price;
    }

    public ArrayList<DiscountOrder> getDiscountOrders() {
        return discountOrders;
    }

    public void setDiscountOrders(ArrayList<DiscountOrder> discountOrders) {
        this.discountOrders = discountOrders;
    }
}

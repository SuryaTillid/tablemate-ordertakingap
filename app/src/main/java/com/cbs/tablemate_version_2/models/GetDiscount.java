package com.cbs.tablemate_version_2.models;

import java.util.ArrayList;
import java.util.List;

/*********************************************************************
 * Created by Barani on 30-08-2016 in TableMateNew
 ***********************************************************************/
public class GetDiscount {

    private ArrayList<Menuitem> menuitems = new ArrayList<Menuitem>();
    private ArrayList<BillPreview> ordereditems = new ArrayList<>();
    private POS_Bill bill = new POS_Bill();
    private ArrayList<DefaultCustomerDetails> hotelprofile = new ArrayList<>();

    public POS_Bill getBill() {
        return bill;
    }

    public void setBill(POS_Bill bill) {
        this.bill = bill;
    }

    public ArrayList<DefaultCustomerDetails> getHotelprofile() {
        return hotelprofile;
    }

    public void setHotelprofile(ArrayList<DefaultCustomerDetails> hotelprofile) {
        this.hotelprofile = hotelprofile;
    }

    public List<Menuitem> getMenuitems() {
        return menuitems;
    }

    public void setMenuitems(ArrayList<Menuitem> menuitems) {
        this.menuitems = menuitems;
    }

    public ArrayList<BillPreview> getOrdereditems() {
        return ordereditems;
    }

    public void setOrdereditems(ArrayList<BillPreview> ordereditems) {
        this.ordereditems = ordereditems;
    }
}
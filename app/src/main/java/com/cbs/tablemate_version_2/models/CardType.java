package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 15-07-2016 in TableMateNew
 ***********************************************************************/
public class CardType {
    private String id;
    private String cardtype;
    private String category;
    private String row_changed;
    private String row_uploaded;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCardtype() {
        return cardtype;
    }

    public void setCardtype(String cardtype) {
        this.cardtype = cardtype;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getRow_changed() {
        return row_changed;
    }

    public void setRow_changed(String row_changed) {
        this.row_changed = row_changed;
    }

    public String getRow_uploaded() {
        return row_uploaded;
    }

    public void setRow_uploaded(String row_uploaded) {
        this.row_uploaded = row_uploaded;
    }
}

package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 07-10-2016 in TableMateNew
 ***********************************************************************/
public class GetBillId {
    private String bill_id;

    public String getBill_id() {
        return bill_id;
    }

    public void setBill_id(String bill_id) {
        this.bill_id = bill_id;
    }
}

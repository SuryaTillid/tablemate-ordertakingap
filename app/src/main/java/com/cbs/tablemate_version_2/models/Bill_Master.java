package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 10-04-2017 in TableMateNew
 ***********************************************************************/
public class Bill_Master {
    private String id;
    private String tnog;
    private String child;
    private String store_id;
    private String delivery_partner_name;
    private String order_type;
    private String total;
    private String user_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTnog() {
        return tnog;
    }

    public void setTnog(String tnog) {
        this.tnog = tnog;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getDelivery_partner_name() {
        return delivery_partner_name;
    }

    public void setDelivery_partner_name(String delivery_partner_name) {
        this.delivery_partner_name = delivery_partner_name;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getChild() {
        return child;
    }

    public void setChild(String child) {
        this.child = child;
    }
}


package com.cbs.tablemate_version_2.models;

public class TakeawayAddon {

    private String addon_price;
    private float takeaway_price;

    public String getAddon_price() {
        return addon_price;
    }

    public void setAddon_price(String addon_price) {
        this.addon_price = addon_price;
    }

    public float getTakeaway_price() {
        return takeaway_price;
    }

    public void setTakeaway_price(float takeaway_price) {
        this.takeaway_price = takeaway_price;
    }
}

package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 20-11-2018 in TableMateNew
 ***********************************************************************/
public class FilterType {
    private String id;
    private String name;
    private String color_value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor_value() {
        return color_value;
    }

    public void setColor_value(String color_value) {
        this.color_value = color_value;
    }
}

package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 13-11-2016 in TableMateNew
 ***********************************************************************/
public class PortionOrder {
    //{"data_attr":"MiniScoop","data_size":"50","data_price":"200"}
    private String data_attr;
    private String data_size;
    private String data_price;

    public String getData_attr() {
        return data_attr;
    }

    public void setData_attr(String data_attr) {
        this.data_attr = data_attr;
    }

    public String getData_size() {
        return data_size;
    }

    public void setData_size(String data_size) {
        this.data_size = data_size;
    }

    public String getData_price() {
        return data_price;
    }

    public void setData_price(String data_price) {
        this.data_price = data_price;
    }
}

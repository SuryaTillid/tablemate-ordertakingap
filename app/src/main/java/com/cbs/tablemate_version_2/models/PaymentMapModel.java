package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 15-07-2016 in TableMateNew
 ***********************************************************************/
public class PaymentMapModel {
    private String id;
    private String payment_id;
    private String payment_field_id;
    private String fields_name;
    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(String payment_id) {
        this.payment_id = payment_id;
    }

    public String getPayment_field_id() {
        return payment_field_id;
    }

    public void setPayment_field_id(String payment_field_id) {
        this.payment_field_id = payment_field_id;
    }

    public String getFields_name() {
        return fields_name;
    }

    public void setFields_name(String fields_name) {
        this.fields_name = fields_name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

package com.cbs.tablemate_version_2.models;

/**
 * Created by Barani on 16-05-2016.
 */
public class CustomerDetailsModel {
    private String success;
    private CustomerData customer_data;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public CustomerData getCustomer_data() {
        return customer_data;
    }

    public void setCustomer_data(CustomerData customer_data) {
        this.customer_data = customer_data;
    }
}

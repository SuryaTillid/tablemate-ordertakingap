package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 03-11-2016 in TableMateNew
 ***********************************************************************/
public class BillPay {
    private String success;
    private String unclearedsplits;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getUnclearedsplits() {
        return unclearedsplits;
    }

    public void setUnclearedsplits(String unclearedsplits) {
        this.unclearedsplits = unclearedsplits;
    }
}

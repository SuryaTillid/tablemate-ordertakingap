package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 09-08-2017 in TableMateNew
 ***********************************************************************/
public class Employee_Data {
    private String id;
    private String employee_types_id;
    private EmployeeTypeName employee_types_name = new EmployeeTypeName();
    private String name;
    private String phone;
    private String email;
    private String address;
    private StoreName store_name = new StoreName();
    private String store_id;
    private String unique_id;
    private String login_status;
    private boolean isSelected;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmployee_types_id() {
        return employee_types_id;
    }

    public void setEmployee_types_id(String employee_types_id) {
        this.employee_types_id = employee_types_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public String getLogin_status() {
        return login_status;
    }

    public void setLogin_status(String login_status) {
        this.login_status = login_status;
    }

    public EmployeeTypeName getEmployee_types_name() {
        return employee_types_name;
    }

    public void setEmployee_types_name(EmployeeTypeName employee_types_name) {
        this.employee_types_name = employee_types_name;
    }

    public StoreName getStore_name() {
        return store_name;
    }

    public void setStore_name(StoreName store_name) {
        this.store_name = store_name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}

package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 23-09-2016 in TableMateNew
 ***********************************************************************/
public class FeedbackSubmitModel {
    private String success;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}

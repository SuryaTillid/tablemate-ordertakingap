package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 01-07-2016 in TableMateNew
 ***********************************************************************/
public class CancelItem {
    private int success;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }
}

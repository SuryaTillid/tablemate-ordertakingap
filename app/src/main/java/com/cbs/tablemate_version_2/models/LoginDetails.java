package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 08-12-2016 in TableMateNew
 ***********************************************************************/
public class LoginDetails {
    private String hotel_code;
    private String name;
    private String password;
    private String employee_id;
    private String inTime;
    private String outTime;
    private String ref_id;
    private String success;

    public LoginDetails() {
    }

    public LoginDetails(String hotel_code, String userName, String passWord, String employeeName, String login_time, String logout_time, String referenceId) {
        this.hotel_code = hotel_code;
        this.name = userName;
        this.password = passWord;
        this.employee_id = employeeName;
        this.inTime = login_time;
        this.outTime = logout_time;
        this.ref_id = referenceId;
    }

    public String getInTime() {
        return inTime;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getHotel_code() {
        return hotel_code;
    }

    public void setHotel_code(String hotel_code) {
        this.hotel_code = hotel_code;
    }

    public String getRef_id() {
        return ref_id;
    }

    public void setRef_id(String ref_id) {
        this.ref_id = ref_id;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}

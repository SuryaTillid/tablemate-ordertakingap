package com.cbs.tablemate_version_2.models;

import android.text.TextUtils;

import java.util.ArrayList;

/*********************************************************************
 * Created by Baraneeswari on 25-04-2016 in TableMateNew
 ***********************************************************************/
public class Item {
    private String customId;
    private String id;
    private String name;
    private String for_ck;
    private String description;
    private String photo;
    private String price;
    private String price_1 = "0";
    private String filename;
    private String delivery_cost;
    private String takeaway_cost;
    private String process_time;
    private String type;
    private String food_type_name;
    private String food_type_disable;
    private String search_id;
    private String short_name;
    private String profit_loss;
    private String disable;
    private String timed_id;
    private String timed_name;
    private String filter;
    private String min;
    private String max;
    private String layout;
    private String dinein;
    private String takeaway;
    private String delivery;
    private String cust_aval;
    private CustomizedItemsPortion customPortion;
    private CustomizationItemAddon customAddOn;
    private ArrayList<CustomizationItemsType> customType = new ArrayList<>();
    private ArrayList<EmployeeTypeName> employeeType = new ArrayList<>();
    private String cust_mandatory;
    private String filter_type_name;
    private String filter_name;
    private String ingredients;
    private String customised_item;
    private String itemName;
    private String itemQty;
    private String itemId;
    private String itemProcessTime;
    private String itemPrice;
    private String itemFilterType;
    private String discountAmt;
    private String discountItemPrice;
    private String stockQuantity;
    private String live_item;
    private String customisation_comments = "";
    private String sub_units;
    private String price_tax2;
    private String price_tax1;
    private String alternate_price1;
    private String alternate_price2;
    private String alternate_price;
    private String remove_from_pos;
    private String enable_NC = "0";
    private String color_filter_type;
    private String filter_id;
    private String internal_costing;
    private String emp_id;
    //  private String order_type;

    public String getCustomId() {
        if (TextUtils.isEmpty(customId)) {
            customId = getId();
        }
        return customId;
    }

    public void setCustomId(String customId) {
        this.customId = customId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFor_ck() {
        return for_ck;
    }

    public void setFor_ck(String for_ck) {
        this.for_ck = for_ck;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getDelivery_cost() {
        return delivery_cost;
    }

    public void setDelivery_cost(String delivery_cost) {
        this.delivery_cost = delivery_cost;
    }

    public String getTakeaway_cost() {
        return takeaway_cost;
    }

    public void setTakeaway_cost(String takeaway_cost) {
        this.takeaway_cost = takeaway_cost;
    }

    public String getProcess_time() {
        return process_time;
    }

    public void setProcess_time(String process_time) {
        this.process_time = process_time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFood_type_name() {
        return food_type_name;
    }

    public void setFood_type_name(String food_type_name) {
        this.food_type_name = food_type_name;
    }

    public String getSearch_id() {
        return search_id;
    }

    public void setSearch_id(String search_id) {
        this.search_id = search_id;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getProfit_loss() {
        return profit_loss;
    }

    public void setProfit_loss(String profit_loss) {
        this.profit_loss = profit_loss;
    }

    public String getDisable() {
        return disable;
    }

    public void setDisable(String disable) {
        this.disable = disable;
    }

    public String getTimed_id() {
        return timed_id;
    }

    public void setTimed_id(String timed_id) {
        this.timed_id = timed_id;
    }

    public String getTimed_name() {
        return timed_name;
    }

    public void setTimed_name(String timed_name) {
        this.timed_name = timed_name;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public String getDinein() {
        return dinein;
    }

    public void setDinein(String dinein) {
        this.dinein = dinein;
    }

    public String getTakeaway() {
        return takeaway;
    }

    public void setTakeaway(String takeaway) {
        this.takeaway = takeaway;
    }

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public String getCust_aval() {
        return cust_aval;
    }

    public void setCust_aval(String cust_aval) {
        this.cust_aval = cust_aval;
    }

    public String getCust_mandatory() {
        return cust_mandatory;
    }

    public void setCust_mandatory(String cust_mandatory) {
        this.cust_mandatory = cust_mandatory;
    }

    public String getFilter_type_name() {
        return filter_type_name;
    }

    public void setFilter_type_name(String filter_type_name) {
        this.filter_type_name = filter_type_name;
    }

    public String getFilter_name() {
        return filter_name;
    }

    public void setFilter_name(String filter_name) {
        this.filter_name = filter_name;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingradients) {
        this.ingredients = ingredients;
    }

    public String getCustomised_item() {
        return customised_item;
    }

    public void setCustomised_item(String customised_item) {
        this.customised_item = customised_item;
    }

    public String getItemProcessTime() {
        return itemProcessTime;
    }

    public void setItemProcessTime(String itemProcessTime) {
        this.itemProcessTime = itemProcessTime;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemFilterType() {
        return itemFilterType;
    }

    public void setItemFilterType(String itemFilterType) {
        this.itemFilterType = itemFilterType;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemQty() {
        return itemQty;
    }

    public void setItemQty(String itemQty) {
        this.itemQty = itemQty;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getDiscountItemPrice() {

        return discountItemPrice;
    }

    public void setDiscountItemPrice(String discountItemPrice) {
        this.discountItemPrice = discountItemPrice;
    }

    public String getDiscountAmt() {
        return discountAmt;
    }

    public void setDiscountAmt(String discountAmt) {
        this.discountAmt = discountAmt;
    }

    public CustomizationItemAddon getCustomAddOn() {
        return customAddOn;
    }

    public void setCustomAddOn(CustomizationItemAddon customAddOn) {
        this.customAddOn = customAddOn;
    }

    public CustomizedItemsPortion getCustomPortion() {
        return customPortion;
    }

    public void setCustomPortion(CustomizedItemsPortion customPortion) {
        this.customPortion = customPortion;
    }

    public ArrayList<CustomizationItemsType> getCustomType() {
        return customType;
    }

    public void setCustomType(ArrayList<CustomizationItemsType> customType) {
        this.customType = customType;
    }

    public String getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(String stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public String getLive_item() {
        return live_item;
    }

    public void setLive_item(String live_item) {
        this.live_item = live_item;
    }

    public String getCustomisation_comments() {
        return customisation_comments;
    }

    public void setCustomisation_comments(String customisation_comments) {
        this.customisation_comments = customisation_comments;
    }

    public String getSub_units() {
        return sub_units;
    }

    public void setSub_units(String sub_units) {
        this.sub_units = sub_units;
    }

    public String getPrice_tax1() {
        return price_tax1;
    }

    public void setPrice_tax1(String price_tax1) {
        this.price_tax1 = price_tax1;
    }

    public String getFood_type_disable() {
        return food_type_disable;
    }

    public void setFood_type_disable(String food_type_disable) {
        this.food_type_disable = food_type_disable;
    }

    public String getPrice_tax2() {
        return price_tax2;
    }

    public void setPrice_tax2(String price_tax2) {
        this.price_tax2 = price_tax2;
    }

    public String getAlternate_price1() {
        return alternate_price1;
    }

    public void setAlternate_price1(String alternate_price1) {
        this.alternate_price1 = alternate_price1;
    }

    public String getAlternate_price2() {
        return alternate_price2;
    }

    public void setAlternate_price2(String alternate_price2) {
        this.alternate_price2 = alternate_price2;
    }

    public String getAlternate_price() {
        return alternate_price;
    }

    public void setAlternate_price(String alternate_price) {
        this.alternate_price = alternate_price;
    }

    public String getRemove_from_pos() {
        return remove_from_pos;
    }

    public void setRemove_from_pos(String remove_from_pos) {
        this.remove_from_pos = remove_from_pos;
    }

    public String getEnable_NC() {
        return enable_NC;
    }

    public void setEnable_NC(String enable_NC) {
        this.enable_NC = enable_NC;
    }

    public String getPrice_1() {
        return price_1;
    }

    public void setPrice_1(String price_1) {
        this.price_1 = price_1;
    }

    public String getColor_filter_type() {
        return color_filter_type;
    }

    public void setColor_filter_type(String color_filter_type) {
        this.color_filter_type = color_filter_type;
    }

    public String getFilter_id() {
        return filter_id;
    }

    public void setFilter_id(String filter_id) {
        this.filter_id = filter_id;
    }

    public String getInternal_costing() {
        return internal_costing;
    }

    public void setInternal_costing(String internal_costing) {
        this.internal_costing = internal_costing;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public ArrayList<EmployeeTypeName> getEmployeeType() {
        return employeeType;
    }

    public void setEmployeeType(ArrayList<EmployeeTypeName> employeeType) {
        this.employeeType = employeeType;
    }

    /* public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }
*/
}
package com.cbs.tablemate_version_2.models;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 18-11-2016 in TableMateNew
 ***********************************************************************/
public class LiveMenuItem {
    private ArrayList<StockItem> liveMenuItems = new ArrayList<>();
    private ArrayList<Item> items = new ArrayList<>();

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public ArrayList<StockItem> getLiveMenuItems() {
        return liveMenuItems;
    }

    public void setLiveMenuItems(ArrayList<StockItem> liveMenuItems) {
        this.liveMenuItems = liveMenuItems;
    }
}

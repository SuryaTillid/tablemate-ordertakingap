package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 11-07-2017 in TableMateNew
 ***********************************************************************/
public class Emp_HoldBills {

    private String total;
    private String nofbill;
    private String date;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getNofbill() {
        return nofbill;
    }

    public void setNofbill(String nofbill) {
        this.nofbill = nofbill;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}

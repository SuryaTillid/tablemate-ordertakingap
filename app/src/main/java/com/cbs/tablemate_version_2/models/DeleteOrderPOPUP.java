package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 06-03-2018 in TableMateNew
 ***********************************************************************/
public class DeleteOrderPOPUP {
    private String id;
    private String kot;
    //private String quantity;
    private String bill_qty;
    private String menu_item_id;
    private String custom_data;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKot() {
        return kot;
    }

    public void setKot(String kot) {
        this.kot = kot;
    }

   /* public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }*/

    public String getBill_qty() {
        return bill_qty;
    }

    public void setBill_qty(String bill_qty) {
        this.bill_qty = bill_qty;
    }

    public String getMenu_item_id() {
        return menu_item_id;
    }

    public void setMenu_item_id(String menu_item_id) {
        this.menu_item_id = menu_item_id;
    }

    public String getCustom_data() {
        return custom_data;
    }

    public void setCustom_data(String custom_data) {
        this.custom_data = custom_data;
    }
}

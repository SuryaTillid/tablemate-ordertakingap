package com.cbs.tablemate_version_2.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/*********************************************************************
 * Created by Barani on 10-08-2018 in TableMateNew
 ***********************************************************************/
public class TierValues implements Parcelable {
    public static final Creator<TierValues> CREATOR = new Creator<TierValues>() {
        @Override
        public TierValues createFromParcel(Parcel in) {
            return new TierValues(in);
        }

        @Override
        public TierValues[] newArray(int size) {
            return new TierValues[size];
        }
    };
    private String tier;
    private List<Table> tableList = new ArrayList<>();

    public TierValues(Parcel in) {
        tier = in.readString();
    }

    public TierValues() {
        super();
    }

    public TierValues(String tier, List<Table> tableList) {
        super();
        this.tier = tier;
        this.tableList = tableList;
    }

    public String getTier() {
        return tier;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }

    public List<Table> getTableList() {
        return tableList;
    }

    public void setTableList(List<Table> tableList) {
        this.tableList = tableList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(tier);
    }
}

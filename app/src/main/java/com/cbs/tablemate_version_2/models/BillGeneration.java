package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 10-04-2017 in TableMateNew
 ***********************************************************************/
public class BillGeneration {
    private String split;
    private Bill_Master BillMaster;

    public String getSplit() {
        return split;
    }

    public void setSplit(String split) {
        this.split = split;
    }

    public Bill_Master getBillMaster() {
        return BillMaster;
    }

    public void setBillMaster(Bill_Master billMaster) {
        BillMaster = billMaster;
    }
}

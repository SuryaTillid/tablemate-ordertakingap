package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 08-10-2018 in TableMateNew
 ***********************************************************************/
public class DeleteItem {
    private String order_id;
    private String delete_comment;
    private String other_reason;

    public DeleteItem(String order_id, String cancel_comment, String other_reason) {
        this.order_id = order_id;
        this.delete_comment = cancel_comment;
        this.other_reason = other_reason;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getDelete_comment() {
        return delete_comment;
    }

    public void setDelete_comment(String delete_comment) {
        this.delete_comment = delete_comment;
    }

    public String getOther_reason() {
        return other_reason;
    }

    public void setOther_reason(String other_reason) {
        this.other_reason = other_reason;
    }
}

package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 20-06-2016 in TableMateNew
 ***********************************************************************/
public class SettingsList {
    private String id;
    private String name;
    private String value;
    private String row_changed;
    private String row_uploaded;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRow_changed() {
        return row_changed;
    }

    public void setRow_changed(String row_changed) {
        this.row_changed = row_changed;
    }

    public String getRow_uploaded() {
        return row_uploaded;
    }

    public void setRow_uploaded(String row_uploaded) {
        this.row_uploaded = row_uploaded;
    }
}


package com.cbs.tablemate_version_2.models;

import java.util.ArrayList;

public class BillCalcModel {

    private ArrayList<Menuitem> menuitems = new ArrayList<Menuitem>();
    private ArrayList<BillPreview> ordereditems = new ArrayList<>();
    private ArrayList<POS_Bill> bill_api = new ArrayList<>();
    private ArrayList<CustomerData> hotelprofile = new ArrayList<>();
    private ArrayList<TaxSplit> tax_splits = new ArrayList<TaxSplit>();
    private ArrayList<CustomizationPreview> customization_preview = new ArrayList<>();
    private ArrayList<BillPreview> grouping_bill_preview = new ArrayList<>();
    private TakeawayAddon takeaway_Addon;
    private POS_Bill bill;
    private CustomerData customer_details;
    private String order_type;
    private int total_ordered_qty;
    private int total_ordered_items;

    public ArrayList<Menuitem> getMenuitems() {
        return menuitems;
    }

    public void setMenuitems(ArrayList<Menuitem> menuitems) {
        this.menuitems = menuitems;
    }

    public ArrayList<BillPreview> getOrdereditems() {
        return ordereditems;
    }

    public void setOrdereditems(ArrayList<BillPreview> ordereditems) {
        this.ordereditems = ordereditems;
    }

    public ArrayList<POS_Bill> getBill_api() {
        return bill_api;
    }

    public void setBill_api(ArrayList<POS_Bill> bill_api) {
        this.bill_api = bill_api;
    }

    public ArrayList<CustomerData> getHotelprofile() {
        return hotelprofile;
    }

    public void setHotelprofile(ArrayList<CustomerData> hotelprofile) {
        this.hotelprofile = hotelprofile;
    }

    public ArrayList<TaxSplit> getTax_splits() {
        return tax_splits;
    }

    public void setTax_splits(ArrayList<TaxSplit> tax_splits) {
        this.tax_splits = tax_splits;
    }

    public TakeawayAddon getTakeaway_Addon() {
        return takeaway_Addon;
    }

    public void setTakeaway_Addon(TakeawayAddon takeaway_Addon) {
        this.takeaway_Addon = takeaway_Addon;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public int getTotal_ordered_qty() {
        return total_ordered_qty;
    }

    public void setTotal_ordered_qty(int total_ordered_qty) {
        this.total_ordered_qty = total_ordered_qty;
    }

    public int getTotal_ordered_items() {
        return total_ordered_items;
    }

    public void setTotal_ordered_items(int total_ordered_items) {
        this.total_ordered_items = total_ordered_items;
    }

    public POS_Bill getBill() {
        return bill;
    }

    public void setBill(POS_Bill bill) {
        this.bill = bill;
    }

    public ArrayList<CustomizationPreview> getCustomization_preview() {
        return customization_preview;
    }

    public void setCustomization_preview(ArrayList<CustomizationPreview> customization_preview) {
        this.customization_preview = customization_preview;
    }

    public CustomerData getCustomer_details() {
        return customer_details;
    }

    public void setCustomer_details(CustomerData customer_details) {
        this.customer_details = customer_details;
    }

    public ArrayList<BillPreview> getGrouping_bill_preview() {
        return grouping_bill_preview;
    }

    public void setGrouping_bill_preview(ArrayList<BillPreview> grouping_bill_preview) {
        this.grouping_bill_preview = grouping_bill_preview;
    }
}

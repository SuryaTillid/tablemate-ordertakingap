package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 22-06-2016 in TableMateNew
 ***********************************************************************/
public class StoreDetails {

    private String success;
    private String store_id;
    private String store_name;
    private String err_msg;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getErr_msg() {
        return err_msg;
    }

    public void setErr_msg(String err_msg) {
        this.err_msg = err_msg;
    }
}

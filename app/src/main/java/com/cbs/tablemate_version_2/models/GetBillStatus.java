package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 27-12-2016 in TableMateNew
 ***********************************************************************/
public class GetBillStatus {
    private String id;
    private String billGrandTotal;
    private String bill_id;
    private String table_id;
    private String table_name;
    private String order_type;
    private String date;
    private String phone;

    public String getBillGrandTotal() {
        return billGrandTotal;
    }

    public void setBillGrandTotal(String billGrandTotal) {
        this.billGrandTotal = billGrandTotal;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTable_name() {
        return table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }

    public String getBill_id() {
        return bill_id;
    }

    public void setBill_id(String bill_id) {
        this.bill_id = bill_id;
    }

    public String getTable_id() {
        return table_id;
    }

    public void setTable_id(String table_id) {
        this.table_id = table_id;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}

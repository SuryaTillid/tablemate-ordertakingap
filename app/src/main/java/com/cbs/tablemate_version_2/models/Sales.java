package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 22-02-2018 in TableMateNew
 ***********************************************************************/
public class Sales {
    private String total;
    private String gtotal;
    private String billtotal;
    private String billtax;
    private String round_off;
    private String discount;
    private String sale_price;
    private String nofbill;
    private String price;
    private String customization_cost;
    private String mrp_price;
    private String netsales;
    private String occupancy;
    private String salesperguest;
    private String date;
    private String paymentMode;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getBilltotal() {
        return billtotal;
    }

    public void setBilltotal(String billtotal) {
        this.billtotal = billtotal;
    }

    public String getBilltax() {
        return billtax;
    }

    public void setBilltax(String billtax) {
        this.billtax = billtax;
    }

    public String getRound_off() {
        return round_off;
    }

    public void setRound_off(String round_off) {
        this.round_off = round_off;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getSale_price() {
        return sale_price;
    }

    public void setSale_price(String sale_price) {
        this.sale_price = sale_price;
    }

    public String getNofbill() {
        return nofbill;
    }

    public void setNofbill(String nofbill) {
        this.nofbill = nofbill;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCustomization_cost() {
        return customization_cost;
    }

    public void setCustomization_cost(String customization_cost) {
        this.customization_cost = customization_cost;
    }

    public String getMrp_price() {
        return mrp_price;
    }

    public void setMrp_price(String mrp_price) {
        this.mrp_price = mrp_price;
    }

    public String getNetsales() {
        return netsales;
    }

    public void setNetsales(String netsales) {
        this.netsales = netsales;
    }

    public String getGtotal() {
        return gtotal;
    }

    public void setGtotal(String gtotal) {
        this.gtotal = gtotal;
    }

    public String getOccupancy() {
        return occupancy;
    }

    public void setOccupancy(String occupancy) {
        this.occupancy = occupancy;
    }

    public String getSalesperguest() {
        return salesperguest;
    }

    public void setSalesperguest(String salesperguest) {
        this.salesperguest = salesperguest;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }
}

package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 26-05-2017 in TableMateNew
 ***********************************************************************/
public class StockList {
    private String si_units;
    private String unitMasterName;
    private String unitMasterId;
    private String store_id;
    private String storeName;
    private String stock_id;
    private String stockName;
    private String availableQuantity;
    private String expiredQuantity;
    private String stockQuantity;
    private String sub_units;
    private String sub_unit_name;

    public String getSi_units() {
        return si_units;
    }

    public void setSi_units(String si_units) {
        this.si_units = si_units;
    }

    public String getUnitMasterName() {
        return unitMasterName;
    }

    public void setUnitMasterName(String unitMasterName) {
        this.unitMasterName = unitMasterName;
    }

    public String getUnitMasterId() {
        return unitMasterId;
    }

    public void setUnitMasterId(String unitMasterId) {
        this.unitMasterId = unitMasterId;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStock_id() {
        return stock_id;
    }

    public void setStock_id(String stock_id) {
        this.stock_id = stock_id;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(String availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    public String getExpiredQuantity() {
        return expiredQuantity;
    }

    public void setExpiredQuantity(String expiredQuantity) {
        this.expiredQuantity = expiredQuantity;
    }

    public String getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(String stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public String getSub_units() {
        return sub_units;
    }

    public void setSub_units(String sub_units) {
        this.sub_units = sub_units;
    }

    public String getSub_unit_name() {
        return sub_unit_name;
    }

    public void setSub_unit_name(String sub_unit_name) {
        this.sub_unit_name = sub_unit_name;
    }
}

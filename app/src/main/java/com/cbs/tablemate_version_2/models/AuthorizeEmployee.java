package com.cbs.tablemate_version_2.models;

/*********************************************************************
 * Created by Barani on 08-05-2018 in TableMateNew
 ***********************************************************************/
public class AuthorizeEmployee {
    private String id;
    private String name;
    private String primary_ph_no;
    private String sec_ph_no;
    private String location;
    private String status;
    private String billDiscount;
    private String bill_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrimary_ph_no() {
        return primary_ph_no;
    }

    public void setPrimary_ph_no(String primary_ph_no) {
        this.primary_ph_no = primary_ph_no;
    }

    public String getSec_ph_no() {
        return sec_ph_no;
    }

    public void setSec_ph_no(String sec_ph_no) {
        this.sec_ph_no = sec_ph_no;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBillDiscount() {
        return billDiscount;
    }

    public void setBillDiscount(String billDiscount) {
        this.billDiscount = billDiscount;
    }

    public String getBill_id() {
        return bill_id;
    }

    public void setBill_id(String bill_id) {
        this.bill_id = bill_id;
    }
}

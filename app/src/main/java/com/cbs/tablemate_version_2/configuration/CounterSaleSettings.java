package com.cbs.tablemate_version_2.configuration;

import android.content.Context;
import android.content.SharedPreferences;

/*********************************************************************
 * Created by Barani on 20-02-2018 in TableMateNew
 ***********************************************************************/
public class CounterSaleSettings {
    //Configuration
    final String IS_CONFIGURED = "isConfigured";
    final String DB_NAME = "dbName";
    private final String PREFERENCE_NAME = "TableMate_CounterConfigure_Pref";
    private final int MODE = 0;
    private Context context;
    private SharedPreferences sPref;
    private SharedPreferences.Editor editor;

    public CounterSaleSettings(Context context) {
        this.context = context;
        sPref = context.getSharedPreferences(PREFERENCE_NAME, MODE);
        editor = sPref.edit();
    }

    public boolean isConfigured() {
        return sPref.getBoolean(IS_CONFIGURED, false);
    }

    public void clearSession() {
        editor.clear();
        editor.commit();
    }

    public String getDb_Name() {
        return sPref.getString(DB_NAME, null);
    }

    public void setDb_Name(String p_dbName) {
        editor.putBoolean(IS_CONFIGURED, true);
        editor.putString(DB_NAME, p_dbName);
        editor.commit();
    }
}

package com.cbs.tablemate_version_2.configuration;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.cbs.tablemate_version_2.models.GetPaymentOptions;
import com.cbs.tablemate_version_2.models.Item;
import com.cbs.tablemate_version_2.models.Table;
import com.cbs.tablemate_version_2.utilities.Utility;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

/*********************************************************************
 * Created by Barani on 14-02-2017 in TableMateNew-2.0
 ***********************************************************************/
public class Settings {
    private static final String BILL_ID = "billId";
    private static final String SELECTED_TABLES = "selected_Tables";
    //Configuration
    final String IS_CONFIGURED = "isConfigured";
    final String DB_NAME = "dbName";
    final String IP_ADDRESS = "ipAddress";
    final String TABLE_ID = "tableId";
    final String TABLE_NAME = "tableName";
    final String USER_NAME = "userName";
    final String USER_ID = "userID";
    final String PASSWORD = "password";
    final String EMPLOYEE_ID = "employeeId";
    final String STORE_ID = "storeId";
    final String STORE_NAME = "storeName";
    final String DISCOUNT_AMOUNT = "discountAmount";
    final String MOBILE_NUMBER = "mobile_number";
    final String TOTAL_NUMBER_OF_GUESTS = "total_guests";
    final String ADDRESS = "address";
    final String MENU = "local_menu";
    final String PAY_OPTIONS = "pay_options";
    final String ORDER_TYPE = "order_type";
    final String CUSTOMER_ID = "c_id";
    final String CUSTOMERNAME = "customer_name";
    final String MEMBERSHIPID = "membership_id";
    final String DELIVERY_PARTNER = "delivery_partner";
    final String HOTEL_NAME = "hotelName";
    private final String PREFERENCE_NAME = "TableMate_Pref";
    private final int MODE = 0;
    private Context context;
    private SharedPreferences sPref;
    private SharedPreferences.Editor editor;


    public Settings(Context context) {
        this.context = context;
        sPref = context.getSharedPreferences(PREFERENCE_NAME, MODE);
        editor = sPref.edit();
    }

    public boolean isConfigured() {
        return sPref.getBoolean(IS_CONFIGURED, false);
    }

    public void clearSession() {
        editor.clear();
        editor.commit();
    }

    public void setDb_Name(String p_dbName) {
        editor.putBoolean(IS_CONFIGURED, true);
        editor.putString(DB_NAME, p_dbName);
        editor.commit();
    }

    public String getDbName() {
        return sPref.getString(DB_NAME, null);
    }

    public void setDiscountAmount(String billId, double discountAmount) {
        editor.putString(DISCOUNT_AMOUNT + billId, "" + discountAmount);
        editor.commit();
    }

    public double getDiscountAmount(String billId) {
        return Utility.convertToDouble(sPref.getString(DISCOUNT_AMOUNT + billId, "0"));
    }

    public void clearDiscountAmount(String p_billId) {
        editor.remove(DISCOUNT_AMOUNT + p_billId);
        editor.commit();
    }

    public String getTableID() {
        Table[] tables = new Gson().fromJson(sPref.getString(SELECTED_TABLES, null), Table[].class);
        if (null != tables) {
            return tables[0].getId();
        }
        return null;
    }

    public void setTableID(String tableID) {
        editor.putString(TABLE_ID, tableID);
        editor.commit();
    }

    public String getUSER_NAME() {
        return sPref.getString(USER_NAME, null);
    }

    public void setUSER_NAME(String userName) {
        editor.putString(USER_NAME, userName);
        editor.commit();
    }

    public String getUSER_ID() {
        return sPref.getString(USER_ID, null);
    }

    public void setUSER_ID(String userId) {
        editor.putString(USER_ID, userId);
        editor.commit();
    }

    public String getPASSWORD() {
        return sPref.getString(PASSWORD, null);
    }

    public void setPASSWORD(String password) {
        editor.putString(PASSWORD, password);
        editor.commit();
    }

    public String getSTORE_ID() {
        return sPref.getString(STORE_ID, null);
    }

    public void setSTORE_ID(String storeId) {
        editor.putString(STORE_ID, storeId);
        editor.commit();
    }

    public String getIP_ADDRESS() {
        return IP_ADDRESS;
    }

    public void setIP_ADDRESS(String ipAddress) {
        editor.putString(IP_ADDRESS, null);
        editor.commit();
    }

    public String getTABLE_NAME() {
        Table[] tables = new Gson().fromJson(sPref.getString(SELECTED_TABLES, null), Table[].class);
        if (null != tables) {
            return tables[0].getTable_name();
        }
        return null;
    }

    public void setTABLE_NAME(String table_name) {
        editor.putString(TABLE_NAME, table_name);
        editor.commit();
    }

    public String getEMPLOYEE_ID() {
        return sPref.getString(EMPLOYEE_ID, null);
    }

    public void setEMPLOYEE_ID(String employee_id) {
        editor.putString(EMPLOYEE_ID, employee_id);
        editor.commit();
    }

    public void setBillId(String p_tableId, String p_billId) {
        editor.putString(BILL_ID + p_tableId, p_billId);
        editor.commit();
    }

    public String getBillId(String p_tableId) {
        return sPref.getString(BILL_ID + p_tableId, null);
    }

    public String getDELIVERY_PARTNER(String p_tableId) {
        return sPref.getString(DELIVERY_PARTNER + p_tableId, null);
    }

    public void setDELIVERY_PARTNER(String table_id, String deliveryPartner) {
        editor.putString(DELIVERY_PARTNER + table_id, deliveryPartner);
        editor.commit();
    }


    public String getTOTAL_NUMBER_OF_GUESTS(String p_tableId) {
        return sPref.getString(TOTAL_NUMBER_OF_GUESTS + p_tableId, null);
    }

    public void setTOTAL_NUMBER_OF_GUESTS(String table_id, String totalNumberOfGuests) {
        editor.putString(TOTAL_NUMBER_OF_GUESTS + table_id, totalNumberOfGuests);
        editor.commit();
    }

    public String getCUSTOMER_ID(String p_tableId) {
        return sPref.getString(CUSTOMER_ID + p_tableId, null);
    }

    public void setCUSTOMER_ID(String table_id, String customer_id) {
        editor.putString(CUSTOMER_ID + table_id, customer_id);
        editor.commit();
    }

    public ArrayList<Item> getMenu() {
        String menuString = sPref.getString(MENU, null);
        if (!TextUtils.isEmpty(menuString)) {
            return new ArrayList<>(Arrays.asList(new Gson().fromJson(menuString, Item[].class)));
        }
        return null;
    }

    public void setMenu(ArrayList<Item> menuItems) {
        editor.putString(MENU, new Gson().toJson(menuItems));
        editor.commit();
    }

    public ArrayList<GetPaymentOptions> getPaymentOptions() {
        String payOptionsString = sPref.getString(PAY_OPTIONS, null);
        if (!TextUtils.isEmpty(payOptionsString)) {
            return new ArrayList<>(Arrays.asList(new Gson().fromJson(payOptionsString, GetPaymentOptions[].class)));
        }
        return null;
    }

    public void setPaymentOptions(ArrayList<GetPaymentOptions> paymentOptions) {
        editor.putString(MENU, new Gson().toJson(paymentOptions));
        editor.commit();
    }

    public String getMOBILE_NUMBER(String p_tableId) {
        return sPref.getString(MOBILE_NUMBER + p_tableId, null);
    }

    public void setMOBILE_NUMBER(String p_tableId, String mobileNumber) {
        editor.putString(MOBILE_NUMBER + p_tableId, mobileNumber);
        editor.commit();
    }

    public String getCUSTOMERNAME(String p_tableId) {
        return sPref.getString(CUSTOMERNAME + p_tableId, null);
    }

    public void setCUSTOMERNAME(String p_tableId, String customerName) {
        editor.putString(CUSTOMERNAME + p_tableId, customerName);
        editor.commit();
    }
    public void setMEMBBERSHIPID(String p_tableId, String membershipid) {
        editor.putString(MEMBERSHIPID + p_tableId, membershipid);
        editor.commit();
    }
    public String getMEMBBERSHIPID(String p_tableId) {
        return sPref.getString(MEMBERSHIPID + p_tableId, null);
    }
    public ArrayList<Table> getSelectedTables() {
        String tables = sPref.getString(SELECTED_TABLES, null);
        if (!TextUtils.isEmpty(tables)) {
            return new ArrayList<>(Arrays.asList(new Gson().fromJson(tables, Table[].class)));
        }
        return null;
    }

    public void setSelectedTables(ArrayList<Table> tables) {
        AppLog.write("setSelectedTables", new Gson().toJson(tables));
        editor.putString(SELECTED_TABLES, new Gson().toJson(tables));
        editor.commit();
    }

    public String getORDER_TYPE() {
        return sPref.getString(ORDER_TYPE, "");
    }

    public void setORDER_TYPE(String order_type) {
        editor.putString(ORDER_TYPE, order_type);
        editor.commit();
    }

    public String getADDRESS(String customerID) {
        return sPref.getString(ADDRESS + customerID, null);
    }

    public void setADDRESS(String customerID, String address) {
        editor.putString(ADDRESS + customerID, address);
        editor.commit();
    }

    public String getHOTEL_NAME() {
        return sPref.getString(HOTEL_NAME, null);
    }

    public void setHOTEL_NAME(String hotel_name) {
        editor.putString(HOTEL_NAME, hotel_name);
        editor.commit();
    }
}


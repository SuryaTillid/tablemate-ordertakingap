package com.cbs.tablemate_version_2.configuration;

import android.content.Context;
import android.content.SharedPreferences;

/*********************************************************************
 * Created by Barani on 14-02-2017 in TableMateNew
 ***********************************************************************/
public class ConfigurationSettings {
    //Configuration
    final String IS_CONFIGURED = "isConfigured";
    final String IP_ADDRESS = "ipAddress";
    final String DB_NAME = "dbName";
    final String HOTEL_CODE = "hotel_code";
    final String USER_NAME = "userName";
    final String STORE_NAME = "store_name";
    final String HOTEL_NAME = "hotelName";
    final String BILLID = "billID";
    private final String PREFERENCE_NAME = "TableMate_Configure_Pref";
    private final int MODE = 0;
    private Context context;
    private SharedPreferences sPref;
    private SharedPreferences.Editor editor;

    public ConfigurationSettings(Context context) {
        this.context = context;
        sPref = context.getSharedPreferences(PREFERENCE_NAME, MODE);
        editor = sPref.edit();
    }

    public boolean isConfigured() {
        return sPref.getBoolean(IS_CONFIGURED, false);
    }

    public String getDb_Name() {
        return sPref.getString(DB_NAME, null);
    }

    public void setDb_Name(String p_dbName) {
        editor.putBoolean(IS_CONFIGURED, true);
        editor.putString(DB_NAME, p_dbName);
        editor.commit();
    }

    public String getIP_ADDRESS() {
        return sPref.getString(IP_ADDRESS, null);
    }

    public void setIP_ADDRESS(String ipAddress) {
        editor.putString(IP_ADDRESS, ipAddress);
        editor.commit();
    }

    public String getSTORE_NAME() {
        return sPref.getString(STORE_NAME, null);
    }

    public void setSTORE_NAME(String storeName) {
        editor.putString(STORE_NAME, storeName);
        editor.commit();
    }

    public String getHOTEL_CODE() {
        return sPref.getString(HOTEL_CODE, null);
    }

    public void setHOTEL_CODE(String hotelCode) {
        editor.putString(HOTEL_CODE, hotelCode);
        editor.commit();
    }

    public String getUSER_NAME() {
        return sPref.getString(USER_NAME, null);
    }

    public void setUSER_NAME(String userName) {
        editor.putString(USER_NAME, userName);
        editor.commit();
    }

    public String getHOTEL_NAME() {
        return sPref.getString(HOTEL_NAME, null);
    }

    public void setHOTEL_NAME(String hotel_name) {
        editor.putString(HOTEL_NAME, hotel_name);
        editor.commit();
    }

    public void setBILL__ID(String p_tableId, String p_billId) {
        editor.putString(BILLID + p_tableId, p_billId);
        editor.commit();
    }

    public String getBILL__ID(String p_tableId) {
        return sPref.getString(BILLID + p_tableId, null);
    }

}

package com.cbs.tablemate_version_2.configuration;

import android.util.Log;

import com.cbs.tablemate_version_2.models.AuthorizeEmployee;
import com.cbs.tablemate_version_2.models.BillCalcModel;
import com.cbs.tablemate_version_2.models.BillPay;
import com.cbs.tablemate_version_2.models.CancelItem;
import com.cbs.tablemate_version_2.models.CreateCustomerDetails;
import com.cbs.tablemate_version_2.models.CustomerData;
import com.cbs.tablemate_version_2.models.CustomerDetailsModel;
import com.cbs.tablemate_version_2.models.CustomizationItemAddon;
import com.cbs.tablemate_version_2.models.CustomizationItemsType;
import com.cbs.tablemate_version_2.models.CustomizedItemsPortion;
import com.cbs.tablemate_version_2.models.DbNameModel;
import com.cbs.tablemate_version_2.models.DefaultCustomerDetails;
import com.cbs.tablemate_version_2.models.DeleteOrderPOPUP;
import com.cbs.tablemate_version_2.models.DeliveryPartnerDetails;
import com.cbs.tablemate_version_2.models.DiscountOrder;
import com.cbs.tablemate_version_2.models.DiscountPercentForEmployee;
import com.cbs.tablemate_version_2.models.DiscountResponse;
import com.cbs.tablemate_version_2.models.EmployeeSessionSummaryDetails;
import com.cbs.tablemate_version_2.models.EmployeeTypeName;
import com.cbs.tablemate_version_2.models.Employee_Data;
import com.cbs.tablemate_version_2.models.FilterType;
import com.cbs.tablemate_version_2.models.FloorMaster;
import com.cbs.tablemate_version_2.models.GetBillStatus;
import com.cbs.tablemate_version_2.models.GetDiscount;
import com.cbs.tablemate_version_2.models.GetOrdersForPrint;
import com.cbs.tablemate_version_2.models.GetPaymentOptions;
import com.cbs.tablemate_version_2.models.GetTotalModel;
import com.cbs.tablemate_version_2.models.Item;
import com.cbs.tablemate_version_2.models.LockTable;
import com.cbs.tablemate_version_2.models.LoginDetails;
import com.cbs.tablemate_version_2.models.OrderResponse;
import com.cbs.tablemate_version_2.models.OrdersForTable;
import com.cbs.tablemate_version_2.models.PreDefinedDiscount;
import com.cbs.tablemate_version_2.models.SalesSummary;
import com.cbs.tablemate_version_2.models.SettingsList;
import com.cbs.tablemate_version_2.models.StockItem;
import com.cbs.tablemate_version_2.models.StoreDetails;
import com.cbs.tablemate_version_2.models.SuccessMessage;
import com.cbs.tablemate_version_2.models.Table;
import com.cbs.tablemate_version_2.models.UnLockTable;
import com.cbs.tablemate_version_2.models.WaiterStatusDetails;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*********************************************************************
 * Created by Barani on 14-02-2017 in TableMateNew
 ***********************************************************************/
public class RestApiCalls {

    public SuccessMessage sendEmailContent(App app, String email, String db_name) {
        RestApi apiService = app.createRestAdaptor();
        SuccessMessage result = new SuccessMessage();
        try {
            result = apiService.send_email(email, db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /*send sales summary e-mail*/
    public SuccessMessage sendSalesSummaryMail(App app, String store_name, String from_date, String to_date, String db_name) {
        RestApi apiService = app.createRestAdaptor();
        SuccessMessage result = new SuccessMessage();
        try {
            result = apiService.send_sales_summary_mail(store_name, from_date, to_date, db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public ArrayList<Item> getMenuList(App app, String p_dbName) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<Item> menuItem = new ArrayList<>();
        try {
            menuItem = apiService.getMenuItems(p_dbName).execute().body();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return menuItem;
    }

    public ArrayList<DeliveryPartnerDetails> getDeliveryPartnersDetails(App app, String db_name) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<DeliveryPartnerDetails> deliveryPartnerDetails = new ArrayList<>();
        try {
            deliveryPartnerDetails = apiService.getDeliveryPartnersDetails(db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deliveryPartnerDetails;
    }


    public ArrayList<Table> getTableList(App app, String db_name) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<Table> menuItem = new ArrayList<>();
        try {
            menuItem = apiService.getTableList(db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return menuItem;
    }

    public String UID_validation(App app, String uniqueId, String db_name) {

        RestApi apiService = app.createRestAdaptor();
        String result = "-1";
        try {
            result = apiService.UID_validation(uniqueId, db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public String validation(App app, String username, String password) {

        RestApi apiService = app.createRestAdaptor();
        String result = "-1";
        try {
            result = apiService.login_validation(username, password).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public String logout(App app, String username, String dbName) {

        RestApi apiService = app.createRestAdaptor();
        String result = "-1";
        try {
            result = apiService.logout(username, dbName).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public CustomerDetailsModel getCustomerDetails(App app, String customer_number, String selected_db_name) {
        RestApi apiService = app.createRestAdaptor();
        CustomerDetailsModel customerDetailsModel = new CustomerDetailsModel();
        try {
            customerDetailsModel = apiService.getCustomerDetails(customer_number, selected_db_name).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return customerDetailsModel;
    }

    public DbNameModel getDB_Name(App app, String hotel_id) {
        DbNameModel result = new DbNameModel();
        try {
            result = app.createRestAdaptor().getDB_Name(hotel_id).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public ArrayList<CustomerData> getHotelProfile(App app, String p_dbName) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<CustomerData> h_profile = new ArrayList<>();
        try {
            h_profile = apiService.getHotelProfile(p_dbName).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return h_profile;
    }

    public ArrayList<WaiterStatusDetails> getWaiterIDConfirmStatus(App app, String waiter_status, String selected_db_name) {

        RestApi apiService = app.createRestAdaptor();
        ArrayList<WaiterStatusDetails> result = new ArrayList<>();
        try {
            result = apiService.getWaiterIDConfirmStatus(waiter_status, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public String checkWaiter(App app, String waiterId, String selected_db_name) {

        RestApi apiService = app.createRestAdaptor();
        String result = "-1";
        try {
            result = apiService.checkWaiter(waiterId, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public String Get_KOT_Value(App app, String selected_db_name) {

        RestApi apiService = app.createRestAdaptor();
        String result = "-1";
        try {
            result = apiService.getKOTValue(selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public OrderResponse submitOrder(App app, String orderDetails, String selected_db_name) {

        RestApi apiService = app.createRestAdaptor();
        OrderResponse response = new OrderResponse();
        try {

            Log.e("response", "" + apiService.submitOrder(orderDetails, selected_db_name).execute().code());
            Log.e("response", "" + apiService.submitOrder(orderDetails, selected_db_name).execute().message());
            response = apiService.submitOrder(orderDetails, selected_db_name).execute().body();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public ArrayList<StoreDetails> getCurrentStoreDetails(App app, String user_name, String db_name) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<StoreDetails> response = new ArrayList<>();
        try {
            response = apiService.getCurrentStoreDetails(user_name, db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public CreateCustomerDetails createCustomerDetails(App app, String c_number, String c_name, String c_membershipid, String c_mail, String c_address, String conduct, String guest, String selected_db_name) {
        RestApi apiService = app.createRestAdaptor();
        CreateCustomerDetails response = new CreateCustomerDetails();
        try {
            response = apiService.createCustomerDetails(c_number, c_name, c_membershipid, c_mail, c_address, conduct, guest, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public LockTable lockTable(App app, String mobileNumber, String customerName, String membership_id, String eMail, String address, String userName, String password, String tableId, String no_of_guests, String selected_db_name) {
        RestApi apiService = app.createRestAdaptor();
        LockTable response = new LockTable();
        try {
            response = apiService.lockTable(mobileNumber, customerName, membership_id, eMail, address, userName, password, tableId, no_of_guests, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public LockTable lockTableModified(App app, String mobileNumber, String customerName, String membership_id, String eMail, String address,
                                       String userName, String password, String tableId, String no_of_guests,
                                       String order_type, String room_ref_no, String child, String selected_db_name) {
        RestApi apiService = app.createRestAdaptor();
        LockTable response = new LockTable();
        try {


            response = apiService.lockTableModified(mobileNumber, customerName, membership_id, eMail, address, userName, password, tableId,
                    no_of_guests, order_type, room_ref_no, child, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public UnLockTable unLockTable(App app, String userName, String password, String tableId, String selected_db_name) {
        RestApi apiService = app.createRestAdaptor();
        UnLockTable response = new UnLockTable();
        try {
            response = apiService.unLockTable(userName, password, tableId, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public ArrayList<OrdersForTable> getOrdersForTable(App app, String tableId, String selected_db_name) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<OrdersForTable> response = new ArrayList<>();
        try {
            response = apiService.getOrdersForTable(tableId, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public List<String> getBillId(App app, String split, String billMaster, String selected_db_name) {
        RestApi apiService = app.createRestAdaptor();
        List<String> result = new ArrayList<>();
        //GetBillId result = new GetBillId();
        try {
            result = apiService.getBillId(split, billMaster, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public CancelItem cancelItem(App app, String orderId, String comments, String selected_db_name) {

        RestApi apiService = app.createRestAdaptor();
        CancelItem response = new CancelItem();
        try {
            response = apiService.cancelItem(orderId, comments, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public GetTotalModel getBillTotal(App app, String tableId, String selected_db_name) {

        RestApi apiService = app.createRestAdaptor();
        GetTotalModel response = new GetTotalModel();
        try {
            response = apiService.getBillTotal(tableId, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public BillCalcModel billCalculation(App app, String billId, String orderType, String selected_db_name) {
        RestApi restApi = app.createRestAdaptor();
        BillCalcModel response = new BillCalcModel();
        try {
            response = restApi.billCalculation(billId, orderType, selected_db_name).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     *  Order Status
     * */
    public ArrayList<OrdersForTable> getBillPreview(App app, String tableId, String selected_db_name) {
        RestApi restApi = app.createRestAdaptor();
        ArrayList<OrdersForTable> response = new ArrayList<>();
        try {
            response = restApi.getBillPreview(tableId, selected_db_name).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String validatePassword(App app, String userName, String password, String selected_db_name) {
        RestApi apiService = app.createRestAdaptor();
        String result = "-1";
        try {
            result = apiService.validatePassword(userName, password, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public ArrayList<CustomizedItemsPortion> getPortion(App app, String menu_item_id, String selected_db_name) {

        RestApi apiService = app.createRestAdaptor();
        ArrayList<CustomizedItemsPortion> result = new ArrayList<>();
        try {
            result = apiService.getPortion(menu_item_id, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public ArrayList<SettingsList> get_details_for_KitchenScreen(App app, String p_dbName) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<SettingsList> result = new ArrayList<>();
        try {
            result = apiService.get_details_for_KitchenScreen(p_dbName).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public ArrayList<CustomizationItemsType> getItemType(App app, String menuId, String selected_db_name) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<CustomizationItemsType> response = new ArrayList<>();
        try {
            response = apiService.getItemType(menuId, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public ArrayList<CustomizationItemAddon> getAddon(App app, String menuId, String selected_db_name) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<CustomizationItemAddon> response = new ArrayList<>();
        try {
            response = apiService.getAddon(menuId, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public ArrayList<GetPaymentOptions> get_paymentOptions(App app, String selected_db_name) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<GetPaymentOptions> response = new ArrayList<>();
        try {
            response = apiService.get_paymentOptions(selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /*public String getDBName(App app) {
        RestApi apiService = app.createRestAdaptor();
        String result = "-1";
        try {
            result = apiService.getDBName().execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }*/

    public DiscountResponse getDiscount(App app, String billId, String discount, String gTotal, String orderId, String remark, String overall_discount, String order_type, String customize_exist, String overallDiscount_type, String selected_db_name) {
        RestApi restApi = app.createRestAdaptor();
        DiscountResponse response = new DiscountResponse();
        try {
            response = restApi.getDiscount(billId, discount, gTotal, orderId, remark, overall_discount, order_type, customize_exist, overallDiscount_type, selected_db_name).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String getDeliveryStatus(App app, String order_id, String dbName) {

        RestApi apiService = app.createRestAdaptor();
        String result = "-1";
        try {
            result = apiService.getDeliveryStatus(order_id, dbName).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public ArrayList<StockItem> getStockList(App app, String p_dbName) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<StockItem> menuItem = new ArrayList<>();
        try {
            menuItem = apiService.getStockList(p_dbName).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return menuItem;
    }

    /*
     * Payment
     * */

    public BillPay billPayment(App app, String bill_id, String split_id, String paymentMode, String pay_mode, String amountReceived, String balanceGiven,
                               String cardNumber, String cardType, String remark, String table_id, String customerMobile, String db_name) {
        RestApi restApi = app.createRestAdaptor();
        BillPay response = new BillPay();
        try {
            response = restApi.payingBill(bill_id, split_id, paymentMode, pay_mode, amountReceived, balanceGiven, cardNumber, cardType, remark, table_id, customerMobile, db_name).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Get Orders for Print
     * */

    public ArrayList<GetOrdersForPrint> getOrdersForPrint(App app, String selected_db_name) {
        RestApi restApi = app.createRestAdaptor();
        ArrayList<GetOrdersForPrint> response = new ArrayList<GetOrdersForPrint>();
        try {
            response = restApi.getOrdersForPrint(selected_db_name).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Send Print Request
     * */

    public String sendPrintRequest_to_Server(App app, String kot, String selected_db_name) {
        RestApi restApi = app.createRestAdaptor();
        String result = "-1";
        try {
            result = restApi.sendPrintRequestForKOT(kot, selected_db_name).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    /*
     * Send Print Request For POS_Bill
     * */

    public String sendPrintRequest_to_ServerForBill(App app, String bill_id, String printBillRequest, String selected_db_name) {
        RestApi restApi = app.createRestAdaptor();
        String result = "-1";
        try {
            result = restApi.sendPrintRequestForBill(bill_id, printBillRequest, selected_db_name).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    /*
     * Switch Table
     * */

    public DiscountResponse swapTable(App app, String table_id, String switch_table_id, String customerName, String selected_db_name) {

        RestApi apiService = app.createRestAdaptor();
        DiscountResponse discountResponse = new DiscountResponse();
        try {
            discountResponse = apiService.swapTable(table_id, switch_table_id, customerName, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return discountResponse;
    }

    /*
     * Switch Table
     * */

    public DiscountResponse swapTableModified(App app, String table_id, String switch_table_id, String customerName, String bill_id, String selected_db_name) {

        RestApi apiService = app.createRestAdaptor();
        DiscountResponse discountResponse = new DiscountResponse();
        try {
            discountResponse = apiService.swapTable_new(table_id, switch_table_id, customerName, bill_id, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return discountResponse;
    }

    /*
     * Get POS_Bill Items after generating bill
     * */

    public GetDiscount getMenuItemsAfterBillGeneration(App app, String billId, String selected_db_name) {
        RestApi restApi = app.createRestAdaptor();
        GetDiscount response = new GetDiscount();
        try {
            response = restApi.getBilledItems(billId, selected_db_name).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Waiter session - login
     * */

    public LoginDetails loginWaiterSession(App app, String hotel_code, String employee_name, String login_time, String reference_id, String selected_db_name) {

        RestApi apiService = app.createRestAdaptor();
        LoginDetails result = new LoginDetails();
        try {
            result = apiService.loginWaiterSession(hotel_code, employee_name, login_time, reference_id, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /*
     * Waiter session - logout
     * */

    public String logoutWaiterSession(App app, String hotel_code, String employee_name, String logout_time, String reference_id, String selected_db_name) {

        RestApi apiService = app.createRestAdaptor();
        String result = "-1";
        try {
            result = apiService.logoutWaiterSession(hotel_code, employee_name, logout_time, reference_id, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /*
     * Get orders with bill id
     * */

    public ArrayList<OrdersForTable> getOrdersWithBillID(App app, String billID, String selected_db_name) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<OrdersForTable> response = new ArrayList<>();
        try {
            response = apiService.getOrdersWithBillId(billID, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     *  Clear All items in order cart
     * */
    public String clearOrderedItems(App app, String order_ids, String comment, String selected_db_name) {

        RestApi apiService = app.createRestAdaptor();
        String result = "-1";
        try {
            result = apiService.clearAll(order_ids, comment, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /*
     *  Get POS_Bill Status-
     * */
    public ArrayList<GetBillStatus> getBillStatus(App app, String selected_db_name) {

        RestApi apiService = app.createRestAdaptor();
        ArrayList<GetBillStatus> result = new ArrayList<>();
        try {
            result = apiService.getBillStatus(selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /*
     * Get Customer Details
     * */

    public ArrayList<DefaultCustomerDetails> getCustomerDetailsForTableID(App app, String tableID, String selected_db_name) {

        RestApi apiService = app.createRestAdaptor();
        ArrayList<DefaultCustomerDetails> result = new ArrayList<DefaultCustomerDetails>();
        try {
            result = apiService.getCustomerDetailsForTableID(tableID, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /*
     * Join Tables
     * */
    public Table JoinMultipleTables(App app, String password, String locked_id, String customer_details, String tableID, String pwd_required, String customer_number,
                                    String no_guests, String child, String room_no, String order_type, String user_name, String selected_db_name) {

        RestApi apiService = app.createRestAdaptor();
        Table result = new Table();
        try {
            result = apiService.JoinMultipleTables(password, locked_id, customer_details, tableID, pwd_required, customer_number,
                    no_guests, child, room_no, order_type, user_name, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /*
     * Discount Confirmation - user credential validation
     * */
    public String discountConfirmation(App app, String username, String password, String dbName) {

        RestApi apiService = app.createRestAdaptor();
        String result = "-1";
        try {
            result = apiService.discount_confirmation(username, password, dbName).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /*
     * Item Cancellation - after bill generation
     * */

    public DiscountResponse CancelMenuItem_in_bill(App app, String itemArray, String billID, String tableID, String update_type, String selected_db_name) {
        RestApi restApi = app.createRestAdaptor();
        DiscountResponse response = new DiscountResponse();
        try {
            response = restApi.itemCancellation(itemArray, billID, tableID, update_type, selected_db_name).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Item Cancellation Modified- after bill generation
     * */

    public SuccessMessage CancelMenuItemAfterBill(App app, String itemArray, String billID, String order_type, String tableID, String bill_status, String user_name, String selected_db_name) {
        RestApi restApi = app.createRestAdaptor();
        SuccessMessage response = new SuccessMessage();
        try {
            response = restApi.itemCancellation_modified(itemArray, billID, order_type, tableID, bill_status, user_name, selected_db_name).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Call - Pay later
     * */

    public String payLater(App app, String billID, String table_id, String billStatus, String selected_db_name) {

        RestApi apiService = app.createRestAdaptor();
        String result = "-1";
        try {
            result = apiService.call_payLater(billID, table_id, billStatus, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /*
     * Modified - POS_Bill paying
     * */

    public BillPay billPaymentModified(App app, String store_id, String split_id, String bill_id, String roundOff, String balanceGiven, String partialAmount, String remark, String table_id, String t_id,
                                       String customerName, String customerNumber, String amountPaid, String payment_amount, String payment_mode, String print_text, String tableName, String delivery_partnerName, String db_name) {
        RestApi restApi = app.createRestAdaptor();
        BillPay response = new BillPay();
        try {
            response = restApi.payingBillModified(store_id, split_id, bill_id, roundOff, balanceGiven, partialAmount, remark, table_id, t_id, customerName, customerNumber, amountPaid, payment_amount, payment_mode, print_text, tableName, delivery_partnerName, db_name).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Get Employee wise sale summary
     * */

    public EmployeeSessionSummaryDetails Employee_sales_summary(App app, String from_date, String to_date, String employee_name, String selected_db_name) {
        RestApi restApi = app.createRestAdaptor();
        EmployeeSessionSummaryDetails response = new EmployeeSessionSummaryDetails();
        String s = null;
        try {
            response = restApi.getEmployee_wise_summary(from_date, to_date, employee_name, selected_db_name).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Get Employee Details
     * */

    public ArrayList<Employee_Data> getEmployeeDetailsList(App app, String db_name) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<Employee_Data> employeeDataList = new ArrayList<>();
        try {
            employeeDataList = apiService.getEmployeeDetails(db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return employeeDataList;
    }

    /*
     * Reset Session
     * */

    public SuccessMessage ResetLoginSession(App app, String userName, String password, String status, String db_name) {

        RestApi apiService = app.createRestAdaptor();
        SuccessMessage result = new SuccessMessage();
        try {
            result = apiService.ResetLoginSession(userName, password, status, db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /*
     * Discount modified
     * */

    public DiscountResponse getDiscountModified(App app, String billId, String discount, String gTotal, String orderId, String remark, String overall_discount, String order_type, String customize_exist, String overallDiscount_type, String flag, String selected_db_name) {
        RestApi restApi = app.createRestAdaptor();
        DiscountResponse response = new DiscountResponse();
        try {
            response = restApi.getDiscount2(billId, discount, gTotal, orderId, remark, overall_discount, order_type, customize_exist, overallDiscount_type, flag, selected_db_name).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Discount modified - new flow
     * */

    public DiscountResponse getDiscountModified_updated(App app, String item_array, String cus_array, String bill_id,
                                                        String discount_type, String overall_discount, String comment, String selected_db_name) {
        RestApi restApi = app.createRestAdaptor();
        DiscountResponse response = new DiscountResponse();
        try {
            response = restApi.updateDiscount(item_array, cus_array, bill_id, discount_type, overall_discount, comment, selected_db_name).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Discount Confirmation - user credential validation
     * */
    public DiscountPercentForEmployee discountPercentApplicableForEmployee(App app, String empName, String dbName) {

        RestApi apiService = app.createRestAdaptor();
        DiscountPercentForEmployee result = new DiscountPercentForEmployee();
        try {
            result = apiService.discountPercentApplicableForEmployee(empName, dbName).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /*
     * Get Sales Summary Report
     * */
    public SalesSummary getSalesSummary(App app, String from, String to, String selected_db_name) {
        RestApi apiService = app.createRestAdaptor();
        SalesSummary sales = new SalesSummary();
        try {
            sales = apiService.getSales_details(from, to, selected_db_name).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sales;
    }

    /*
     * OrdersDeletePOPUPDetails
     * */
    public ArrayList<DeleteOrderPOPUP> OrdersDeletePOPUPDetails(App app, String menu_id, String table_id, String custom_exist,
                                                                String custom_data, String selected_db_name) {

        RestApi apiService = app.createRestAdaptor();
        ArrayList<DeleteOrderPOPUP> result = new ArrayList<>();
        try {
            result = apiService.OrdersDeletePOPUPDetails(menu_id, table_id, custom_exist, custom_data, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    //SubmitDeleteOrders

    public SuccessMessage SubmitDeleteOrders(App app, String cancel_comment, String menu_id,
                                             String cancel_qty, String bill_qty,
                                             String table_id, String order_id,
                                             String selected_db_name) {
        RestApi apiService = app.createRestAdaptor();
        SuccessMessage result = new SuccessMessage();
        try {
            result = apiService.SubmitDeleteOrders(cancel_comment, menu_id, cancel_qty, bill_qty, table_id, order_id, selected_db_name).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    //Get Authorized Employee Details

    public ArrayList<AuthorizeEmployee> GetAuthorizeEmployeeDetails(App app, String selected_db_name) {

        RestApi apiService = app.createRestAdaptor();
        ArrayList<AuthorizeEmployee> result = new ArrayList<>();
        try {
            result = apiService.get_details_for_authorized_employee(selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    //Get Authorized Employee Details - Bill ID

    public ArrayList<AuthorizeEmployee> GetAuthorizeEmployeeDetails_For_Bill(App app, String bill_id, String selected_db_name) {

        RestApi apiService = app.createRestAdaptor();
        ArrayList<AuthorizeEmployee> result = new ArrayList<>();
        try {
            result = apiService.get_employee_details_for_bill(bill_id, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    //OTP Authorize

    public AuthorizeEmployee OTP_Authorize_submit(App app, String auth_emp, String req_emp, String otp, String bill_id, String pax, String usage_flag, String selected_db_name) {
        RestApi apiService = app.createRestAdaptor();
        AuthorizeEmployee result = new AuthorizeEmployee();
        try {
            result = apiService.OTP_authorize(auth_emp, req_emp, otp, bill_id, pax, usage_flag, selected_db_name).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public SuccessMessage Apply_OTP_Value(App app, String bill_id, String table_id, String otp, String db_name) {

        RestApi apiService = app.createRestAdaptor();
        SuccessMessage result = new SuccessMessage();
        try {
            result = apiService.Apply_OTP(bill_id, table_id, otp, db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /*
     * Update Bill Id
     * */
    public SuccessMessage Update_Bill(App app, String bill_id, String order_type, String table_id, String u_name, String db_name) {

        RestApi apiService = app.createRestAdaptor();
        SuccessMessage result = new SuccessMessage();
        try {
            result = apiService.updateBillId(bill_id, order_type, table_id, u_name, db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    //Get Floor Details

    public ArrayList<FloorMaster> GetFloorDetails(App app, String selected_db_name) {

        RestApi apiService = app.createRestAdaptor();
        ArrayList<FloorMaster> result = new ArrayList<>();
        try {
            result = apiService.getFloorDetails(selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /*
     * Get Split Orders for Mobile
     * */

    public ArrayList<OrdersForTable> get_split_orders(App app, String table_id, String menu_id, String custom_exist,
                                                      String custom_data, String nc_order, String selected_db_name) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<OrdersForTable> response = new ArrayList<>();
        try {
            response = apiService.get_orders_split(table_id, menu_id, custom_exist, custom_data, nc_order, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Get Split Orders for Mobile
     * */

    public ArrayList<DiscountOrder> get_split_orders_for_discount(App app, String bill_id, String menu_id,
                                                                  String custom_data, String selected_db_name) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<DiscountOrder> response = new ArrayList<>();
        try {
            response = apiService.get_discount_items_split(bill_id, menu_id, custom_data, selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Cancel item mobile orders - modified
     * */

    public SuccessMessage cancelItem_for_mobile(App app, String order_array, String user_name, String db_name) {
        RestApi apiService = app.createRestAdaptor();
        SuccessMessage result = new SuccessMessage();
        try {
            result = apiService.cancelItemMobile(order_array, user_name, db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /*
     * Update Modify Price
     * */

    public SuccessMessage Update_modify_price(App app, String item_array, String bill_id, String order_type, String db_name) {
        RestApi apiService = app.createRestAdaptor();
        SuccessMessage result = new SuccessMessage();
        try {
            result = apiService.update_modify_price(item_array, bill_id, order_type, db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    //Get PredefinedDiscountValues

    public ArrayList<PreDefinedDiscount> GetPredefinedDiscount_values(App app, String selected_db_name) {

        RestApi apiService = app.createRestAdaptor();
        ArrayList<PreDefinedDiscount> result = new ArrayList<>();
        try {
            result = apiService.get_pre_defined_discount_values(selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    //Submit Pre-defined discount

    public SuccessMessage Submit_pre_defined_discount(App app, String bill_id,
                                                      String promo_comment, String promo_percent,
                                                      String selected_db_name) {
        RestApi apiService = app.createRestAdaptor();
        SuccessMessage result = new SuccessMessage();
        try {
            result = apiService.submitPredefinedDiscount(bill_id, promo_comment, promo_percent, selected_db_name).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    //Submit Additional Charges

    public SuccessMessage Submit_Additional_charges(App app, String delivery_charge,
                                                    String package_charge, String other_charge,
                                                    String selected_db_name) {
        RestApi apiService = app.createRestAdaptor();
        SuccessMessage result = new SuccessMessage();
        try {
            result = apiService.submitAdditionalCharge(delivery_charge, package_charge, other_charge, selected_db_name).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public SuccessMessage CancelBill(App app, String userName, String password, String remarks, String bill_id, String db_name) {

        RestApi apiService = app.createRestAdaptor();
        SuccessMessage result = new SuccessMessage();
        try {
            result = apiService.cancelBill(userName, password, remarks, bill_id, db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /*
     * Get Filter Type List
     * */

    public ArrayList<FilterType> getItemFilterType(App app, String selected_db_name) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<FilterType> response = new ArrayList<>();
        try {
            response = apiService.getFilterTypeList(selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
     * Get Employee Name list for NC
     * */
    public ArrayList<EmployeeTypeName> getEmployeeListNC(App app, String selected_db_name) {
        RestApi apiService = app.createRestAdaptor();
        ArrayList<EmployeeTypeName> response = new ArrayList<>();
        try {
            response = apiService.getEmployeeType(selected_db_name).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

}
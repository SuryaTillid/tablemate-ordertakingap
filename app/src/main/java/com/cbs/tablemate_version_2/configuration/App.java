package com.cbs.tablemate_version_2.configuration;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/*********************************************************************
 * Created by Barani on 14-02-2017 in TableMateNew-2.0
 ***********************************************************************/
public class App extends Application {
    public static String IP_ADDRESS = "https://tablemate.codebase.bz";
    private static String SERVER_ADDRESS;
    private static App mInstance;
    private RestApi restApi;

    public static synchronized App getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        //TypefaceUtil.overrideFonts(this);
    }

    public RestApi createRestAdaptor() {
        setServerAddress();

        Gson gson = new GsonBuilder().setLenient().create();

        Retrofit client = new Retrofit.Builder().baseUrl(SERVER_ADDRESS).client(getClient())
                .addConverterFactory(GsonConverterFactory.create(gson)).build();
        restApi = client.create(RestApi.class);

        return restApi;
    }

    private void setServerAddress() {
        if (AppLog.isDebugUrl)
            SERVER_ADDRESS = IP_ADDRESS;// getResources().getString(R.string.debug_url);
        else
            //SERVER_ADDRESS = getResources().getString(R.string.debug_url);
            SERVER_ADDRESS = IP_ADDRESS;
    }

    private OkHttpClient getClient() {

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.MINUTES).readTimeout(5, TimeUnit.MINUTES).writeTimeout(5, TimeUnit.MINUTES).addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Interceptor.Chain chain) throws IOException {
                        Response response = chain.proceed(chain.request());
                        try {
//                                AppLog.write("RestRequest", new Gson().toJson(response.request().url()));
                            RequestBody body = chain.request().body();
                            AppLog.write("RestResult", new Gson().toJson(response.code()));
                            Buffer buffer = new Buffer();
                            if (body != null) {
                                body.writeTo(buffer);
                                String bodya = buffer.readUtf8();
                                AppLog.write("RestInput", "" + bodya);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return response;
                    }
                }).build();
        return client;
    }
}


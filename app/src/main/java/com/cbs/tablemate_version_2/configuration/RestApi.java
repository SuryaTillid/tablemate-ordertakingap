package com.cbs.tablemate_version_2.configuration;

import com.cbs.tablemate_version_2.models.AuthorizeEmployee;
import com.cbs.tablemate_version_2.models.BillCalcModel;
import com.cbs.tablemate_version_2.models.BillPay;
import com.cbs.tablemate_version_2.models.CancelItem;
import com.cbs.tablemate_version_2.models.CreateCustomerDetails;
import com.cbs.tablemate_version_2.models.CustomerData;
import com.cbs.tablemate_version_2.models.CustomerDetailsModel;
import com.cbs.tablemate_version_2.models.CustomizationItemAddon;
import com.cbs.tablemate_version_2.models.CustomizationItemsType;
import com.cbs.tablemate_version_2.models.CustomizedItemsPortion;
import com.cbs.tablemate_version_2.models.DbNameModel;
import com.cbs.tablemate_version_2.models.DefaultCustomerDetails;
import com.cbs.tablemate_version_2.models.DeleteOrderPOPUP;
import com.cbs.tablemate_version_2.models.DeliveryPartnerDetails;
import com.cbs.tablemate_version_2.models.DiscountOrder;
import com.cbs.tablemate_version_2.models.DiscountPercentForEmployee;
import com.cbs.tablemate_version_2.models.DiscountResponse;
import com.cbs.tablemate_version_2.models.EmployeeSessionSummaryDetails;
import com.cbs.tablemate_version_2.models.EmployeeTypeName;
import com.cbs.tablemate_version_2.models.Employee_Data;
import com.cbs.tablemate_version_2.models.FeedbackSubmitModel;
import com.cbs.tablemate_version_2.models.FilterType;
import com.cbs.tablemate_version_2.models.FloorMaster;
import com.cbs.tablemate_version_2.models.GetBillStatus;
import com.cbs.tablemate_version_2.models.GetDiscount;
import com.cbs.tablemate_version_2.models.GetOrdersForPrint;
import com.cbs.tablemate_version_2.models.GetPaymentOptions;
import com.cbs.tablemate_version_2.models.GetTotalModel;
import com.cbs.tablemate_version_2.models.Item;
import com.cbs.tablemate_version_2.models.LockTable;
import com.cbs.tablemate_version_2.models.LoginDetails;
import com.cbs.tablemate_version_2.models.OrderResponse;
import com.cbs.tablemate_version_2.models.OrdersForTable;
import com.cbs.tablemate_version_2.models.PreDefinedDiscount;
import com.cbs.tablemate_version_2.models.SalesSummary;
import com.cbs.tablemate_version_2.models.SettingsList;
import com.cbs.tablemate_version_2.models.StockItem;
import com.cbs.tablemate_version_2.models.StockList;
import com.cbs.tablemate_version_2.models.StoreDetails;
import com.cbs.tablemate_version_2.models.SuccessMessage;
import com.cbs.tablemate_version_2.models.Table;
import com.cbs.tablemate_version_2.models.UnLockTable;
import com.cbs.tablemate_version_2.models.WaiterStatusDetails;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/*********************************************************************
 * Created by Barani on 14-02-2017 in TableMateNew-2.0
 ***********************************************************************/
public interface RestApi {

    @FormUrlEncoded
    @POST("index.php/customermaster/sendeventmail")
    Call<SuccessMessage> send_email(@Field("guest_email") String email_id, @Query("selected_db_name") String db_name);

    @FormUrlEncoded
    @POST("index.php/settings/emailformobile")
    Call<SuccessMessage> send_sales_summary_mail(@Field("store_name") String store_name,
                                                 @Field("from") String from_date, @Field("to") String to_date,
                                                 @Query("selected_db_name") String db_name);

    @GET("index.php/menuitems/getmenuitems")
    Call<ArrayList<Item>> getMenuItems(@Query("selected_db_name") String p_dbName);

    @GET("index.php/stocks/stocklist_mobile")
    Call<ArrayList<StockItem>> getStockList(@Query("selected_db_name") String p_dbName);

    @GET("index.php/floormaster/listformobile")
    Call<ArrayList<FloorMaster>> getFloorDetails(@Query("selected_db_name") String p_dbName);

    @GET("index.php/tablesmaster/list")
    Call<ArrayList<Table>> getTableList(@Query("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("index.php/site/get_current_store_id")
    Call<ArrayList<StoreDetails>> getCurrentStoreDetails(@Field("user_name") String user_name, @Query("selected_db_name") String p_dbName);

    @GET("index.php/deliverypartners/get_deliverypartners_mobile")
    Call<ArrayList<DeliveryPartnerDetails>> getDeliveryPartnersDetails(@Query("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("db_setup/index.php/hotellist/check_hotel_list_viaapi")
    Call<DbNameModel> getDB_Name(@Field("content") String hotel_id);

    @FormUrlEncoded
    @POST("index.php/employeemaster/getlistformobile")
    Call<ArrayList<Employee_Data>> getEmployeeDetails(@Field("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("index.php/site/before_login_uid")
    Call<String> UID_validation(@Field("swipeId") String uniqueId, @Field("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("index.php/site/loginviaapi")
    Call<String> login_validation(@Field("username") String username, @Field("password") String password);

    @FormUrlEncoded
    @POST("index.php/site/logoutapi")
    Call<String> logout(@Field("username") String username, @Field("selected_db_name") String dbName);

    @FormUrlEncoded
    @POST("index.php/menuitems/customermasterlistapi")
    Call<CustomerDetailsModel> getCustomerDetails(@Field("customer_number") String customer_number, @Field("selected_db_name") String selected_db_name);

    @GET("index.php/customermaster/list")
    Call<ArrayList<CustomerData>> getHotelProfile(@Query("selected_db_name") String p_dbName);

    @GET("index.php/orders/getkotorder")
    Call<String> getKOTValue(@Query("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("index.php/settings/get_setting_value")
    Call<ArrayList<WaiterStatusDetails>> getWaiterIDConfirmStatus(@Field("name") String waiter_status, @Field("selected_db_name") String selected_db_name);

    @FormUrlEncoded
    @POST("index.php/orders/createmobile")
    Call<OrderResponse> submitOrder(@Field("Orders") String p_orderDetails, @Query("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("index.php/menuitems/lockapi")
    Call<LockTable> lockTable(@Field("MenuItems[customer_number]") String customer_number, @Field("MenuItems[customer]") String customer_name,
                              @Field("MenuItems[customer_mailid]") String customer_mailId, @Field("MenuItems[customer_address]") String customer_address,
                              @Field("MenuItems[uname]") String userName, @Field("MenuItems[pwd]") String password,
                              @Field("MenuItems[table_id]") String tableId, @Field("MenuItems[customer_guest]") String guests,
                              @Field("selected_db_name") String selected_db_name, String selectedDbName);

    @FormUrlEncoded
    @POST("index.php/menuitems/lockapi")
    Call<LockTable> lockTableModified(@Field("MenuItems[customer_number]") String customer_number, @Field("MenuItems[customer]") String customer_name,
                                      @Field("MenuItems[membership_id]") String mem,
                                      @Field("MenuItems[customer_mailid]") String customer_mailId, @Field("MenuItems[customer_address]") String customer_address,
                                      @Field("MenuItems[uname]") String userName, @Field("MenuItems[pwd]") String password,
                                      @Field("MenuItems[table_id]") String tableId, @Field("MenuItems[customer_guest]") String guests,
                                      @Field("MenuItems[order_type]") String order_type, @Field("MenuItems[room_ref_no]") String room_ref,
                                      @Field("MenuItems[child]") String child_pax, @Field("selected_db_name") String selected_db_name);

    @FormUrlEncoded
    @POST("index.php/menuitems/unlockapi")
    Call<UnLockTable> unLockTable(@Field("uname") String userName, @Field("pwd") String password, @Field("table_id") String tableId, @Field("selected_db_name") String selected_db_name);

    @FormUrlEncoded
    @POST("index.php/orders/customercreate")
    Call<CreateCustomerDetails> createCustomerDetails(String cNumber, @Field("customer_number") String c_number,
                                                      @Field("customer_name") String c_name, @Field("membership_id") String c_mem,
                                                      @Field("customer_mailid") String c_mailId,
                                                      @Field("customer_address") String c_address,
                                                      @Field("customer_conduct") String c_conduct,
                                                      @Field("customer_guest") String guest);

    @FormUrlEncoded
    @POST("index.php/wtrlist/valide_wtr_id")
    Call<String> checkWaiter(@Field("wtr_id") String waiterId, @Field("selected_db_name") String selected_db_name);

    @FormUrlEncoded
    @POST("index.php/billmaster/discountupdateapi")
    Call<DiscountResponse> getDiscount(@Field("bill_id") String billId, @Field("discount") String discount,
                                       @Field("gtotal") String grandTotal, @Field("orderIds") String orderID,
                                       @Field("remark") String remark, @Field("overalldiscount") String overAllDiscount,
                                       @Field("order_type") String order_type,
                                       @Field("cus_exist") String customization_exist,
                                       @Field("overalldiscount_type") String overalldiscount_type,
                                       @Field("selected_db_name") String selected_db_name);

    @FormUrlEncoded
    @POST("index.php/orders/orderconformationviaapi")
    Call<String> validatePassword(@Field("uname") String user_name, @Field("pwd") String password, @Field("selected_db_name") String selected_db_name);

    @GET("index.php/orders/billpreview/{id}")
    Call<ArrayList<OrdersForTable>> getBillPreview(@Path("id") String tableId, @Query("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("index.php/orders/cancelitem")
    Call<CancelItem> cancelItem(@Field("id") String orderId, @Field("update_comments") String comments, @Query("selected_db_name") String p_dbName);

    @GET("index.php/orders/orderspage/{id}")
    Call<GetTotalModel> getBillTotal(@Path("id") String tableId, @Query("selected_db_name") String p_dbName);

    @GET("index.php/orders/getordersfortable/{table_id}")
    Call<ArrayList<OrdersForTable>> getOrdersForTable(@Path("table_id") String tableId, @Query("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("index.php/orders/billcalculation")
    Call<BillCalcModel> billCalculation(@Field("id") String billId, @Field("order_type") String orderType, @Field("selected_db_name") String db_name);

    @GET("index.php/orders/billdiscount/{bill_id}")
    Call<GetDiscount> getBilledItems(@Path("bill_id") String billId, @Query("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("index.php/billmaster/create_mobile")
    Call<List<String>> getBillId(@Field("split") String split, @Field("BillMaster") String bill_master, @Field("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("index.php/billmaster/updatebillapi")
    Call<SuccessMessage> updateBillId(@Field("bill_id") String bill_id, @Field("order_type") String order_type,
                                      @Field("table_id") String table_id, @Field("uname") String u_name, @Field("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("index.php/menuitemsportion/get_menu_item_portion_mapping")
    Call<ArrayList<CustomizedItemsPortion>> getPortion(@Field("menu_item_id") String item_id, @Query("selected_db_name") String p_dbName);

    @GET("index.php/menuitemscustomisation/customizationmapping/{menu_item_id}")
    Call<ArrayList<CustomizationItemsType>> getItemType(@Path("menu_item_id") String menuId, @Query("selected_db_name") String p_dbName);

    @GET("index.php/menuitemstockmapping/getaddonbymenuitem/{menu_item_id}")
    Call<ArrayList<CustomizationItemAddon>> getAddon(@Path("menu_item_id") String menuId, @Query("selected_db_name") String p_dbName);

    @GET("index.php/settings/list")
    Call<ArrayList<SettingsList>> get_details_for_KitchenScreen(@Query("selected_db_name") String p_dbName);

    @GET("index.php/payment/list")
    Call<ArrayList<GetPaymentOptions>> get_paymentOptions(@Query("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("index.php/orders/update_deliver_status")
    Call<String> getDeliveryStatus(@Field("id") String order_id, @Field("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("index.php/feedback/createapi")
    Call<FeedbackSubmitModel> getFeedbackSubmit(@Field("food") String rate_food, @Field("service") String rate_service, @Field("ambience") String rate_ambience,
                                                @Field("overall") String rate_overall, @Field("recommendation") String recommend,
                                                @Field("mrs_ms") String mr_ms, @Field("name") String name, @Field("mobile_no") String mobile, @Field("email_no") String mail,
                                                @Field("date_of_birth") String dob, @Field("anniversary") String anniversary, @Field("remarks") String remarks,
                                                @Field("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("index.php/billmaster/billpayingapi")
    Call<BillPay> payingBill(@Field("bill_id") String bill_id, @Field("split_id") String split_id,
                             @Field("paymentMode") String paymentMode, @Field("payment_mode") String pay_mode,
                             @Field("amountReceived") String amountReceived,
                             @Field("balanceGiven") String balanceGiven, @Field("cardNumber") String cardNumber,
                             @Field("cardType") String cardType, @Field("remark") String remark,
                             @Field("table_id") String table_id,
                             @Field("customernumber") String customer_mobileNum, @Field("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("index.php/billmaster/billpayingapi")
    Call<BillPay> payingBillModified(@Field("store_id") String store_id, @Field("split_id") String split_id,
                                     @Field("bill_id") String bill_id, @Field("round_off") String round_off,
                                     @Field("balanceGiven") String balanceGiven, @Field("partialamount") String partialAmount,
                                     @Field("remark") String remark, @Field("table_id") String table_id,
                                     @Field("tab_id") String tab_id, @Field("customername") String customerName,
                                     @Field("customernumber") String customer_mobileNum, @Field("amountPaid") String amountPaid,
                                     @Field("paymen") String payment_amount, @Field("payment_mode") String pay_mode,
                                     @Field("print_table_for_txt_file") String print_table_for_txt_file, @Field("table_name") String tableName,
                                     @Field("delivery_partner") String delivery_partner, @Field("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("index.php/orders/sendprintrequest")
    Call<String> sendPrintRequestForKOT(@Field("kot") String kot_id, @Field("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("index.php/orders/sendprintrequestbill")
    Call<String> sendPrintRequestForBill(@Field("bill_id") String bill_id, @Field("print_req_bill") String printBillRequest, @Field("selected_db_name") String p_dbName);

    @GET("index.php/orders/getordersforprint")
    Call<ArrayList<GetOrdersForPrint>> getOrdersForPrint(@Query("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("/index.php/tablesmaster/switchtable")
    Call<DiscountResponse> swapTable(@Field("table_id") String tableId, @Field("switch_table_id") String switchTableID,
                                     @Field("customername") String customer_name, @Field("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("/index.php/tablesmaster/switchtable")
    Call<DiscountResponse> swapTable_new(@Field("table_id") String tableId, @Field("switch_table_id") String switchTableID,
                                         @Field("customername") String customer_name, @Field("bill_id") String bill_id, @Field("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("/index.php/waiterSession/createapi")
    Call<LoginDetails> loginWaiterSession(@Field("hotel_code") String hotel_code, @Field("employee_name") String employee_name,
                                          @Field("login_time") String login_time, @Field("refrence_id") String reference_id,
                                          @Field("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("/index.php/waiterSession/update_api")
    Call<String> logoutWaiterSession(@Field("hotel_code") String hotel_code, @Field("employee_name") String employee_name,
                                     @Field("logout_time") String logout_time, @Field("refrence_id") String reference_id,
                                     @Field("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("/index.php/billmaster/order_item_api")
    Call<ArrayList<OrdersForTable>> getOrdersWithBillId(@Field("bill_id") String bill_id, @Query("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("/index.php/orders/delete_all_orders_mobile")
    Call<String> clearAll(@Field("ids") String order_ids, @Field("update_comments") String comment, @Field("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("/index.php/billmaster/bill_status_api")
    Call<ArrayList<GetBillStatus>> getBillStatus(@Field("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("/index.php/menuitems/getdetailsfortable_api")
    Call<ArrayList<DefaultCustomerDetails>> getCustomerDetailsForTableID(@Field("table_id") String table_id, @Field("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("/index.php/menuitems/jointableapi")
    Call<Table> JoinMultipleTables(@Field("pwd") String password, @Field("locked_table_id") String locked_table_id,
                                   @Field("customername") String customer_details, @Field("table_id") String tables_to_join,
                                   @Field("pw_required") String pwd_required, @Field("customernumbr") String customer_number,
                                   @Field("no_of_guests") String no_of_guests, @Field("child") String child,
                                   @Field("room_ref_no") String room_ref_no, @Field("order_type") String order_type,
                                   @Field("user_name") String user_name,
                                   @Field("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("index.php/orders/DiscountConformation")
    Call<String> discount_confirmation(@Field("user_for_discount") String username,
                                       @Field("password_for_discount") String password,
                                       @Query("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("index.php/billmaster/updateorderstatusapi")
    Call<DiscountResponse> itemCancellation(@Field("item_array") String item_array, @Field("bill_id") String bill_id,
                                            @Field("tableid") String tableId,
                                            @Field("update_type") String update_type, @Query("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("index.php/billmaster/updateorderstatusapi")
    Call<SuccessMessage> itemCancellation_modified(@Field("item_array") String item_array, @Field("bill_id") String bill_id,
                                                   @Field("order_type") String order_type, @Field("table_id") String tableId,
                                                   @Field("bill_status") String bill_status, @Field("user_name") String user_name,
                                                   @Query("selected_db_name") String p_dbName);

    @GET("index.php/stocks/_stockslist/")
    Call<ArrayList<StockList>> get_StockList(@Query("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("index.php/billmaster/releasetableandbillid")
    Call<String> call_payLater(@Field("billid") String bill_id, @Field("table_id") String tableId, @Field("check_bill_status") String billStatus, @Field("selected_db_name") String selected_db_name);

    @FormUrlEncoded
    @POST("index.php/wtrlist/employeesalesformobile")
    Call<EmployeeSessionSummaryDetails> getEmployee_wise_summary(@Field("from") String from_date, @Field("to") String to_date, @Field("employee_name") String employee_name, @Field("selected_db_name") String p_dbName);

    /*Employee Reset Session*/
    @FormUrlEncoded
    @POST("index.php/employeemaster/change_login_status_mobile")
    Call<SuccessMessage> ResetLoginSession(@Field("user_id") String userName, @Field("password") String password,
                                           @Field("status") String status, @Field("selected_db_name") String p_dbName);

    /*Modified api for discount*/
    @FormUrlEncoded
    @POST("index.php/billmaster/discountupdateapi")
    Call<DiscountResponse> getDiscount2(@Field("bill_id") String billId, @Field("discount") String discount,
                                        @Field("gtotal") String grandTotal, @Field("orderIds") String orderID, @Field("remark") String remark, @Field("overalldiscount") String overAllDiscount,
                                        @Field("order_type") String order_type,
                                        @Field("cus_exist") String customization_exist,
                                        @Field("overalldiscount_type") String overalldiscount_type,
                                        @Field("order_flag") String flag,
                                        @Field("selected_db_name") String selected_db_name);

    /*Modified api for discount*/
    @FormUrlEncoded
    @POST("index.php/billmaster/discountupdateapi")
    Call<DiscountResponse> updateDiscount(@Field("item_array") String item_array,
                                          @Field("cus_item_array") String cus_item_array,
                                          @Field("bill_id") String bill_id,
                                          @Field("discount_type") String discount_type,
                                          @Field("overall_discount") String overall_discount,
                                          @Field("discount_comment") String discount_comment,
                                          @Field("selected_db_name") String selected_db_name);

    @FormUrlEncoded
    @POST("index.php/employeemaster/discountforemp_mobile")
    Call<DiscountPercentForEmployee> discountPercentApplicableForEmployee(@Field("emp_name") String emp_name, @Field("selected_db_name") String selected_db_name);

    @FormUrlEncoded
    @POST("index.php/settings/salesvalueformobile")
    Call<SalesSummary> getSales_details(@Field("from") String from_date, @Field("to") String to_date, @Field("selected_db_name") String dbName);

    @FormUrlEncoded
    @POST("index.php/orders/getkotwiseordersformobile")
    Call<ArrayList<DeleteOrderPOPUP>> OrdersDeletePOPUPDetails(@Field("menu_item_id") String menu_item_id, @Field("table_id") String table_id,
                                                               @Field("custom_exists") String custom_exists,
                                                               @Field("custom_data") String custom_data, @Query("selected_db_name") String selected_db_name);

    @FormUrlEncoded
    @POST("index.php/orders/cancelitemmobile")
    Call<SuccessMessage> SubmitDeleteOrders(@Field("cancel_comment") String cancel_comment,
                                            @Field("menu_item_id") String menu_item_id,
                                            @Field("cancelled_qty") String cancelled_qty,
                                            @Field("bill_qty") String bill_qty,
                                            @Field("table_id") String table_id,
                                            @Field("order_id") String order_id,
                                            @Field("selected_db_name") String selected_db_name);

    @GET("index.php/orders/ncauthorization")
    Call<ArrayList<AuthorizeEmployee>> get_details_for_authorized_employee(@Query("selected_db_name") String p_dbName);

    @GET("index.php/predefineddiscount/pospredefineddiscount")
    Call<ArrayList<PreDefinedDiscount>> get_pre_defined_discount_values(@Query("selected_db_name") String p_dbName);

    @FormUrlEncoded
    @POST("index.php/billmaster/predefinedDiscountapi")
    Call<SuccessMessage> submitPredefinedDiscount(@Field("bill_id") String bill,
                                                  @Field("promo_comments") String promo_comments,
                                                  @Field("promo_percentage") String promo_percentage,
                                                  @Field("selected_db_name") String selected_db_name);

    @FormUrlEncoded
    @POST("index.php/orders/otpinsertintoemployeelist")
    Call<AuthorizeEmployee> OTP_authorize(@Field("authorizing_employee") String authorizing_employee,
                                          @Field("requesting_employee") String requesting_employee,
                                          @Field("otp") String otp,
                                          @Field("bill_id_for_authorization") String bill_id_for_authorization,
                                          @Field("pax") String pax,
                                          @Field("usage_flag") String usage_flag,
                                          @Field("selected_db_name") String selected_db_name);

    @FormUrlEncoded
    @POST("index.php/orders/nc")
    Call<SuccessMessage> Apply_OTP(@Field("bill_id") String bill_id,
                                   @Field("tableid") String table_id,
                                   @Field("otp") String otp,
                                   @Field("selected_db_name") String selected_db_name);

    @FormUrlEncoded
    @POST("index.php/orders/sms_bill_amount_for_otp_success")
    Call<ArrayList<AuthorizeEmployee>> get_employee_details_for_bill(@Field("bill_id") String bill_id,
                                                                     @Field("selected_db_name") String selected_db_name);

    @FormUrlEncoded
    @POST("index.php/billmaster/getsplitordersformobile")
    Call<ArrayList<OrdersForTable>> get_orders_split(@Field("table_id") String table_id,
                                                     @Field("menu_item_id") String menu_item_id,
                                                     @Field("custom_exists") String custom_exists,
                                                     @Field("custom_data") String custom_data, @Field("nc_order") String nc_order,
                                                     @Field("selected_db_name") String selected_db_name);

    @FormUrlEncoded
    @POST("index.php/orders/cancelitemmobile")
    Call<SuccessMessage> cancelItemMobile(@Field("order_array") String order_array,
                                          @Field("user_name") String user_name,
                                          @Field("selected_db_name") String db_name);

    @FormUrlEncoded
    @POST("index.php/billmaster/getbatchwiseordersapi")
    Call<ArrayList<DiscountOrder>> get_discount_items_split(@Field("bill_id") String bill_id,
                                                            @Field("menu_item_id") String menu_item_id,
                                                            @Field("custom_data") String custom_data,
                                                            @Field("selected_db_name") String selected_db_name);

    @FormUrlEncoded
    @POST("index.php/billmaster/Updatemodifypriceapi")
    Call<SuccessMessage> update_modify_price(@Field("item_array") String item_array,
                                             @Field("bill_id") String bill_id,
                                             @Field("order_type") String order_type,
                                             @Field("selected_db_name") String selected_db_name);

    @FormUrlEncoded
    @POST("index.php/billmaster/updateadditionalchargesapi")
    Call<SuccessMessage> submitAdditionalCharge(@Field("delivery_charges") String delivery_charges,
                                                @Field("packaging_charges") String packaging_charges,
                                                @Field("other_charges") String other_charges,
                                                @Field("selected_db_name") String selected_db_name);

    /*Cancel Bill*/
    @FormUrlEncoded
    @POST("index.php/billmaster/updatecancelbillapi")
    Call<SuccessMessage> cancelBill(@Field("user_name") String userName, @Field("password") String password,
                                    @Field("remarks") String remarks, @Field("bill_id") String bill_id,
                                    @Field("selected_db_name") String p_dbName);

    /*Filter Type List*/
    @GET("index.php/filtermaster/getfiltersapi")
    Call<ArrayList<FilterType>> getFilterTypeList(@Query("selected_db_name") String p_dbName);

    /*For NC order - employee types*/
    @GET("index.php/employeetypes/getemployeetypes")
    Call<ArrayList<EmployeeTypeName>> getEmployeeType(@Query("selected_db_name") String p_dbName);
}
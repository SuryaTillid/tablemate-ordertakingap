package com.cbs.tablemate_version_2.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.UIWidgets.DividerItemDecoration;
import com.cbs.tablemate_version_2.UIWidgets.MyCustomDialog;
import com.cbs.tablemate_version_2.adapter.SessionResetAdapter;
import com.cbs.tablemate_version_2.configuration.App;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.configuration.ConfigurationSettings;
import com.cbs.tablemate_version_2.configuration.RestApiCalls;
import com.cbs.tablemate_version_2.configuration.Settings;
import com.cbs.tablemate_version_2.helper.LoginDB;
import com.cbs.tablemate_version_2.helper.RecyclerItemClickListener;
import com.cbs.tablemate_version_2.interfaces.ChangeColorListener;
import com.cbs.tablemate_version_2.models.Employee_Data;
import com.cbs.tablemate_version_2.models.LoginDetails;
import com.cbs.tablemate_version_2.models.SuccessMessage;
import com.google.gson.Gson;

import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Codebase-4 on 12-04-2016.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener, ChangeColorListener {

    private EditText ed_UserName;
    private EditText ed_Password;
    private Button btn_login;
    private Button btn_UID;
    private Button btn_login_using_UniqueID;
    private Button btn_login_using_UserName;
    private String userName, passWord, uniqueId, employeeId;
    private Context context;
    private String db_name, emp_name;
    private Settings settings;
    private ConfigurationSettings configurationSettings;
    private EditText ed_uniqueId;
    private LoginDB loginDB = new LoginDB(this);
    private SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private long current_date = System.currentTimeMillis();
    private String login_time, referenceId, logout_time;
    private SessionResetAdapter sessionResetAdapter;
    private ArrayList<Employee_Data> employeeDetailsList = new ArrayList<>();
    private RecyclerView rv_employee_details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = LoginActivity.this;
        settings = new Settings(context);
        configurationSettings = new ConfigurationSettings(context);
        db_name = configurationSettings.getDb_Name();
        AppLog.write("DB Name---", db_name);
        login_time = timeFormat.format(current_date);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(configurationSettings.getHOTEL_NAME() + " - Login");

        ed_UserName = findViewById(R.id.ed_userName);
        ed_Password = findViewById(R.id.ed_password);
        ed_uniqueId = findViewById(R.id.ed_uniqueId);
        btn_login = findViewById(R.id.btnLogin);
        btn_UID = findViewById(R.id.btnUID);
        btn_login_using_UniqueID = findViewById(R.id.btnUIDLogin);
        btn_login_using_UserName = findViewById(R.id.btnLoginviaUserName);

        btn_login.setOnClickListener(this);
        btn_UID.setOnClickListener(this);
        btn_login_using_UniqueID.setOnClickListener(this);
        btn_login_using_UserName.setOnClickListener(this);

        new GetEmployeeDetailsList().execute();
    }

    @Override
    public void onClick(View v) {

        if (DetectConnection.isOnline(context)) {
            switch (v.getId()) {
                case R.id.btnLogin:
                    userName = ed_UserName.getText().toString().trim();
                    passWord = ed_Password.getText().toString().trim();
                    if (validate()) {
                        if (userName != null && passWord != null) {
                            new Validate_login().execute(userName, passWord);
                        }
                    }
                    break;
                case R.id.btnUIDLogin:
                    ed_UserName.setVisibility(View.GONE);
                    ed_Password.setVisibility(View.GONE);
                    ed_uniqueId.setVisibility(View.VISIBLE);
                    btn_login_using_UniqueID.setVisibility(View.GONE);
                    btn_login.setVisibility(View.GONE);
                    btn_login_using_UserName.setVisibility(View.VISIBLE);
                    btn_UID.setVisibility(View.VISIBLE);
                    break;
                case R.id.btnUID:
                    uniqueId = ed_uniqueId.getText().toString();
                    if (uniqueID_validate()) {
                        new Validate_uniqueId().execute(uniqueId);
                    }
                    break;
                case R.id.btnLoginviaUserName:
                    ed_UserName.setVisibility(View.VISIBLE);
                    ed_Password.setVisibility(View.VISIBLE);
                    ed_uniqueId.setVisibility(View.GONE);
                    btn_login_using_UserName.setVisibility(View.GONE);
                    btn_UID.setVisibility(View.GONE);
                    btn_login.setVisibility(View.VISIBLE);
                    btn_login_using_UniqueID.setVisibility(View.VISIBLE);
                    break;
            }
        } else {
            show_noInternet();
        }
    }

    private void show_noInternet() {
        Intent i = new Intent(context, Show_NoInternetMessage_Activity.class);
        startActivity(i);
    }

    private boolean uniqueID_validate() {
        uniqueId = ed_uniqueId.getText().toString();
        if (uniqueId.equals("")) {
            ed_uniqueId.setError("Enter Unique Id");
            ed_uniqueId.requestFocus();
            return false;
        }
        return true;
    }

    private boolean validate() {
        userName = ed_UserName.getText().toString().trim();
        passWord = ed_Password.getText().toString().trim();
        if (TextUtils.isEmpty(userName)) {
            ed_UserName.setError("Enter Username");
            ed_UserName.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(passWord)) {
            ed_Password.setError("Enter Password");
            ed_Password.requestFocus();
            return false;
        }
        return true;
    }

    @Override
    public void changeColor(int position) {
        for (Employee_Data employee_data : employeeDetailsList) {
            employee_data.setSelected(false);
        }
        employeeDetailsList.get(position).setSelected(true);
        sessionResetAdapter.MyDataChanged(employeeDetailsList);
    }

    @Override
    public void onBackPressed() {
        confirmExit();
    }

    private void confirmExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Are you sure want to exit?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (DetectConnection.isOnline(context)) {
                    Intent i = new Intent(context, ConfigurationActivity.class);
                    startActivity(i);
                } else {
                    Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                    homeIntent.addCategory(Intent.CATEGORY_HOME);
                    homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(homeIntent);
                }
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_signin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.reset_session) {
            sessionReset();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void sessionReset() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_emp_details_view);
        dialog.setCancelable(false);

        Button btn_cancel = dialog.findViewById(R.id.btnCancel);
        Button btn_submit = dialog.findViewById(R.id.btnSubmit);
        rv_employee_details = dialog.findViewById(R.id.rc_employeeList);

        sessionResetAdapter = new SessionResetAdapter(context, employeeDetailsList, this);
        @SuppressLint("WrongConstant")
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv_employee_details.addItemDecoration(new DividerItemDecoration(context, null));
        rv_employee_details.setLayoutManager(mLayoutManager);
        rv_employee_details.setAdapter(sessionResetAdapter);

        rv_employee_details.addOnItemTouchListener(new RecyclerItemClickListener(context, rv_employee_details,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Employee_Data employeeData = employeeDetailsList.get(position);
                        emp_name = employeeData.getId();
                    }

                    @Override
                    public void onItemLongClick(View view, int position) {
                    }
                }));

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                passwordValidation(emp_name);
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void passwordValidation(final String emp_name) {
        final Dialog d_dialog = new Dialog(context);
        d_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d_dialog.setContentView(R.layout.verify_password_credential);
        final EditText ed_password = d_dialog.findViewById(R.id.ed_password);
        Button btnCancel = d_dialog.findViewById(R.id.btnCancel);
        Button btnSubmit = d_dialog.findViewById(R.id.btnSubmit);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d_dialog.cancel();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String password = ed_password.getText().toString().trim();
                if (validatePassword(ed_password)) {
                    new ResetUserSession().execute(emp_name, password);
                    dismissKeyBoard();
                } else {
                    dismissKeyBoard();
                }
                d_dialog.dismiss();
            }
        });
        d_dialog.show();
    }

    private boolean validatePassword(EditText ed_password) {
        boolean isValid = true;
        String pass_word = ed_password.getText().toString().trim();
        if (TextUtils.isEmpty(pass_word)) {
            ed_password.setError("Please enter the password..!");
            isValid = false;
        } else {
            ed_password.setError(null);
        }
        return isValid;
    }

    private void dismissKeyBoard() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class Validate_login extends AsyncTask<String, String, String> {

        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                final String url = App.IP_ADDRESS + "/index.php/site/loginviaapi";
                AppLog.write("URL:::::::::::=>", url);
                RestTemplate restTemplate = new RestTemplate();
                List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
                messageConverters.add(new FormHttpMessageConverter());
                messageConverters.add(new StringHttpMessageConverter());
                restTemplate.setMessageConverters(messageConverters);
                MultiValueMap<String, String> part = new LinkedMultiValueMap<String, String>();
                part.add("username", userName);
                part.add("password", passWord);
                part.add("selected_db_name", db_name);
                String results = restTemplate.postForObject(url, part, String.class);
                AppLog.write("Request", new Gson().toJson(restTemplate.getRequestFactory()) + "\n" + "Values sent:" + part);
                AppLog.write("Result=========>", results);
                return results;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("Login output-------", s);
            if (!TextUtils.isEmpty(s)) {
                switch (s) {
                    case "2":
                        Toast.makeText(context, "Already Logged..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "3":
                        Toast.makeText(context, "Incorrect Username..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "4":
                        Toast.makeText(context, "Incorrect Password..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "5":
                        Toast.makeText(context, "Incorrect Username and Password..!", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        if (result != "-1") {
                            employeeId = s;
                            settings.setUSER_NAME(userName);
                            configurationSettings.setUSER_NAME(userName);
                            settings.setPASSWORD(passWord);
                            settings.setEMPLOYEE_ID(employeeId);
                            Toast.makeText(context, "Login Succeed...", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(LoginActivity.this, SelectOrderTypeActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(context, "Login Failed...", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
            } else {
                Toast.makeText(context, "Please check your network connection and try again..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class Validate_uniqueId extends AsyncTask<String, String, String> {
        MyCustomDialog dialog;
        String result = "";

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            RestApiCalls call = new RestApiCalls();
            result = call.UID_validation((App) getApplication(), params[0], db_name);
            AppLog.write("Parameters", "" + params[0]);
            AppLog.write("Result----->", result);
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();

            AppLog.write("Result", s);

            if (!TextUtils.isEmpty(s)) {
                switch (s) {
                    case "2":
                        Toast.makeText(context, "Already Logged..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "0":
                        Toast.makeText(context, "Invalid Unique Id..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "1":
                        Toast.makeText(context, "Login Succeed", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(LoginActivity.this, SelectOrderTypeActivity.class);
                        startActivity(intent);
                        break;
                    default:
                        break;
                }
            } else {
                Toast.makeText(context, "Please check your network connection and try again..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class WaiterSessionLogin extends AsyncTask<String, String, LoginDetails> {

        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected LoginDetails doInBackground(String... params) {
            RestApiCalls call = new RestApiCalls();
            LoginDetails result = call.loginWaiterSession((App) getApplication(), db_name, params[0], login_time, params[1], db_name);
            AppLog.write("Result----->", "-" + new Gson().toJson(result));
            return result;
        }

        @Override
        protected void onPostExecute(LoginDetails details) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != details) {
                AppLog.write("Waiter session---", "" + details.getSuccess());
                if (details.getSuccess().equalsIgnoreCase("true") || details.getSuccess().equals("1"))
                    AppLog.write("Saved in DB---", "--------------------------");
                loginDB.saveLoginDetails(new LoginDetails(db_name, userName, passWord, userName, login_time, logout_time, referenceId));
            } else {
                AppLog.write("Waiter Session Empty---", "--------------------------");
            }
            Intent intent = new Intent(LoginActivity.this, SelectOrderTypeActivity.class);
            startActivity(intent);
        }
    }

    private class GetEmployeeDetailsList extends AsyncTask<Void, Void, ArrayList<Employee_Data>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<Employee_Data> doInBackground(Void... params) {
            ArrayList<Employee_Data> employeeList = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            employeeList = call.getEmployeeDetailsList((App) getApplication(), db_name);
            return employeeList;
        }

        @Override
        protected void onPostExecute(ArrayList<Employee_Data> employee_dataArrayList) {

            if (null != dialog && dialog.isShowing()) ;
            dialog.dismiss();
            AppLog.write("TAG---", "500 Error" + "/Empty Values" + new Gson().toJson(employee_dataArrayList));
            try {
                if (null != employee_dataArrayList && employee_dataArrayList.size() > 0) {
                    employeeDetailsList = employee_dataArrayList;
                    settings.setSTORE_ID(employee_dataArrayList.get(0).getStore_id());
                    configurationSettings.setSTORE_NAME(employee_dataArrayList.get(0).getStore_name().getName());
                    AppLog.write("Store---", "" + configurationSettings.getSTORE_NAME() + "---" + settings.getSTORE_ID());
                } else {
                    AppLog.write("TAG---", "500 Error" + "/Empty Values");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class ResetUserSession extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... params) {
            SuccessMessage success = new SuccessMessage();
            RestApiCalls call = new RestApiCalls();
            success = call.ResetLoginSession((App) getApplication(), params[0], params[1], "0", db_name);
            return success;
        }

        @Override
        protected void onPostExecute(SuccessMessage message) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != message) {
                if (message.getSuccess().equals("1")) {
                    Toast.makeText(context, "Password is not matched..!", Toast.LENGTH_SHORT).show();
                } else if (message.getSuccess().equals("0")) {
                    Toast.makeText(context, "Login Status Updated Successfully..!", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(context, LoginActivity.class);
                    startActivity(i);
                }
            } else {
                Toast.makeText(context, " Failed..!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
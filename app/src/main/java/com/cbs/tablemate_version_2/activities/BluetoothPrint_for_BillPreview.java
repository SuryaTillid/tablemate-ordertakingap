package com.cbs.tablemate_version_2.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.UIWidgets.MyCustomDialog;
import com.cbs.tablemate_version_2.configuration.App;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.configuration.RestApiCalls;
import com.cbs.tablemate_version_2.configuration.Settings;
import com.cbs.tablemate_version_2.models.BillCalcModel;
import com.cbs.tablemate_version_2.models.BillPreview;
import com.cbs.tablemate_version_2.models.CustomizationPreview;
import com.cbs.tablemate_version_2.models.TaxSplit;
import com.cbs.tablemate_version_2.utilities.ConnectionException;
import com.cbs.tablemate_version_2.utilities.DateUtil;
import com.cbs.tablemate_version_2.utilities.MyBluetoothConnector;
import com.cbs.tablemate_version_2.utilities.StringUtils;
import com.cbs.tablemate_version_2.utilities.Utility;
import com.google.gson.Gson;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/*********************************************************************
 * Created by Barani on 18-04-2017 in TableMateNew
 ***********************************************************************/
public class BluetoothPrint_for_BillPreview extends Activity {
    private Button mConnectBtn;
    private Button mEnableBtn;
    private Button btn_Print;
    private Spinner spinner_deviceList;
    private ProgressDialog myProgressDialog;
    private ProgressDialog connectivityDialog;
    private Context context;
    private String db_name, bill_Id, table_id, table_name;
    private Settings settings;
    private ArrayList<BillPreview> billItemsList = new ArrayList<>();
    private BluetoothAdapter bluetoothAdapter;
    private MyBluetoothConnector bluetoothConnector;
    private ArrayList<BluetoothDevice> myDeviceList = new ArrayList<>();
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                if (state == BluetoothAdapter.STATE_ON) {
                    showEnabled();
                } else if (state == BluetoothAdapter.STATE_OFF) {
                    showDisabled();
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                myDeviceList = new ArrayList<>();
                updateDeviceList();
                myProgressDialog.show();
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                myProgressDialog.dismiss();
                updateDeviceList();
            } else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                myDeviceList.add(device);
                showToast("Found device..!" + device.getName());
            } else if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);
                if (state == BluetoothDevice.BOND_BONDED) {
                    showToast("Paired..!");
                    connectDevice();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_printer);

        context = BluetoothPrint_for_BillPreview.this;
        settings = new Settings(context);
        db_name = settings.getDbName();
        table_id = settings.getTableID();
        Intent i = getIntent();
        bill_Id = i.getStringExtra("BILL_ID");
        table_name = i.getStringExtra("TABLE_NAME");
        AppLog.write("TableName----", "--" + table_name);

        mConnectBtn = findViewById(R.id.btn_connect);
        mEnableBtn = findViewById(R.id.btn_enable);
        btn_Print = findViewById(R.id.btn_print);
        spinner_deviceList = findViewById(R.id.spinner_deviceList);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        final Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

        //enable bluetooth
        mEnableBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(intent, 1000);
                if (bluetoothAdapter.isEnabled()) {
                    AppLog.write("Bluetooth Enabled", "----");
                    myDeviceList.addAll(pairedDevices);
                    updateDeviceList();
                }
            }
        });

        //connect/disconnect
        mConnectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                connect();
                new BillCalculate().execute();
            }
        });

        //print
        btn_Print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //connect();
            }
        });

        IntentFilter filter = new IntentFilter();

        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);

        registerReceiver(mReceiver, filter);

        if (bluetoothAdapter == null) {
            showDeviceNotSupports();
        } else {
            if (!bluetoothAdapter.isEnabled()) {
                showDisabled();
            } else {
                showEnabled();
                //Set<BluetoothDevice> pairedDevices1 = bluetoothAdapter.getBondedDevices();
                //if (pairedDevices != null)
                //{
                myDeviceList.addAll(pairedDevices);
                updateDeviceList();
                //}
                myProgressDialog = new ProgressDialog(this);
                myProgressDialog.setMessage("Scanning...");
                myProgressDialog.setCancelable(false);
                myProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        bluetoothAdapter.cancelDiscovery();
                    }
                });

                connectivityDialog = new ProgressDialog(this);
                connectivityDialog.setMessage("Connecting...");
                connectivityDialog.setCancelable(false);
                callConnector();
            }
        }
    }

    @Override
    public void onPause() {
        if (bluetoothAdapter != null) {
            if (bluetoothAdapter.isDiscovering()) {
                bluetoothAdapter.cancelDiscovery();
            }
        }

        if (bluetoothConnector != null) {
            try {
                bluetoothConnector.disconnect();
            } catch (ConnectionException e) {
                e.printStackTrace();
            }
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    private void printContent(BillCalcModel billList) {
        String formatFirstLine = "%1$-14s %2$14s";
        String formatSecondLine = "%1$-12s %2$7s %3$3s %4$6s";
        int maxChars = 32;
        String newLine = "\r\n";
        StringBuilder content = new StringBuilder();

        content.append(StringUtils.center("IRCTC Envy", maxChars));
        content.append(StringUtils.center("SPI Diners", maxChars));
        content.append(StringUtils.center("Chennai Central Railway Junction", maxChars));
        content.append(StringUtils.center("Chennai - 600 003.", maxChars));
        content.append(StringUtils.center("GSTIN# - " + billList.getHotelprofile().get(0).getTIN_number(), maxChars));
        content.append(StringUtils.center("--------------------------------", maxChars));
        content.append(String.format(formatFirstLine, "Bill : " + billList.getBill_api().get(0).getBill_id(), "Table : " + table_name) + newLine);
        content.append(StringUtils.center("--------------------------------", maxChars));
        String b_date = billList.getBill().getBill_date();
        String bill_date = DateUtil.formatDateFromString("yyyy-MM-dd HH:mm:ss", "dd-MM-yy HH:mm", b_date);
        content.append(String.format(formatFirstLine, bill_date, "PAX : " + billList.getBill_api().get(0).getOccupancy()) + newLine);
        content.append(StringUtils.center("--------------------------------", maxChars));
        content.append(String.format(formatSecondLine, StringUtils.center("Item", 12), StringUtils.center("Amt", 7), StringUtils.center("Qty", 3), StringUtils.center("Total", 6)));
        content.append(StringUtils.center("--------------------------------", maxChars)).append(newLine);
        for (BillPreview billPreview : billList.getGrouping_bill_preview()) {
            String name = "";
            double price = 0;
            // Double billAmount = (double) billPreview.getOrderQuantity() * billPreview.getPrice();
            ArrayList<CustomizationPreview> customizationPreviewArrayList = new ArrayList<>();
            customizationPreviewArrayList = billList.getCustomization_preview();
            if (isExist(customizationPreviewArrayList, billPreview.getOrder_id())) {
                for (CustomizationPreview cPreview : customizationPreviewArrayList) {
                    if (billPreview.getOrder_id().equals(cPreview.getOrder_id())) {
                        name += TextUtils.isEmpty(name) ? cPreview.getCustomisation_name() : "," + cPreview.getCustomisation_name();
                        price += Utility.convertToDouble(cPreview.getPrice());
                    }
                }
                String itemName = billPreview.getMenu_item_name();
                List<String> itemNames = StringUtils.splitEqually(itemName, 12);
                content.append(String.format(formatSecondLine, itemNames.get(0), billPreview.getMrp_price(), billPreview.getOrderQuantity(), "" + billPreview.getPrice()) + newLine);
                content.append(String.format(formatSecondLine, "C:" + name, price, "", "" + price) + newLine);
                for (int i = 1; i < itemNames.size(); i++) {
                    content.append(String.format(formatSecondLine, itemNames.get(i), "", "", "") + newLine);
                }
            } else {
                String itemName = billPreview.getMenu_item_name();
                List<String> itemNames = StringUtils.splitEqually(itemName, 12);
                content.append(String.format(formatSecondLine, itemNames.get(0), billPreview.getMrp_price(), billPreview.getOrderQuantity(), "" + billPreview.getPrice()) + newLine);
                for (int i = 1; i < itemNames.size(); i++) {
                    content.append(String.format(formatSecondLine, itemNames.get(i), "", "", "") + newLine);
                }
            }
        }
        content.append(StringUtils.center("--------------------------------", maxChars));
        int order_qty = 0;
        for (int i = 0; i < billItemsList.size(); i++) {
            order_qty += Utility.convertToInteger(billItemsList.get(0).getOrderQuantity());
        }
        content.append(String.format(formatFirstLine, "Total Item: " + billList.getTotal_ordered_items(), "Total Qty : " + order_qty) + newLine);
        content.append(StringUtils.center("--------------------------------", maxChars));
        content.append(String.format(formatFirstLine, "Gross Total", "" + billList.getBill_api().get(0).getBillTotal()) + newLine);
        double discount = Utility.convertToDouble(billList.getBill().getBillDiscount());
        if (discount > 0) {
            double subTotal = Utility.convertToDouble(billList.getBill_api().get(0).getBillTotal());
            double billPercent = ((discount) * 100) / subTotal;
            double total = subTotal - discount;
            String d = String.format("%.2f", discount);
            content.append(String.format(formatFirstLine, "Discount", "(" + String.format("%.2f", billPercent) + "%) " + "-" + d)).append(newLine);
            content.append(String.format(formatFirstLine, "Sub Total", "" + String.format("%.2f", total))).append(newLine);
        }
        content.append(StringUtils.center("--------------------------------", maxChars));
        for (TaxSplit taxSplit : billList.getTax_splits()) {
            if (!taxSplit.getTax_sum_api().equalsIgnoreCase("0.00")) {
                content.append(String.format(formatFirstLine, taxSplit.getTax_name(), taxSplit.getTax_sum_api()) + newLine);
            }
        }
        Double grandTotal = Utility.convertToDouble(billList.getBill_api().get(0).getBillGrandTotal());
        content.append(String.format(formatFirstLine, "Grand Total", "" + billList.getBill_api().get(0).getBillGrandTotal()) + newLine);
        long amountPayable = Math.round(grandTotal);
        double roundOff = Utility.convertToDouble("" + amountPayable) - grandTotal;
        String round = String.format("%.2f", roundOff);
        content.append(String.format(formatFirstLine, "Round Off", round) + newLine);
        content.append(StringUtils.center("--------------------------------", maxChars));
        content.append(String.format(formatFirstLine, "Amount Payable", "Rs. " + Utility.convertToDouble("" + amountPayable)) + newLine);
        content.append(StringUtils.center("--------------------------------", maxChars));
        content.append(StringUtils.center("Grand Total is Inclusive of All Taxes", maxChars)).append(newLine);
        String customer_id = billList.getCustomer_details().getId();
        AppLog.write("CUSTOMER_ID", "--" + customer_id + "---" + billList.getBill().getTable_id());
        if (customer_id != null) {
            if (!customer_id.equalsIgnoreCase("0")) {
                content.append(StringUtils.center("--------------------------------", maxChars));
                content.append(String.format(formatFirstLine, "Name", billList.getCustomer_details().getName()) + newLine);
                content.append(String.format(formatFirstLine, "Mobile", billList.getCustomer_details().getPhone()) + newLine);
                if (billList.getBill().getOrder_type().equalsIgnoreCase("delivery")) {
                    content.append(String.format(formatFirstLine, "Address", billList.getCustomer_details().getAddress()) + newLine);
                    content.append(String.format(formatFirstLine, "Delivery Partner", "None") + newLine);
                }
            } else {
                content.append(StringUtils.center("", maxChars));
            }
        } else {
            content.append(StringUtils.center("", maxChars));
        }
        content.append(StringUtils.center("*******************************", maxChars));
        content.append(StringUtils.center("Thank You ! Visit Again", maxChars));
        content.append(StringUtils.center("*******************************", maxChars)).append(newLine);
        AppLog.write("POS_Bill_Payment-1------------->", "-" + content);
        sendData(content);
    }

    private boolean isExist
            (ArrayList<CustomizationPreview> customizationPreviewArrayList, String order_id) {
        for (CustomizationPreview customizationPreview : customizationPreviewArrayList) {
            if (customizationPreview.getOrder_id().equalsIgnoreCase(order_id)) {
                return true;
            }
        }
        return false;
    }

    private void sendData(StringBuilder s) {
        try {
            bluetoothConnector.sendData(s);
            Intent i = new Intent(context, BillPreviewActivity.class);
            i.putExtra("BILL_ID", bill_Id);
            i.putExtra("TABLE_ID", table_id);
            i.putExtra("TABLE_NAME", table_name);
            startActivity(i);
        } catch (ConnectionException e) {
            e.printStackTrace();
        }
    }

    private void connect() {
        if (myDeviceList == null || myDeviceList.size() == 0) {
            AppLog.write("Device List size----", "" + myDeviceList.size());
            return;
        }

        BluetoothDevice device = myDeviceList.get(spinner_deviceList.getSelectedItemPosition());

        if (device.getBondState() == BluetoothDevice.BOND_NONE) {
            try {
                createBond(device);
            } catch (Exception e) {
                showToast("Failed to pair device");
                return;
            }
        }

        try {
            if (!bluetoothConnector.isConnected()) {
                bluetoothConnector.connect(device);
            } else {
                bluetoothConnector.disconnect();
                showDisconnected();
            }
        } catch (ConnectionException e) {
            e.printStackTrace();
        }
    }

    private void showDeviceNotSupports() {
        showToast("Bluetooth is unsupported by this device");
        mConnectBtn.setEnabled(false);
        btn_Print.setEnabled(false);
        spinner_deviceList.setEnabled(false);
    }

    private void callConnector() {
        bluetoothConnector = new MyBluetoothConnector(new MyBluetoothConnector.BluetoothConnectionListener() {

            @Override
            public void onStartConnecting() {
                connectivityDialog.show();
            }

            @Override
            public void onConnectionSuccess() {
                connectivityDialog.dismiss();
                showConnected();
            }

            @Override
            public void onConnectionFailed(String error) {
                Toast.makeText(context, "Error in Connectivity,Try Again..!", Toast.LENGTH_SHORT).show();
                connectivityDialog.dismiss();
            }

            @Override
            public void onConnectionCancelled() {
                Toast.makeText(context, "Error in Connectivity,Try Again..!", Toast.LENGTH_SHORT).show();
                connectivityDialog.dismiss();
            }

            @Override
            public void onDisconnected() {
                showDisconnected();
            }
        });
    }

    private void showConnected() {
        showToast("Connected..!");
        mConnectBtn.setText(R.string.text_disconnect);
        btn_Print.setEnabled(true);
        spinner_deviceList.setEnabled(false);
    }

    private void showDisconnected() {
        showToast("Disconnected..!");
        mConnectBtn.setText(R.string.text_connect);
        btn_Print.setEnabled(true);
        spinner_deviceList.setEnabled(true);
    }

    private void showDisabled() {
        showToast("Bluetooth disabled..!");
        mEnableBtn.setVisibility(View.VISIBLE);
        mConnectBtn.setVisibility(View.GONE);
        spinner_deviceList.setVisibility(View.GONE);
    }

    private void showToast(String s) {
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
    }

    private void showEnabled() {
        showToast("Bluetooth enabled..!");
        mEnableBtn.setVisibility(View.GONE);
        mConnectBtn.setVisibility(View.VISIBLE);
        spinner_deviceList.setVisibility(View.VISIBLE);
    }

    private void updateDeviceList() {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.simple_spinner_item, getArray(myDeviceList));
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner_deviceList.setAdapter(adapter);
        spinner_deviceList.setSelection(0);
    }

    private String[] getArray(ArrayList<BluetoothDevice> myDeviceList) {
        String[] list = new String[0];
        if (myDeviceList == null)
            return list;
        int size = myDeviceList.size();
        AppLog.write("Device List size----", "-" + myDeviceList.size());
        list = new String[size];

        for (int i = 0; i < size; i++) {
            list[i] = myDeviceList.get(i).getName();
        }

        return list;
    }

    private void connectDevice() {
        if (myDeviceList == null || myDeviceList.size() == 0) {
            AppLog.write("Device List size----", "" + myDeviceList.size());
            return;
        } else if (myDeviceList.size() == 1) {
            BluetoothDevice device = myDeviceList.get(0);
            try {
                bluetoothConnector.connect(device);
            } catch (ConnectionException e) {
                e.printStackTrace();
            }
        } else {
            BluetoothDevice device = myDeviceList.get(spinner_deviceList.getSelectedItemPosition());
            try {
                if (!bluetoothConnector.isConnected()) {
                    bluetoothConnector.connect(device);
                } else {
                    bluetoothConnector.disconnect();
                    showDisconnected();
                }
            } catch (ConnectionException e) {
                e.printStackTrace();
            }

            if (device.getBondState() == BluetoothDevice.BOND_NONE) {
                try {
                    createBond(device);
                } catch (Exception e) {
                    showToast("Failed to pair device..!");
                }
            }
        }
    }

    private void createBond(BluetoothDevice device) throws Exception {
        try {
            Class<?> cl = Class.forName("android.bluetooth.BluetoothDevice");
            Class<?>[] par = {};
            Method method = cl.getMethod("createBond", par);
            method.invoke(device);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public void onBackPressed() {
      /*  Intent i = new Intent(context,BillPreviewActivity.class);
        i.putExtra("BILL_ID",bill_Id);
        i.putExtra("TABLE_ID",table_id);
        startActivity(i);*/
        finish();
    }

    private class BillCalculate extends AsyncTask<String, String, BillCalcModel> {
        MyCustomDialog my_dialog;

        @Override
        protected void onPreExecute() {
            my_dialog = new MyCustomDialog(context, "Loading...");
            my_dialog.show();
        }

        @Override
        protected BillCalcModel doInBackground(String... params) {
            RestApiCalls call = new RestApiCalls();
            AppLog.write("Bill ID-------------------------", bill_Id);
            AppLog.write("Order Type---", settings.getORDER_TYPE());
            BillCalcModel billCalcModel = call.billCalculation((App) getApplication(), bill_Id, settings.getORDER_TYPE(), db_name);
            AppLog.write("Result------------=>", "" + "\n" + new Gson().toJson(billCalcModel));
            return billCalcModel;
        }

        @Override
        protected void onPostExecute(BillCalcModel billCalcModel) {
            if (null != my_dialog && my_dialog.isShowing())
                my_dialog.dismiss();
            AppLog.write("Result------------=1...>", "" + "\n" + new Gson().toJson(billCalcModel));
            try {
                billItemsList = billCalcModel.getGrouping_bill_preview();
                if (null != billItemsList && billItemsList.size() > 0) {
                    printContent(billCalcModel);
                    table_id = billCalcModel.getBill().getTable_id();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
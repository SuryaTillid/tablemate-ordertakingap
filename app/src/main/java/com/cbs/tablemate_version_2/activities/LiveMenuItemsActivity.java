package com.cbs.tablemate_version_2.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.UIWidgets.DividerItemDecoration;
import com.cbs.tablemate_version_2.UIWidgets.MyCustomDialog;
import com.cbs.tablemate_version_2.adapter.LiveMenuAdapter;
import com.cbs.tablemate_version_2.configuration.App;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.configuration.ConfigurationSettings;
import com.cbs.tablemate_version_2.configuration.RestApiCalls;
import com.cbs.tablemate_version_2.configuration.Settings;
import com.cbs.tablemate_version_2.models.StockItem;
import com.google.gson.Gson;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 01-11-2016 in TableMateNew
 ***********************************************************************/
public class LiveMenuItemsActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
    private RecyclerView liveMenuListView;
    private LinearLayoutManager mLayoutManager;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ArrayList<StockItem> liveMenuList = new ArrayList<>();
    private LiveMenuAdapter liveMenuAdapter;
    private Settings settings;
    private ConfigurationSettings configurationSettings;
    private Context context;
    private String db_name;
    private SearchView searchView;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_menu);

        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(false);*/

        liveMenuListView = findViewById(R.id.rc_LiveMenuList);
        mSwipeRefreshLayout = findViewById(R.id.swipe_container);
        searchView = findViewById(R.id.search);
        searchView.setOnQueryTextListener(this);

        context = LiveMenuItemsActivity.this;
        settings = new Settings(context);
        configurationSettings = new ConfigurationSettings(context);
        db_name = configurationSettings.getDb_Name();

        liveMenuAdapter = new LiveMenuAdapter(context, liveMenuList);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        liveMenuListView.setLayoutManager(mLayoutManager);
        liveMenuListView.setAdapter(liveMenuAdapter);
        liveMenuListView.addItemDecoration(new DividerItemDecoration(context, null));

        new getLiveMenuItems().execute();

        /*
         * * When refresh items using P to R
         * */
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                refresh();
            }
        });
    }

    private void refresh() {
        new getLiveMenuItems().execute();
        stop();
    }

    private void stop() {
        //stop P to R
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final ArrayList<StockItem> filteredModelList = filter(liveMenuList, newText);

        liveMenuAdapter.setFilter(filteredModelList);
        return true;
    }

    private ArrayList<StockItem> filter(ArrayList<StockItem> liveMenuList, String query) {
        query = query.toLowerCase();
        final ArrayList<StockItem> stockList = new ArrayList<>();
        for (StockItem model : liveMenuList) {
            final String text = model.getStockName().toLowerCase();
            if (text.contains(query)) {
                stockList.add(model);
            }
        }
        return stockList;
    }

    private class getLiveMenuItems extends AsyncTask<String, String, ArrayList<StockItem>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<StockItem> doInBackground(String... params) {
            ArrayList<StockItem> bills = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            bills = call.getStockList((App) getApplication(), db_name);
            AppLog.write("Result----------=>", "" + new Gson().toJson(bills));
            return bills;
        }

        @Override
        protected void onPostExecute(ArrayList<StockItem> stockList) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != stockList) {
                liveMenuList = stockList;
                if (liveMenuList.size() != 0) {
                    AppLog.write("Stock quantity----", "" + stockList.get(0).getStockQuantity());
                    liveMenuAdapter.MyDataChanged(liveMenuList);
                } else if (liveMenuList.size() == 0) {
                    Toast.makeText(context, "Nothing", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

}

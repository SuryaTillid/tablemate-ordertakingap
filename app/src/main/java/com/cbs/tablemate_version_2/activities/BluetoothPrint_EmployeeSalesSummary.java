package com.cbs.tablemate_version_2.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.UIWidgets.MyCustomDialog;
import com.cbs.tablemate_version_2.configuration.App;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.configuration.ConfigurationSettings;
import com.cbs.tablemate_version_2.configuration.RestApiCalls;
import com.cbs.tablemate_version_2.models.Emp_SalesSummary;
import com.cbs.tablemate_version_2.models.Emp_SettlementSummary;
import com.cbs.tablemate_version_2.models.EmployeeSessionSummaryDetails;
import com.cbs.tablemate_version_2.utilities.ConnectionException;
import com.cbs.tablemate_version_2.utilities.DateUtil;
import com.cbs.tablemate_version_2.utilities.MyBluetoothConnector;
import com.cbs.tablemate_version_2.utilities.StringUtils;
import com.cbs.tablemate_version_2.utilities.Utility;
import com.google.gson.Gson;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Set;

/********************************************************************
 * Created by Barani on 10-07-2017 in TableMateNew
 ********************************************************************/
public class BluetoothPrint_EmployeeSalesSummary extends Activity {
    private Button mConnectBtn;
    private Button mEnableBtn;
    private Button btn_Print;
    private Spinner spinner_deviceList;
    private ProgressDialog myProgressDialog;
    private ProgressDialog connectivityDialog;
    private Context context;
    private String db_name, userName;
    private String date;
    private ConfigurationSettings settings;
    private BluetoothAdapter bluetoothAdapter;
    private MyBluetoothConnector bluetoothConnector;
    private ArrayList<BluetoothDevice> myDeviceList = new ArrayList<>();
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                if (state == BluetoothAdapter.STATE_ON) {
                    showEnabled();
                } else if (state == BluetoothAdapter.STATE_OFF) {
                    showDisabled();
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                myDeviceList = new ArrayList<>();
                updateDeviceList();
                myProgressDialog.show();
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                myProgressDialog.dismiss();
                updateDeviceList();
            } else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                myDeviceList.add(device);
                showToast("Found device..!" + device.getName());
            } else if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);
                if (state == BluetoothDevice.BOND_BONDED) {
                    showToast("Paired..!");
                    connectDevice();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_printer);

        context = BluetoothPrint_EmployeeSalesSummary.this;
        settings = new ConfigurationSettings(context);

        db_name = settings.getDb_Name();
        userName = settings.getUSER_NAME();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd");
        date = date_format.format(calendar.getTime());

        mConnectBtn = findViewById(R.id.btn_connect);
        mEnableBtn = findViewById(R.id.btn_enable);
        btn_Print = findViewById(R.id.btn_print);
        spinner_deviceList = findViewById(R.id.spinner_deviceList);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        final Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

        //enable bluetooth
        mEnableBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(intent, 1000);
                if (bluetoothAdapter.isEnabled()) {
                    AppLog.write("Bluetooth Enabled", "----");
                    myDeviceList.addAll(pairedDevices);
                    updateDeviceList();
                }
            }
        });

        //connect/disconnect
        mConnectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                connect();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                new EmployeeSalesSummaryList().execute();

            }
        });

        //print
        btn_Print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        IntentFilter filter = new IntentFilter();

        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);

        registerReceiver(mReceiver, filter);

        if (bluetoothAdapter == null) {
            showDeviceNotSupports();
        } else {
            if (!bluetoothAdapter.isEnabled()) {
                showDisabled();
            } else {
                showEnabled();
                myDeviceList.addAll(pairedDevices);
                updateDeviceList();
                myProgressDialog = new ProgressDialog(context);
                myProgressDialog.setMessage("Scanning...");
                myProgressDialog.setCancelable(false);
                myProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        bluetoothAdapter.cancelDiscovery();
                    }
                });

                connectivityDialog = new ProgressDialog(context);
                connectivityDialog.setMessage("Connecting...");
                connectivityDialog.setCancelable(false);
                callConnector();
            }
        }
    }

    @Override
    public void onPause() {
        if (bluetoothAdapter != null) {
            if (bluetoothAdapter.isDiscovering()) {
                bluetoothAdapter.cancelDiscovery();
            }
        }

        if (bluetoothConnector != null) {
            try {
                bluetoothConnector.disconnect();
            } catch (ConnectionException e) {
                e.printStackTrace();
            }
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    private void printContent(EmployeeSessionSummaryDetails employeeSessionSummaryDetails) {

        String formatFirstLine = "%1$-20s %2$10s";
        int maxChars = 32;
        String newLine = "\r\n";
        StringBuilder content = new StringBuilder();

        content.append(StringUtils.center("Employee Sales Summary", maxChars)).append(newLine).append(newLine);
        content.append(StringUtils.center("Employee Name : " + userName, maxChars)).append(newLine);
        content.append(StringUtils.center("Date : " + DateUtil.format(System.currentTimeMillis()), maxChars)).append(newLine);
        content.append(StringUtils.center("----------------------------------------------", maxChars)).append(newLine);
        content.append(StringUtils.center("Sales Summary", maxChars)).append(newLine);
        content.append(StringUtils.center("----------------------------------------------", maxChars)).append(newLine);
        content.append(String.format(formatFirstLine, "Sales(W Tax)", formatValue(employeeSessionSummaryDetails.getBilldetail().get(0).getTotal()))).append(newLine);
        content.append(String.format(formatFirstLine, "Total Tax", formatValue("" + employeeSessionSummaryDetails.getBilldetail().get(0).getBilltax()))).append(newLine);
        content.append(String.format(formatFirstLine, "Discounts", formatValue(employeeSessionSummaryDetails.getBilldetail().get(0).getDiscount()))).append(newLine);
        content.append(String.format(formatFirstLine, "Bills#", employeeSessionSummaryDetails.getBilldetail().get(0).getNofbill())).append(newLine);
        content.append(StringUtils.center("----------------------------------------------", maxChars)).append(newLine);
        content.append(StringUtils.center("Sales Type", maxChars)).append(newLine);
        content.append(StringUtils.center("----------------------------------------------", maxChars)).append(newLine);
        content.append(String.format(formatFirstLine, StringUtils.center("Type", 14), StringUtils.center("Amount", 12))).append(newLine);
        content.append(StringUtils.center("----------------------------------------------", maxChars)).append(newLine);
        for (Emp_SalesSummary emp_salesSummary : employeeSessionSummaryDetails.getDinein()) {
            content.append(String.format(formatFirstLine, emp_salesSummary.getOrder_type(), formatValue(emp_salesSummary.getTotal()))).append(newLine);
        }
        content.append(StringUtils.center("----------------------------------------------", maxChars)).append(newLine);
        content.append(String.format(formatFirstLine, "Pending Bills#", employeeSessionSummaryDetails.getPendingval().get(0).getNofbill())).append(newLine);
        content.append(String.format(formatFirstLine, "Pending Bills Value", formatValue(employeeSessionSummaryDetails.getPendingval().get(0).getTotal()))).append(newLine);
        content.append(String.format(formatFirstLine, "Hold Bills#", employeeSessionSummaryDetails.getHoldval().get(0).getNofbill())).append(newLine);
        content.append(String.format(formatFirstLine, "Hold Bills Value", formatValue(employeeSessionSummaryDetails.getHoldval().get(0).getTotal()))).append(newLine);
        content.append(String.format(formatFirstLine, "Cancelled Bills#", employeeSessionSummaryDetails.getHoldval().get(0).getNofbill())).append(newLine);
        content.append(String.format(formatFirstLine, "Cancelled Bills Value", formatValue(employeeSessionSummaryDetails.getCancelled().get(0).getTotal()))).append(newLine);
        content.append(StringUtils.center("----------------------------------------------", maxChars)).append(newLine);
        content.append(StringUtils.center("Guest", maxChars)).append(newLine);
        content.append(StringUtils.center("----------------------------------------------", maxChars)).append(newLine);
        content.append(String.format(formatFirstLine, "Guests#", employeeSessionSummaryDetails.getGuestsummary().get(0).getOccupancy())).append(newLine);
        content.append(String.format(formatFirstLine, "Average per Guest", formatValue(employeeSessionSummaryDetails.getGuestsummary().get(0).getAveragetotal()))).append(newLine);
        content.append(StringUtils.center("----------------------------------------------", maxChars)).append(newLine);
        content.append(StringUtils.center("Bills", maxChars)).append(newLine);
        content.append(StringUtils.center("----------------------------------------------", maxChars)).append(newLine);
        content.append(String.format(formatFirstLine, "Bills#", employeeSessionSummaryDetails.getBillsummary().get(0).getNofbill())).append(newLine);
        content.append(String.format(formatFirstLine, "Average per Bill", formatValue(employeeSessionSummaryDetails.getBillsummary().get(0).getTable_result()))).append(newLine);
        content.append(StringUtils.center("----------------------------------------------", maxChars)).append(newLine);
        content.append(StringUtils.center("Settlement Summary", maxChars)).append(newLine);
        content.append(StringUtils.center("----------------------------------------------", maxChars)).append(newLine);
        content.append(String.format(formatFirstLine, StringUtils.center("Payment Mode", 14), StringUtils.center("Amount", 12))).append(newLine);
        content.append(StringUtils.center("----------------------------------------------", maxChars)).append(newLine);
        for (Emp_SettlementSummary emp_settlementSummary : employeeSessionSummaryDetails.getListpayment()) {
            String total = formatValue(emp_settlementSummary.getTotal());
            content.append(String.format(formatFirstLine, emp_settlementSummary.getPaymentMode(), total)).append(newLine);
        }
        content.append(StringUtils.center("**********************************************", maxChars)).append(newLine);
        AppLog.write("Value------------->", "-" + content);
        sendData(content);
    }

    @SuppressLint("DefaultLocale")
    private String formatValue(String total) {
        double roundOff = Utility.convertToDouble(total);
        total = String.format("%.2f", roundOff);
        AppLog.write("Value-----***", "---" + total);
        return total;
    }

    private void sendData(StringBuilder s) {
        try {
            bluetoothConnector.sendData(s);
            Intent i = new Intent(context, LoginActivity.class);
            startActivity(i);
        } catch (ConnectionException e) {
            e.printStackTrace();
        }
    }

    private void connect() {
        if (myDeviceList == null || myDeviceList.size() == 0) {
            AppLog.write("Device List size----", "" + myDeviceList.size());
            return;
        }

        BluetoothDevice device = myDeviceList.get(spinner_deviceList.getSelectedItemPosition());

        if (device.getBondState() == BluetoothDevice.BOND_NONE) {
            try {
                createBond(device);
            } catch (Exception e) {
                showToast("Failed to pair device");
                return;
            }
        }

        try {
            if (!bluetoothConnector.isConnected()) {
                bluetoothConnector.connect(device);
            } else {
                bluetoothConnector.disconnect();
                showDisconnected();
            }
        } catch (ConnectionException e) {
            e.printStackTrace();
        }
    }

    private void showDeviceNotSupports() {
        showToast("Bluetooth is unsupported by this device");
        mConnectBtn.setEnabled(false);
        btn_Print.setEnabled(false);
        spinner_deviceList.setEnabled(false);
    }

    private void callConnector() {
        bluetoothConnector = new MyBluetoothConnector(new MyBluetoothConnector.BluetoothConnectionListener() {
            @Override
            public void onStartConnecting() {
                connectivityDialog.show();
            }

            @Override
            public void onConnectionSuccess() {
                connectivityDialog.dismiss();
                showConnected();
            }

            @Override
            public void onConnectionFailed(String error) {
                connectivityDialog.dismiss();
            }

            @Override
            public void onConnectionCancelled() {
                connectivityDialog.dismiss();
            }

            @Override
            public void onDisconnected() {
                showDisconnected();
            }
        });
    }

    private void showConnected() {
        showToast("Connected..!");
        mConnectBtn.setText(R.string.text_connect);
        btn_Print.setEnabled(true);
        spinner_deviceList.setEnabled(false);
    }

    private void showDisconnected() {
        showToast("Disconnected..!");
        mConnectBtn.setText(R.string.text_disconnect);
        btn_Print.setEnabled(true);
        spinner_deviceList.setEnabled(true);
    }

    private void showDisabled() {
        showToast("Bluetooth disabled..!");
        mEnableBtn.setVisibility(View.VISIBLE);
        mConnectBtn.setVisibility(View.GONE);
        spinner_deviceList.setVisibility(View.GONE);
    }

    private void showToast(String s) {
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
    }

    private void showEnabled() {
        showToast("Bluetooth enabled..!");
        mEnableBtn.setVisibility(View.GONE);
        mConnectBtn.setVisibility(View.VISIBLE);
        spinner_deviceList.setVisibility(View.VISIBLE);
    }

    private void updateDeviceList() {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.simple_spinner_item, getArray(myDeviceList));
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner_deviceList.setAdapter(adapter);
        spinner_deviceList.setSelection(0);
    }

    private String[] getArray(ArrayList<BluetoothDevice> myDeviceList) {
        String[] list = new String[0];

        if (myDeviceList == null)
            return list;

        int size = myDeviceList.size();
        AppLog.write("Device List size----", "-" + myDeviceList.size());
        list = new String[size];

        for (int i = 0; i < size; i++) {
            list[i] = myDeviceList.get(i).getName();
        }

        return list;
    }

    private void connectDevice() {
        if (myDeviceList == null || myDeviceList.size() == 0) {
            AppLog.write("Device List size----", "--" + myDeviceList.size());
            return;
        } else if (myDeviceList.size() == 1) {
            BluetoothDevice device = myDeviceList.get(0);
            try {
                bluetoothConnector.connect(device);
            } catch (ConnectionException e) {
                e.printStackTrace();
            }
        } else {
            BluetoothDevice device = myDeviceList.get(spinner_deviceList.getSelectedItemPosition());
            try {
                if (!bluetoothConnector.isConnected()) {
                    bluetoothConnector.connect(device);
                } else {
                    bluetoothConnector.disconnect();
                    showDisconnected();
                }
            } catch (ConnectionException e) {
                e.printStackTrace();
            }

            if (device.getBondState() == BluetoothDevice.BOND_NONE) {
                try {
                    createBond(device);
                } catch (Exception e) {
                    showToast("Failed to pair device..!");
                }
            }
        }
    }

    private void createBond(BluetoothDevice device) throws Exception {
        try {
            Class<?> cl = Class.forName("android.bluetooth.BluetoothDevice");
            Class<?>[] par = {};

            Method method = cl.getMethod("createBond", par);
            AppLog.write("PAR---", "-" + par);

            method.invoke(device);

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(context, LoginActivity.class);
        startActivity(i);
    }

    private class EmployeeSalesSummaryList extends AsyncTask<String, String, EmployeeSessionSummaryDetails> {

        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected EmployeeSessionSummaryDetails doInBackground(String... params) {
            RestApiCalls restApiCalls = new RestApiCalls();
            return restApiCalls.Employee_sales_summary((App) getApplication(), date, date, userName, db_name);
        }

        @Override
        protected void onPostExecute(EmployeeSessionSummaryDetails employeeSessionSummaryDetails) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();

            try {
                if (null != employeeSessionSummaryDetails) {
                    AppLog.write("Sales summary---", "----" + new Gson().toJson(employeeSessionSummaryDetails));
                    printContent(employeeSessionSummaryDetails);
                } else {
                    Toast.makeText(context, "Failed to get values", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
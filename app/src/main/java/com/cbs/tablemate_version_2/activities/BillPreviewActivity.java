package com.cbs.tablemate_version_2.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.UIWidgets.MyCustomDialog;
import com.cbs.tablemate_version_2.adapter.BillCalculationAdapter;
import com.cbs.tablemate_version_2.adapter.CancelMenuItemAdapter;
import com.cbs.tablemate_version_2.adapter.TaxListAdapter;
import com.cbs.tablemate_version_2.configuration.App;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.configuration.ConfigurationSettings;
import com.cbs.tablemate_version_2.configuration.CounterSaleSettings;
import com.cbs.tablemate_version_2.configuration.RestApiCalls;
import com.cbs.tablemate_version_2.configuration.Settings;
import com.cbs.tablemate_version_2.interfaces.CancelItem_after_BillingListener;
import com.cbs.tablemate_version_2.models.AuthorizeEmployee;
import com.cbs.tablemate_version_2.models.BillCalcModel;
import com.cbs.tablemate_version_2.models.BillPreview;
import com.cbs.tablemate_version_2.models.CancelItemArray;
import com.cbs.tablemate_version_2.models.CustomizationPreview;
import com.cbs.tablemate_version_2.models.DiscountResponse;
import com.cbs.tablemate_version_2.models.GetDiscount;
import com.cbs.tablemate_version_2.models.POS_Bill;
import com.cbs.tablemate_version_2.models.PreDefinedDiscount;
import com.cbs.tablemate_version_2.models.SuccessMessage;
import com.cbs.tablemate_version_2.models.TaxSplit;
import com.cbs.tablemate_version_2.utilities.DateUtil;
import com.cbs.tablemate_version_2.utilities.Utility;
import com.google.gson.Gson;

import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/*********************************************************************
 * Created by Barani on 19-05-2016 in TableMateNew
 ***********************************************************************/
public class BillPreviewActivity extends AppCompatActivity implements View.OnClickListener, CancelItem_after_BillingListener {
    SimpleDateFormat timeFormat = new SimpleDateFormat("MMddHHmmss");
    private RecyclerView rc_billedItems, rc_cancelItems, rc_tax_list;
    private BillCalculationAdapter adapter;
    private CancelMenuItemAdapter cancelMenuItemAdapter;
    private Context context;
    private String db_name;
    private Settings settings;
    private ConfigurationSettings configurationSettings;
    private CounterSaleSettings counterSettings;
    private String bill_Id;
    private String remarks;
    private String bill_status;
    private String display_bill_id;
    private String tableId, tableName;
    private String userName;
    private String password;
    private String grandTotal, subTotal;
    private String amountPayable;
    private Button btnPay, btnDiscount, btnPrint;
    private String c_date, c_time, tax;
    private Calendar myCalendar = Calendar.getInstance();
    private long current_date = System.currentTimeMillis();
    private LinearLayoutManager layoutManager;
    private LinearLayout layout_discount;
    private TextView txtDiscountedAmt, txt_totQty, txt_totItem_Qty, txtAddon, txtSubTotal, txtGrossTotal, txtGrandTotal, txtDate, txtTableId;
    private TextView txt_round_off, txt_amountPayable, txtBillId, txtHotelAddress, txtPAXCount;
    private TextView txtCustomerName, txtC_Mobile, txtC_address, txtDeliveryPartner;
    private TextView txt_delivery_charge, txt_pack_charge, txt_other_charge;
    private LinearLayout layout_name, layout_mobile, layout_address, layout_deliveryPartner, layout_subTotal, layout_add_charge;
    private EditText et_password, et_remarks;
    private String s_mobile, s_guest, s_pax, s_authorize_emp, s_req_emp;
    private ArrayList<String> employeeArrayList = new ArrayList<>();
    private ArrayList<AuthorizeEmployee> auth_emp_List = new ArrayList<>();
    private ArrayList<BillPreview> billItemsList = new ArrayList<>();
    //private ArrayList<BillPreview> billItemsList_forAdapter = new ArrayList<>();
    private double billDiscountAmount = 0;
    private boolean isDiscount = true;
    private String no_of_qty, no_of_guests, discount = "0.00";
    private String otp_stamp, primary_num, secondary_num;
    private Dialog m_dialog;
    private TaxListAdapter taxListAdapter;
    private ArrayList<TaxSplit> taxSplits = new ArrayList<>();
    private ArrayList<BillPreview> deletedList = new ArrayList<>();
    private ArrayList<CustomizationPreview> customization_valuesList = new ArrayList<>();
    private ArrayList<PreDefinedDiscount> discount_value_list = new ArrayList<>();
    private POS_Bill bill_list = new POS_Bill();
    private String promo_comment, promo_percent;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_redesign);
        context = BillPreviewActivity.this;
        initializeValues();
        /*Get Details from Shared Preferences*/
        settings = new Settings(context);
        configurationSettings = new ConfigurationSettings(context);
        counterSettings = new CounterSaleSettings(context);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(settings.getHOTEL_NAME() + " - Bill");

        db_name = configurationSettings.getDb_Name();
        userName = settings.getUSER_NAME();
        password = settings.getPASSWORD();

        Intent i = getIntent();
        bill_Id = i.getStringExtra("BILL_ID");
        tableId = i.getStringExtra("TABLE_ID");
        tableName = i.getStringExtra("TABLE_NAME");
        AppLog.write("B_Details", "--" + tableId + "\n" + tableName);

        /* Get Date and Time*/

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        c_date = df.format(myCalendar.getTime());

        txtDate.setText(c_date);
        txtTableId.setText(tableName);

        AppLog.write("POS_Bill_ID----@@@@@@@@@@@@@", "---" + bill_Id);
        refreshBill();
        new GetAuthorizeEmployeeInfo().execute();
        new GetDetails_for_OTP().execute(bill_Id);

        isDiscount = false;

        adapter = new BillCalculationAdapter(context, customization_valuesList, billItemsList);
        layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rc_billedItems.setLayoutManager(layoutManager);
        rc_billedItems.setAdapter(adapter);

        taxListAdapter = new TaxListAdapter(context, taxSplits);
        layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rc_tax_list.setLayoutManager(layoutManager);
        rc_tax_list.setAdapter(taxListAdapter);
    }

    private void refreshBill() {
        new Bill_Calculation().execute(bill_Id);
    }

    private void initializeValues() {
        rc_billedItems = findViewById(R.id.ListBills);
        rc_tax_list = findViewById(R.id.rc_tax_layout);
        txtHotelAddress = findViewById(R.id.txtHotelAddr);
        txtBillId = findViewById(R.id.txtBillId);
        txtTableId = findViewById(R.id.txtTableId);
        txtDate = findViewById(R.id.txtdateTime);
        txt_totQty = findViewById(R.id.txt_totQty);
        txt_totItem_Qty = findViewById(R.id.txt_totQty_item);
        txtAddon = findViewById(R.id.txtAddon);
        txtGrossTotal = findViewById(R.id.txtGrossTotal);
        txtSubTotal = findViewById(R.id.txtSubTotal);
        txtGrandTotal = findViewById(R.id.txtGrandTotal);
        txtDiscountedAmt = findViewById(R.id.txtDiscountedAmt);
        txtPAXCount = findViewById(R.id.txt_pax_count);
        txt_round_off = findViewById(R.id.txtRoundOff);
        txt_amountPayable = findViewById(R.id.txtAmountPayable);
        txtCustomerName = findViewById(R.id.txtCustomerName);
        txtC_Mobile = findViewById(R.id.txtCustomerMobile);
        txtC_address = findViewById(R.id.txtCustomerAddress);
        txtDeliveryPartner = findViewById(R.id.txtDeliveryPartnerName);
        txt_delivery_charge = findViewById(R.id.txtDeliveryCharge);
        txt_pack_charge = findViewById(R.id.txtPackageCharge);
        txt_other_charge = findViewById(R.id.txtOtherCharge);
        layout_discount = findViewById(R.id.discountLayout);
        layout_subTotal = findViewById(R.id.subTotalLayout);
        layout_address = findViewById(R.id.layout_address);
        layout_deliveryPartner = findViewById(R.id.layout_deliveryPartner);
        layout_mobile = findViewById(R.id.layout_mobile);
        layout_name = findViewById(R.id.layout_cName);
        layout_add_charge = findViewById(R.id.additional_charge_layout);
        btnPay = findViewById(R.id.btnPay);
        btnDiscount = findViewById(R.id.btnDiscount);
        btnPrint = findViewById(R.id.btnPrint);
        Button btn_pay_later = findViewById(R.id.btn_pay_later);
        btnPay.setOnClickListener(this);
        btnDiscount.setOnClickListener(this);

        btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (counterSettings.isConfigured()) {
                    AppLog.write("It_is_counter_sale_type_bill", "---");
                    moveToPrintPage();
                } else {
                    AppLog.write("It_is_normal_sale_type_bill", "---");
                    moveToPrintPage();
                }
            }
        });
        btn_pay_later.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnPay:
                new OrderConform().execute(userName, password);
                AppLog.write("Username===>", userName + "\nPassword===>" + password);
                settings.clearDiscountAmount(bill_Id);
                btnPay.setEnabled(false);
                break;
            case R.id.btn_pay_later:
                if (bill_status.equalsIgnoreCase("3")) {
                    Toast.makeText(context, "Cant proceed pay later option for this bill..!", Toast.LENGTH_SHORT).show();
                } else {
                    savePendingBill();
                }
                break;
            case R.id.btnDiscount:
                moreOptions();
                //moveToPrintPage();
                break;
        }
    }

    private void navigate_paymentPage() {
        grandTotal = txtGrandTotal.getText().toString();
        amountPayable = txt_amountPayable.getText().toString();
        checkCounterSettings(amountPayable);
    }

    private void checkCounterSettings(String amountPayable) {
        if (counterSettings.isConfigured()) {
            Intent i = new Intent(BillPreviewActivity.this, PaymentRedesignedActivity.class);
            i.putExtra("BILL_ID", bill_Id);
            i.putExtra("DISPLAY_BILL", display_bill_id);
            i.putExtra("BILL_AMOUNT", amountPayable);
            i.putExtra("TABLE_ID", tableId);
            i.putExtra("TABLE_NAME", tableName);
            startActivity(i);
        } else {
            Intent i = new Intent(BillPreviewActivity.this, PaymentActivityModified.class);
            i.putExtra("BILL_ID", bill_Id);
            i.putExtra("DISPLAY_BILL", display_bill_id);
            i.putExtra("BILL_AMOUNT", amountPayable);
            i.putExtra("TABLE_ID", tableId);
            i.putExtra("TABLE_NAME", tableName);
            startActivity(i);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (isDiscount) {
            AppLog.write("Value---isDiscount", "--" + isDiscount);
            //grandTotalCalculation();
        }
    }

    private void moreOptions() {
        Intent intent = new Intent(context, DiscountActivity.class);
        intent.putExtra("BILL_ID", bill_Id);
        String billList = new Gson().toJson(billItemsList);
        String c_list = new Gson().toJson(customization_valuesList);
        AppLog.write("Bill values list----", billList);
        intent.putExtra("TABLE_ID", tableId);
        intent.putExtra("TABLE_NAME", tableName);
        intent.putExtra("BILL_VALUES", billList);
        intent.putExtra("CUSTOMIZE_VALUES", c_list);
        intent.putExtra("TAX", tax);
        intent.putExtra("SUB_TOTAL", subTotal);
        intent.putExtra("TOTAL", grandTotal);
        intent.putExtra("TOTAL_QUANTITY", no_of_qty);
        startActivity(intent);
    }

    private void moveToPrintPage() {
        AppLog.write("Called--", "bluetooth print");
        Intent intent = new Intent(context, BluetoothPrint_for_BillPreview.class);
        intent.putExtra("BILL_ID", bill_Id);
        String billList = new Gson().toJson(billItemsList);
        String c_list = new Gson().toJson(customization_valuesList);
        AppLog.write("Bill_values_list----", billList);
        intent.putExtra("TABLE_ID", tableId);
        intent.putExtra("TABLE_NAME", tableName);
        intent.putExtra("BILL_VALUES", billList);
        intent.putExtra("CUSTOMIZE_VALUES", c_list);
        intent.putExtra("TAX", tax);
        intent.putExtra("SUB_TOTAL", subTotal);
        intent.putExtra("TOTAL", grandTotal);
        intent.putExtra("TOTAL_QUANTITY", no_of_qty);
        startActivity(intent);
    }

    @Override
    public void cancelItem_fromListListener(int position, String comment) {
        // this.orderId = billItemsList.get(position).getOrder_id();
        BillPreview billItem = billItemsList.remove(position);
        billItem.setComments(comment);
        deletedList.add(billItem);
        //cancelMenuItemAdapter.MyDataChanged(billItemsList);
        cancelMenuItemAdapter.notifyItemRemoved(position);
    }

    @Override
    public void deleteItem_fromListListener(int position, boolean isChecked, String comment, String other_reason) {
    }

    private void setAdditionalChargeVisibleStatus() {
        if (settings.getORDER_TYPE().equalsIgnoreCase("2") || settings.getORDER_TYPE().equalsIgnoreCase("3")) {
            layout_add_charge.setVisibility(View.VISIBLE);
            txt_delivery_charge.setText(bill_list.getDelivery_charges());
            txt_pack_charge.setText(bill_list.getPackaging_charges());
            txt_other_charge.setText(bill_list.getOther_charges());
        } else {
            layout_add_charge.setVisibility(View.GONE);
        }
    }

    private void displayCustomerDetails(BillCalcModel billCalcModel) {
        if (!settings.getCUSTOMER_ID(tableId).equalsIgnoreCase("0")) {
            layout_name.setVisibility(View.VISIBLE);
            layout_mobile.setVisibility(View.VISIBLE);
            txtCustomerName.setText(billCalcModel.getCustomer_details().getName());
            txtC_Mobile.setText(billCalcModel.getCustomer_details().getPhone());
            if (billCalcModel.getBill().getOrder_type().equalsIgnoreCase("delivery")) {
                layout_address.setVisibility(View.VISIBLE);
                layout_deliveryPartner.setVisibility(View.VISIBLE);
                txtDeliveryPartner.setText(billCalcModel.getBill().getDelivery_partner_name());
                txtC_address.setText(billCalcModel.getCustomer_details().getAddress());
            }
        } else {
            layout_name.setVisibility(View.GONE);
            layout_mobile.setVisibility(View.GONE);
            layout_address.setVisibility(View.GONE);
            layout_deliveryPartner.setVisibility(View.GONE);
        }
    }

    public void grandTotalCalculation(String g_Total, POS_Bill pos_bill) {
        try {
            String billDiscount = pos_bill.getBillDiscount();
            AppLog.write("Discounted_Amount-----", "--" + billDiscount);
            billDiscountAmount = Utility.convertToDouble(billDiscount);
            if (billDiscountAmount > 0) {
                layout_discount.setVisibility(View.VISIBLE);
                layout_subTotal.setVisibility(View.VISIBLE);
                double total = Utility.convertToDouble(subTotal) - billDiscountAmount;
                double billPercent = ((billDiscountAmount) * 100) / Utility.convertToDouble(subTotal);
                txtDiscountedAmt.setText("(" + String.format("%.2f", billPercent) + "%) " + "-" +
                        String.format("%.2f", billDiscountAmount));
                txtSubTotal.setText(String.format("%.2f", total));
            } else {
                layout_discount.setVisibility(View.GONE);
                layout_subTotal.setVisibility(View.GONE);
            }
            discount = String.format("%.2f", billDiscountAmount);
            AppLog.write("Discounted_Amount-----", "--" + billDiscountAmount + "--" + discount);
            if (settings.getORDER_TYPE().equalsIgnoreCase("2") || settings.getORDER_TYPE().equalsIgnoreCase("3")) {
                double total = Utility.convertToDouble(g_Total) + Utility.convertToDouble(pos_bill.getDelivery_charges()) +
                        Utility.convertToDouble(pos_bill.getPackaging_charges()) + Utility.convertToDouble(pos_bill.getOther_charges());
                g_Total = String.valueOf(total);
            }
            txtGrandTotal.setText(g_Total);
            double amount_Payable = Math.round(Utility.convertToDouble(g_Total));
            double roundOff = Utility.convertToDouble("" + amount_Payable) - Utility.convertToDouble(g_Total);
            String round = String.format("%.2f", roundOff);
            txt_round_off.setText(round);
            txt_amountPayable.setText(String.format("%.2f", amount_Payable));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
    }

    private void callPredefinedDiscount() {
        new GetPredefinedDiscountValues().execute();
    }

    private void callModify() {
        Intent i = new Intent(context, ModifyItemPriceActivity.class);
        i.putExtra("BILL_ID", bill_Id);
        i.putExtra("TABLE_NAME", tableName);
        i.putExtra("TABLE_ID", tableId);
        startActivity(i);
    }

    private void showDialog_for_generate_otp() {
        m_dialog = new Dialog(context);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        m_dialog.setContentView(R.layout.activity_generate_otp);

        final Spinner spinner_table_list = m_dialog.findViewById(R.id.sp_authorize_employee);
        Button btnCancel = m_dialog.findViewById(R.id.btnCancel);
        Button btnGenerate = m_dialog.findViewById(R.id.btnGenerate);
        final EditText edit_guest_name = m_dialog.findViewById(R.id.edit_guest);
        final EditText edit_requesting_emp = m_dialog.findViewById(R.id.edit_req_employee);
        final EditText edit_pax = m_dialog.findViewById(R.id.edit_pax);
        final EditText edit_bill_id = m_dialog.findViewById(R.id.edit_bill_id);

        edit_bill_id.setText(bill_Id);
        edit_pax.setText(no_of_guests);

        ArrayAdapter<String> ad = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, employeeArrayList);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_table_list.setAdapter(ad);
        spinner_table_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AuthorizeEmployee emp = auth_emp_List.get(position);
                s_authorize_emp = emp.getName();
                primary_num = emp.getPrimary_ph_no();
                secondary_num = emp.getSec_ph_no();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
            }
        });

        btnGenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otp_stamp = timeFormat.format(current_date);
                if (validateGenerateOTPDetails(edit_guest_name, edit_pax, edit_requesting_emp)) {
                    s_mobile = validate_mob_num(primary_num, secondary_num);
                    s_guest = edit_guest_name.getText().toString().trim();
                    s_pax = edit_pax.getText().toString().trim();
                    s_req_emp = edit_requesting_emp.getText().toString().trim();
                    String s = "Guest : " + s_guest + "\n" + "PAX : " + s_pax + "\n" + "Requested by : " + s_req_emp + "\n" + "OTP : " + otp_stamp;
                    new SendOTP_via_SMS().execute(s, s_mobile);
                    new OTP_Authorize().execute(s_authorize_emp, s_req_emp, otp_stamp, s_pax);
                }
            }
        });
        m_dialog.show();
    }

    private boolean validateGenerateOTPDetails(EditText edit_guest_name, EditText edit_pax,
                                               EditText edit_requesting_emp) {
        String guest = edit_guest_name.getText().toString().trim();
        String pax = edit_pax.getText().toString().trim();
        String req_emp = edit_requesting_emp.getText().toString().trim();
        boolean isValid = true;

        if (TextUtils.isEmpty(guest)) {
            edit_guest_name.setError("Please Enter Guest Name..!");
            isValid = false;
        } else {
            edit_guest_name.setError(null);
        }

        if (TextUtils.isEmpty(pax)) {
            edit_pax.setError("Please Enter PAX Count..!");
            isValid = false;
        } else {
            edit_pax.setError(null);
        }

        if (TextUtils.isEmpty(req_emp)) {
            edit_requesting_emp.setError("Please Enter Employee Name");
            isValid = false;
        } else {
            edit_requesting_emp.setError(null);
        }
        return isValid;
    }

    private void showDialog_for_apply_otp() {
        m_dialog = new Dialog(context);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        m_dialog.setContentView(R.layout.activity_apply_otp);

        Button btnCancel = m_dialog.findViewById(R.id.btnCancel);
        Button btnApply = m_dialog.findViewById(R.id.btnApply);
        final EditText edit_OTP = m_dialog.findViewById(R.id.edit_otp);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
            }
        });

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateOTP(edit_OTP)) {
                    String otp = edit_OTP.getText().toString().trim();
                    new ApplyOTP().execute(otp);
                    m_dialog.dismiss();
                }
            }
        });
        m_dialog.show();
    }

    private boolean validateOTP(EditText edit_OTP) {
        String otp = edit_OTP.getText().toString().trim();
        boolean isValid;
        if (TextUtils.isEmpty(otp)) {
            edit_OTP.setError("Please Enter OTP");
            isValid = false;
        } else {
            edit_OTP.setError(null);
            return true;
        }
        return isValid;
    }

    /*
     *  Delete menu items from list
     * */
    private void deleteMenuItem() {
        new GetBilledItemDetails().execute(bill_Id);
    }

    @SuppressLint("WrongConstant")
    private void cancelMenuItemDialog(GetDiscount bills) {
        m_dialog = new Dialog(context);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        m_dialog.setContentView(R.layout.activity_billed_menu_status);
        m_dialog.setCancelable(false);

        rc_cancelItems = m_dialog.findViewById(R.id.rvMenuStatus);
        Button btn_ok = m_dialog.findViewById(R.id.btn_ok);
        Button btn_cancel = m_dialog.findViewById(R.id.btn_cancel);
        TextView txtCurrentTableName = m_dialog.findViewById(R.id.txtCurrentTableName);
        txtCurrentTableName.setText(tableName);
        AppLog.write("BILL_ID--", "--" + bill_Id);
        if (null != bills.getOrdereditems()) {
            //cancelMenuItemAdapter = new CancelMenuItemAdapter(context, bills.getOrdereditems());
            cancelMenuItemAdapter.setCancelItemListener(this);
            layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            rc_cancelItems.setLayoutManager(layoutManager);
            rc_cancelItems.setAdapter(cancelMenuItemAdapter);
        }

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
                cancelBilledMenu(deletedList);
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
            }
        });
        m_dialog.show();
    }

    private ArrayList<CancelItemArray> cancelBilledMenu(ArrayList<BillPreview> deletedList) {
        ArrayList<CancelItemArray> list = new ArrayList<>();
        for (BillPreview billPreview : deletedList) {
            CancelItemArray cancel_array = new CancelItemArray();
            cancel_array.setOrder_id(billPreview.getOrder_id());
            cancel_array.setComments(billPreview.getComments());
            list.add(cancel_array);
        }
        String cancel = new Gson().toJson(list);
        AppLog.write("Cancel Array List---", "---" + cancel);
        new CancelMenuItem_after_bill().execute(cancel);
        return list;
    }

    private void savePendingBill() {
        AppLog.write("----Method---", "--");
        new PayLater().execute();
    }

    private void moveToTablesActivity() {
        Intent view_progress = new Intent(context, SelectOrderTypeActivity.class);
        startActivity(view_progress);
    }

    private void getInfo() {
        new Bill_Calculation().execute(bill_Id);
        callSMS(auth_emp_List);
    }

    private void callSMS(ArrayList<AuthorizeEmployee> auth_emp_List) {
        String sms_content = "NC has been applied for Bill ID -  " + bill_Id + " for Rs. " + discount;
        String sms_mobile = validate_mob_num(auth_emp_List.get(0).getPrimary_ph_no(), auth_emp_List.get(0).getSec_ph_no());
        new SendOTP_via_SMS().execute(sms_content, sms_mobile);
    }

    private String validate_mob_num(String primary_ph_no, String sec_ph_no) {
        String mobile;
        if (!TextUtils.isEmpty(sec_ph_no)) {
            mobile = primary_ph_no + "," + sec_ph_no;
        } else {
            mobile = primary_ph_no;
        }
        return mobile;
    }

    private void showDialogPredefinedDiscount() {
        m_dialog = new Dialog(context);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        m_dialog.setContentView(R.layout.activity_pre_defined_discount);

        final Spinner spinner_predefined_discount = m_dialog.findViewById(R.id.spinnerPredefinedDiscount);
        Button btn_submit = m_dialog.findViewById(R.id.btnSubmit);
        Button btn_cancel = m_dialog.findViewById(R.id.btnCancel);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item, getValues_PD(discount_value_list));
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_predefined_discount.setAdapter(dataAdapter);

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                m_dialog.dismiss();
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SubmitPredefinedDiscountValues().execute();
            }
        });

        // Spinner click listener
        spinner_predefined_discount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String s = parent.getItemAtPosition(position).toString();
                promo_comment = discount_value_list.get(position).getDescription();
                promo_percent = discount_value_list.get(position).getPrecentage();
                AppLog.write("PROMO_VALUES", "" + s + "--" + promo_percent + "--" + promo_comment);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        m_dialog.show();
    }

    private List<String> getValues_PD(ArrayList<PreDefinedDiscount> pre_discount) {
        List<String> list = new ArrayList<>();
        for (PreDefinedDiscount p : pre_discount) {
            list.add(p.getName());
        }
        return list;
    }

    private void callAdditionalCharges() {
        m_dialog = new Dialog(context);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        m_dialog.setContentView(R.layout.activity_additional_charge);

        Button btn_submit = m_dialog.findViewById(R.id.btnSubmit);
        Button btn_cancel = m_dialog.findViewById(R.id.btnCancel);
        final EditText ed_delivery_charge = m_dialog.findViewById(R.id.ed_delivery_charge);
        final EditText ed_package_charge = m_dialog.findViewById(R.id.ed_packaging_charge);
        final EditText ed_other_charge = m_dialog.findViewById(R.id.ed_other_charge);

        ed_delivery_charge.setText(bill_list.getDelivery_charges());
        ed_package_charge.setText(bill_list.getPackaging_charges());
        ed_other_charge.setText(bill_list.getOther_charges());

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                m_dialog.dismiss();
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String delivery_charge = ed_delivery_charge.getText().toString().trim();
                String package_charge = ed_package_charge.getText().toString().trim();
                String other_charge = ed_other_charge.getText().toString().trim();
                new SubmitAdditionalChargeValues().execute(delivery_charge, package_charge, other_charge);
            }
        });
        m_dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_tables_list, menu);
        MenuItem item_additional_charge = menu.findItem(R.id.action_additional_charge);
        if (settings.getORDER_TYPE().equalsIgnoreCase("1") ||
                settings.getORDER_TYPE().equalsIgnoreCase("4")) {
            item_additional_charge.setVisible(false);
        } else {
            item_additional_charge.setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_view_tables:
                if (counterSettings.isConfigured()) {
                    Intent i = new Intent(context, SelectOrderTypeActivity.class);
                    startActivity(i);
                } else {
                    moveToTablesActivity();
                }
                return true;
            case R.id.action_cancel_menu_item:
                //deleteMenuItem();
                delete();
                return true;
            case R.id.action_apply_otp:
                showDialog_for_apply_otp();
                return true;
            case R.id.action_generate_otp:
                showDialog_for_generate_otp();
                return true;
            case R.id.action_modify:
                callModify();
                return true;
            case R.id.action_prdefined_discount:
                callPredefinedDiscount();
                return true;
            case R.id.action_additional_charge:
                callAdditionalCharges();
                return true;
            case R.id.action_void_bill:
                callVoidBillDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void callVoidBillDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.pos_activity_bill_cancel_dialog);
        dialog.setCancelable(false);

        AppLog.write("BILL_ID--", "--" + bill_Id);

        Button btn_cancel = dialog.findViewById(R.id.btn_Cancel);
        Button btn_submit = dialog.findViewById(R.id.btn_Submit);
        et_password = dialog.findViewById(R.id.et_password);
        et_remarks = dialog.findViewById(R.id.et_remarks);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateCancel()) {
                    new BillCancellation().execute(userName, password, remarks, bill_Id);
                    dialog.dismiss();
                }
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private boolean validateCancel() {
        remarks = et_remarks.getText().toString().trim();
        password = et_password.getText().toString().trim();
        if (remarks.equals("")) {
            et_remarks.setError("Enter Comment..!");
            et_remarks.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(password)) {
            et_password.setError("Enter Password..!");
            et_password.requestFocus();
            return false;
        }
        return true;
    }

    private void delete() {
        Intent i = new Intent(context, DeleteItemAfterBillActivity.class);
        i.putExtra("BILL_ID", bill_Id);
        i.putExtra("TABLE_NAME", tableName);
        i.putExtra("TABLE_ID", tableId);
        i.putExtra("BILL_STATUS", bill_status);
        startActivity(i);
    }

    private class Bill_Calculation extends AsyncTask<String, String, BillCalcModel> {
        MyCustomDialog my_dialog;

        @Override
        protected void onPreExecute() {
            my_dialog = new MyCustomDialog(context, "Loading...");
            my_dialog.show();
        }

        @Override
        protected BillCalcModel doInBackground(String... params) {
            BillCalcModel billCalcModel = new BillCalcModel();
            RestApiCalls call = new RestApiCalls();
            AppLog.write("Bill_ID------------", bill_Id);
            AppLog.write("Order_Type---------", settings.getORDER_TYPE());
            billCalcModel = call.billCalculation((App) getApplication(), bill_Id, settings.getORDER_TYPE(), db_name);
            return billCalcModel;
        }

        @Override
        protected void onPostExecute(BillCalcModel billCalcModel) {
            if (null != my_dialog && my_dialog.isShowing())
                my_dialog.dismiss();
            AppLog.write("Result-------@", "--" + new Gson().toJson(billCalcModel));
            try {
                billItemsList = billCalcModel.getGrouping_bill_preview();
                bill_list = billCalcModel.getBill();
                setAdditionalChargeVisibleStatus();
                customization_valuesList = billCalcModel.getCustomization_preview();
                AppLog.write("Result----", new Gson().toJson(billItemsList) + "--" +
                        new Gson().toJson(customization_valuesList));
                if (null != billItemsList) {
                    bill_status = billCalcModel.getBill().getBill_status();
                    AppLog.write("BILL_STATUS", "--" + bill_status);
                    adapter.MyDataChanged(customization_valuesList, billItemsList);
                    String add_on = billCalcModel.getTakeaway_Addon().getAddon_price();
                    String date = billCalcModel.getBill().getDate();
                    String date_after = DateUtil.formatDateFromString("yyyy-MM-dd HH:mm:ss", "dd-MM-yy HH:mm", date);
                    AppLog.write("Date_after_correction--", "--" + date_after);
                    txtDate.setText(date_after);

                    taxSplits.clear();
                    for (TaxSplit tax : billCalcModel.getTax_splits()) {
                        if (!tax.getTax_sum_api().equalsIgnoreCase("0.00")) {
                            AppLog.write("TaxValue--", "--" + tax.getTax_sum_api());
                            taxSplits.add(tax);
                        }
                    }

                    taxListAdapter.MyDataChanged(taxSplits);
                    AppLog.write("Size of Tax splits--------", "" + billCalcModel.getTax_splits().size() + "\n" + add_on);
                    if (!TextUtils.isEmpty(add_on)) {
                        txtAddon.setText(billCalcModel.getTakeaway_Addon().getAddon_price());
                    } else {
                        AppLog.write("--Addon--", "----");
                    }
                    AppLog.write("Display_bill--", "---" + billCalcModel.getBill_api().get(0).getBill_id());
                    display_bill_id = billCalcModel.getBill_api().get(0).getBill_id();
                    txtBillId.setText(display_bill_id);
                    no_of_guests = billCalcModel.getBill_api().get(0).getOccupancy();
                    txtPAXCount.setText(no_of_guests);
                    AppLog.write("Occupancy----", "--" + billCalcModel.getBill_api().get(0).getOccupancy() + "----" + billCalcModel.getTax_splits().size());
                    int order_qty = 0;
                    for (int i = 0; i < billItemsList.size(); i++) {
                        order_qty += Utility.convertToInteger(billItemsList.get(0).getOrderQuantity());
                    }
                    txt_totQty.setText("" + order_qty);
                    //new Gson().toJson(billCalcModel.getTotal_ordered_qty())
                    txt_totItem_Qty.setText(new Gson().toJson(billCalcModel.getTotal_ordered_items()));
                    no_of_qty = new Gson().toJson(billCalcModel.getTotal_ordered_qty());
                    txtGrossTotal.setText(billCalcModel.getBill_api().get(0).getBillTotal());
                    subTotal = txtGrossTotal.getText().toString();
                    grandTotal = billCalcModel.getBill_api().get(0).getBillGrandTotal();
                    isDiscount = true;
                    AppLog.write("Total Amount-----", "----" + grandTotal);
                    grandTotalCalculation(grandTotal, billCalcModel.getBill());
                    displayCustomerDetails(billCalcModel);
                }
            } catch (Exception e) {
                AppLog.write("Exception---", e.getMessage());
                e.printStackTrace();
            }
        }
    }

    private class OrderConform extends AsyncTask<String, String, String> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Processing..");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            RestApiCalls calls = new RestApiCalls();
            AppLog.write("Validation Result--->", "---" + params[0] + params[1] + db_name);
            return calls.validatePassword((App) getApplication(), params[0], params[1], db_name);
        }

        @Override
        protected void onPostExecute(String s) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            btnPay.setEnabled(true);
            if (s.equals("1")) {
                navigate_paymentPage();
            }
           /* else if(bill_status.equalsIgnoreCase("pending"))
            {
                navigate_paymentPage();
            }*/
        }
    }

    private class SendPrintRequestForBillPreview extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            RestApiCalls calls = new RestApiCalls();
            AppLog.write("Bill request--bill paying---->", "" + params[0] + "\n" + params[1]);
            return calls.sendPrintRequest_to_ServerForBill((App) getApplication(), params[0], params[1], db_name);
        }

        @Override
        protected void onPostExecute(String s) {
            AppLog.write("Bill preview result--->", s);
        }
    }

    private class CancelMenuItem_after_bill extends AsyncTask<String, String, DiscountResponse> {
        String tableID = settings.getTableID();
        MyCustomDialog dialog = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected DiscountResponse doInBackground(String... params) {
            RestApiCalls restApiCalls = new RestApiCalls();
            AppLog.write("Values for cancellation---", "--" + bill_Id + "\n" + tableID);
            return restApiCalls.CancelMenuItem_in_bill((App) getApplication(), params[0], bill_Id, tableID, "cancel", db_name);
        }

        @Override
        protected void onPostExecute(DiscountResponse discountResponse) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            new Bill_Calculation().execute(bill_Id);
        }
    }

    private class GetBilledItemDetails extends AsyncTask<String, String, GetDiscount> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Processing..");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected GetDiscount doInBackground(String... params) {
            RestApiCalls call = new RestApiCalls();
            AppLog.write("POS_Bill_ID-------=>", bill_Id);
            GetDiscount bills = call.getMenuItemsAfterBillGeneration((App) getApplication(), bill_Id, db_name);
            AppLog.write("Result------------=>", "--" + new Gson().toJson(bills));
            return bills;
        }

        @Override
        protected void onPostExecute(GetDiscount bills) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            try {
                billItemsList = bills.getOrdereditems();
                if (null != bills && !billItemsList.isEmpty()) {
                    AppLog.write("Bills--@@@@@@", "--" + new Gson().toJson(bills));
                    cancelMenuItemDialog(bills);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //pay later
    private class PayLater extends AsyncTask<String, String, String> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            RestApiCalls call = new RestApiCalls();
            return call.payLater((App) getApplication(), bill_Id, tableId, "1", db_name);
        }

        @Override
        protected void onPostExecute(String s) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (!TextUtils.isEmpty(s)) {
                AppLog.write("---Result----", "-----" + s);
                if (s.equals("1")) {
                    Toast.makeText(context, "Bill proceed to pay later..!", Toast.LENGTH_SHORT).show();
                    if (counterSettings.isConfigured()) {
                        Intent i = new Intent(context, SelectOrderTypeActivity.class);
                        startActivity(i);
                    } else {
                        moveToTablesActivity();
                    }
                }
            } else {
                AppLog.write("---Result----", "-----**" + s);
            }
        }
    }

    private class ApplyOTP extends AsyncTask<String, String, SuccessMessage> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... params) {
            RestApiCalls call = new RestApiCalls();
            return call.Apply_OTP_Value((App) getApplication(), bill_Id, tableId, params[0], db_name);
        }

        @Override
        protected void onPostExecute(SuccessMessage message) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (message != null) {
                if (message.getSuccess().equalsIgnoreCase("1")) {
                    Toast.makeText(context, "Succeed..!", Toast.LENGTH_SHORT).show();
                    getInfo();
                }
            } else {
                Toast.makeText(context, "Invalid OTP", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class GetDetails_for_OTP extends AsyncTask<String, Void, ArrayList<AuthorizeEmployee>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading Menu...");
            dialog.show();
        }

        @Override
        protected ArrayList<AuthorizeEmployee> doInBackground(String... params) {
            ArrayList<AuthorizeEmployee> result = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            result = call.GetAuthorizeEmployeeDetails_For_Bill((App) getApplication(), params[0], db_name);
            AppLog.write("Emp_TAG", "--" + new Gson().toJson(result));
            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<AuthorizeEmployee> employeeList) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("Auth_Emp_TAG", "--" + new Gson().toJson(employeeList));
            if (null != employeeList && employeeList.size() > 0) {
                //auth_emp_list = employeeList;
            } else {
                AppLog.write("Auth_Emp_TAG", "--" + new Gson().toJson(employeeList));
            }
        }
    }

    private class SendOTP_via_SMS extends AsyncTask<String, String, String> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading Menu...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                final String url = App.IP_ADDRESS + "/index.php/settings/send_sms_mobile";
                AppLog.write("URL:::::::::::=>", url);
                RestTemplate restTemplate = new RestTemplate();
                List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
                messageConverters.add(new FormHttpMessageConverter());
                messageConverters.add(new StringHttpMessageConverter());
                restTemplate.setMessageConverters(messageConverters);
                MultiValueMap<String, String> part = new LinkedMultiValueMap<String, String>();
                part.add("sms_values", params[0]);
                part.add("mobile_number", params[1]);
                part.add("selected_db_name", db_name);
                String results = restTemplate.postForObject(url, part, String.class);
                AppLog.write("Request", new Gson().toJson(restTemplate.getRequestFactory()) + "\n" + "Values sent:" + part);
                AppLog.write("Result=========>", results);
                return results;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (!TextUtils.isEmpty(s)) {
                m_dialog.dismiss();
                switch (s) {
                    case "1701":
                        Toast.makeText(context, "Message Sent Successfully..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "1702":
                        Toast.makeText(context, "Already Logged..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "1703":
                        Toast.makeText(context, "Invalid value in username or password field..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "1704":
                        Toast.makeText(context, "Invalid value in 'type' field..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "1705":
                        Toast.makeText(context, "Invalid Message..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "1706":
                        Toast.makeText(context, "Invalid Mobile Number..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "1707":
                        Toast.makeText(context, "Invalid Source(Sender)..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "1708":
                        Toast.makeText(context, "Invalid value for 'dlr' field..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "1709":
                        Toast.makeText(context, "User validation failed..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "1710":
                        Toast.makeText(context, "Internal Error..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "1025":
                        Toast.makeText(context, "Insufficient Credit..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "1715":
                        Toast.makeText(context, "Response Timeout..!", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        Toast.makeText(context, "Please check your network connection and try again..!", Toast.LENGTH_SHORT).show();
                        break;
                }
            } else {
                Toast.makeText(context, "Please check your network connection and try again..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class GetAuthorizeEmployeeInfo extends AsyncTask<String, Void, ArrayList<AuthorizeEmployee>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading Menu...");
            dialog.show();
        }

        @Override
        protected ArrayList<AuthorizeEmployee> doInBackground(String... params) {
            ArrayList<AuthorizeEmployee> result = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            result = call.GetAuthorizeEmployeeDetails((App) getApplication(), db_name);
            AppLog.write("Emp_TAG_1", "--" + new Gson().toJson(result));
            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<AuthorizeEmployee> employeeList) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            employeeArrayList.clear();
            for (AuthorizeEmployee a : employeeList) {
                employeeArrayList.add(a.getName());
                auth_emp_List.add(a);
            }
            AppLog.write("Emp_TAG", "--" + new Gson().toJson(employeeArrayList));
        }
    }

    private class OTP_Authorize extends AsyncTask<String, Void, AuthorizeEmployee> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading Menu...");
            dialog.show();
        }

        @Override
        protected AuthorizeEmployee doInBackground(String... params) {
            AuthorizeEmployee result = new AuthorizeEmployee();
            RestApiCalls call = new RestApiCalls();
            result = call.OTP_Authorize_submit((App) getApplication(), params[0], params[1], params[2], bill_Id, params[3],
                    "0", db_name);
            AppLog.write("OTP_Submit", "--" + new Gson().toJson(result));
            return result;
        }

        @Override
        protected void onPostExecute(AuthorizeEmployee result) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (result != null) {
                m_dialog.dismiss();
                if (result.getStatus().equalsIgnoreCase("1")) {
                    AppLog.write("TAG", "OTP Value Successfully Saved");
                }
            }
        }
    }

    private class GetPredefinedDiscountValues extends AsyncTask<String, Void, ArrayList<PreDefinedDiscount>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading Menu...");
            dialog.show();
        }

        @Override
        protected ArrayList<PreDefinedDiscount> doInBackground(String... params) {
            RestApiCalls call = new RestApiCalls();
            return call.GetPredefinedDiscount_values((App) getApplication(), db_name);
        }

        @Override
        protected void onPostExecute(ArrayList<PreDefinedDiscount> discount) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            discount_value_list.clear();
            discount_value_list.addAll(discount);
            showDialogPredefinedDiscount();
        }
    }

    private class SubmitPredefinedDiscountValues extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog my_dialog;

        @Override
        protected void onPreExecute() {
            my_dialog = new MyCustomDialog(context, "Loading...");
            my_dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... s) {
            RestApiCalls call = new RestApiCalls();
            return call.Submit_pre_defined_discount((App) getApplication(), bill_Id, promo_comment, promo_percent, db_name);
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != my_dialog && my_dialog.isShowing())
                my_dialog.dismiss();
            AppLog.write("Success--", new Gson().toJson(success));
            if (success.getSuccess().equalsIgnoreCase("1")) {
                m_dialog.dismiss();
                refreshBill();
            } else {
                Toast.makeText(context, "Unable to delete the menu item..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class SubmitAdditionalChargeValues extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog my_dialog;

        @Override
        protected void onPreExecute() {
            my_dialog = new MyCustomDialog(context, "Loading...");
            my_dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... s) {
            RestApiCalls call = new RestApiCalls();
            return call.Submit_Additional_charges((App) getApplication(), s[0], s[1], s[2], db_name);
        }

        @Override
        protected void onPostExecute(SuccessMessage success) {
            if (null != my_dialog && my_dialog.isShowing())
                my_dialog.dismiss();
            AppLog.write("Success--", new Gson().toJson(success));
            if (success.getSuccess().equalsIgnoreCase("1")) {
                m_dialog.dismiss();
                refreshBill();
            } else {
                Toast.makeText(context, "Unable to process..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class BillCancellation extends AsyncTask<String, String, SuccessMessage> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... params) {
            SuccessMessage success = new SuccessMessage();
            RestApiCalls call = new RestApiCalls();
            success = call.CancelBill((App) getApplication(), params[0], params[1], params[2], params[3], db_name);
            return success;
        }

        @Override
        protected void onPostExecute(SuccessMessage successMessage) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (successMessage != null) {
                if (successMessage.getSuccess().equalsIgnoreCase("success")) {
                    Toast.makeText(context, "Bill Cancelled..!", Toast.LENGTH_SHORT).show();
                    moveToTablesActivity();
                } else if (successMessage.getSuccess().equalsIgnoreCase("mismatch")) {
                    Toast.makeText(context, "Invalid Password..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Bill Cancellation Failed..!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
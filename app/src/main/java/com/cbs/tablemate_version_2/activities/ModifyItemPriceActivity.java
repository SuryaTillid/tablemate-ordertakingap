package com.cbs.tablemate_version_2.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.UIWidgets.DividerItemDecoration;
import com.cbs.tablemate_version_2.UIWidgets.MyCustomDialog;
import com.cbs.tablemate_version_2.adapter.ModifyItemAdapter;
import com.cbs.tablemate_version_2.adapter.ModifyItemPrimaryAdapter;
import com.cbs.tablemate_version_2.configuration.App;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.configuration.ConfigurationSettings;
import com.cbs.tablemate_version_2.configuration.RestApiCalls;
import com.cbs.tablemate_version_2.configuration.Settings;
import com.cbs.tablemate_version_2.interfaces.ModifyItemListener;
import com.cbs.tablemate_version_2.models.BillPreview;
import com.cbs.tablemate_version_2.models.DiscountOrder;
import com.cbs.tablemate_version_2.models.GetDiscount;
import com.cbs.tablemate_version_2.models.ModifyOrder;
import com.cbs.tablemate_version_2.models.SuccessMessage;
import com.google.gson.Gson;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 08-10-2018 in TableMateNew
 ***********************************************************************/
public class ModifyItemPriceActivity extends AppCompatActivity implements ModifyItemListener {
    private RecyclerView rc_MenuListView;
    private LinearLayoutManager mLayoutManager;
    private GetDiscount bill_list = new GetDiscount();
    private ArrayList<BillPreview> billItemsList = new ArrayList<>();
    private ArrayList<DiscountOrder> orderList = new ArrayList<>();
    private ModifyItemPrimaryAdapter adapter;
    private ConfigurationSettings configurationSettings;
    private Settings settings;
    private Context context;
    private String db_name, bill_Id, tableName, tableId, menu_item_name;
    private Dialog dialog;
    private TextView t_label;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        rc_MenuListView = findViewById(R.id.rvItemsList);
        t_label = findViewById(R.id.t_label);
        t_label.setVisibility(View.VISIBLE);

        context = ModifyItemPriceActivity.this;
        configurationSettings = new ConfigurationSettings(context);
        settings = new Settings(context);
        db_name = configurationSettings.getDb_Name();

        Intent i = getIntent();
        bill_Id = i.getStringExtra("BILL_ID");
        tableName = i.getStringExtra("TABLE_NAME");
        tableId = i.getStringExtra("TABLE_ID");

        adapter = new ModifyItemPrimaryAdapter(context, bill_list.getOrdereditems());
        adapter.setClickMenuItem_Listener(this);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rc_MenuListView.setLayoutManager(mLayoutManager);
        rc_MenuListView.setAdapter(adapter);
        rc_MenuListView.addItemDecoration(new DividerItemDecoration(context, null));

        new GetModifyItemsList().execute();
    }

    @Override
    public void update_ModifyItem_fromListListener(int position, boolean isCheck, String value) {
        DiscountOrder order = orderList.get(position);
        AppLog.write("TAG----", "Position-" + position + "--" + isCheck + "--" + new Gson().toJson(order));
        if (isCheck) {
            order.setChecked(isCheck);
            order.setModified_sale_price(value);
        } else {
            order.setChecked(isCheck);
            order.setModified_sale_price(order.getMrp_price());
        }
    }

    @Override
    public void modifyItem_clickListener(int position) {
        callSplitModifyValues(position);
    }

    private void callSplitModifyValues(int position) {
        final BillPreview billPreview = billItemsList.get(position);
        String menu_id = billPreview.getMenu_item_id();
        String custom_data = billPreview.getCustom_data();
        String bill = billPreview.getBill_id();
        menu_item_name = billPreview.getMenu_item_name();
        new ExpandableModifyPriceValues().execute(bill, menu_id, custom_data);
    }

    private void showPopup(final ArrayList<DiscountOrder> orderList) {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View view = getLayoutInflater().inflate(R.layout.activity_modify_price, null);
        dialog.setContentView(view);
        RecyclerView rv_OrdersList = view.findViewById(R.id.rvMenuItemsList);
        TextView t_menu_name = view.findViewById(R.id.txt_menu_name);
        ModifyItemAdapter itemAdapter = new ModifyItemAdapter(context, orderList);
        itemAdapter.setModifyUpdateListener(this);
        @SuppressLint("WrongConstant") LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv_OrdersList.setLayoutManager(layoutManager);
        rv_OrdersList.setAdapter(itemAdapter);

        Button delete = view.findViewById(R.id.btn_Update);
        Button cancel = view.findViewById(R.id.btn_Cancel);

        t_menu_name.setText(menu_item_name);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<ModifyOrder> order_modify_list = new ArrayList<>();
                for (DiscountOrder d_order : orderList) {
                    if (d_order.isChecked()) {
                        ModifyOrder item = new ModifyOrder(d_order.getId(), d_order.getModified_sale_price());
                        order_modify_list.add(item);
                    }
                }
                if (order_modify_list.size() > 0) {
                    new UpdateModifyPriceFromList().execute(new Gson().toJson(order_modify_list));
                } else {
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }

    private void callBillDialog() {
        Intent i = new Intent(context, BillPreviewActivity.class);
        i.putExtra("BILL_ID", bill_Id);
        i.putExtra("TABLE_NAME", tableName);
        i.putExtra("TABLE_ID", tableId);
        startActivity(i);
    }

    private class GetModifyItemsList extends AsyncTask<String, String, GetDiscount> {

        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Processing..");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected GetDiscount doInBackground(String... params) {
            RestApiCalls call = new RestApiCalls();
            AppLog.write("POS_Bill_ID-------=>", bill_Id);
            GetDiscount bills = call.getMenuItemsAfterBillGeneration((App) getApplication(), bill_Id, db_name);
            AppLog.write("Result------------=>", "--" + new Gson().toJson(bills));
            return bills;
        }

        @Override
        protected void onPostExecute(GetDiscount bills) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            try {
                billItemsList.clear();
                if (null != bills) {
                    billItemsList.addAll(bills.getOrdereditems());
                }
                adapter.myDataSetChanged(billItemsList);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class ExpandableModifyPriceValues extends AsyncTask<String, String, ArrayList<DiscountOrder>> {

        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<DiscountOrder> doInBackground(String... params) {
            RestApiCalls call = new RestApiCalls();
            return call.get_split_orders_for_discount((App) getApplication(), params[0], params[1], params[2], db_name);
        }

        @Override
        protected void onPostExecute(ArrayList<DiscountOrder> order) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("Discount_Result", "" + new Gson().toJson(order));
            orderList.clear();
            if (null != order && order.size() > 0) {
                orderList.addAll(order);
            }
            showPopup(orderList);
        }
    }

    private class UpdateModifyPriceFromList extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog my_dialog;

        @Override
        protected void onPreExecute() {
            my_dialog = new MyCustomDialog(context, "Loading...");
            my_dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... s) {
            RestApiCalls call = new RestApiCalls();
            return call.Update_modify_price((App) getApplication(), s[0], bill_Id, settings.getORDER_TYPE(), db_name);
        }

        @Override
        protected void onPostExecute(SuccessMessage deleteOrder) {
            if (null != my_dialog && my_dialog.isShowing())
                my_dialog.dismiss();
            AppLog.write("Success--", new Gson().toJson(deleteOrder));
            if (deleteOrder.getSuccess().equalsIgnoreCase("1")) {
                dialog.dismiss();
                callBillDialog();
            } else {
                Toast.makeText(context, "Unable to modify the price..!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
package com.cbs.tablemate_version_2.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.UIWidgets.DividerItemDecoration;
import com.cbs.tablemate_version_2.UIWidgets.MyCustomDialog;
import com.cbs.tablemate_version_2.adapter.ConfirmOrdersAdapter;
import com.cbs.tablemate_version_2.adapter.DeleteItemAdapter;
import com.cbs.tablemate_version_2.configuration.App;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.configuration.ConfigurationSettings;
import com.cbs.tablemate_version_2.configuration.CounterSaleSettings;
import com.cbs.tablemate_version_2.configuration.RestApiCalls;
import com.cbs.tablemate_version_2.configuration.Settings;
import com.cbs.tablemate_version_2.helper.ConnectivityReceiver;
import com.cbs.tablemate_version_2.interfaces.CancelItemListener;
import com.cbs.tablemate_version_2.interfaces.CancelItem_after_BillingListener;
import com.cbs.tablemate_version_2.models.Bill_Master;
import com.cbs.tablemate_version_2.models.CancelOrderItem;
import com.cbs.tablemate_version_2.models.DeleteOrderPOPUP;
import com.cbs.tablemate_version_2.models.GetTotalModel;
import com.cbs.tablemate_version_2.models.OrdersForTable;
import com.cbs.tablemate_version_2.models.SuccessMessage;
import com.cbs.tablemate_version_2.models.UnLockTable;
import com.cbs.tablemate_version_2.utilities.Utility;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/*********************************************************************
 * Created by Barani on 16-06-2016 in TableMateNew
 ***********************************************************************/
public class OrderStatusActivity extends AppCompatActivity implements CancelItemListener, CancelItem_after_BillingListener, ConnectivityReceiver.ConnectivityReceiverListener {
    private String tableID;
    private String db_name;

    private String bill_id = "";
    private String bill_total = "";
    private String userName = "";
    private String total_guests;
    private String cNotes, item_name;
    private int order_qty;
    private ConfigurationSettings configurationSettings;
    private CounterSaleSettings counterSettings;
    private Settings settings;
    private Context context;
    private RecyclerView ordersListView, rv_delete_OrdersList;
    private LinearLayoutManager mLayoutManager;
    private TextView currentTableName, txtTotalAmount;
    private ConfirmOrdersAdapter confirmOrdersAdapter;
    private ArrayList<OrdersForTable> ordersList = new ArrayList<>();
    private ArrayList<OrdersForTable> ordersList_for_delete = new ArrayList<>();
    private FloatingActionButton fab_bill, fab_table_release;
    private boolean isConnected = ConnectivityReceiver.isConnected();
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private Dialog dialog;
    private String comment;
    private String flag_clear;
    private String tableName;
    private EditText edit_delete_qty;
    private TextView txt_billable_qty, txt_deleted_qty, txt_order_qty, t_menu_item_name, t_menu_custom_notes;
    private DeleteItemAdapter deleteItemAdapter;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_status);

        ordersListView = findViewById(R.id.rvOrderStatus);
        currentTableName = findViewById(R.id.txtCurrentTableName);
        txtTotalAmount = findViewById(R.id.txt_total);
        fab_bill = findViewById(R.id.fab);
        fab_table_release = findViewById(R.id.fab2);
        mSwipeRefreshLayout = findViewById(R.id.swipe_container);

        context = OrderStatusActivity.this;

        settings = new Settings(context);
        configurationSettings = new ConfigurationSettings(context);
        counterSettings = new CounterSaleSettings(context);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(configurationSettings.getHOTEL_NAME() + " - Order Status");

        if (counterSettings.isConfigured()) {
            tableID = "0";
            tableName = "admin";
            total_guests = "1";
        } else {
            tableID = settings.getTableID();
            tableName = settings.getTABLE_NAME();
            total_guests = settings.getTOTAL_NUMBER_OF_GUESTS(tableID);
        }
        db_name = configurationSettings.getDb_Name();
        userName = settings.getUSER_NAME();
        bill_id = configurationSettings.getBILL__ID(tableID);

        AppLog.write("-----Bill_ID-----", "##" + bill_id + "\n--Guests--" + total_guests + "---" + userName);
        currentTableName.setText(tableName);

        confirmOrdersAdapter = new ConfirmOrdersAdapter(context, ordersList);
        confirmOrdersAdapter.setCancelItemListener(this);
        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        ordersListView.setLayoutManager(mLayoutManager);
        AppLog.write("Order Status Adapter..", "-" + confirmOrdersAdapter);
        ordersListView.setAdapter(confirmOrdersAdapter);
        ordersListView.addItemDecoration(new DividerItemDecoration(context, null));

        if (isConnected) {
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    refreshOrders();
                }
            });
            getOrders();

            //new GetBillPreview().execute(tableID);

            fab_bill.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ordersList.size() != 0) {
                        if (isAllFinished()) {
                            //generateBill();
                            new UpdateBillID().execute();
                            fab_bill.setEnabled(false);
                        } else {
                            Toast.makeText(context, "Your orders in progress! Unable to generate bill..!", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, "Cart is Empty..!", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            fab_table_release.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    conformDialog();
                }
            });
        } else {
            go_to_internetDisconnectMessage();
        }
    }

    //get bill modification
    private Bill_Master generateBill() {
        Bill_Master billMaster = new Bill_Master();
        billMaster.setId(tableID);
        /*String order_type;
        if (settings.getORDER_TYPE().equalsIgnoreCase("4")) {//if it is rooms the order type is considered as dine-in
            order_type = "4";
        } else {
            order_type = settings.getORDER_TYPE();
        }*/
        billMaster.setOrder_type(settings.getORDER_TYPE());
        String delivery_partner_name = settings.getDELIVERY_PARTNER(tableID);
        if (!TextUtils.isEmpty(delivery_partner_name)) {
            billMaster.setDelivery_partner_name(delivery_partner_name);
        } else {
            billMaster.setDelivery_partner_name("");
        }
        billMaster.setTnog(total_guests);
        billMaster.setStore_id(settings.getSTORE_ID());
        billMaster.setTotal(bill_total);
        billMaster.setChild("0");
        AppLog.write("User name---", "---" + settings.getUSER_NAME() + "--" + settings.getSTORE_ID());
        billMaster.setUser_id(settings.getUSER_NAME());
        String s_bill = new Gson().toJson(billMaster);
        new BillMasterCreate().execute(s_bill);
        AppLog.write("POS_Bill~~~~~~~~~~~~~~~~~~~~~~~~~~~", s_bill);
        return billMaster;
    }

    private void getOrders() {
        new GetOrdersForTable().execute();
        new GetBillTotal().execute(tableID);
    }

    private boolean isAllFinished() {
        for (OrdersForTable ordersForTable : ordersList) {
            if (!ordersForTable.getStatus().equalsIgnoreCase("2")) {
                return false;
            }
        }
        return true;
    }

    private void go_to_internetDisconnectMessage() {
        Intent i = new Intent(context, Show_NoInternetMessage_Activity.class);
        startActivity(i);
    }

    private void go_to_bill(String billId) {
        if (isConnected) {
            Intent i = new Intent(context, BillPreviewActivity.class);
            i.putExtra("BILL_ID", billId);
            i.putExtra("TABLE_NAME", tableName);
            i.putExtra("TABLE_ID", tableID);
            startActivity(i);
        } else {
            go_to_internetDisconnectMessage();
        }
    }

    private void refreshOrders() {
        getOrders();
        stop();
    }

    private void stop() {
        //stop P to R
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void cancelItem_fromListListener(String order_id) {
        AppLog.write("Order-Id------------->", order_id);
        if (isConnected) {
            flag_clear = "clear_one";
            // getComments_delete(order_id, "", flag_clear);
        } else {
            go_to_internetDisconnectMessage();
        }
    }

    @Override
    public void cancelItem_fromListListenerModified(String menu_name, String custom_note, String menu_item_id, String custom_data, String nc_order) {
        String custom_exist;
        item_name = menu_name;
        if (!TextUtils.isEmpty(custom_data)) {
            custom_exist = "1";
            cNotes = custom_note;
        } else {
            custom_exist = "0";
            cNotes = "";
        }
        //new CancelPopup().execute(menu_item_id, custom_exist, custom_data);
        new GetOrdersSplit().execute(tableID, menu_item_id, custom_exist, custom_data, nc_order);
    }

    /*
     * Get comments for delete menu item
     * */
    private void getComments_delete(final ArrayList<DeleteOrderPOPUP> deleteOrder) {
        if (DetectConnection.isOnline(context)) {
            final Spinner spinner_comments;
            Button btn_proceed, btn_cancel;
            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.activity_delete_popup_row);

            spinner_comments = dialog.findViewById(R.id.spinnerComments);
            edit_delete_qty = dialog.findViewById(R.id.ed_deleteQty);
            txt_order_qty = dialog.findViewById(R.id.txt_order_qty);
            txt_deleted_qty = dialog.findViewById(R.id.txt_deleted_qty);
            txt_billable_qty = dialog.findViewById(R.id.txt_billable_qty);
            t_menu_item_name = dialog.findViewById(R.id.t_menu_item_name);
            t_menu_custom_notes = dialog.findViewById(R.id.t_menu_custom_notes);
            btn_proceed = dialog.findViewById(R.id.btn_Submit);
            btn_cancel = dialog.findViewById(R.id.btn_Cancel);


            Log.e("Item name", item_name);
            t_menu_item_name.setText(item_name);

            for (DeleteOrderPOPUP popup : deleteOrder) {
                order_qty = Utility.convertToInteger(popup.getBill_qty());
            }
            txt_order_qty.setText("" + order_qty);
            txt_billable_qty.setText("" + order_qty);
            if (!TextUtils.isEmpty(cNotes)) {
                t_menu_custom_notes.setVisibility(View.VISIBLE);
                t_menu_custom_notes.setText(cNotes);
            } else {
                t_menu_custom_notes.setVisibility(View.GONE);
            }

            edit_delete_qty.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!TextUtils.isEmpty(s)) {
                        AppLog.write("TAG---", "--" + s.toString());
                        int deleteQty = Utility.convertToInteger(s.toString());
                        if (deleteQty > order_qty) {
                            edit_delete_qty.setText("0");
                            edit_delete_qty.setSelection(edit_delete_qty.getText().length());
                            Toast.makeText(context, "Delete quantity cannot be greater than total ordered quantity..!", Toast.LENGTH_SHORT).show();
                        } else {
                            int qty = order_qty - deleteQty;
                            changeValues(s.toString(), qty);
                        }
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });

            // Spinner Drop down elements
            List<String> commentsList = new ArrayList<String>();
            commentsList.add("Select Comment");
            commentsList.add("Cancelled by Customer");
            commentsList.add("Did not Like");
            commentsList.add("Wrong order by Captain");
            commentsList.add("Delayed by Kitchen");
            commentsList.add("Did not Serve");
            commentsList.add("Dish not Available");
            commentsList.add("POS Error");
            commentsList.add("Double Entry");

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, commentsList);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_comments.setAdapter(dataAdapter);

            // Spinner click listener
            spinner_comments.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String s = parent.getItemAtPosition(position).toString();
                    if (!s.equalsIgnoreCase("Select Comment")) {
                        comment = s;
                    } else {
                        Toast.makeText(parent.getContext(), "Please select a comment..!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            btn_proceed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (spinner_comments.getSelectedItemPosition() == 0) {
                        Toast.makeText(context, "Please select a valid comment..!", Toast.LENGTH_SHORT).show();
                    } else {
                        String cancel_qty = edit_delete_qty.getText().toString().trim();
                        if (validateQty(cancel_qty)) {
                            double qty = (Utility.convertToInteger(deleteOrder.get(0).getBill_qty()) - Utility.convertToInteger(cancel_qty));
                            String bill_qty = String.valueOf(qty);
                            new DeleteOrder().execute(comment, deleteOrder.get(0).getMenu_item_id(), cancel_qty, bill_qty, tableID, deleteOrder.get(0).getId());
                        } else {
                            Toast.makeText(context, "Please enter a valid quantity..!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });

            btn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        } else {
            Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean validateQty(String cancel_qty) {
        double qty = Utility.convertToDouble(cancel_qty);
        return qty > 0;
    }

    private void changeValues(String s, int qty) {
        txt_billable_qty.setText("" + qty);
        txt_deleted_qty.setText(s);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
    }

    @Override
    public void cancelItem_fromListListener(int position, String comment) {
    }

    //Todo - cancellation with split
    @Override
    public void deleteItem_fromListListener(int position, boolean isChecked, String comment, String other_reason) {

        OrdersForTable table = ordersList_for_delete.get(position);
        AppLog.write("TAG----", "Position" + position + "--" + isChecked + "--" + new Gson().toJson(ordersList_for_delete));
        if (isChecked) {
            table.setCancel_comments(comment);
            table.setChecked(isChecked);
            table.setOther_reason(other_reason);
        } else {
            table.setCancel_comments("");
            table.setChecked(isChecked);
            table.setOther_reason("");
        }
        //deleteItemAdapter.myDataSetChanged(ordersList_for_delete,position);
    }

    @SuppressLint("RestrictedApi")
    private void hideButtons() {
        ordersList.clear();
        fab_bill.setVisibility(View.GONE);
        fab_table_release.setVisibility(View.GONE);
    }

    private void show_releaseButton() {
        Toast.makeText(context, "You have ordered nothing..!", Toast.LENGTH_SHORT).show();
        hideButtons();
    }

    private void conformDialog() {
        if (DetectConnection.isOnline(context)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setCancelable(false);
            builder.setMessage("Do you really want to release the table..?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    new UnLockSelectedTable().execute();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //if user select "No", just cancel this dialog and continue with app
                    dialog.cancel();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        } else {
            Toast.makeText(context, "Please check your internet connection..!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(context, MenuActivityWithFilters.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_order_progress, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.view_order_progress:
                Intent view_progress = new Intent(context, ViewOrderProgressActivity.class);
                startActivity(view_progress);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void editBoxValidation(EditText editOtherCost, EditText edit_dispatch_qty) {

        editOtherCost.addTextChangedListener((new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable e) {
                try {
                    String temp = e.toString();
                    int posDot = temp.indexOf(".");
                    AppLog.write("Pos_DOT---", "--" + posDot);
                    if (posDot <= 0) {
                        return;
                    }
                    if (temp.length() - posDot - 1 > 2) {
                        e.delete(posDot + 2, posDot + 3);
                    }
                } catch (Exception exp) {
                    exp.printStackTrace();
                }
            }
        }));
    }

    private void show_cancelMenuItemPopup(final ArrayList<OrdersForTable> deleteOrder) {
        //AppLog.write("SIZE_OF_ORDERS_DELETE_STRUCTURE",""+deleteOrder.size());
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View view = getLayoutInflater().inflate(R.layout.activity_cancel_item, null);
        dialog.setContentView(view);
        rv_delete_OrdersList = view.findViewById(R.id.rvOrdersList);
        deleteItemAdapter = new DeleteItemAdapter(context, deleteOrder);
        deleteItemAdapter.setCancelItem_Listener(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rv_delete_OrdersList.setLayoutManager(layoutManager);
        rv_delete_OrdersList.setAdapter(deleteItemAdapter);

        Button delete = view.findViewById(R.id.btn_Submit);
        Button cancel = view.findViewById(R.id.btn_Cancel);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<CancelOrderItem> cancelOrderItems = new ArrayList<>();
                for (OrdersForTable table : ordersList_for_delete) {
                    if (table.isChecked()) {
                        CancelOrderItem item = new CancelOrderItem(table.getOrder_id(), table.getCancel_comments(), table.getOther_reason());
                        cancelOrderItems.add(item);
                    }
                }
                AppLog.write("Cancel_order", "--" + new Gson().toJson(cancelOrderItems));
                if (cancelOrderItems.size() > 0) {
                    new DeleteOrderedItem().execute(new Gson().toJson(cancelOrderItems));
                } else {
                    dialog.dismiss();
                }

            }
        });
        dialog.show();
    }

    private class GetOrdersForTable extends AsyncTask<Void, Void, ArrayList<OrdersForTable>> {

        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<OrdersForTable> doInBackground(Void... params) {
            RestApiCalls call = new RestApiCalls();
            return call.getBillPreview((App) getApplication(), configurationSettings.getBILL__ID(tableID), db_name);
            //return call.getOrdersForTable((App) getApplication(), configurationSettings.getBILL__ID(tableID), db_name);
        }

        @Override
        protected void onPostExecute(ArrayList<OrdersForTable> ordersForTables) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != ordersForTables) {
                AppLog.write("---Orders---", "--" + new Gson().toJson(ordersForTables));
                ordersList.clear();
                for (OrdersForTable ordersForTable : ordersForTables) {
                    if (!ordersForTable.getStatus().trim().equalsIgnoreCase("Unable to Process")) {
                        ordersList.add(ordersForTable);
                    }
                }
                confirmOrdersAdapter.MyDataChanged(ordersList);
                if (ordersList.size() == 0 && counterSettings.isConfigured()) {
                    hideButtons();
                } else if (ordersList.size() == 0) {
                    show_releaseButton();
                }
            }
        }
    }

    private class GetBillTotal extends AsyncTask<String, String, GetTotalModel> {

        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected GetTotalModel doInBackground(String... params) {
            GetTotalModel total = new GetTotalModel();
            try {
                RestApiCalls calls = new RestApiCalls();
                AppLog.write("Table_Id---->", "--" + params[0]);
                total = calls.getBillTotal((App) getApplication(), configurationSettings.getBILL__ID(tableID), db_name);
                //total = calls.getBillTotal((App) getApplication(), tableID, db_name);
                bill_total = total.getTotal();
                AppLog.write("Result-BillTotal----->", bill_total);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return total;
        }

        @Override
        protected void onPostExecute(GetTotalModel getTotalModel) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != getTotalModel) {
                Double total = Utility.convertToDouble(getTotalModel.getTotal());
                txtTotalAmount.setText("SubTotal:  ₹ " + String.format("%.2f", total));
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class BillMasterCreate extends AsyncTask<String, String, List<String>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected List<String> doInBackground(String... params) {
            RestApiCalls call = new RestApiCalls();
            List<String> bills = call.getBillId((App) getApplication(), "0", params[0], db_name);
            AppLog.write("Bills", "--" + new Gson().toJson(bills));
            return bills;
        }

        @Override
        protected void onPostExecute(List<String> s) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != s && !s.isEmpty()) {
                settings.setBillId(tableID, s.get(0));
                go_to_bill(settings.getBillId(tableID));
                fab_bill.setEnabled(true);
            } else {
                Toast.makeText(context, "Not Responding..!,Please try again", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class UnLockSelectedTable extends AsyncTask<String, Void, UnLockTable> {

        String table_ID = tableID;
        String userName = settings.getUSER_NAME();
        String password = settings.getPASSWORD();
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Progressing...");
            dialog.show();
        }

        @Override
        protected UnLockTable doInBackground(String... params) {
            RestApiCalls restApiCalls = new RestApiCalls();
            AppLog.write("Values---->>", "--" + table_ID + "--" + userName + "--" + password);
            return restApiCalls.unLockTable((App) getApplication(), userName, password, table_ID, db_name);
        }

        @Override
        protected void onPostExecute(UnLockTable unLockTable) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != unLockTable && "1".equalsIgnoreCase(unLockTable.getSuccess())) {
                Toast.makeText(context, "Table Released Successfully..!", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(context, TableActivity_1.class);
                startActivity(i);
            }
        }
    }

    private class CancelPopup extends AsyncTask<String, String, ArrayList<DeleteOrderPOPUP>> {

        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<DeleteOrderPOPUP> doInBackground(String... params) {
            RestApiCalls call = new RestApiCalls();
            return call.OrdersDeletePOPUPDetails((App) getApplication(), params[0], tableID, params[1], params[2], db_name);
        }

        @Override
        protected void onPostExecute(ArrayList<DeleteOrderPOPUP> deleteOrder) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != deleteOrder && deleteOrder.size() > 0) {
                getComments_delete(deleteOrder);
            }
        }
    }

    private class DeleteOrder extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog my_dialog;

        @Override
        protected void onPreExecute() {
            my_dialog = new MyCustomDialog(context, "Loading...");
            my_dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... s) {
            RestApiCalls call = new RestApiCalls();
            return call.SubmitDeleteOrders((App) getApplication(), s[0], s[1], s[2], s[3], s[4], s[5], db_name);
        }

        @Override
        protected void onPostExecute(SuccessMessage deleteOrder) {
            if (null != my_dialog && my_dialog.isShowing())
                my_dialog.dismiss();
            AppLog.write("Success--", new Gson().toJson(deleteOrder));
            if (deleteOrder.getSuccess().equalsIgnoreCase("1")) {
                dialog.dismiss();
                refreshOrders();
            } else {
                Toast.makeText(context, "Unable to delete the menu item..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class UpdateBillID extends AsyncTask<String, String, SuccessMessage> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... params) {
            RestApiCalls call = new RestApiCalls();
            return call.Update_Bill((App) getApplication(), configurationSettings.getBILL__ID(tableID),
                    settings.getORDER_TYPE(), tableID, settings.getUSER_NAME(), db_name);
        }

        @Override
        protected void onPostExecute(SuccessMessage s) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != s) {
                settings.setBillId(tableID, s.getBill_id());
                configurationSettings.setBILL__ID(tableID, s.getBill_id());
                go_to_bill(settings.getBillId(tableID));
                fab_bill.setEnabled(true);
            } else {
                Toast.makeText(context, "Not Responding..!,Please try again", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class GetOrdersSplit extends AsyncTask<String, String, ArrayList<OrdersForTable>> {

        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<OrdersForTable> doInBackground(String... p) {
            RestApiCalls call = new RestApiCalls();
            return call.get_split_orders((App) getApplication(), p[0], p[1], p[2], p[3], p[4], db_name);
        }

        @Override
        protected void onPostExecute(ArrayList<OrdersForTable> deleteOrder) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            ordersList_for_delete.clear();
            if (null != deleteOrder && deleteOrder.size() > 0) {
                //getComments_delete(deleteOrder);
                for (OrdersForTable orders : deleteOrder) {
                    ordersList_for_delete.add(orders);
                }
                show_cancelMenuItemPopup(deleteOrder);
            }
        }
    }

    private class DeleteOrderedItem extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog my_dialog;

        @Override
        protected void onPreExecute() {
            my_dialog = new MyCustomDialog(context, "Loading...");
            my_dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... s) {
            RestApiCalls call = new RestApiCalls();
            return call.cancelItem_for_mobile((App) getApplication(), s[0], userName, db_name);
        }

        @Override
        protected void onPostExecute(SuccessMessage deleteOrder) {
            if (null != my_dialog && my_dialog.isShowing())
                my_dialog.dismiss();
            AppLog.write("Success--", new Gson().toJson(deleteOrder));
            if (deleteOrder.getSuccess().equalsIgnoreCase("1")) {
                dialog.dismiss();
                refreshOrders();
            } else {
                Toast.makeText(context, "Unable to delete the menu item..!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
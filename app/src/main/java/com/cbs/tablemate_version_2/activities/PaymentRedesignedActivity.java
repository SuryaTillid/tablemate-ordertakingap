package com.cbs.tablemate_version_2.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.UIWidgets.MyCustomDialog;
import com.cbs.tablemate_version_2.configuration.App;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.configuration.ConfigurationSettings;
import com.cbs.tablemate_version_2.configuration.CounterSaleSettings;
import com.cbs.tablemate_version_2.configuration.RestApiCalls;
import com.cbs.tablemate_version_2.configuration.Settings;
import com.cbs.tablemate_version_2.helper.PendingBillsDB;
import com.cbs.tablemate_version_2.models.BillCalcModel;
import com.cbs.tablemate_version_2.models.BillPay;
import com.cbs.tablemate_version_2.models.GetPaymentAmount;
import com.cbs.tablemate_version_2.models.GetPaymentOptions;
import com.cbs.tablemate_version_2.models.POS_Bill;
import com.cbs.tablemate_version_2.models.PaymentOption;
import com.cbs.tablemate_version_2.utilities.Utility;
import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/*********************************************************************
 * Created by Barani on 20-04-2018 in TableMateNew
 ***********************************************************************/
public class PaymentRedesignedActivity extends AppCompatActivity {

    private Settings settings;
    private CounterSaleSettings counterSettings;
    private String billId;
    private String db_name;
    private String storeID;
    private String tableId;
    private Context context;
    private String tableName;
    private String billAmount;
    private String pay_option;
    private String billDetails;
    private String amount_received;
    private double balance_amount, amountReceived;
    private ConfigurationSettings configurationSettings;
    private ArrayList<PaymentOption> options = new ArrayList<>();
    private ArrayList<POS_Bill> billList = new ArrayList<>();
    private Button btn_pay, btn_proceed_pay;
    private ArrayList<GetPaymentOptions> pay_options = new ArrayList<>();
    private EditText et_GrandTotal, et_amount_received, et_balance_due, et_tip, edit_amount, edit_card_num;
    private double balance;
    private Spinner spinner_option;

    @SuppressLint("DefaultLocale")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        Intent i = getIntent();
        context = PaymentRedesignedActivity.this;
        settings = new Settings(context);
        configurationSettings = new ConfigurationSettings(context);
        counterSettings = new CounterSaleSettings(context);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(settings.getHOTEL_NAME() + " - Pay");

        db_name = configurationSettings.getDb_Name();
        tableId = i.getStringExtra("TABLE_ID");
        tableName = i.getStringExtra("TABLE_NAME");
        String mobile_number = settings.getMOBILE_NUMBER(tableId);
        storeID = settings.getSTORE_ID();
        billId = i.getStringExtra("BILL_ID");
        String display_bill_id = i.getStringExtra("DISPLAY_BILL");
        billAmount = i.getStringExtra("BILL_AMOUNT");
        AppLog.write("Bill values are---->", display_bill_id + "---" + billAmount + "---" + billId + "---" + storeID + "---" + tableId + "---" + tableName);
        initialize();

        if (DetectConnection.isOnline(context)) {
            new SendPrintRequestForBillPreview().execute(billId, "1");
            new GetPaymentType().execute();
        } else {
            showNoInternet();
        }
        updateBalance();

        billAmount = String.format("%.2f", Utility.convertToDouble(billAmount));
        et_GrandTotal.setText("₹ " + billAmount);
        edit_amount.setText("" + balance);
        edit_amount.setSelection(edit_amount.getText().length());

        spinner_option.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    String s = getPayOptions(pay_options).get(position);
                    AppLog.write("Position_spinner--", "--" + s);
                    if (s.equalsIgnoreCase("Cash")) {
                        edit_card_num.setVisibility(View.GONE);
                    } else {
                        edit_card_num.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btn_proceed_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new BillCalculation().execute(billId);
                double amount = Utility.convertToDouble(edit_amount.getText().toString());
                if (validate(amount)) {
                    AppLog.write("Pay_option--", "--" + amount + spinner_option.getSelectedItem());
                    //callPaymentFunction(options);
                    addPayment(spinner_option.getSelectedItemPosition(), edit_amount.getText().toString(), edit_card_num.getText().toString());
                }
            }
        });

        et_tip.addTextChangedListener((new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable edt) {
                try {
                    String temp = edt.toString();
                    int posDot = temp.indexOf(".");
                    AppLog.write("POS DOT---", "" + posDot);
                    if (posDot <= 0) {
                        return;
                    }
                    if (temp.length() - posDot - 1 > 2) {
                        edt.delete(posDot + 2, posDot + 3);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }));
    }

    private void validateBillStatus(ArrayList<POS_Bill> billList) {
        /*AppLog.write("Bill_status---",""+new Gson().toJson(billList.get(0).getBill_status()));
        if (!billList.get(0).getBill_status().equalsIgnoreCase("2")) {
            return true;
        } else {
            Toast.makeText(context, "Bill already paid.!", Toast.LENGTH_SHORT).show();
        }*/

        for (POS_Bill bill : billList) {
            if (bill.getBill_status().equalsIgnoreCase("4")) {
                Toast.makeText(context, "Bill cancelled.!", Toast.LENGTH_SHORT).show();
            } else if (bill.getBill_status().equalsIgnoreCase("2")) {
                Toast.makeText(context, "Bill already paid.!", Toast.LENGTH_SHORT).show();
            } else {
                amount_received = String.valueOf(amountReceived);
                roundValue(amount_received, billAmount);
                String amount = String.valueOf(balance_amount);
                AppLog.write("Balance Amount-----", "--" + amount);
                callPaymentFunction(options);
            }
        }
    }

    private void addPayment(int position, String amount, String pin) {
        options.clear();
        PaymentOption option = new PaymentOption();
        option.setAmount(TextUtils.isEmpty(amount) ? "" : amount);
        option.setPin(TextUtils.isEmpty(pin) ? "" : pin);
        option.setPaymentType(pay_options.get(position).getPaymentmode());
        options.add(option);
        AppLog.write("TAG1-", "-" + new Gson().toJson(options));
        updateBalance();
        if (balance <= 0) {
            amount_received = String.valueOf(amountReceived);
            roundValue(amount_received, billAmount);
            String amt = String.valueOf(balance_amount);
            AppLog.write("Balance Amount-----", "--" + amt);
            callPaymentFunction(options);
        } else {
            Toast.makeText(context, "Please enter a valid amount,cant proceed for payment.!", Toast.LENGTH_SHORT).show();
        }
    }

    private void roundValue(String amount_received, String billAmount) {
        balance_amount = (Utility.convertToFloat(amount_received) - Utility.convertToFloat(billAmount));
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        balance_amount = Double.valueOf(twoDForm.format(balance_amount));
        AppLog.write("Balance--", "" + balance_amount);
    }

    private void callPaymentFunction(ArrayList<PaymentOption> pay_options) {
        String get_pay_amount = new Gson().toJson(getPaymentAmount(pay_options));
        String get_pay_option = new Gson().toJson(getPaymentMode(pay_options));
        AppLog.write("OPTION------", "--" + get_pay_amount + "--" + get_pay_option);
        new BillPayment().execute(amount_received, "0", get_pay_amount, get_pay_option);
    }

    private ArrayList<GetPaymentAmount> getPaymentMode(ArrayList<PaymentOption> pay_option) {
        ArrayList<GetPaymentAmount> pay_mode_list = new ArrayList<>();
        for (PaymentOption getPaymentOptions : pay_option) {
            GetPaymentAmount payment_mode = new GetPaymentAmount();
            payment_mode.setMode(getPaymentOptions.getPaymentType());
            pay_mode_list.add(payment_mode);
        }
        return pay_mode_list;
    }

    private ArrayList<GetPaymentAmount> getPaymentAmount(ArrayList<PaymentOption> amount_received) {
        ArrayList<GetPaymentAmount> pay_list = new ArrayList<>();
        for (PaymentOption getPaymentOptions : amount_received) {
            GetPaymentAmount payment = new GetPaymentAmount();
            payment.setAmount(getPaymentOptions.getAmount());
            pay_list.add(payment);
        }
        return pay_list;
    }

    private List<String> getPayOptions(ArrayList<GetPaymentOptions> pay_options) {
        List<String> list = new ArrayList<>();
        for (GetPaymentOptions payOption : pay_options) {
            list.add(payOption.getPaymentmode());
        }
        return list;
    }

    private void initialize() {
        et_GrandTotal = findViewById(R.id.et_GrandTotal);
        et_amount_received = findViewById(R.id.et_amount_received);
        et_balance_due = findViewById(R.id.et_balance_due);
        et_tip = findViewById(R.id.et_tip);
        btn_pay = findViewById(R.id.btn_pay_bill);
        spinner_option = findViewById(R.id.pay_option);
        edit_amount = findViewById(R.id.edit_amount_pay);
        edit_card_num = findViewById(R.id.edit_card_num);
        btn_proceed_pay = findViewById(R.id.btn_proceed_pay);
    }

    private void showNoInternet() {
        Intent i = new Intent(context, Show_NoInternetMessage_Activity.class);
        startActivity(i);
    }

    private void checkCounterSaleType() {
        if (counterSettings.isConfigured()) {
            Intent intent = new Intent(context, PrintForCounterSaleBill.class);
            intent.putExtra("BILL_ID", billId);
            intent.putExtra("PAY_MODE", pay_option);
            intent.putExtra("TABLE_ID", tableId);
            intent.putExtra("TABLE_NAME", tableName);
            intent.putExtra("CUSTOMER_ID", settings.getCUSTOMER_ID(tableId));
            startActivity(intent);
        } else {
            Intent intent = new Intent(context, BluetoothPrint_for_Payment.class);
            intent.putExtra("BILL_ID", billId);
            intent.putExtra("PAY_MODE", pay_option);
            intent.putExtra("TABLE_ID", tableId);
            intent.putExtra("TABLE_NAME", tableName);
            intent.putExtra("CUSTOMER_ID", settings.getCUSTOMER_ID(tableId));
            startActivity(intent);
        }
    }

    private boolean validate(double amount) {
        if (amount > 0) {
            return true;
        } else {
            Toast.makeText(context, "Please enter valid amount..!", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    private void updateBalance() {
        double amountPaid = 0;
        for (PaymentOption option : options) {
            amountPaid += Utility.convertToDouble(option.getAmount());
        }
        balance = Utility.convertToDouble(billAmount) - amountPaid;
        et_balance_due.setText("₹ " + balance);
        amountReceived = amountPaid;
        et_amount_received.setText("₹ " + amountPaid);
        if (balance <= 0) {
            btn_pay.setBackgroundResource(R.drawable.add_btn);
            btn_pay.setTextColor(Color.WHITE);
            btn_pay.setAlpha((float) 1);
            btn_pay.setFocusable(true);
        } else {
            btn_pay.setBackgroundResource(R.drawable.edit_background_disabled);
            btn_pay.setTextColor(Color.BLACK);
            btn_pay.setAlpha((float) 0.5);
            btn_pay.setFocusable(false);
        }
    }

    private class GetPaymentType extends AsyncTask<Boolean, Void, ArrayList<GetPaymentOptions>> {

        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<GetPaymentOptions> doInBackground(Boolean... params) {
            ArrayList<GetPaymentOptions> list;
            RestApiCalls call = new RestApiCalls();
            list = call.get_paymentOptions((App) getApplication(), db_name);
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<GetPaymentOptions> options) {
            if (null != dialog && dialog.isShowing()) {
                dialog.dismiss();
            }
            AppLog.write("Options------>", "--" + new Gson().toJson(options));
            if (null != options && options.size() > 0) {
                pay_options = options;
                AppLog.write("Options-------------->", "-----" + new Gson().toJson(pay_options));
            }
            spinner_option.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_dropdown_item_1line, getPayOptions(pay_options)));
        }
    }

    //Pay Bill
    private class BillPayment extends AsyncTask<String, Void, BillPay> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Submitting...");
            dialog.show();
        }

        @Override
        protected BillPay doInBackground(String... params) {

            RestApiCalls calls = new RestApiCalls();
            BillPay submit = calls.billPaymentModified((App) getApplication(), "1", "0", billId, "0.00", params[1], "0.00", "", tableId,
                    "", "", "", params[0], params[2], params[3], billDetails, tableName, "None", db_name);
            AppLog.write("Value------", "" + new Gson().toJson(submit));
            pay_option = params[3];
            return submit;
        }

        @Override
        protected void onPostExecute(BillPay billPay) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("BillPay---", "---" + new Gson().toJson(billPay));
            if (null != billPay) {
                if (billPay.getSuccess().equals("1")) {
                    Toast.makeText(context, "Bill paid successfully..!", Toast.LENGTH_SHORT).show();
                    new SendPrintRequestForBillPreview().execute(billId, "2");
                    PendingBillsDB db = new PendingBillsDB(context);
                    db.updateBillDetails(billId);
                    checkCounterSaleType();
                } else {
                    Toast.makeText(context, "Error occurred in bill paying..!", Toast.LENGTH_SHORT).show();
                }
                AppLog.write("BillPay--", "" + new Gson().toJson(billPay));
            } else {
                Toast.makeText(context, "Error occurred in bill paying..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class SendPrintRequestForBillPreview extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            RestApiCalls calls = new RestApiCalls();
            AppLog.write("Bill_request_bill_paying---->", "" + params[0] + "\n" + params[1]);
            return calls.sendPrintRequest_to_ServerForBill((App) getApplication(), params[0], params[1], db_name);
        }

        @Override
        protected void onPostExecute(String s) {
            AppLog.write("Bill preview result--->", s);
        }
    }

    private class BillCalculation extends AsyncTask<String, String, BillCalcModel> {
        MyCustomDialog my_dialog;

        @Override
        protected void onPreExecute() {
            my_dialog = new MyCustomDialog(context, "Loading...");
            my_dialog.show();
        }

        @Override
        protected BillCalcModel doInBackground(String... params) {
            RestApiCalls call = new RestApiCalls();
            AppLog.write("POS_Bill_ID--------", billId);
            AppLog.write("Order_Type---------", settings.getORDER_TYPE());
            return call.billCalculation((App) getApplication(), billId, settings.getORDER_TYPE(), db_name);
        }

        @Override
        protected void onPostExecute(BillCalcModel billCalcModel) {
            if (null != my_dialog && my_dialog.isShowing())
                my_dialog.dismiss();
            if (null != billCalcModel) {
                billDetails = new Gson().toJson(billCalcModel);
                billList.addAll(billCalcModel.getBill_api());
                /*if (balance <= 0) {
                    validateBillStatus(billList);
                } else {
                    Toast.makeText(context, "Please enter a valid amount,cant proceed for payment.!", Toast.LENGTH_SHORT).show();
                }*/
            } else {
                Toast.makeText(context, "Error occurred in bill paying..!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
package com.cbs.tablemate_version_2.activities;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.UIWidgets.MyCustomDialog;
import com.cbs.tablemate_version_2.adapter.CustomerDataAdapter;
import com.cbs.tablemate_version_2.adapter.TableHeaderAdapter;
import com.cbs.tablemate_version_2.adapter.TierAdapter;
import com.cbs.tablemate_version_2.configuration.App;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.configuration.ConfigurationSettings;
import com.cbs.tablemate_version_2.configuration.RestApiCalls;
import com.cbs.tablemate_version_2.configuration.Settings;
import com.cbs.tablemate_version_2.helper.CartManager;
import com.cbs.tablemate_version_2.helper.ConnectivityReceiver;
import com.cbs.tablemate_version_2.helper.LoginDB;
import com.cbs.tablemate_version_2.interfaces.TableGroupClickListener;
import com.cbs.tablemate_version_2.models.CreateCustomerDetails;
import com.cbs.tablemate_version_2.models.CustomerData;
import com.cbs.tablemate_version_2.models.CustomerDetailsModel;
import com.cbs.tablemate_version_2.models.DefaultCustomerDetails;
import com.cbs.tablemate_version_2.models.DeliveryPartnerDetails;
import com.cbs.tablemate_version_2.models.DiscountResponse;
import com.cbs.tablemate_version_2.models.LockTable;
import com.cbs.tablemate_version_2.models.LoginDetails;
import com.cbs.tablemate_version_2.models.StoreDetails;
import com.cbs.tablemate_version_2.models.Table;
import com.cbs.tablemate_version_2.models.TierValues;
import com.cbs.tablemate_version_2.utilities.Utility;
import com.google.gson.Gson;

import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/*********************************************************************
 * Created by Baraneeswari on 04-05-2016 in TableMateNew
 ***********************************************************************/

public class TableActivity extends AppCompatActivity implements View.OnClickListener,
        ConnectivityReceiver.ConnectivityReceiverListener, TableGroupClickListener {

    ArrayList<TierValues> tier = new ArrayList<>();
    private CartManager cart;
    private Context context;
    private GridView table_grid;

    private TierAdapter tierAdapter;
    private TableHeaderAdapter headerAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ArrayList<Table> swapTableList = new ArrayList<Table>();
    private ArrayList<Table> tableList = new ArrayList<Table>();
    private ArrayList<String> table_list_to_swap = new ArrayList<>();
    private ArrayList<DeliveryPartnerDetails> deliveryPartnerList = new ArrayList<>();
    private Settings settings;
    private ConfigurationSettings configurationSettings;
    private String db_name, userName, tableName, tableID, deliveryPartnerName;
    private String order_type;
    private Dialog dialog;
    private Button btnShowDetails, btnLock, btnSkip, btnSwitchTable, btnShowLockScreen;
    private EditText txt_MobileNumber, txtCustomerName,txtMemershipId, txtMailId, txtTotalGuests, txtAddress;
    private String cust_name,memership_id, cust_address, cust_mobile_num, cust_guest_num, cust_mail;
    private LinearLayout row1, row2, row22,row3, row4, spinner_row;
    private SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private long current_date = System.currentTimeMillis();
    private String logout_time, referenceId;
    private String spinner_selected_value_tableID;
    private LoginDB loginDB = new LoginDB(this);
    private CustomerDataAdapter cAdapter;
    private Spinner spinner_delivery_partner;
    private ArrayList<DefaultCustomerDetails> hotel_profile;
    private List<String> partnerNames = new ArrayList<>();
    private List<String> tierName = new ArrayList<>();
    private String c_id;
    private RecyclerView rc_tier_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablelist);

        context = TableActivity.this;
        settings = new Settings(context);
        configurationSettings = new ConfigurationSettings(context);
        db_name = configurationSettings.getDb_Name();
        userName = settings.getUSER_NAME();
        order_type = settings.getORDER_TYPE();
        AppLog.write("DB Name - Order Type", db_name + "---" + order_type);
        logout_time = timeFormat.format(current_date);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(configurationSettings.getHOTEL_NAME() + " - Table");

        btnShowLockScreen = findViewById(R.id.btnShowLockScreen);
        table_grid = findViewById(R.id.gridTableStatus);
        mSwipeRefreshLayout = findViewById(R.id.swipe_container);
        rc_tier_view = findViewById(R.id.r_view_table_list);

        btnShowLockScreen.setOnClickListener(this);
        cart = new CartManager(context);

        if (DetectConnection.isOnline(context)) {
            new LoadTablesList().execute();
            //new GetStoreDetails().execute();
        } else {
            show_noInternet();
        }

        /*
         * When refresh items using P to R
         * */
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                refreshTables();
            }
        });

        /*tAdapter = new TableAdapter(context, tableList,this);
        tAdapter.setTableOnClickListener(this);
        rc_tier_view.setAdapter(tAdapter);
       */

        tierAdapter = new TierAdapter(context, tier, this);
        //headerAdapter.isGroupExpanded(1);
        rc_tier_view.setLayoutManager(new LinearLayoutManager(this));
        rc_tier_view.setAdapter(tierAdapter);

        List<LoginDetails> details = loginDB.getLoginDetails();
        for (LoginDetails c : details) {
            referenceId = c.getRef_id();
        }
    }

    private boolean hasHoldTable(ArrayList<TierValues> tierValues) {
        for (TierValues tierValue : tierValues) {
            for (Table table : tierValue.getTableList()) {
                String bill = table.getBill_id();
                if (table.isSelected() && !TextUtils.isEmpty(bill)) {
                    return true;
                }
            }
        }
        return false;
    }

    private void tableSelection(Table table) {
        table.setSelected(!table.isSelected());
        String bill_id = table.getBill_id();
        if (table.getStatus().equalsIgnoreCase("Hold")) {
            Toast.makeText(context, "Table is not yet released..!", Toast.LENGTH_SHORT).show();
        }
        if (getSelectedTableCount(tier) == 0) {
            btnShowLockScreen.setText(getString(R.string.lock_table));
            btnShowLockScreen.setVisibility(View.GONE);
        }
        if (getSelectedTableCount(tier) > 1 &&
                !table.getStatus().equalsIgnoreCase("Joined") &&
                !table.getStatus().equalsIgnoreCase("Hold") &&
                order_type.equalsIgnoreCase("dine-in") && TextUtils.isEmpty(bill_id)) {
            btnShowLockScreen.setText(getString(R.string.join_table));
            btnShowLockScreen.setVisibility(View.VISIBLE);
        } else if (getSelectedTableCount(tier) == 0) {
            btnShowLockScreen.setText(getString(R.string.lock_table));
            btnShowLockScreen.setVisibility(View.GONE);
        } else if (getSelectedTableCount(tier) == 1 && !table.getStatus().equalsIgnoreCase("Hold") &&
                !table.getStatus().equalsIgnoreCase("Joined")) {
            btnShowLockScreen.setText(getString(R.string.lock_table));
            btnShowLockScreen.setVisibility(View.VISIBLE);
        } else if (order_type.equalsIgnoreCase("takeaway") ||
                order_type.equalsIgnoreCase("delivery") || !TextUtils.isEmpty(bill_id)) {
            if (getSelectedTableCount(tier) > 1) {
                btnShowLockScreen.setVisibility(View.GONE);
            }
        }
    }

    private void checkTableStatus(final Table table) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("This Table has Bill. Are you sure want to Continue?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tableSelection(table);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                btnShowLockScreen.setVisibility(View.GONE);
                table.setSelected(false);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /*private int getSelectedTableCount(ArrayList<Table> tableList) {
        int selectedTable = 0;
        for (Table table : tableList) {
            if (table.isSelected()) {
                selectedTable++;
            }
        }
        return selectedTable;
    }*/


    private int getSelectedTableCount(ArrayList<TierValues> tire) {
        int selectedTable = 0;
        for (TierValues values : tire) {
            for (Table table : values.getTableList()) {
                if (table.isSelected()) {
                    selectedTable++;
                    break;
                }
            }
        }
        AppLog.write("Selected_Table-", "-" + selectedTable);
        return selectedTable;
    }

    private void show_noInternet() {
        Intent i = new Intent(context, Show_NoInternetMessage_Activity.class);
        startActivity(i);
    }

    private void stop() {
        //stop P to R
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private void refreshTables() {
        if (DetectConnection.isOnline(context)) {
            new LoadTablesList().execute();
            btnShowLockScreen.setVisibility(View.GONE);
            stop();
        } else {
            show_noInternet();
            stop();
        }
    }

    private void move_to_menuPage() {
        AppLog.write("Method Called", "----------------");
        cart.clearCart();
        Intent intent = new Intent(TableActivity.this, MenuActivityWithFilters.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        if (DetectConnection.isOnline(context)) {
            switch (v.getId()) {
                case R.id.btnSwitchTable:
                    // TODO Switch Table
                    tableID = settings.getTableID();
                    AppLog.write("TableID--Switch--1", "--" + tableID);
                    if (null != dialog && dialog.isShowing())
                        dialog.dismiss();
                    callSwitchTableDialog(tableID);
                    break;
                case R.id.btnSkipDetails:
                    loadDefaultData();
                    break;
                case R.id.btnShowLockScreen:
                    new CartManager(context).clearCart();
                    Utility.customCounter = 0;
                    try {
                        callCustom_dialog(setSelectedTables());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        } else {
            show_noInternet();
        }
    }

    private void callLockTable() {
        Toast.makeText(TableActivity.this,"lock",Toast.LENGTH_LONG).show();
        cust_mobile_num = txt_MobileNumber.getText().toString();
        cust_name = txtCustomerName.getText().toString();
        memership_id=txtMemershipId.getText().toString();

        cust_mail = txtMailId.getText().toString();
        cust_address = txtAddress.getText().toString();

        ArrayList<Table> selectedTables = settings.getSelectedTables();

        if (selectedTables.size() > 1) {
            createCustomerInfo();
        } else if (!selectedTables.isEmpty()) {
            if ("Locked".equalsIgnoreCase(selectedTables.get(0).getStatus())) {
                AppLog.write("Method called---", "---");
                if (validate()) {
                    new TableLock().execute(cust_mobile_num, cust_name,memership_id, cust_mail, cust_address);
                } else {
                    AppLog.write("Not Valid---", "----");
                    dismissKeyBoard();
                }
            } else {
                createCustomerInfo();
            }
        }
    }

    private void callSwitchTableDialog(String existingTableID) {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_table_list_in_spinner);

        final Spinner spinner_table_list = dialog.findViewById(R.id.table_list_to_swap);
        Button btnCancel = dialog.findViewById(R.id.btnCancel);
        Button btnSubmit = dialog.findViewById(R.id.btnSubmit);
        AppLog.write("Table List to swap----", "" + table_list_to_swap);
        table_list_to_swap.remove(existingTableID);
        AppLog.write("TableID---callSwitchTableDialog", "-" + existingTableID);
        ArrayAdapter<String> ad = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, table_list_to_swap);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_table_list.setAdapter(ad);
        spinner_table_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String tableId = swapTableList.get(position).getId();
                AppLog.write("Swap Table List---", "" + new Gson().toJson(swapTableList.get(0).getId()));
                AppLog.write("TableID---New_Table", "" + tableId);
                spinner_selected_value_tableID = tableId;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(spinner_selected_value_tableID))
                    dialog.dismiss();
                AppLog.write("Swap table values---", "--" + TableActivity.this.tableID + "--" + spinner_selected_value_tableID);
                new SwitchTable().execute(TableActivity.this.tableID, spinner_selected_value_tableID, userName);
            }
        });
        dialog.show();
    }

    private String setSelectedTables() {
        ArrayList<Table> tables = new ArrayList<>();
        for (Table table : tableList) {
            if (table.isSelected()) {
                tables.add(table);
            }
        }
        settings.setSelectedTables(tables);
        AppLog.write("Selected Tables........", "----" + new Gson().toJson(tables));
        AppLog.write("Get_Selected_Tables----", "----" + new Gson().toJson(settings.getSelectedTables()));
        return tables.get(0).getStatus();
    }

    private void loadDefaultData() {
        cust_guest_num = txtTotalGuests.getText().toString();
        if (validate_guests_info()) {
            new GetDefaultCustomer_Details().execute();
        } else {
            AppLog.write("Not Valid---", "----");
            dismissKeyBoard();
        }
    }

    private void createCustomerInfo() {
        cust_name = txtCustomerName.getText().toString();
        memership_id=txtMemershipId.getText().toString();
        cust_mobile_num = txt_MobileNumber.getText().toString();
        cust_guest_num = txtTotalGuests.getText().toString();
        cust_mail = txtMailId.getText().toString();
        cust_address = txtAddress.getText().toString();
        AppLog.write("Valid Checking---", "--");
        if (validate()) {
            new CustomerDetailsCreate().execute(cust_mobile_num, cust_name,memership_id, cust_mail, cust_address, cust_guest_num);
            AppLog.write("Valid---", "----" + cust_guest_num);
            dismissKeyBoard();
        } else {
            AppLog.write("Not Valid---", "----");
            dismissKeyBoard();
        }
    }

    private void dismissKeyBoard() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean validate() {
        cust_name = txtCustomerName.getText().toString().trim();
        memership_id=txtMemershipId.getText().toString();
        cust_mobile_num = txt_MobileNumber.getText().toString().trim();
        cust_guest_num = txtTotalGuests.getText().toString().trim();
        cust_mail = txtMailId.getText().toString().trim();
        cust_address = txtAddress.getText().toString().trim();
        //Get Values from edit text - name,mobile,mail,remarks
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        boolean isValid = true;

        validate_guests_info();

        if (TextUtils.isEmpty(cust_address) && settings.getORDER_TYPE().equalsIgnoreCase("delivery")) {
            txtAddress.setError("Please enter address");
            isValid = false;
        } else {
            txtAddress.setError(null);
        }

        if (TextUtils.isEmpty(cust_name)) {
            txtCustomerName.setError("Please enter your name");
            isValid = false;
        } else if (txtCustomerName.getText().toString().trim().length() < 3) {
            txtCustomerName.setError("Please enter valid name");
            isValid = false;
        } else {
            txtCustomerName.setError(null);
        }
        if (TextUtils.isEmpty(memership_id)) {
            txtMemershipId.setError("Please enter your name");
            isValid = false;
        } else if (txtMemershipId.getText().toString().trim().length() < 3) {
            txtMemershipId.setError("Please enter valid name");
            isValid = false;
        } else {
            txtMemershipId.setError(null);
        }


        if (TextUtils.isEmpty(cust_mobile_num)) {
            txt_MobileNumber.setError("Please enter mobile number");
            isValid = false;
        }
        if (!TextUtils.isEmpty(cust_mail) && !(cust_mail.matches(emailPattern))) {
            txtMailId.setError("Please enter valid mail id");
            isValid = false;
        } else {
            txtMailId.setError(null);
        }
        return isValid;
    }

    private boolean validate_guests_info() {
        boolean isValid = true;
        if (TextUtils.isEmpty(cust_guest_num)) {
            txtTotalGuests.setError("Please enter number of guests");
            isValid = false;
        } else if (!(Integer.parseInt(cust_guest_num) >= 1)) {
            txtTotalGuests.setError("Please enter the value between 1-99");
            isValid = false;
        } else {
            txtTotalGuests.setError(null);
            tableID = settings.getTableID();
            AppLog.write("validate_guests_info", tableID);
        }
        return isValid;
    }

    private void setControlsVisible() {
        row1.setVisibility(View.VISIBLE);
        row2.setVisibility(View.VISIBLE);
        row22.setVisibility(View.VISIBLE);
        row3.setVisibility(View.VISIBLE);
        row4.setVisibility(View.VISIBLE);
        if (settings.getORDER_TYPE().equalsIgnoreCase("delivery")) {
            spinner_row.setVisibility(View.VISIBLE);
        } else {
            spinner_row.setVisibility(View.GONE);
        }
    }

    private void callCustom_dialog(final String tableStatus) {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.customer_details);
        initializeCustomDialog();
        new GetDeliveryPartnersList().execute();

        cust_guest_num = txtTotalGuests.getText().toString();

        txt_MobileNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String mob = txt_MobileNumber.getText().toString().trim();
                    if (!TextUtils.isEmpty(mob)) {
                        if (DetectConnection.isOnline(context))
                            loadData();
                        else {
                            show_noInternet();
                        }
                    } else {
                        clearCustomerContent();
                    }
                }
            }
        });

        if (tableStatus.equalsIgnoreCase("Locked") && (order_type.equalsIgnoreCase("dine-in") ||
                order_type.equalsIgnoreCase("rooms") || order_type.equalsIgnoreCase("takeaway"))) {
            new ShowCustomerDetails().execute();
            btnLock.setVisibility(View.VISIBLE);
            btnLock.setText("Go to Table");

            btnSkip.setVisibility(View.GONE);
            btnSwitchTable.setVisibility(View.VISIBLE);
        }
        if (getSelectedTableCount(tier) > 1) {
            btnSwitchTable.setVisibility(View.GONE);
        }
        if (order_type.equalsIgnoreCase("delivery")) {
            btnSkip.setVisibility(View.GONE);
            btnSwitchTable.setVisibility(View.GONE);
        }
        if (tableStatus.equalsIgnoreCase("Locked") && order_type.equalsIgnoreCase("delivery")) {
            new ShowCustomerDetails().execute();
            btnSkip.setVisibility(View.GONE);
            btnLock.setVisibility(View.VISIBLE);
            btnLock.setText("Go to Table");

            btnShowDetails.setVisibility(View.VISIBLE);
            clickOnShowDetails();
        }

        btnShowDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppLog.write("Table status-------", "" + tableStatus);
                if (tableStatus.equalsIgnoreCase("1")) {
                    clickOnShowDetails();
                    btnSkip.setVisibility(View.GONE);
                    new ShowCustomerDetails().execute();
                } else {
                    clickOnShowDetails();
                }
            }
        });

        btnLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cust_guest_num = txtTotalGuests.getText().toString();
                Toast.makeText(TableActivity.this,"Run success1",Toast.LENGTH_LONG).show();
                if (validate_guests_info()) {
                    callLockTable();
                    Toast.makeText(TableActivity.this,"Run success2",Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(TableActivity.this,"Run success3",Toast.LENGTH_LONG).show();
                    dismissKeyBoard();
                }
            }
        });
        dialog.show();
    }

    private void initializeCustomDialog() {
        btnShowDetails = dialog.findViewById(R.id.btnShowCustomerDetails);
        btnLock = dialog.findViewById(R.id.btnLockTable);
        btnSkip = dialog.findViewById(R.id.btnSkipDetails);
        btnSwitchTable = dialog.findViewById(R.id.btnSwitchTable);
        txt_MobileNumber = dialog.findViewById(R.id.txtcustmobno);
        txtCustomerName = dialog.findViewById(R.id.txtcustname);
        txtMemershipId=dialog.findViewById(R.id.txtmembershipid);
        txtMailId = dialog.findViewById(R.id.txtmailid);
        txtTotalGuests = dialog.findViewById(R.id.txtnoguest);
        txtAddress = dialog.findViewById(R.id.txtaddress);
        spinner_delivery_partner = dialog.findViewById(R.id.spinnerDeliveryPartner);

        row1 = dialog.findViewById(R.id.row1);
        row2 = dialog.findViewById(R.id.row2);
        row22=dialog.findViewById(R.id.row_mem);
        row3 = dialog.findViewById(R.id.row3);
        row4 = dialog.findViewById(R.id.row4);
        spinner_row = dialog.findViewById(R.id.row7);

        txtTotalGuests.setSelection(txtTotalGuests.getText().length());

        spinner_delivery_partner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1, int arg2, long arg3) {
                String partnerId = deliveryPartnerList.get(arg2).getId();
                deliveryPartnerName = deliveryPartnerList.get(arg2).getPartner_name();
                AppLog.write("Delivery partner ID-----", partnerId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                Toast.makeText(getApplicationContext(), "Nothing Selected..!", Toast.LENGTH_SHORT).show();
            }
        });
        btnSkip.setOnClickListener(this);
        btnSwitchTable.setOnClickListener(this);
    }

    private void clickOnShowDetails() {
        btnShowDetails.setVisibility(View.GONE);
        btnLock.setVisibility(View.VISIBLE);
        setControlsVisible();
        txt_MobileNumber.requestFocus();
    }

    private void loadData() {
        AppLog.write("OnFocusChanged", "True");
        String customerNo = txt_MobileNumber.getText().toString().trim();
        AppLog.write("Mobile Number---", customerNo);
        if (!TextUtils.isEmpty(customerNo)) {
            new GetCustomerDetailsByMobile().execute(customerNo);
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
    }

    private void clearCustomerContent() {
        txtCustomerName.setText("");
        txtMailId.setText("");
        txtAddress.setText("");
    }

    private void callJoinTable( String number, String name,String id, String mail, String address) {
        // TODO JoinTable implementation
        ArrayList<Table> selectedTables = settings.getSelectedTables();
        List<Integer> lockedTables = new ArrayList<>();
        List<Integer> s = new ArrayList<>();
        for (Table selectedTable : selectedTables) {
            if (!"Locked".equalsIgnoreCase(selectedTable.getStatus())) {
                s.add(Utility.convertToInteger(selectedTable.getId()));
            } else {
                lockedTables.add(Utility.convertToInteger(selectedTable.getId()));
            }
        }

        AppLog.write("Join Tables-------", "" + new Gson().toJson(s));
        String c_details = new Gson().toJson(getCustomerInfo(number, name,id, mail, address));
        AppLog.write("CustomerDetails---", "" + c_details);
        String lockedTableString = "" + (lockedTables.size() > 0 ? lockedTables.size() > 1 ? new Gson().toJson(lockedTables) : new Gson().toJson(lockedTables) : "");
        String availableTable = "" + (s.size() > 0 ? s.size() > 1 ? new Gson().toJson(s) : new Gson().toJson(s) : "");
        AppLog.write("Locked_Table-------", "-" + lockedTableString);
        AppLog.write("Available_Table-------", "-" + availableTable);
        new JoinTables().execute(lockedTableString, availableTable, c_details, getNumber(number));
    }

    private String getNumber(String number) {
        if (TextUtils.isEmpty(number)) {
            number = txt_MobileNumber.getText().toString();
        }
        AppLog.write("Mobile-", "--" + number);
        return number;
    }

    private CustomerData getCustomerInfo(String number, String name,String id, String mail, String address) {
        cust_guest_num = txtTotalGuests.getText().toString();
        if (TextUtils.isEmpty(number)) {
            name = txtCustomerName.getText().toString();
            id=txtMemershipId.getText().toString();
            number = txt_MobileNumber.getText().toString();
            mail = txtMailId.getText().toString();
            address = txtAddress.getText().toString();
        }
        CustomerData data = new CustomerData();
        data.setName(name);
        data.setName(id);
        data.setNumber(number);
        data.setEmail(mail);
        data.setAddress(address);
        data.setGuest(cust_guest_num);
        AppLog.write("C_Data----", "--" + new Gson().toJson(data));
        return data;
    }

    private void confirmAlertLogout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Are you sure want to logout?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (DetectConnection.isOnline(context))
                    new Logout().execute(userName, db_name);
                else {
                    show_noInternet();
                }
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
           /* case R.id.pending_bills:
                Intent view_progress = new Intent(context, PendingBillsActivity.class);
                startActivity(view_progress);
                return true;*/
            case R.id.logout:
                confirmAlertLogout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void check_joinTable(Table lockTable) {
        if (lockTable.getSuccess().equals("1")) {
            move_to_menuPage();
        } else if (lockTable.getSuccess().equals("2")) {
            Toast.makeText(context, "Table doesn't have join option..!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Failed to join..!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppLog.write("Destroyed", "---------------------");
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(context, SelectOrderTypeActivity.class);
        startActivity(i);
    }

    // @Override
    public void OnTableClick(String id) {
        AppLog.write("Working", "---");
        if (DetectConnection.isOnline(context)) {
            for (Table table : tableList) {
                if (table.getId().equalsIgnoreCase(id)) {
                    AppLog.write("POSITION______", "--" + new Gson().toJson(table));
                    if (getSelectedTableCount(tier) > 0 && (hasHoldTable(tier) || !table.getBill_id().isEmpty())) {
                        table.setSelected(false);
                    } else {
                        AppLog.write("SelectedTable", table.getId());
                        tableSelection(table);
                    }
                    break;
                }
            }
        } else {
            show_noInternet();
        }
    }

    @Override
    public void OnTableClick(int groupPosition, int childPosition) {
        try {
            Table table = tier.get(groupPosition).getTableList().get(childPosition);
            if (getSelectedTableCount(tier) > 0 && (hasHoldTable(tier) || !table.getBill_id().isEmpty())) {
                table.setSelected(false);
            } else {
                AppLog.write("SelectedTable", table.getId());
                tableSelection(table);
            }
            // tier.get(groupPosition).getTableList().get(childPosition).setSelected(!table.isSelected());
            tierAdapter.MyDataChanged(tier);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*
     * Load Tables
     * */
    private class LoadTablesList extends AsyncTask<Void, Void, ArrayList<Table>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<Table> doInBackground(Void... params) {
            ArrayList<Table> tables = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            tables = call.getTableList((App) getApplication(), db_name);
            return tables;
        }

        @Override
        protected void onPostExecute(ArrayList<Table> tableSelects) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            try {
                table_list_to_swap.clear();
                swapTableList.clear();
                tableList.clear();
                if (null != tableSelects) {
                    AppLog.write("Table_List", "---" + new Gson().toJson(tableSelects));
                    for (Table t : tableSelects) {
                        /*Add tier values to list*/
                        tierName.add(t.getFloor_name());
                        switch (order_type) {
                            case "dine-in":
                                if (!(t.getTables_flag().equalsIgnoreCase("0")) && !(t.getDelivery().equals("1") ||
                                        t.getTakeaway().equals("1") || t.getRooms().equals("1")) &&
                                        !(t.getTable_type().equalsIgnoreCase("Counter"))) {
                                    tableList.add(t);
                                    AppLog.write("tablesList", "---" + new Gson().toJson(tableList));
                                }
                                break;
                            case "takeaway":
                                if (t.getTables_flag().equalsIgnoreCase("2") || t.getTakeaway().equals("1")) {
                                    tableList.add(t);
                                }
                                break;
                            case "delivery":
                                if (t.getTables_flag().equalsIgnoreCase("3") || t.getDelivery().equals("1")) {
                                    tableList.add(t);
                                }
                                break;
                            case "rooms":
                                if (t.getRooms().equals("1")) {
                                    tableList.add(t);
                                }
                                break;
                        }

                        if (!(t.getStatus().equalsIgnoreCase("1") || t.getStatus().equalsIgnoreCase("2") || t.getStatus().equalsIgnoreCase("Hold")) && (!(t.getDelivery().equals("1") || t.getTakeaway().equals("1")))) {
                            table_list_to_swap.add(t.getTable_name());
                            swapTableList.add(t);
                        }
                    }

                    Set<String> tierWithoutDuplicates = new LinkedHashSet<String>(tierName);
                    tierName.clear();
                    tierName.addAll(tierWithoutDuplicates);
                    AppLog.write("TIER---", "-" + new Gson().toJson(tierName));
                    for (String s : tierName) {
                        List<Table> tables = new ArrayList<>();
                        for (Table table : tableList) {
                            String table_floor = table.getFloor_name();
                            if (table_floor.equalsIgnoreCase(s) && !TextUtils.isEmpty(table_floor)) {
                                tables.add(table);
                            }
                        }
                        TierValues val = new TierValues(s, tables);
                        tier.add(val);
                        AppLog.write("TIER~~~~~@" + s, "-" + tables.size());
                    }

                    tierAdapter.MyDataChanged(tier);
                    //tAdapter.MyDataChanged(tableList);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class GetDefaultCustomer_Details extends AsyncTask<String, Void, CustomerData> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected CustomerData doInBackground(String... params) {
            ArrayList<CustomerData> details = new ArrayList<>();
            CustomerData defaultCustomerDetails = new CustomerData();
            RestApiCalls restApiCalls = new RestApiCalls();
            details = restApiCalls.getHotelProfile((App) getApplication(), db_name);
            if (null != details && details.size() > 0)
                defaultCustomerDetails = details.get(0);
            return defaultCustomerDetails;
        }

        @Override
        protected void onPostExecute(CustomerData defaultDetails) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            //TODO Add hotel default details to shared preferences
            if (DetectConnection.isOnline(context)) {
                if (null != defaultDetails) {
                    AppLog.write("Profile---", "--" + new Gson().toJson(defaultDetails));
                    cust_mobile_num = defaultDetails.getPhone();
                    String email = defaultDetails.getEmail();
                    c_id = defaultDetails.getId();
                    AppLog.write("custom_id---", "--" + c_id);
                    if (!TextUtils.isEmpty(email)) {
                        email = defaultDetails.getEmail();
                    } else {
                        email = "info@codebase.bz";
                    }
                    if (getSelectedTableCount(tier) > 1) {
                        callJoinTable(defaultDetails.getPhone(), defaultDetails.getName(),defaultDetails.getIdd(), email, defaultDetails.getAddress());
                    } else {
                        new TableLock().execute(defaultDetails.getPhone(), defaultDetails.getName(),defaultDetails.getIdd(), email, defaultDetails.getAddress());
                    }
                    dialog.dismiss();
                }
            } else {
                show_noInternet();
            }
        }
    }

    private class TableLock extends AsyncTask<String, Void, LockTable> {

        String tableID = settings.getTableID();
        String userName = settings.getUSER_NAME();
        String password = settings.getPASSWORD();
        String cust_address = txtAddress.getText().toString();
        String cust_guest_num = txtTotalGuests.getText().toString();

        @Override
        protected LockTable doInBackground(String... params) {
            RestApiCalls restApiCalls = new RestApiCalls();
            AppLog.write("TableLock==", "==>" + tableID + "====" + cust_guest_num);
            /*return restApiCalls.lockTableModified((App) getApplication(), params[0], params[1], params[2], params[3],
                    userName, password, tableID, cust_guest_num, settings.getORDER_TYPE(), "", "0", db_name);*/
            return restApiCalls.lockTable((App) getApplication(), params[0], params[1], params[2], params[3], params[4],
                    userName, password, tableID, cust_guest_num, db_name);
        }

        @Override
        protected void onPostExecute(LockTable lockTable) {
            dialog.dismiss();
            if (DetectConnection.isOnline(context)) {
                if (null != lockTable) {
                    AppLog.write("CID", "--" + c_id + "---" + new Gson().toJson(lockTable));
                    AppLog.write("Save_bill_id", "--" + new Gson().toJson(lockTable.getSaved_bill_id()));
                    if (!TextUtils.isEmpty(c_id)) {
                        c_id = c_id.replace("\"", "");
                        settings.setCUSTOMER_ID(tableID, c_id);
                    }
                    settings.setDELIVERY_PARTNER(tableID, deliveryPartnerName);
                    settings.setTOTAL_NUMBER_OF_GUESTS(tableID, cust_guest_num);
                    settings.setMOBILE_NUMBER(tableID, cust_mobile_num);
                    settings.setCUSTOMERNAME(tableID, cust_name);
                    settings.setMEMBBERSHIPID(tableID, memership_id);
                    settings.setADDRESS(c_id, cust_address);
                    configurationSettings.setBILL__ID(tableID, lockTable.getSaved_bill_id());
                    AppLog.write("Save--", "" + configurationSettings.getBILL__ID(tableID));
                    move_to_menuPage();
                } else {
                    Toast.makeText(context, "Internal Server Error..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Please check your network connection and try again..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    class GetCustomerDetailsByMobile extends AsyncTask<String, Void, CustomerDetailsModel> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected CustomerDetailsModel doInBackground(String... params) {
            CustomerDetailsModel customerDetailsModel = new CustomerDetailsModel();
            RestApiCalls restApiCalls = new RestApiCalls();
            customerDetailsModel = restApiCalls.getCustomerDetails((App) getApplication(), params[0], db_name);
            return customerDetailsModel;
        }

        @Override
        protected void onPostExecute(CustomerDetailsModel customerDetailsModel) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != customerDetailsModel) {
                CustomerData customerData = customerDetailsModel.getCustomer_data();
                AppLog.write("Customer Info----", "--" + new Gson().toJson(customerData));
                if (null != customerData) {
                    c_id = new Gson().toJson(customerData.getId());
                    txt_MobileNumber.setText(customerData.getPhone());
                    txtCustomerName.setText(customerData.getName());
                    txtMemershipId.setText(customerData.getIdd());
                    txtMailId.setText(customerData.getEmail());
                    txtAddress.setText(customerData.getAddress());
                } else {
                    clearCustomerContent();
                }
            }
        }
    }

    private class CustomerDetailsCreate extends AsyncTask<String, Void, CreateCustomerDetails> {

        @Override
        protected CreateCustomerDetails doInBackground(String... params) {
            CreateCustomerDetails createCustomerDetails = new CreateCustomerDetails();
            RestApiCalls restApiCalls = new RestApiCalls();
            createCustomerDetails = restApiCalls.createCustomerDetails((App) getApplication(), params[0], params[1],
                    params[2], params[3], params[4], "", "0", db_name);
            return createCustomerDetails;
        }

        @Override
        protected void onPostExecute(CreateCustomerDetails customerDetails) {
            if (DetectConnection.isOnline(context)) {
                if (null != customerDetails) {
                    AppLog.write("Result--customer Details", "----" + new Gson().toJson(customerDetails));
                    if (!customerDetails.getSuccess().equalsIgnoreCase("0")) {
                        c_id = new Gson().toJson(customerDetails.getId());
                    }
                    AppLog.write("custom-Id--", "---" + c_id);
                    if (getSelectedTableCount(tier) > 1) {
                        callJoinTable(cust_mobile_num, cust_name,memership_id, cust_mail, cust_address);
                    } else {
                        new TableLock().execute(cust_mobile_num, cust_name,memership_id, cust_mail, cust_address);
                    }
                } else {
                    Toast.makeText(context, "Internal Server Error..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Please check your network connection and try again..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class GetStoreDetails extends AsyncTask<String, String, ArrayList<StoreDetails>> {

        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<StoreDetails> doInBackground(String... params) {
            ArrayList<StoreDetails> storeDetails = new ArrayList<>();
            try {
                RestApiCalls restApiCalls = new RestApiCalls();
                storeDetails = restApiCalls.getCurrentStoreDetails((App) getApplication(), userName, db_name);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return storeDetails;
        }

        @Override
        protected void onPostExecute(ArrayList<StoreDetails> storeDetails) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            try {
                if (null != storeDetails) {
                    AppLog.write("Store", "--------------------" + new Gson().toJson(storeDetails));
                    String s = storeDetails.get(0).getStore_id();
                    AppLog.write("Store ID", "----" + s);
                } else {
                    AppLog.write("Error:", "Empty values..");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class Logout extends AsyncTask<String, String, String> {
        String result;
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                final String url = App.IP_ADDRESS + "/index.php/site/logoutapi";
                AppLog.write("URL:::::::::::=>", url);
                RestTemplate restTemplate = new RestTemplate();
                List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
                messageConverters.add(new FormHttpMessageConverter());
                messageConverters.add(new StringHttpMessageConverter());
                restTemplate.setMessageConverters(messageConverters);
                MultiValueMap<String, String> part = new LinkedMultiValueMap<String, String>();
                part.add("username", userName);
                part.add("selected_db_name", db_name);
                result = restTemplate.postForObject(url, part, String.class);
                AppLog.write("Request", new Gson().toJson(restTemplate.getRequestFactory()) + "\n" + "Values sent:" + part);
                AppLog.write("Result=========>", result);
                return result;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("Logout", "--------------" + s);
            if (s != "-1" && s.equals("1")) {
                settings.clearSession();
                Toast.makeText(context, "Logged out Successfully...", Toast.LENGTH_SHORT).show();
                //Intent intent = new Intent(context, BluetoothPrint_EmployeeSalesSummary.class);
                Intent intent = new Intent(context, LoginActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(context, "Logout Failed...", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class LogoutWaiterSession extends AsyncTask<String, String, String> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            RestApiCalls call = new RestApiCalls();
            String result = call.logoutWaiterSession((App) getApplication(), db_name, userName, logout_time, referenceId, db_name);
            AppLog.write("Parameters.......", "" + db_name + "\n" + logout_time + "\n" + referenceId + "\n" + userName);
            AppLog.write("Result----->", "" + new Gson().toJson(result));
            return result;
        }

        @Override
        protected void onPostExecute(String details) {
            AppLog.write("Waiter session---", "---" + details);
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != details) {
                if (details.equalsIgnoreCase("true") || details.equals("1")) {
                    loginDB.deleteTable();
                } else {
                    AppLog.write("Error........", "..............");
                }
            }
        }
    }

    private class ShowCustomerDetails extends AsyncTask<String, String, ArrayList<DefaultCustomerDetails>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<DefaultCustomerDetails> doInBackground(String... params) {
            RestApiCalls call = new RestApiCalls();
            ArrayList<DefaultCustomerDetails> details = new ArrayList<>();
            tableID = settings.getTableID();
            AppLog.write("Values-Sent----", "--" + tableID + "----" + db_name);
            details = call.getCustomerDetailsForTableID((App) getApplication(), tableID, db_name);
            AppLog.write("Result_customerDetails----->", "--" + new Gson().toJson(details));
            return details;
        }

        @Override
        protected void onPostExecute(ArrayList<DefaultCustomerDetails> defaultCustomerDetails) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != defaultCustomerDetails) {
                try {
                    for (DefaultCustomerDetails details : defaultCustomerDetails)
                        if (null != details) {
                            AppLog.write("@@@@@@---", details.getCustomer_list().get(0).getId());
                            settings.setDELIVERY_PARTNER(tableID, deliveryPartnerName);
                            settings.setTOTAL_NUMBER_OF_GUESTS(tableID, cust_guest_num);
                            settings.setMOBILE_NUMBER(tableID, details.getCustomer_list().get(0).getPhone());
                            settings.setCUSTOMERNAME(tableID, details.getCustomer_list().get(0).getName());
                            settings.setCUSTOMER_ID(tableID, details.getCustomer_list().get(0).getId());
                            settings.setMEMBBERSHIPID(tableID, details.getCustomer_list().get(0).getIdd());
                            txt_MobileNumber.setText(details.getCustomer_list().get(0).getPhone());
                            txtCustomerName.setText(details.getCustomer_list().get(0).getName());
                            txtMemershipId.setText(details.getCustomer_list().get(0).getIdd());
                            txtMailId.setText(details.getCustomer_list().get(0).getEmail());
                            txtAddress.setText(details.getCustomer_list().get(0).getAddress());
                            txtTotalGuests.setText(details.getTable_list().get(0).getNo_of_guests());
                        } else {
                            AppLog.write("Customer Details empty", "---------");
                        }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class SwitchTable extends AsyncTask<String, String, DiscountResponse> {
        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected DiscountResponse doInBackground(String... params) {
            RestApiCalls calls = new RestApiCalls();
            AppLog.write("Switch Values---", "--" + params[0] + "--" + params[1] + "--" + params[2] + "----" + settings.getCUSTOMER_ID(params[0]));
            return calls.swapTable((App) getApplication(), params[0], params[1], settings.getCUSTOMER_ID(params[0]), db_name);
            //return calls.swapTable((App) getApplication(), params[0], params[1], settings.getCUSTOMER_ID(params[0]), configurationSettings.getBILL__ID(tableID), db_name);
        }

        @Override
        protected void onPostExecute(DiscountResponse discountResponse) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("Switch Response-----", "----" + discountResponse);
            if (null != discountResponse) {
                int response = discountResponse.getSuccess();
                if (response == 1) {
                    refreshTables();
                    table_list_to_swap.clear();
                } else {
                    Toast.makeText(context, "Failed to Swap tables...", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Failed to Swap tables...", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class JoinTables extends AsyncTask<String, String, Table> {

        @Override
        protected Table doInBackground(String... params) {
            RestApiCalls call = new RestApiCalls();
            Table lock = new Table();
            String password = settings.getPASSWORD();
            AppLog.write("Values sent----", "" + params[3] + "----" + params[2] + "locked--->" + params[0] + "available--->" + params[1]);
            lock = call.JoinMultipleTables((App) getApplication(), "", params[0], params[2], params[1], "false", params[3], cust_guest_num, "0", "", order_type, userName, db_name);
            AppLog.write("Result----->", "" + new Gson().toJson(lock));
            return lock;
        }

        @Override
        protected void onPostExecute(Table lockTable) {
            dialog.dismiss();
            if (null != lockTable) {
                AppLog.write("Table Locked", "------" + new Gson().toJson(lockTable));
                settings.setDELIVERY_PARTNER(tableID, deliveryPartnerName);
                settings.setTOTAL_NUMBER_OF_GUESTS(tableID, cust_guest_num);
                settings.setMOBILE_NUMBER(tableID, cust_mobile_num);
                settings.setCUSTOMERNAME(tableID, cust_name);
                settings.setMEMBBERSHIPID(tableID, memership_id);
                check_joinTable(lockTable);
            } else {
                Toast.makeText(context, "Internal Server Error..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class GetDeliveryPartnersList extends AsyncTask<String, String, ArrayList<DeliveryPartnerDetails>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<DeliveryPartnerDetails> doInBackground(String... params) {
            ArrayList<DeliveryPartnerDetails> details = new ArrayList<>();
            RestApiCalls restApiCalls = new RestApiCalls();
            details = restApiCalls.getDeliveryPartnersDetails((App) getApplication(), db_name);
            return details;
        }

        @Override
        protected void onPostExecute(ArrayList<DeliveryPartnerDetails> details) {

            if (details != null && !details.isEmpty()) {
                AppLog.write("Details", new Gson().toJson(details));
                deliveryPartnerList.clear();
                partnerNames.clear();
                for (DeliveryPartnerDetails partnerDetails : details) {
                    partnerNames.add(partnerDetails.getPartner_name());
                    deliveryPartnerList.add(partnerDetails);
                }
                ArrayAdapter<String> ad = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item,
                        partnerNames);
                spinner_delivery_partner.setAdapter(ad);
            }
        }
    }
}
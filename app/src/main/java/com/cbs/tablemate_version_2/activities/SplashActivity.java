package com.cbs.tablemate_version_2.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.configuration.Settings;


public class SplashActivity extends Activity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    private Settings settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        settings = new Settings(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
               /* startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                if (settings.isConfigured()) {
                    //load Login page
                    //settings.setIP_ADDRESS(IpAddress);
                    App.IP_ADDRESS=settings.getIP_ADDRESS();
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);
                } else {*/
                // load configuration page
                Intent i = new Intent(SplashActivity.this, ConfigurationActivity.class);
                startActivity(i);
                //}
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}

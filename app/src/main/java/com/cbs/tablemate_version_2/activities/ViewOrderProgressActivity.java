package com.cbs.tablemate_version_2.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.UIWidgets.DividerItemDecoration;
import com.cbs.tablemate_version_2.adapter.OrderStatusAdapter;
import com.cbs.tablemate_version_2.configuration.App;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.configuration.ConfigurationSettings;
import com.cbs.tablemate_version_2.configuration.CounterSaleSettings;
import com.cbs.tablemate_version_2.configuration.RestApiCalls;
import com.cbs.tablemate_version_2.configuration.Settings;
import com.cbs.tablemate_version_2.models.OrdersForTable;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 23-11-2016 in TableMateNew
 ***********************************************************************/
public class ViewOrderProgressActivity extends AppCompatActivity {
    private Settings settings;
    private CounterSaleSettings counterSettings;
    private ConfigurationSettings configurationSettings;
    private Context context;
    private RecyclerView ordersListView;
    private LinearLayoutManager mLayoutManager;
    private OrderStatusAdapter orderStatusAdapter;
    private ArrayList<OrdersForTable> ordersList = new ArrayList<>();
    private String tableID;
    private String db_name;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_progress);

        context = ViewOrderProgressActivity.this;

        settings = new Settings(context);
        counterSettings = new CounterSaleSettings(context);

        configurationSettings = new ConfigurationSettings(context);
        if (counterSettings.isConfigured()) {
            tableID = "0";
        } else {
            tableID = settings.getTableID();
        }
        db_name = configurationSettings.getDb_Name();

        ordersListView = findViewById(R.id.rc_list_orderProgress);
        mSwipeRefreshLayout = findViewById(R.id.swipe_container);

        AppLog.write("BILL--", "" + configurationSettings.getBILL__ID(tableID));

        orderStatusAdapter = new OrderStatusAdapter(context, ordersList);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        ordersListView.setLayoutManager(mLayoutManager);
        AppLog.write("Order Status Adapter..", "" + orderStatusAdapter);
        ordersListView.setAdapter(orderStatusAdapter);
        ordersListView.addItemDecoration(new DividerItemDecoration(context, null));

        if (DetectConnection.isOnline(context)) {
            new getOrderProgress().execute();

            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    refreshOrders();
                }
            });
        } else {
            showNoInternet();
        }

    }

    private void showNoInternet() {
        Toast.makeText(context, "Please check your internet connectivity..!", Toast.LENGTH_SHORT).show();
    }

    private void refreshOrders() {
        new getOrderProgress().execute();
    }

    private void stop() {
        //stop P to R
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private class getOrderProgress extends AsyncTask<Void, Void, ArrayList<OrdersForTable>> {

        @Override
        protected ArrayList<OrdersForTable> doInBackground(Void... params) {
            RestApiCalls call = new RestApiCalls();
            //return call.getOrdersForTable((App) getApplication(), tableID, db_name);
            return call.getBillPreview((App) getApplication(), configurationSettings.getBILL__ID(tableID), db_name);
        }

        @Override
        protected void onPostExecute(ArrayList<OrdersForTable> ordersForTables) {
            stop();
            if (null != ordersForTables) {
                ordersList.clear();
                for (OrdersForTable ordersForTable : ordersForTables) {
                    ordersList.add(ordersForTable);
                }

                if (ordersList.size() != 0) {
                    orderStatusAdapter.MyDataChanged(ordersList);
                } else {
                    Toast.makeText(context, "You have not ordered anything..!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}

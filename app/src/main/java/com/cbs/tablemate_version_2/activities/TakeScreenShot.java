package com.cbs.tablemate_version_2.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.cbs.tablemate_version_2.R;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/*********************************************************************
 * Created by Barani on 02-11-2016 in TableMateNew
 ***********************************************************************/
public class TakeScreenShot extends AppCompatActivity {

    TextView textView;
    ImageView imageView;
    Bitmap bitmap;
    Button captureScreenShot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.take_snap);


        textView = findViewById(R.id.textView);
        textView.setText("Your ScreenShot Image:");

        captureScreenShot = findViewById(R.id.capture_screen_shot);
        imageView = findViewById(R.id.imageView);
        captureScreenShot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Socket sock = new Socket("192.168.10.219", 9100);
                    PrintWriter oStream = new PrintWriter(sock.getOutputStream());
                    oStream.println("Hi,test from Android Device");
                    oStream.println("\n\n\n\f");
                    oStream.close();
                    sock.close();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    /*public void screenShot(View view) {
        bitmap = getBitmapOFRootView(captureScreenShot);
        imageView.setImageBitmap(bitmap);
        createImage(bitmap);
    }

    public Bitmap getBitmapOFRootView(View v) {
        View rootView = v.getRootView();
        rootView.setDrawingCacheEnabled(true);
        Bitmap bitmap1 = rootView.getDrawingCache();
        return bitmap1;
    }

    public void createImage(Bitmap bmp) {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);
        String mPath = Environment.getExternalStorageDirectory().toString() + "/PICTURES/Screenshots/" + now + ".jpg";
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
        File file = new File(mPath);
        try {
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);
            outputStream.write(bytes.toByteArray());
            outputStream.close();
            openScreenshot(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openScreenshot(File file) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(file);
        intent.setDataAndType(uri, "image*//*");
        startActivity(intent);
    }*/

}
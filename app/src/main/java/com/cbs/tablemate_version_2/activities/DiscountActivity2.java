package com.cbs.tablemate_version_2.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.UIWidgets.DividerItemDecoration;
import com.cbs.tablemate_version_2.UIWidgets.MyCustomDialog;
import com.cbs.tablemate_version_2.adapter.DiscountAdapterModified;
import com.cbs.tablemate_version_2.adapter.DiscountSplitAdapter;
import com.cbs.tablemate_version_2.configuration.App;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.configuration.ConfigurationSettings;
import com.cbs.tablemate_version_2.configuration.RestApiCalls;
import com.cbs.tablemate_version_2.configuration.Settings;
import com.cbs.tablemate_version_2.interfaces.DiscountListener;
import com.cbs.tablemate_version_2.interfaces.DiscountSplitListener;
import com.cbs.tablemate_version_2.models.BillPreview;
import com.cbs.tablemate_version_2.models.CustomizationPreview;
import com.cbs.tablemate_version_2.models.DiscountModel;
import com.cbs.tablemate_version_2.models.DiscountOrder;
import com.cbs.tablemate_version_2.models.DiscountPercentForEmployee;
import com.cbs.tablemate_version_2.models.DiscountResponse;
import com.cbs.tablemate_version_2.models.GetDiscount;
import com.cbs.tablemate_version_2.utilities.Utility;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

/*********************************************************************
 * Created by Barani on 04-10-2018 in TableMateNew
 ***********************************************************************/
public class DiscountActivity2 extends AppCompatActivity implements DiscountListener, DiscountSplitListener, View.OnClickListener {
    TextWatcher discountWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String discountValue = s.toString();
            /*if (!TextUtils.isEmpty(discountValue)) {
                for (BillPreview billPreview : billItemsList) {
                    ArrayList<DiscountOrder> discountOrders = new ArrayList<>();
                    int orderQuantity = Integer.parseInt(billPreview.getOrderQuantity());
                    for (int i = 0; i < orderQuantity; i++) {
                        DiscountOrder order = new DiscountOrder();
                        order.setSale_price(billPreview.getMrp_price());
                        double discountAmount = 0.0;
                        AppLog.write("Discount Type",discountType);

                        if ("percentage".equalsIgnoreCase(discountType)) {
                            //value1 = Item price * Percentage Entered / 100
                            discountAmount = (Utility.convertToDouble(order.getSale_price()) / Utility.convertToDouble(discountValue)) * 100;
                        } else if ("rupee".equalsIgnoreCase(discountType)) {
                            // value1 = Item price / sub Total * Amount Entered
                            discountAmount = (Utility.convertToDouble(order.getSale_price()) / Utility.convertToDouble(subTotal)) * Utility.convertToDouble(discountValue);
                        }
                        AppLog.write("DiscountAmount",""+discountAmount);

                        order.setDiscount("" + discountAmount);
                        order.setDiscount_type(discountType);
                        discountOrders.add(order);
                    }
                    billPreview.setDiscountOrders(discountOrders);
                }
                //double calculatedDiscount = calculateOverallTotal();
                //double calculatedDiscount = calculateDiscount(discountType, discountValue, subTotal);
                //AppLog.write("Subtotal", "--" + subTotal + calculatedDiscount);
                //txtGrandTotal.setText(String.format("%.2f", calculatedDiscount));
            }*/
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };
    private String[] discount_1 = {"₹", "%", "NC"};
    private String[] discount = {"rupee", "percentage", "NC"};
    private ArrayList<BillPreview> billItemsList = new ArrayList<>();
    private DiscountAdapterModified discountAdapter;
    private String billId, individualDiscount, overAllDiscount, subTotal, tax, discountType = "₹";
    private String remark, values;
    private String dbName;
    private String userName, password;
    private Settings settings;
    private ConfigurationSettings configurationSettings;
    private Context context;
    private RecyclerView rc_discountedItemsList;
    private TextView txtGrandTotal;
    private EditText ed_Discount;
    private TextView txtTableId;
    private TextView txtSubTotal;
    private TextView txtTax;
    private EditText et_comment;
    private Button btnUpdate, btnCancel;
    private Spinner spinner_DiscountType;
    private RadioGroup radioGroup_dis_type;
    private RadioButton rb_individual, rb_over_all;
    private String discount_type_select = "split";
    private LinearLayoutManager layoutManager;
    private LinearLayout linearList_menu_items;
    private Dialog d_dialog;
    private EditText ed_userName, ed_password;
    private String order_type, no_of_qty, table_id, table_name;
    private String d_list;
    private String discount_t = "0.0";
    private ArrayList<BillPreview> bill_orderList = new ArrayList<>();
    private ArrayList<DiscountOrder> discount_orderList = new ArrayList<>();
    private ArrayList<CustomizationPreview> customize_values_list = new ArrayList<>();

    /* private double calculateOverallTotal() {
         double total = 0.0;
         double d_t = 0;
         for (BillPreview billPreview : billItemsList) {
             if (billPreview.getDiscountOrders().size() > 0) {
                 for (DiscountOrder discountOrder : billPreview.getDiscountOrders()) {
                     d_t +=  Utility.convertToDouble(discountOrder.getDiscount());
                     AppLog.write("d_t-----", "" + d_t);
                 }
                 //total += calculateDiscount(billPreview.getDiscountType(), billPreview.getDiscount(), billPreview.getMrp_price());
                 total += (Utility.convertToDouble(billPreview.getMrp_price()) - d_t);

                 //calculateDiscount(billPreview.getDiscountType(), billPreview.getBillDiscount(), billPreview.getMrp_price());
             } else {
                 total += Utility.convertToDouble(billPreview.getMrp_price());
             }

         }
         discount_t = String.format("%.2f", d_t);
         AppLog.write("Discount-----", "---" + total + "---" + discount_t);
         return total;
     }
 */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pos_activity_discount);
        context = DiscountActivity2.this;

        settings = new Settings(context);
        configurationSettings = new ConfigurationSettings(context);
        dbName = configurationSettings.getDb_Name();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(settings.getHOTEL_NAME() + " - Table");

        order_type = settings.getORDER_TYPE();

        initControls();
        setControlsEnable(discount_type_select);

        radioGroup_dis_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId == R.id.radio_individual) {
                    discount_type_select = "split";
                    setControlsEnable(discount_type_select);
                } else if (checkedId == R.id.radio_overall) {
                    discount_type_select = "overall";
                    setControlsEnable(discount_type_select);
                }
            }
        });
        loadData();
    }

    private void setControlsEnable(String discountType) {
        AppLog.write("Discount_type", "--" + discountType);
        if (discountType.equalsIgnoreCase("split")) {
            spinner_DiscountType.setEnabled(false);
            ed_Discount.setEnabled(false);
        } else if (discountType.equalsIgnoreCase("overall")) {
            spinner_DiscountType.setEnabled(true);
            ed_Discount.setEnabled(true);
        }
    }

    private void loadData() {
        Intent intent = getIntent();
        billId = intent.getStringExtra("BILL_ID");
        String billList = intent.getStringExtra("BILL_VALUES");
        String c_list = intent.getStringExtra("CUSTOMIZE_VALUES");
        subTotal = intent.getStringExtra("SUB_TOTAL");
        no_of_qty = intent.getStringExtra("TOTAL_QUANTITY");
        tax = intent.getStringExtra("TAX");
        table_id = intent.getStringExtra("TABLE_ID");
        table_name = intent.getStringExtra("TABLE_NAME");
        AppLog.write("POS_Bill List-----------##", "" + billList + "\n" + subTotal);

        txtGrandTotal.setText(String.format("%.2f", Utility.convertToDouble(subTotal)));
        txtSubTotal.setText(String.format("%.2f", Utility.convertToDouble(subTotal)));
        new GetValuesBillDiscount().execute(billId);
        try {
            billItemsList = new ArrayList<>(Arrays.asList(new Gson().fromJson(billList, BillPreview[].class)));
            customize_values_list = new ArrayList<>(Arrays.asList(new Gson().fromJson(c_list, CustomizationPreview[].class)));
            discountAdapter = new DiscountAdapterModified(bill_orderList, customize_values_list, this);
            layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            rc_discountedItemsList.setLayoutManager(layoutManager);
            rc_discountedItemsList.setAdapter(discountAdapter);
            txtTax.setText("" + tax);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void initControls() {
        txtTableId = findViewById(R.id.txtTableId);
        txtSubTotal = findViewById(R.id.txtSubTotal);
        txtTax = findViewById(R.id.txtTax);
        txtGrandTotal = findViewById(R.id.txtGrandTotal);
        ed_Discount = findViewById(R.id.edit_Discount);
        et_comment = findViewById(R.id.edit_comment);
        btnUpdate = findViewById(R.id.btnUpdate);
        btnCancel = findViewById(R.id.btnCancel);
        spinner_DiscountType = findViewById(R.id.spinnerDiscountType);
        radioGroup_dis_type = findViewById(R.id.rgroup_dis_type);
        rb_individual = findViewById(R.id.radio_individual);
        rb_over_all = findViewById(R.id.radio_overall);
        rc_discountedItemsList = findViewById(R.id.rc_discountList);
        linearList_menu_items = findViewById(R.id.linearList_menu_items);
        ed_Discount.addTextChangedListener(discountWatcher);
        btnCancel.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);
        ArrayAdapter<String> disAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, discount_1);
        spinner_DiscountType.setAdapter(disAdapter);

        spinner_DiscountType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String d = discount_1[position];
                discountType = discount[position];
                AppLog.write("Key_Values~~~", "---" + d + "---" + discountType);
                String discountValue = ed_Discount.getText().toString();
                if (!TextUtils.isEmpty(discountValue)) {
                    double calculatedDiscount = calculateDiscount(discountType, discountValue, subTotal);
                    AppLog.write("Subtotal", "-" + subTotal + "--" + calculatedDiscount);
                    txtGrandTotal.setText(String.format("%.2f", calculatedDiscount));
                    updateDiscountForItems(calculatedDiscount);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void updateDiscountForItems(double calculatedDiscount) {
        double discount = calculatedDiscount / billItemsList.size();
        for (BillPreview billPreview : billItemsList) {
            billPreview.setBillDiscount(String.valueOf(discount));
            billPreview.setDiscountType("rupee");
        }
        discountAdapter.myDataSetChanged(bill_orderList);
    }

    private double calculateDiscount(String discountType, String discountValue, String totalAmount) {
        Double discountAmount = 0.0;
        Double amount = Utility.convertToDouble(totalAmount);
        if (!TextUtils.isEmpty(discountType)) {
            if ("percentage".equalsIgnoreCase(discountType)) {
                discountAmount = amount * (Utility.convertToDouble(discountValue) / 100);
            } else if ("rupee".equalsIgnoreCase(discountType)) {
                discountAmount = Utility.convertToDouble(discountValue);
            } else if ("NC".equalsIgnoreCase(discountType)) {
                discountAmount = amount;
            }
        }
        AppLog.write("Individual_discount_val---", "--" + (amount - discountAmount));
        return amount - discountAmount;
    }

    private void confirm() {
        d_dialog = new Dialog(context);
        d_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d_dialog.setContentView(R.layout.pos_verify_credential_discount);
        ed_userName = d_dialog.findViewById(R.id.ed_userName);
        ed_password = d_dialog.findViewById(R.id.ed_password);
        ed_password.setTypeface(Typeface.createFromAsset(getAssets(), "Proxima_Nova_Regular.otf"));
        Button btnCancel = d_dialog.findViewById(R.id.btnCancel);
        Button btnSubmit = d_dialog.findViewById(R.id.btnSubmit);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d_dialog.cancel();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userName = ed_userName.getText().toString().trim();
                password = ed_password.getText().toString().trim();
                if (validate()) {
                    new DiscountConfirmation().execute(userName, password);
                    dismissKeyBoard();
                } else {
                    dismissKeyBoard();
                }
            }
        });
        d_dialog.show();
    }

    private boolean validate() {
        boolean isValid = true;
        password = ed_password.getText().toString().trim();
        if (TextUtils.isEmpty(password)) {
            ed_password.setError("Please enter the password..!");
            isValid = false;
        } else {
            ed_password.setError(null);
        }
        return isValid;
    }

    private void dismissKeyBoard() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ArrayList<DiscountOrder> collectOrders(ArrayList<BillPreview> orderList) {
        ArrayList<DiscountOrder> orders = new ArrayList<>();
        String d_type = "";
        d_type = discountType;
        String d_price = ed_Discount.getText().toString();
        AppLog.write("Value--1", "--" + d_price + "--" + d_type);
        Double discountTotal = calculateDiscountTotal(d_price, d_type);
        AppLog.write("Discount_total----", "---" + discountTotal);

        for (BillPreview billPreview : orderList) {
            DiscountOrder order = new DiscountOrder();
            order.setOrder_id(billPreview.getOrder_id());
            order.setDiscount_value(discountTotal);
            order.setItem_price(Utility.convertToDouble(billPreview.getMrp_price()));
            if (!TextUtils.isEmpty(d_price)) {
                Double dis_price = Utility.convertToDouble(billPreview.getMrp_price()) - discountTotal;
                order.setDiscount_price("" + dis_price);
            } else {
                order.setDiscount_price("0.00");
            }
            AppLog.write("Type---", "--" + billPreview.getDiscountType());
            order.setDiscount_type(d_type);
            orders.add(order);
        }
        return orders;
    }

    private Double calculateDiscountTotal(String d_price, String d_type) {
        Double dis_total = 0.0;
        AppLog.write("Values----Discount", "--" + d_price + "---" + dis_total + "---" + d_type);
        if (d_type.equalsIgnoreCase("rupee")) {
            dis_total = Utility.convertToDouble(ed_Discount.getText().toString()) / Utility.convertToDouble(no_of_qty);
        } else if (d_type.equalsIgnoreCase("percentage")) {
            Double percentValue = (Utility.convertToDouble(ed_Discount.getText().toString()) * Utility.convertToDouble(subTotal)) / 100;
            AppLog.write("Values---percent", "--" + percentValue);
            dis_total = percentValue / Utility.convertToDouble(no_of_qty);
        } else if (d_type.equalsIgnoreCase("NC")) {
            dis_total = 0.0;
        }
        return dis_total;
    }

    @Override
    public void onDiscountAdded(String discountType, String discountAmount, int position, boolean isChecked) {
        DiscountOrder order = discount_orderList.get(position);
        AppLog.write("TAG----@", "Position: " + position + "--" + isChecked + "--" + new Gson().toJson(order));

        if (isChecked) {
            order.setSale_price(order.getMrp_price());
            order.setDiscount(discountAmount);
            order.setDiscount_type(discountType);
        } else {
            order.setSale_price("");
            order.setDiscount("");
            order.setDiscount_type("");
        }
        order.setChecked(isChecked);
        //discount_orderList.set(position, order);
    }

    @SuppressLint("DefaultLocale")
    private double calculateTotal() {
        double total = 0.0;
        double d_t = 0;
        for (BillPreview billPreview : billItemsList) {
            if (billPreview.getDiscountOrders().size() > 0) {
                for (DiscountOrder discountOrder : billPreview.getDiscountOrders()) {
                    AppLog.write("DiscountAmount", discountOrder.getMrp_price());
                    String dType = discountOrder.getDiscount_type();
                    double discountAmount = 0;
                    Double amount = Utility.convertToDouble(discountOrder.getMrp_price());

                    if (!TextUtils.isEmpty(dType)) {
                        if ("percentage".equalsIgnoreCase(dType)) {
                            discountAmount = amount * (Utility.convertToDouble(discountOrder.getDiscount()) / 100);
                        } else if ("rupee".equalsIgnoreCase(dType)) {
                            discountAmount = Utility.convertToDouble(discountOrder.getDiscount());
                        } else if ("NC".equalsIgnoreCase(dType)) {
                            discountAmount = amount;
                        }
                    }
                    d_t += discountAmount;
                    total += Utility.convertToDouble(billPreview.getMrp_price()) - d_t;
                }
                //total += calculateDiscount(billPreview.getDiscountType(), billPreview.getBillDiscount(), billPreview.getMrp_price());
            } else {
                total += Utility.convertToDouble(billPreview.getMrp_price());
            }
            AppLog.write("Discount-----", "---" + total + "---" + billPreview.getDiscount());
        }
        return total;
    }

    @Override
    public void onDiscountTypeChanged(int DiscountPosition, int position) {
        billItemsList.get(position).setDiscountType(discount[DiscountPosition]);
    }

    @Override
    public void onDiscountClick(int position) {
        if (discount_type_select.equalsIgnoreCase("split")) {
            callDiscountSplitAdapter(position);
        }
    }

    private void callDiscountSplitAdapter(int position) {
        final BillPreview billPreview = bill_orderList.get(position);
        String menu_id = billPreview.getMenu_item_id();
        String custom_data = billPreview.getCustom_data();
        String bill = billPreview.getBill_id();
        if (billPreview.getDiscountOrders().size() > 0) {
            showDiscountDialog(billPreview.getDiscountOrders());
        } else {
            new ExpandableDiscountValues().execute(bill, menu_id, custom_data);
        }
    }

    private void showDiscountDialog(final ArrayList<DiscountOrder> discount_orderList) {
        AppLog.write("DiscountOrder", new Gson().toJson(discount_orderList));
        this.discount_orderList = discount_orderList;

        d_dialog = new Dialog(context);
        d_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = getLayoutInflater().inflate(R.layout.activity_modify_price, null);
        d_dialog.setContentView(view);
        RecyclerView rv_OrdersList = view.findViewById(R.id.rvMenuItemsList);
        DiscountSplitAdapter itemAdapter = new DiscountSplitAdapter(context, discount_orderList);
        itemAdapter.setDiscountListener(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv_OrdersList.addItemDecoration(new DividerItemDecoration(context, null));
        rv_OrdersList.setLayoutManager(layoutManager);
        rv_OrdersList.setAdapter(itemAdapter);

        Button update = view.findViewById(R.id.btn_Update);
        Button cancel = view.findViewById(R.id.btn_Cancel);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d_dialog.dismiss();
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String itemId = discount_orderList.get(0).getMenu_item_id();
                for (BillPreview billPreview : billItemsList) {
                    if (billPreview.getMenu_item_id().equalsIgnoreCase(itemId)) {
                        billPreview.setDiscountOrders(discount_orderList);
                    }
                }

                double total = calculateTotal();
                txtGrandTotal.setText(String.format("%.2f", total));
                ed_Discount.setText(discount_t);
                d_dialog.dismiss();
            }
        });
        d_dialog.show();
    }

    private String formList() {
        ArrayList<DiscountModel> list = new ArrayList<>();
        for (BillPreview billPreview : billItemsList) {
            if (billPreview.getDiscountOrders().size() > 0)
                for (DiscountOrder d_order : billPreview.getDiscountOrders()) {
                    if (d_order.isChecked()) {
                        double price = Utility.convertToDouble(d_order.getMrp_price()) - Utility.convertToDouble(d_order.getDiscount());
                        DiscountModel item = new DiscountModel(d_order.getId(), d_order.getDiscount(), String.valueOf(price),
                                d_order.getDiscount_type());
                        list.add(item);
                    }
                }
        }
        return new Gson().toJson(list);
    }


    private void updateBillOrderListValues(ArrayList<DiscountModel> list) {
        BillPreview bill = new BillPreview();
        for (DiscountModel d_m : list) {
            bill.setBillDiscount(d_m.getDiscount());
            bill.setDiscountType("rupee");
        }
        discountAdapter.myDataSetChanged(bill_orderList);
    }

    private boolean validateDiscountAmount(double discounted_amount, String discount_type, BillPreview billPreview) {
        boolean isValid = true;
        if ((discount_type.equalsIgnoreCase("rupee") && !(discounted_amount <= billPreview.getPrice())) || (discount_type.equalsIgnoreCase("%") && !(discounted_amount <= 100))) {
            AppLog.write("Hit_1--", "--" + isValid);
            Toast.makeText(context, "Amount cannot be greater than the original price", Toast.LENGTH_SHORT).show();
            isValid = false;
        } /*else if (discount_type.equalsIgnoreCase("%") && !(discounted_amount <= 100)) {
            AppLog.write("Hit 2--", "--" + isValid);
            Toast.makeText(context, "Amount cannot be greater than the original price", Toast.LENGTH_SHORT).show();
            isValid = false;}*/ else {
            return isValid;
        }
        return isValid;
    }

    private void set_discount_type_editable_values(EditText edit_discountAmount) {
        edit_discountAmount.setBackgroundResource(R.drawable.editext_style_normal);
        edit_discountAmount.setHint("Enter discount amount..");
        edit_discountAmount.setEnabled(true);
    }

    @Override
    public void onClick(View v) {
        int i1 = v.getId();
        if (i1 == R.id.btnCancel) {
            setResult(Activity.RESULT_CANCELED);
            finish();
        } else if (i1 == R.id.btnUpdate) {
            String discountedAmount = ed_Discount.getText().toString().trim();
            double discountAmount = Utility.convertToDouble(discountedAmount);
            double total = Utility.convertToDouble(subTotal);
            double grandTotal = Utility.convertToDouble(txtGrandTotal.getText().toString());
            AppLog.write("Discount Total", "---" + total + "--" + grandTotal);
            settings.setDiscountAmount(billId, discountAmount);
            if (discountAmount <= total) {
                String comment = et_comment.getText().toString().trim();
                if (!TextUtils.isEmpty(comment)) {
                    //confirm();
                    userName = settings.getUSER_NAME();
                    password = settings.getPASSWORD();
                    new DiscountConfirmation().execute(userName, password);
                } else {
                    Toast.makeText(context, "Don't leave comment field empty..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Please enter valid amount..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //Individual Discount
    private ArrayList<DiscountOrder> collectValuesForIndividualDiscount(ArrayList<BillPreview> billList) {
        ArrayList<DiscountOrder> orders = new ArrayList<>();
        String d_type = "rupee";
        String d_price = ed_Discount.getText().toString();

        for (BillPreview billPreview : billList) {
            DiscountOrder order = new DiscountOrder();
            order.setOrder_id(billPreview.getOrder_id());
            order.setDiscount_value(Utility.convertToDouble(billPreview.getBillDiscount()));
            order.setItem_price(Utility.convertToDouble(billPreview.getMrp_price()));
            if (!TextUtils.isEmpty(d_price)) {
                Double dis_price = Utility.convertToDouble(billPreview.getMrp_price()) - Utility.convertToDouble(billPreview.getBillDiscount());
                order.setDiscount_price(String.valueOf(dis_price));
            } else {
                order.setDiscount_price("0.00");
            }
            if (!TextUtils.isEmpty(billPreview.getDiscountType())) {
                d_type = billPreview.getDiscountType();
            }
            order.setDiscount_type(d_type);
            orders.add(order);
        }
        return orders;
    }

    private String getOverallDiscount(String discountType, String overAllDiscount) {
        String dis_total = "";
        if (discountType.equalsIgnoreCase("rupee")) {
            dis_total = overAllDiscount;
        } else if (discountType.equalsIgnoreCase("percentage")) {
            Double percentValue = (Utility.convertToDouble(ed_Discount.getText().toString()) * Utility.convertToDouble(subTotal)) / 100;
            dis_total = String.valueOf(percentValue);
        } else if (discountType.equalsIgnoreCase("NC")) {
            dis_total = "0.0";
        }
        return dis_total;
    }

    private void getDiscountPercentageAuthentication(String userName) {
        new GetDiscountPercentageForEmployee().execute(userName);
    }

    private void calculateApplicableDiscountForEmployee(double discountPercentage) {
        if (discount_type_select.equalsIgnoreCase("split")) {
            values = new Gson().toJson(collectValuesForIndividualDiscount(bill_orderList));
            overAllDiscount = "0";
            individualDiscount = ed_Discount.getText().toString();
        } else {
            values = new Gson().toJson(collectOrders(bill_orderList));
            individualDiscount = "0";
            overAllDiscount = getOverallDiscount(discountType, ed_Discount.getText().toString());
        }

        Double total = Utility.convertToDouble(subTotal) - Utility.convertToDouble(overAllDiscount);
        remark = et_comment.getText().toString();
        String g_Total = String.valueOf(total);
        AppLog.write("Grand total-=>", "--" + g_Total);
        AppLog.write("Values------=>", "--" + values + "--" + individualDiscount + "--" + overAllDiscount + "--" + discount_type_select);
        confirmDiscount(overAllDiscount, individualDiscount, discountPercentage, g_Total, values, remark);
    }

    private void confirmDiscount(String overAllDiscount, String individualDiscount, double discountPercentage,
                                 String g_Total, String values, String remark) {
        double overall = Utility.convertToDouble(overAllDiscount);
        double individual = Utility.convertToDouble(individualDiscount);
        double total = Utility.convertToDouble(subTotal);
        double percentage_value;
        if (overall != 0 && individual == 0) {
            percentage_value = overall / total * 100;
        } else {
            percentage_value = individual / total * 100;
        }
        AppLog.write("Discount_percentage", "----" + percentage_value);
        if (percentage_value <= discountPercentage) {
            //new DiscountUpdate().execute(billId, individualDiscount, g_Total, values, remark, overAllDiscount);
            String discountList = formList();
            AppLog.write("Discount_ORDER", "----" + discountList);
            //new DiscountUpdate().execute(discountList, remark);
        } else {
            Toast.makeText(context, "Cant proceed with discount..!Please Enter Discount Value between" + discountPercentage + "%", Toast.LENGTH_SHORT).show();
        }
    }

    private class DiscountUpdate extends AsyncTask<String, Void, DiscountResponse> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected DiscountResponse doInBackground(String... params) {
            RestApiCalls call = new RestApiCalls();
            //DiscountResponse result = call.getDiscountModified((App) getApplication(), billId, params[1], params[2], params[3], params[4], params[5], order_type, "false", "", "0", dbName);
            DiscountResponse result = call.getDiscountModified_updated((App) getApplication(), params[0], "", billId, discount_type_select, discount_t, params[1], dbName);
            AppLog.write("Checking", "--" + params[0] + "--");
            return result;
        }

        @Override
        protected void onPostExecute(DiscountResponse discountResponse) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("Response", ":::::::::::" + new Gson().toJson(discountResponse));
            if (null != discountResponse) {
                int response = discountResponse.getSuccess();
                AppLog.write("Success message--", "--" + new Gson().toJson(discountResponse));
                if (response != -1) {
                    if (response == 1) {
                        d_dialog.dismiss();
                        Intent data = new Intent(context, BillPreviewActivity.class);
                        data.putExtra("BILL_ID", billId);
                        data.putExtra("TABLE_ID", table_id);
                        data.putExtra("TABLE_NAME", table_name);
                        startActivity(data);
                    } else {
                        Toast.makeText(context, "Failed to update...", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "Failed to update...", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private class DiscountConfirmation extends AsyncTask<String, String, String> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Processing..");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            RestApiCalls calls = new RestApiCalls();
            AppLog.write("Validation Result----->", params[0] + params[1] + dbName);
            return calls.discountConfirmation((App) getApplication(), params[0], params[1], dbName);
        }

        @Override
        protected void onPostExecute(String s) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("Discount validate result---", "------" + s);
            if (s != null) {
                if (s.equals("0")) {
                    dialog.dismiss();
                    getDiscountPercentageAuthentication(userName);
                } else if (s.equals("1")) {
                    Toast.makeText(context, "Incorrect Username..!", Toast.LENGTH_SHORT).show();
                } else if (s.equals("2")) {
                    Toast.makeText(context, "Incorrect Password..!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Permission Denied..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Failed to update discount..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class GetValuesBillDiscount extends AsyncTask<String, String, GetDiscount> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected GetDiscount doInBackground(String... params) {
            RestApiCalls call = new RestApiCalls();
            return call.getMenuItemsAfterBillGeneration((App) getApplication(), params[0], dbName);
        }

        @Override
        protected void onPostExecute(GetDiscount billedMenuItemsList) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            bill_orderList.clear();
            if (null != billedMenuItemsList) {
                bill_orderList = billedMenuItemsList.getOrdereditems();
                collectOrders(bill_orderList);
                if (bill_orderList.size() != 0) {
                    discountAdapter.myDataSetChanged(bill_orderList);
                } else {
                    Toast.makeText(context, "Nothing to display..!", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class GetDiscountPercentageForEmployee extends AsyncTask<String, String, DiscountPercentForEmployee> {
        MyCustomDialog dialog = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected DiscountPercentForEmployee doInBackground(String... params) {
            RestApiCalls restApiCalls = new RestApiCalls();
            return restApiCalls.discountPercentApplicableForEmployee((App) getApplication(), params[0], dbName);
        }

        @Override
        protected void onPostExecute(DiscountPercentForEmployee s) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            double discountPercentage = Utility.convertToDouble(s.getDiscount_percentage());
            AppLog.write("Discount Percentage---", "--" + s.getDiscount_percentage());
            calculateApplicableDiscountForEmployee(discountPercentage);
        }
    }

    private class ExpandableDiscountValues extends AsyncTask<String, String, ArrayList<DiscountOrder>> {

        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<DiscountOrder> doInBackground(String... params) {
            RestApiCalls call = new RestApiCalls();
            return call.get_split_orders_for_discount((App) getApplication(), params[0], params[1], params[2], dbName);
        }

        @Override
        protected void onPostExecute(ArrayList<DiscountOrder> order) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("Discount_Result", "--" + new Gson().toJson(order));
            discount_orderList.clear();
            if (null != order && order.size() > 0) {
                String menuId = order.get(0).getMenu_item_id();
                for (BillPreview billPreview : billItemsList) {
                    if (billPreview.getMenu_item_id().equalsIgnoreCase(menuId)) {
                        billPreview.setDiscountOrders(order);
                        showDiscountDialog(billPreview.getDiscountOrders());
                        break;
                    }
                }
                // discount_orderList.addAll(order);
            }
            // AppLog.write("LIST_DISCOUNT",""+new Gson().toJson(discount_orderList));
            //showDiscountDialog(discount_orderList);
        }
    }
}
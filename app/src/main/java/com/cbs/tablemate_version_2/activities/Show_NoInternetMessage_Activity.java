package com.cbs.tablemate_version_2.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.cbs.tablemate_version_2.R;

/*********************************************************************
 * Created by Barani on 06-10-2016 in TableMateNew
 ***********************************************************************/
public class Show_NoInternetMessage_Activity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.no_internet);
    }
}

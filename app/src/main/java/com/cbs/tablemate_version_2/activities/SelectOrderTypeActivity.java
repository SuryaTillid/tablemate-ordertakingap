package com.cbs.tablemate_version_2.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.UIWidgets.MyCustomDialog;
import com.cbs.tablemate_version_2.configuration.App;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.configuration.ConfigurationSettings;
import com.cbs.tablemate_version_2.configuration.CounterSaleSettings;
import com.cbs.tablemate_version_2.configuration.RestApiCalls;
import com.cbs.tablemate_version_2.configuration.Settings;
import com.cbs.tablemate_version_2.helper.ConnectivityReceiver;
import com.cbs.tablemate_version_2.models.AuthorizeEmployee;
import com.cbs.tablemate_version_2.models.CustomerData;
import com.cbs.tablemate_version_2.models.Employee_Data;
import com.cbs.tablemate_version_2.models.LockTable;
import com.cbs.tablemate_version_2.models.SuccessMessage;
import com.google.gson.Gson;

import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*********************************************************************
 * Created by Barani on 16-02-2017 in TableMateNew
 ***********************************************************************/
public class SelectOrderTypeActivity extends AppCompatActivity implements View.OnClickListener,
        ConnectivityReceiver.ConnectivityReceiverListener {

    SimpleDateFormat timeFormat = new SimpleDateFormat("MMddHHmmss");
    private Button btn_dineIn, btn_takeaway, btn_delivery, btn_rooms, btn_NC, btn_CounterSales;
    private String order_type;
    private Context context;
    private Settings settings;
    private CounterSaleSettings counterSettings;
    private ConfigurationSettings configSettings;
    private String db_name, userName;
    private String c_id, store_name, user_id;
    private String start_Date, end_date;
    private String otp_stamp, primary_num, secondary_num;
    private String s_mobile, s_guest, s_pax, s_authorize_emp, s_req_emp;
    private long current_date = System.currentTimeMillis();
    private ArrayList<String> employeeArrayList = new ArrayList<>();
    private ArrayList<AuthorizeEmployee> auth_emp_List = new ArrayList<>();
    private Dialog o_dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_type);

        context = SelectOrderTypeActivity.this;
        settings = new Settings(context);
        configSettings = new ConfigurationSettings(context);
        counterSettings = new CounterSaleSettings(context);
        db_name = settings.getDbName();
        userName = settings.getUSER_NAME();
        store_name = configSettings.getSTORE_NAME();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(configSettings.getHOTEL_NAME() + " - Order Type");

        btn_dineIn = findViewById(R.id.btn_dineIn);
        btn_takeaway = findViewById(R.id.btn_takeAway);
        btn_delivery = findViewById(R.id.btn_delivery);
        btn_rooms = findViewById(R.id.btn_rooms);
        btn_NC = findViewById(R.id.btn_NC);
        btn_CounterSales = findViewById(R.id.btn_CounterSales);
        btn_dineIn.setOnClickListener(this);
        btn_takeaway.setOnClickListener(this);
        btn_delivery.setOnClickListener(this);
        btn_rooms.setOnClickListener(this);

        btn_CounterSales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadDefaultCustomerDetails();
            }
        });

        btn_NC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetails_for_NC();
            }
        });

        new GetAuthorizeEmployeeInfo().execute();
    }

    private void loadDefaultCustomerDetails() {
        new GetDefaultCustomer_Details().execute();
    }

    @Override
    public void onClick(View v) {
        counterSettings.clearSession();
        switch (v.getId()) {
            case R.id.btn_dineIn:
                order_type = "1";
                break;
            case R.id.btn_takeAway:
                order_type = "2";
                break;
            case R.id.btn_delivery:
                order_type = "3";
                break;
            case R.id.btn_rooms:
                order_type = "4";
                break;
        }

        boolean isConnected = ConnectivityReceiver.isConnected();
        if (isConnected) {
            Intent i = new Intent(context, TableActivity_1.class);
            settings.setORDER_TYPE(order_type);
            AppLog.write("ORDERTYPE", "" + settings.getORDER_TYPE());
            startActivity(i);
        } else {
            Intent i = new Intent(context, Show_NoInternetMessage_Activity.class);
            startActivity(i);
        }
    }

    private void showDetails_for_NC() {
        o_dialog = new Dialog(context);
        o_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        o_dialog.setContentView(R.layout.activity_nc);

        final Spinner spinner_table_list = o_dialog.findViewById(R.id.sp_authorize_employee);
        Button btnCancel = o_dialog.findViewById(R.id.btnCancel);
        Button btnGenerate = o_dialog.findViewById(R.id.btnGenerate);
        final EditText edit_guest_name = o_dialog.findViewById(R.id.edit_guest);
        final EditText edit_requesting_emp = o_dialog.findViewById(R.id.edit_req_employee);
        final EditText edit_pax = o_dialog.findViewById(R.id.edit_pax);

        ArrayAdapter<String> ad = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, employeeArrayList);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_table_list.setAdapter(ad);
        spinner_table_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AuthorizeEmployee emp = auth_emp_List.get(position);
                s_authorize_emp = emp.getName();
                primary_num = emp.getPrimary_ph_no();
                secondary_num = emp.getSec_ph_no();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                o_dialog.dismiss();
            }
        });
        btnGenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otp_stamp = timeFormat.format(current_date);
                if (validate(edit_guest_name, edit_pax, edit_requesting_emp)) {
                    s_mobile = validate_mob_num(primary_num, secondary_num);
                    s_guest = edit_guest_name.getText().toString().trim();
                    s_pax = edit_pax.getText().toString().trim();
                    s_req_emp = edit_requesting_emp.getText().toString().trim();
                    String s = "Guest : " + s_guest + "\n" + "PAX : " + s_pax + "\n" + "Requested by : " + s_req_emp + "\n" + "OTP : " + otp_stamp;
                    new SendOTP_via_SMS().execute(s, s_mobile);
                }
            }
        });
        o_dialog.show();
    }

    private boolean validate(EditText edit_guest_name, EditText edit_pax, EditText edit_requesting_emp) {
        String guest = edit_guest_name.getText().toString().trim();
        String pax = edit_pax.getText().toString().trim();
        String req_emp = edit_requesting_emp.getText().toString().trim();
        boolean isValid = true;

        if (TextUtils.isEmpty(guest)) {
            edit_guest_name.setError("Please Enter Guest Name");
            isValid = false;
        } else {
            edit_guest_name.setError(null);
        }

        if (TextUtils.isEmpty(pax)) {
            edit_pax.setError("Please Enter PAX Count");
            isValid = false;
        } else {
            edit_pax.setError(null);
        }

        if (TextUtils.isEmpty(req_emp)) {
            edit_requesting_emp.setError("Please Enter Employee Name");
            isValid = false;
        } else {
            edit_requesting_emp.setError(null);
        }
        return isValid;
    }

    private String validate_mob_num(String primary_num, String secondary_num) {
        String mobile;
        if (!TextUtils.isEmpty(secondary_num)) {
            mobile = primary_num + "," + secondary_num;
        } else {
            mobile = primary_num;
        }
        return mobile;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
    }

    @Override
    public void onBackPressed() {
        confirmAlertLogout();
    }

    private void callPrintPage() {
        Intent view_progress = new Intent(context, PrintSalesSummary.class);
        startActivity(view_progress);
    }

    private void confirmAlertLogout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Are you sure want to logout?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (DetectConnection.isOnline(context))
                    new Logout().execute(userName, db_name);
                else {
                    show_noInternet();
                }
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void show_noInternet() {
        Intent i = new Intent(context, Show_NoInternetMessage_Activity.class);
        startActivity(i);
    }

    private void move_to_menuPage() {
        counterSettings.setDb_Name(db_name);
        counterSettings.isConfigured();
        AppLog.write("IS_CONFIGURED--", "--" + counterSettings.isConfigured());
        if (counterSettings.isConfigured()) {
            Intent intent = new Intent(context, MenuActivityWithFilters.class);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_sale_summary, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
           /* case R.id.ic_sales_summary:
                getDate();
                callPrintPage();
                //new SendSalesSummary_E_Mail().execute();
                return true;*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getDate() {
        Date curDate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        start_Date = format.format(curDate) + " " + "00:00:00";
        end_date = format.format(curDate) + " " + "23:59:59";
    }

    private class SendSalesSummary_E_Mail extends AsyncTask<String, String, SuccessMessage> {

        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Submitting...");
            dialog.show();
        }

        @Override
        protected SuccessMessage doInBackground(String... params) {
            SuccessMessage submit;
            RestApiCalls calls = new RestApiCalls();
            submit = calls.sendSalesSummaryMail((App) getApplication(), store_name, start_Date, end_date, db_name);
            AppLog.write("Value------", "" + new Gson().toJson(submit));
            return submit;
        }

        @Override
        protected void onPostExecute(SuccessMessage message) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            callPrintPage();
            if (message != null) {
                String success_message = message.getMessage();
                if (!TextUtils.isEmpty(success_message)) {
                    AppLog.write("Message---", "Success");
                }
            }
        }
    }

    private class Logout extends AsyncTask<String, String, String> {
        String result;
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                final String url = App.IP_ADDRESS + "/index.php/site/logoutapi";
                AppLog.write("URL:::::::::::=>", url);
                RestTemplate restTemplate = new RestTemplate();
                List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
                messageConverters.add(new FormHttpMessageConverter());
                messageConverters.add(new StringHttpMessageConverter());
                restTemplate.setMessageConverters(messageConverters);
                MultiValueMap<String, String> part = new LinkedMultiValueMap<String, String>();
                part.add("username", userName);
                part.add("selected_db_name", db_name);
                result = restTemplate.postForObject(url, part, String.class);
                AppLog.write("Request", new Gson().toJson(restTemplate.getRequestFactory()) + "\n" + "Values sent:" + part);
                AppLog.write("Result=========>", result);
                return result;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("Logout", "--------------" + s);
            if (s != "-1" && s.equals("1")) {
                settings.clearSession();
                Toast.makeText(context, "Logged out Successfully...", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, LoginActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(context, "Logout Failed...", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class GetDefaultCustomer_Details extends AsyncTask<String, Void, CustomerData> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected CustomerData doInBackground(String... params) {
            ArrayList<CustomerData> details = new ArrayList<>();
            CustomerData defaultCustomerDetails = new CustomerData();
            RestApiCalls restApiCalls = new RestApiCalls();
            details = restApiCalls.getHotelProfile((App) getApplication(), db_name);
            if (null != details && details.size() > 0)
                defaultCustomerDetails = details.get(0);
            return defaultCustomerDetails;
        }

        @Override
        protected void onPostExecute(CustomerData defaultDetails) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            //TODO Add hotel default details to shared preferences
            if (DetectConnection.isOnline(context)) {
                if (null != defaultDetails) {
                    AppLog.write("Profile---", "--" + new Gson().toJson(defaultDetails));
                    String email = defaultDetails.getEmail();
                    c_id = defaultDetails.getId();
                    if (!TextUtils.isEmpty(email)) {
                        email = defaultDetails.getEmail();
                    } else {
                        email = "info@codebase.bz";
                    }
                    new TableLock().execute(defaultDetails.getPhone(), defaultDetails.getName(), email, defaultDetails.getAddress());
                    dialog.dismiss();
                }
            } else {
                show_noInternet();
            }
        }
    }

    private class TableLock extends AsyncTask<String, Void, LockTable> {

        String tableID = "0";
        String userName = settings.getUSER_NAME();
        String password = settings.getPASSWORD();

        @Override
        protected LockTable doInBackground(String... params) {
            RestApiCalls restApiCalls = new RestApiCalls();
            return restApiCalls.lockTableModified((App) getApplication(), params[0], params[1], params[2], params[3], params[4],
                    userName, password, tableID, "1", "1", "", "0", db_name);
        }

        @Override
        protected void onPostExecute(LockTable lockTable) {
            if (DetectConnection.isOnline(context)) {
                if (null != lockTable) {
                    if (!TextUtils.isEmpty(c_id)) {
                        c_id = c_id.replace("\"", "");
                        settings.setCUSTOMER_ID(tableID, c_id);
                        String orderType = "1";
                        settings.setORDER_TYPE(orderType);
                        configSettings.setBILL__ID(tableID, lockTable.getSaved_bill_id());
                        AppLog.write("Save--", "" + configSettings.getBILL__ID(tableID));
                    }
                    AppLog.write("LOCK---", "--" + new Gson().toJson(lockTable));
                    move_to_menuPage();
                } else {
                    Toast.makeText(context, "Internal Server Error..!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Please check your network connection and try again..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class GetUserIdDetails extends AsyncTask<Void, Void, ArrayList<Employee_Data>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<Employee_Data> doInBackground(Void... params) {
            ArrayList<Employee_Data> employeeList = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            employeeList = call.getEmployeeDetailsList((App) getApplication(), db_name);
            return employeeList;
        }

        @Override
        protected void onPostExecute(ArrayList<Employee_Data> employee_dataArrayList) {
            if (null != dialog && dialog.isShowing()) ;
            dialog.dismiss();
            if (null != employee_dataArrayList) {
                for (Employee_Data t : employee_dataArrayList) {
                    if (userName.equalsIgnoreCase(t.getName())) {
                        user_id = t.getId();
                        settings.setUSER_ID(user_id);
                        AppLog.write("-----------", "--" + user_id);
                    }
                }
            }
        }
    }

    private class GetAuthorizeEmployeeInfo extends AsyncTask<String, Void, ArrayList<AuthorizeEmployee>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading Menu...");
            dialog.show();
        }

        @Override
        protected ArrayList<AuthorizeEmployee> doInBackground(String... params) {
            ArrayList<AuthorizeEmployee> result = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            result = call.GetAuthorizeEmployeeDetails((App) getApplication(), db_name);
            AppLog.write("Emp_TAG_", "--" + new Gson().toJson(result));
            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<AuthorizeEmployee> employeeList) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            employeeArrayList.clear();
            try {
                for (AuthorizeEmployee a : employeeList) {
                    employeeArrayList.add(a.getName());
                    auth_emp_List.add(a);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            AppLog.write("Emp_TAG", "--" + new Gson().toJson(employeeArrayList));
        }
    }

    private class SendOTP_via_SMS extends AsyncTask<String, String, String> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading Menu...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                final String url = App.IP_ADDRESS + "/index.php/settings/send_sms_mobile";
                AppLog.write("URL:::::::::::=>", url);
                RestTemplate restTemplate = new RestTemplate();
                List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
                messageConverters.add(new FormHttpMessageConverter());
                messageConverters.add(new StringHttpMessageConverter());
                restTemplate.setMessageConverters(messageConverters);
                MultiValueMap<String, String> part = new LinkedMultiValueMap<String, String>();
                part.add("sms_values", params[0]);
                part.add("mobile_number", params[1]);
                part.add("selected_db_name", db_name);
                String results = restTemplate.postForObject(url, part, String.class);
                AppLog.write("Request", new Gson().toJson(restTemplate.getRequestFactory()) + "\n" + "Values sent:" + part);
                AppLog.write("Result=========>", results);
                return results;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (!TextUtils.isEmpty(s)) {
                switch (s) {
                    case "1701":
                        new OTP_Authorize().execute(s_authorize_emp, s_req_emp, otp_stamp, s_pax);
                        Toast.makeText(context, "Message Sent Successfully..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "1702":
                        Toast.makeText(context, "Already Logged..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "1703":
                        Toast.makeText(context, "Invalid value in username or password field..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "1704":
                        Toast.makeText(context, "Invalid value in 'type' field..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "1705":
                        Toast.makeText(context, "Invalid Message..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "1706":
                        Toast.makeText(context, "Invalid Mobile Number..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "1707":
                        Toast.makeText(context, "Invalid Source(Sender)..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "1708":
                        Toast.makeText(context, "Invalid value for 'dlr' field..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "1709":
                        Toast.makeText(context, "User validation failed..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "1710":
                        Toast.makeText(context, "Internal Error..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "1025":
                        Toast.makeText(context, "Insufficient Credit..!", Toast.LENGTH_SHORT).show();
                        break;
                    case "1715":
                        Toast.makeText(context, "Response Timeout..!", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        Toast.makeText(context, "Please check your network connection and try again..!", Toast.LENGTH_SHORT).show();
                        break;
                }
            } else {
                Toast.makeText(context, "Please check your network connection and try again..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class OTP_Authorize extends AsyncTask<String, Void, AuthorizeEmployee> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading Menu...");
            dialog.show();
        }

        @Override
        protected AuthorizeEmployee doInBackground(String... params) {
            AuthorizeEmployee result = new AuthorizeEmployee();
            RestApiCalls call = new RestApiCalls();
            result = call.OTP_Authorize_submit((App) getApplication(), params[0], params[1], params[2], "", params[3], "0", db_name);
            AppLog.write("OTP_Submit", "--" + new Gson().toJson(result));
            return result;
        }

        @Override
        protected void onPostExecute(AuthorizeEmployee result) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (result != null) {
                o_dialog.dismiss();
                if (result.getStatus().equalsIgnoreCase("1")) {
                    AppLog.write("TAG", "OTP Value Successfully Saved");
                }
            }
        }
    }
}
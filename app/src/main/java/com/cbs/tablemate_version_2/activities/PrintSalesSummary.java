package com.cbs.tablemate_version_2.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.UIWidgets.MyCustomDialog;
import com.cbs.tablemate_version_2.configuration.App;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.configuration.RestApiCalls;
import com.cbs.tablemate_version_2.configuration.Settings;
import com.cbs.tablemate_version_2.models.BillPreview;
import com.cbs.tablemate_version_2.models.Sales;
import com.cbs.tablemate_version_2.models.SalesSummary;
import com.cbs.tablemate_version_2.utilities.ConnectionException;
import com.cbs.tablemate_version_2.utilities.DateUtil;
import com.cbs.tablemate_version_2.utilities.MyBluetoothConnector;
import com.cbs.tablemate_version_2.utilities.StringUtils;
import com.cbs.tablemate_version_2.utilities.Utility;
import com.google.gson.Gson;

import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

/*********************************************************************
 * Created by Barani on 22-02-2018 in TableMateNew
 ***********************************************************************/
public class PrintSalesSummary extends Activity {

    private Button mConnectBtn;
    private Button mEnableBtn;
    private Button btn_Print;
    private Spinner spinner_deviceList;
    private ProgressDialog myProgressDialog;
    private ProgressDialog connectivityDialog;
    private Context context;
    private String db_name, bill_Id, table_id, table_name;
    private String start_Date, end_date, userName;
    private Settings settings;
    private ArrayList<BillPreview> billItemsList = new ArrayList<>();
    private BluetoothAdapter bluetoothAdapter;
    private MyBluetoothConnector bluetoothConnector;
    private ArrayList<BluetoothDevice> myDeviceList = new ArrayList<>();
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                if (state == BluetoothAdapter.STATE_ON) {
                    showEnabled();
                } else if (state == BluetoothAdapter.STATE_OFF) {
                    showDisabled();
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                myDeviceList = new ArrayList<>();
                updateDeviceList();
                myProgressDialog.show();
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                myProgressDialog.dismiss();
                updateDeviceList();
            } else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                myDeviceList.add(device);
                showToast("Found device..!" + device.getName());
            } else if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);
                if (state == BluetoothDevice.BOND_BONDED) {
                    showToast("Paired..!");
                    connectDevice();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_printer);

        context = PrintSalesSummary.this;
        settings = new Settings(context);

        userName = settings.getUSER_NAME();
        db_name = settings.getDbName();
        table_id = settings.getTableID();
        Intent i = getIntent();
        bill_Id = i.getStringExtra("BILL_ID");
        table_name = i.getStringExtra("TABLE_NAME");
        AppLog.write("TableName----", "--" + table_name);

        mConnectBtn = findViewById(R.id.btn_connect);
        mEnableBtn = findViewById(R.id.btn_enable);
        btn_Print = findViewById(R.id.btn_print);
        spinner_deviceList = findViewById(R.id.spinner_deviceList);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        final Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

        //enable bluetooth
        mEnableBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(intent, 1000);
                if (bluetoothAdapter.isEnabled()) {
                    AppLog.write("Bluetooth Enabled", "----");
                    myDeviceList.addAll(pairedDevices);
                    updateDeviceList();
                }
            }
        });

        //connect/disconnect
        mConnectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                connect();
                getDate();
                new GetSalesSummary().execute();
            }
        });

        //print
        btn_Print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //connect();
            }
        });

        IntentFilter filter = new IntentFilter();

        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);

        registerReceiver(mReceiver, filter);

        if (bluetoothAdapter == null) {
            showDeviceNotSupports();
        } else {
            if (!bluetoothAdapter.isEnabled()) {
                showDisabled();
            } else {
                showEnabled();
                //Set<BluetoothDevice> pairedDevices1 = bluetoothAdapter.getBondedDevices();
                //if (pairedDevices != null)
                //{
                myDeviceList.addAll(pairedDevices);
                updateDeviceList();
                //}
                myProgressDialog = new ProgressDialog(this);
                myProgressDialog.setMessage("Scanning...");
                myProgressDialog.setCancelable(false);
                myProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        bluetoothAdapter.cancelDiscovery();
                    }
                });

                connectivityDialog = new ProgressDialog(this);
                connectivityDialog.setMessage("Connecting...");
                connectivityDialog.setCancelable(false);
                callConnector();
            }
        }
    }

    private void getDate() {
        try {
            Date curDate = new Date();
            String time2Compare = "05:00:00";
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat t_format = new SimpleDateFormat("HH:mm:ss");
            String c_time = t_format.format(curDate);
            AppLog.write("S-E-1", "--" + c_time);
            Date c_time_format = t_format.parse(c_time);
            Date c_compare_format = t_format.parse(time2Compare);
            AppLog.write("S-E-2", "--" + c_time_format + "--" + c_compare_format);
            if (c_time_format.before(c_compare_format)) {
                Calendar c = Calendar.getInstance();
                c.add(Calendar.DATE, -1);
                start_Date = (c.get(Calendar.YEAR) + "-" + (c.get(Calendar.MONTH) + 1) + "-" + c.get(Calendar.DATE)) + " " + "00:00:00";
                end_date = format.format(curDate) + " " + c_time;
            } else {
                start_Date = format.format(curDate) + " " + "00:00:00";
                end_date = format.format(curDate) + " " + c_time;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        AppLog.write("S-E", "--" + start_Date + "_________" + end_date);
    }

    @Override
    public void onPause() {
        if (bluetoothAdapter != null) {
            if (bluetoothAdapter.isDiscovering()) {
                bluetoothAdapter.cancelDiscovery();
            }
        }

        if (bluetoothConnector != null) {
            try {
                bluetoothConnector.disconnect();
            } catch (ConnectionException e) {
                e.printStackTrace();
            }
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    private void connect() {
        if (myDeviceList == null || myDeviceList.size() == 0) {
            AppLog.write("Device List size----", "" + myDeviceList.size());
            return;
        }

        BluetoothDevice device = myDeviceList.get(spinner_deviceList.getSelectedItemPosition());

        if (device.getBondState() == BluetoothDevice.BOND_NONE) {
            try {
                createBond(device);
            } catch (Exception e) {
                showToast("Failed to pair device");
                return;
            }
        }

        try {
            if (!bluetoothConnector.isConnected()) {
                bluetoothConnector.connect(device);
            } else {
                bluetoothConnector.disconnect();
                showDisconnected();
            }
        } catch (ConnectionException e) {
            e.printStackTrace();
        }
    }

    private void showDeviceNotSupports() {
        showToast("Bluetooth is unsupported by this device");
        mConnectBtn.setEnabled(false);
        btn_Print.setEnabled(false);
        spinner_deviceList.setEnabled(false);
    }

    private void callConnector() {
        bluetoothConnector = new MyBluetoothConnector(new MyBluetoothConnector.BluetoothConnectionListener() {

            @Override
            public void onStartConnecting() {
                connectivityDialog.show();
            }

            @Override
            public void onConnectionSuccess() {
                connectivityDialog.dismiss();
                showConnected();
            }

            @Override
            public void onConnectionFailed(String error) {
                AppLog.write("Error-----", error);
                Toast.makeText(context, "Error in Connectivity,Try Again..!", Toast.LENGTH_SHORT).show();
                connectivityDialog.dismiss();
            }

            @Override
            public void onConnectionCancelled() {
                Toast.makeText(context, "Error in Connectivity,Try Again..!", Toast.LENGTH_SHORT).show();
                connectivityDialog.dismiss();
            }

            @Override
            public void onDisconnected() {
                showDisconnected();
            }
        });
    }

    private void showConnected() {
        showToast("Connected..!");
        mConnectBtn.setText(R.string.text_disconnect);
        btn_Print.setEnabled(true);
        spinner_deviceList.setEnabled(false);
    }

    private void showDisconnected() {
        showToast("Disconnected..!");
        mConnectBtn.setText(R.string.text_connect);
        btn_Print.setEnabled(true);
        spinner_deviceList.setEnabled(true);
    }

    private void showDisabled() {
        showToast("Bluetooth disabled..!");
        mEnableBtn.setVisibility(View.VISIBLE);
        mConnectBtn.setVisibility(View.GONE);
        spinner_deviceList.setVisibility(View.GONE);
    }

    private void showToast(String s) {
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
    }

    private void showEnabled() {
        showToast("Bluetooth enabled..!");
        mEnableBtn.setVisibility(View.GONE);
        mConnectBtn.setVisibility(View.VISIBLE);
        spinner_deviceList.setVisibility(View.VISIBLE);
    }

    private void updateDeviceList() {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.simple_spinner_item, getArray(myDeviceList));
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner_deviceList.setAdapter(adapter);
        spinner_deviceList.setSelection(0);
    }

    private String[] getArray(ArrayList<BluetoothDevice> myDeviceList) {
        String[] list = new String[0];
        if (myDeviceList == null)
            return list;
        int size = myDeviceList.size();
        AppLog.write("Device List size----", "-" + myDeviceList.size());
        list = new String[size];

        for (int i = 0; i < size; i++) {
            list[i] = myDeviceList.get(i).getName();
        }

        return list;
    }

    private void connectDevice() {
        if (myDeviceList == null || myDeviceList.size() == 0) {
            AppLog.write("Device List size----", "-" + myDeviceList.size());
            return;
        } else if (myDeviceList.size() == 1) {
            BluetoothDevice device = myDeviceList.get(0);
            try {
                bluetoothConnector.connect(device);
            } catch (ConnectionException e) {
                e.printStackTrace();
            }
        } else {
            BluetoothDevice device = myDeviceList.get(spinner_deviceList.getSelectedItemPosition());
            try {
                if (!bluetoothConnector.isConnected()) {
                    bluetoothConnector.connect(device);
                } else {
                    bluetoothConnector.disconnect();
                    showDisconnected();
                }
            } catch (ConnectionException e) {
                e.printStackTrace();
            }

            if (device.getBondState() == BluetoothDevice.BOND_NONE) {
                try {
                    createBond(device);
                } catch (Exception e) {
                    showToast("Failed to pair device..!");
                }
            }
        }
    }

    private void createBond(BluetoothDevice device) throws Exception {
        try {
            Class<?> cl = Class.forName("android.bluetooth.BluetoothDevice");
            Class<?>[] par = {};
            Method method = cl.getMethod("createBond", par);
            method.invoke(device);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public void onBackPressed() {
        //finish();
    }

    private class GetSalesSummary extends AsyncTask<String, String, SalesSummary> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected SalesSummary doInBackground(String... params) {
            RestApiCalls restApiCalls = new RestApiCalls();
            SalesSummary sales = restApiCalls.getSalesSummary((App) getApplication(), start_Date, end_date, db_name);
            return sales;
        }

        @Override
        protected void onPostExecute(SalesSummary salesSummary) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != salesSummary) {
                AppLog.write("Sales_summary---", "---" + new Gson().toJson(salesSummary));
                printContent(salesSummary);
            }
        }

        private void printContent(SalesSummary salesSummary) {

            String formatSecondLine = "%1$-20s %2$10s";
            String formatThirdLine = "%1$-15s %2$15s";
            int maxChars = 32;
            String newLine = "\r\n";
            StringBuilder content = new StringBuilder();
            content.append(StringUtils.center("SOUTHISH", maxChars));
            content.append(StringUtils.center("SPI Cinemas", maxChars));
            content.append(StringUtils.center("Employee : " + userName, maxChars));
            content.append(StringUtils.center("Date : " + DateUtil.format(System.currentTimeMillis()), maxChars));
            content.append(StringUtils.center("--------------------------------", maxChars));
            content.append(StringUtils.center("SALES SUMMARY", maxChars));
            content.append(StringUtils.center("--------------------------------", maxChars)).append(newLine);
            content.append(String.format(formatSecondLine, "Gross Sales(W/O Tax)", "" + salesSummary.getBill_sales().get(0).getBilltotal())).append(newLine);
            String c_cost = salesSummary.getCustomization_cost().get(0).getCustomization_cost();
            if (!TextUtils.isEmpty(c_cost)) {
                content.append(String.format(formatSecondLine, "Customization Costs", "" + salesSummary.getCustomization_cost().get(0).getCustomization_cost())).append(newLine);
            } else {
                content.append(String.format(formatSecondLine, "Customization Costs", "" + "0.00")).append(newLine);
            }
            content.append(String.format(formatSecondLine, "Discounts", "" + salesSummary.getBill_sales().get(0).getDiscount())).append(newLine);
            content.append(String.format(formatSecondLine, "Sub Total(W/O Tax)", "" + salesSummary.getBill_sales().get(0).getNetsales())).append(newLine);
            content.append(String.format(formatSecondLine, "Taxes", "" + salesSummary.getBill_sales().get(0).getBilltax())).append(newLine);
            content.append(String.format(formatSecondLine, "Round off", "" + salesSummary.getBill_sales().get(0).getRound_off())).append(newLine);
            content.append(String.format(formatSecondLine, "Net Sales(W Tax)", "" + salesSummary.getBill_sales().get(0).getGtotal())).append(newLine);
            content.append(String.format(formatSecondLine, "Occupancy", "" + salesSummary.getBill_sales().get(0).getOccupancy())).append(newLine);
            content.append(String.format(formatSecondLine, "Sales/Guest", "" + salesSummary.getGuestsummary().get(0).getSalesperguest())).append(newLine);
            content.append(String.format(formatSecondLine, "Total num of bills", "" + salesSummary.getBill_sales().get(0).getNofbill())).append(newLine);
            double total = Utility.convertToDouble(salesSummary.getBill_sales().get(0).getTotal());
            double totalBills = Utility.convertToDouble(salesSummary.getBill_sales().get(0).getNofbill());
            double sales_per_bill = total / totalBills;
            content.append(String.format(formatSecondLine, "Sales/Bill", "" + "" + String.format("%.2f", sales_per_bill))).append(newLine);
            content.append(StringUtils.center("--------------------------------", maxChars));
            content.append(StringUtils.center("Settlement Summary", maxChars));
            content.append(StringUtils.center("--------------------------------", maxChars)).append(newLine);
            for (Sales sales : salesSummary.getPayment()) {
                String paymentName = sales.getPaymentMode();
                List<String> itemNames = StringUtils.splitEqually(paymentName, 16);
                content.append(String.format(formatSecondLine, itemNames.get(0), sales.getTotal()) + newLine);
                for (int i = 1; i < itemNames.size(); i++) {
                    content.append(String.format(formatSecondLine, itemNames.get(i), "") + newLine);
                }
            }
            content.append(StringUtils.center("--------------------------------", maxChars)).append(newLine);
            AppLog.write("Content---", "---" + content);
            sendData(content);
        }

        private void sendData(StringBuilder content) {
            try {
                bluetoothConnector.sendData(content);
                startActivity(new Intent(context, SelectOrderTypeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
            } catch (ConnectionException e) {
                e.printStackTrace();
            }
        }
    }
}
package com.cbs.tablemate_version_2.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.UIWidgets.MyCustomDialog;
import com.cbs.tablemate_version_2.adapter.CategoryAdapter;
import com.cbs.tablemate_version_2.adapter.CustomizationAddonAdapter;
import com.cbs.tablemate_version_2.adapter.CustomizationEmployeeAdapter;
import com.cbs.tablemate_version_2.adapter.CustomizationItemTypeAdapter;
import com.cbs.tablemate_version_2.adapter.CustomizationPortionAdapter;
import com.cbs.tablemate_version_2.adapter.FilterTypeAdapter;
import com.cbs.tablemate_version_2.adapter.MenuAdapter;
import com.cbs.tablemate_version_2.configuration.App;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.configuration.ConfigurationSettings;
import com.cbs.tablemate_version_2.configuration.CounterSaleSettings;
import com.cbs.tablemate_version_2.configuration.RestApiCalls;
import com.cbs.tablemate_version_2.configuration.Settings;
import com.cbs.tablemate_version_2.helper.CartManager;
import com.cbs.tablemate_version_2.helper.RecyclerItemClickListener;
import com.cbs.tablemate_version_2.interfaces.MenuItemQtyChangeListener;
import com.cbs.tablemate_version_2.interfaces.MenuListener;
import com.cbs.tablemate_version_2.models.CustomizationItemsType;
import com.cbs.tablemate_version_2.models.CustomizationModel;
import com.cbs.tablemate_version_2.models.CustomizedItemsPortion;
import com.cbs.tablemate_version_2.models.EmployeeTypeName;
import com.cbs.tablemate_version_2.models.FilterType;
import com.cbs.tablemate_version_2.models.GetTotalModel;
import com.cbs.tablemate_version_2.models.Item;
import com.cbs.tablemate_version_2.models.LiveMenuItem;
import com.cbs.tablemate_version_2.models.StockItem;
import com.cbs.tablemate_version_2.models.UnLockTable;
import com.cbs.tablemate_version_2.utilities.Utility;
import com.google.gson.Gson;

import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

public class MenuActivityWithFilters extends FragmentActivity implements MenuListener, MenuItemQtyChangeListener,
        View.OnClickListener, SearchView.OnQueryTextListener {

    private RecyclerView rvCategoryList, rvMenuList, rvFoodDeptList;
    private MenuAdapter mAdapter;
    private CustomizationItemTypeAdapter customizationTypeAdapter;
    private CustomizationAddonAdapter customizationAddonAdapter;
    private CustomizationPortionAdapter customizationPortionAdapter;
    private CustomizationEmployeeAdapter customizationEmployeeAdapter;
    private CategoryAdapter cAdapter;
    private FilterTypeAdapter filterTypeAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<String> categoryList = new ArrayList<>();
    private ArrayList<Item> menuList = new ArrayList<>();
    private ArrayList<StockItem> sList = new ArrayList<>();
    private ArrayList<Item> categoryFilterMenu = new ArrayList<>();
    private ArrayList<Item> mainFilter = new ArrayList<>();
    private ArrayList<FilterType> filterTypeArrayList = new ArrayList<>();
    private Context context;
    private RelativeLayout layout_goCart;
    private String db_name;
    private String userName;
    private Settings settings;
    private ConfigurationSettings configurationSettings;
    private CounterSaleSettings counterSettings;
    private CartManager cartManager;
    private String currentCategory, orderType;
    private ArrayList<Item> cartItems = new ArrayList<>();
    private TextView txt_totalAmount;
    private TextView txt_tableName;
    private TextView txt_hotelName;
    private Button btn_viewStatus, btn_unLockTable, btn_Logout, btn_view_lock_table, btnBeverages, btnAlcohol, btnBakery, btnDessert, btnNonVegetarian, btnVegetarian, btn_view_live_menu;
    private Item customItem;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private String Addon;
    private String Portion;
    private String Type;
    private String Employee;
    private String table_name, tableId;
    private String bill_total;
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        context = MenuActivityWithFilters.this;
        initControls();
        settings = new Settings(context);
        configurationSettings = new ConfigurationSettings(context);
        counterSettings = new CounterSaleSettings(context);
        userName = settings.getUSER_NAME();
        db_name = configurationSettings.getDb_Name();
        txt_hotelName.setText(configurationSettings.getHOTEL_NAME());

        if (counterSettings.isConfigured()) {
            orderType = "0";
            tableId = "0";
            table_name = "admin";
        } else {
            orderType = settings.getORDER_TYPE();
            table_name = settings.getTABLE_NAME();
            tableId = settings.getTableID();
            txt_tableName.setText(table_name);
        }
        AppLog.write("DB_Name---->", db_name + "----" + tableId + "---" + table_name);

        cartManager = new CartManager(context);

        if (DetectConnection.isOnline(context)) {
            AppLog.write("Internet Detected", "--");
            loadMenuItems();
            new GetBillTotalForOrders().execute(tableId);
            /*
             * When refresh items using P to R
             * */
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    refresh();
                }
            });
        } else {
            AppLog.write("No-Internet--", "--");
            showNoInternet();
        }

        cartItems = cartManager.getItemList();
        for (Item cartItem : cartItems) {
            if (cartItem.getId().startsWith("cust")) {
                String itemId = cartItem.getCustomId();
                if (itemIdExist(itemId)) {
                    incrementExistingItem(itemId);
                } else {
                    cartItem.setId(itemId);
                }
            }
        }

        btn_unLockTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmAlertTableRelease();
            }
        });

        btn_Logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmAlertLogout();
            }
        });

        layout_goCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cartItems.clear();
                for (Item item : menuList) {
                    int qty = Utility.convertToInteger(item.getItemQty());
                    if (qty > 0)
                        AppLog.write("Cart Items-------", "" + new Gson().toJson(item));
                    cartItems.add(item);
                }
                if (counterSettings.isConfigured()) {
                    Intent intent = new Intent(context, ViewCartActivity_CounterSales.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(context, ViewCartActivity.class);
                    startActivity(intent);
                }
            }
        });

        cAdapter = new CategoryAdapter(context, categoryList);
        cAdapter.setMenuListener(this);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvCategoryList.setLayoutManager(mLayoutManager);
        rvCategoryList.setAdapter(cAdapter);

        filterTypeAdapter = new FilterTypeAdapter(context, filterTypeArrayList);
        filterTypeAdapter.setFilterListener(this);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvFoodDeptList.setLayoutManager(mLayoutManager);
        rvFoodDeptList.setAdapter(filterTypeAdapter);

        mLayoutManager = new LinearLayoutManager(this);
        rvMenuList.setLayoutManager(mLayoutManager);
        mAdapter = new MenuAdapter(context, menuList);
        mAdapter.setMenuListener(this);
        rvMenuList.setAdapter(mAdapter);


        //rvMenuList.addItemDecoration(new DividerItemDecoration(context, null));
    }

    private void loadMenuItems() {
        new GetFilterTypes().execute();
        ArrayList<Item> menu = settings.getMenu();
        if (null != menu && menu.size() > 0) {
            new GetMenuItems().execute(true);
        } else {
            new GetMenuItems().execute(false);
        }
    }

    private void showNoInternet() {
        Intent i = new Intent(context, Show_NoInternetMessage_Activity.class);
        startActivity(i);
    }

    /*
     * Refresh POS_Bill Amount
     * */
    private void refresh() {
        new GetBillTotalForOrders().execute(tableId);
    }

    private void stop() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private void confirmAlertTableRelease() {
        if (DetectConnection.isOnline(context)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false);
            builder.setMessage("Do you really want to release the table?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    new UnLockSelectedTable().execute();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        } else {
            Toast.makeText(context, "Please check your internet connection..!", Toast.LENGTH_SHORT).show();
        }
    }

    /* *
     * Alert - Logout
     */
    private void confirmAlertLogout() {
        if (DetectConnection.isOnline(context)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setCancelable(false);
            builder.setMessage("Are you sure want to logout?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    new Logout().execute(userName, db_name);
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //if user select "No", just cancel this dialog and continue with app
                    dialog.cancel();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        } else {
            Toast.makeText(context, "Please check your internet connection..!", Toast.LENGTH_SHORT).show();
        }
    }

    /*
     * Initialize controls
     * */
    private void initControls() {
        rvCategoryList = findViewById(R.id.rvCategoryList);
        rvFoodDeptList = findViewById(R.id.rvFoodDeptList);
        rvMenuList = findViewById(R.id.rvMenuList);
        layout_goCart = findViewById(R.id.layout_go_to_cart);
        txt_totalAmount = findViewById(R.id.txt_total_amt);
        txt_tableName = findViewById(R.id.txtCurrentTableName);
        txt_hotelName = findViewById(R.id.txtHotelName);
        btn_view_live_menu = findViewById(R.id.btn_view_live_menu);
        btn_viewStatus = findViewById(R.id.btn_view_status);
        btn_unLockTable = findViewById(R.id.btn_unlock_table);
        btn_view_lock_table = findViewById(R.id.btn_view_lock_table);
        mSwipeRefreshLayout = findViewById(R.id.swipe_container);
        searchView = findViewById(R.id.search);
        btn_Logout = findViewById(R.id.btn_logout);
        btnBeverages = findViewById(R.id.btnBeverages);
        btnDessert = findViewById(R.id.btnDessert);
        btnNonVegetarian = findViewById(R.id.btnNonVegetarian);
        btnVegetarian = findViewById(R.id.btnVegetarian);
        btnAlcohol = findViewById(R.id.btnAlcohol);//additional for PRAVAS
        btnBakery = findViewById(R.id.btnBakery);//additional for cafe de paris

        btn_viewStatus.setOnClickListener(this);
        btnBeverages.setOnClickListener(this);
        btnDessert.setOnClickListener(this);
        btnNonVegetarian.setOnClickListener(this);
        btnVegetarian.setOnClickListener(this);
        btnAlcohol.setOnClickListener(this);
        btnBakery.setOnClickListener(this);
        btn_view_live_menu.setOnClickListener(this);
        searchView.setOnQueryTextListener(this);

        btn_view_lock_table.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (counterSettings.isConfigured()) {
                    Intent i = new Intent(context, SelectOrderTypeActivity.class);
                    startActivity(i);
                } else {
                    moveToTablesActivity();
                }
            }
        });
    }

    private void moveToTablesActivity() {
        Intent i = new Intent(context, TableActivity_1.class);
        startActivity(i);
    }

    private int incrementExistingItem(String itemId) {
        //int counter = 0;
        for (Item cartItem : cartItems) {
            if (cartItem.getId().equalsIgnoreCase(itemId))
                cartItem.setItemQty("" + (Utility.convertToInteger(cartItem.getItemQty()) + 1));
        }
        return 0;
    }

    private boolean itemIdExist(String itemId) {
        for (Item cartItem : cartItems) {
            if (cartItem.getId().equalsIgnoreCase(itemId)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onMenuClick(String p_category) {
        currentCategory = p_category;
        filterMenu(p_category);
    }

    @Override
    public void onFilterClick(String p_filterType, String id, String color) {
        AppLog.write("String_Type", p_filterType + "--" + color + "---" + id + "--" + new Gson().toJson(menuList));
        for (Item i : menuList) {
            i.setFilter_name(p_filterType);
            i.setColor_filter_type(color);
            i.setFilter_id(id);
        }
        filterMainCategory(id);
    }

    private void filterMainCategory(String p_mainFilter) {
        mainFilter.clear();
        for (Item item : menuList) {
            String mainCategoryName = item.getFilter_type_name();
            AppLog.write("Main Category Name---", mainCategoryName);

            if (!TextUtils.isEmpty(mainCategoryName) && mainCategoryName.equalsIgnoreCase(p_mainFilter)) {
                mainFilter.add(item);
            }
        }
        collectCategory();
        filterMenu("");
        if (mainFilter.size() == 0) {
            Toast.makeText(this, "No Item found in this category..!", Toast.LENGTH_SHORT).show();
        }
    }

    private void collectCategory() {
        categoryList.clear();
        for (Item item : mainFilter) {
            String category = item.getFood_type_name();
            if (!TextUtils.isEmpty(category) && !categoryList.contains(category))
                categoryList.add(item.getFood_type_name());
        }
        if (categoryList.size() > 0) {
            currentCategory = categoryList.get(0);
            cAdapter.MyDataChanged(categoryList);
        }
    }

    private void filterMenu(String p_category) {
        categoryFilterMenu.clear();
        if (TextUtils.isEmpty(p_category)) {
            currentCategory = categoryList.size() > 0 ? categoryList.get(0) : "";
        }

        for (Item item : mainFilter) {
            String categoryName = item.getFood_type_name();
            if (!TextUtils.isEmpty(categoryName) && categoryName.contains(currentCategory)) {
                categoryFilterMenu.add(item);
            }
        }
        mAdapter.MyDataChanged(categoryFilterMenu);
    }

    /* *
     * Method to increment Quantity
     * */

    @Override
    public void incrementQuantity(String p_itemId, int position) {
        for (Item item : menuList) {
            if (p_itemId.equals(item.getId())) {
                customItem = item;
            }
        }
        if (customItem.getCustomised_item().equals("1")) {
            new ValidateCustomization().execute(customItem.getId());
        } else {
            incrementQuantity(customItem);
            updateMenu(position, customItem);
        }
    }

    private void incrementQuantity(Item item) {
        Log.e("itemincremen", "" + item);
        if (item.getLive_item().equals("1")) {
            Double stockQty = Utility.convertToDouble(item.getStockQuantity());
            int itemQty = Utility.convertToInteger(item.getItemQty());
            AppLog.write("Stock_Quantity---", "-" + stockQty);
            if (stockQty > itemQty) {
                item.setItemQty("" + cartManager.addToCart(item));
            } else {
                Toast.makeText(context, "Item not available", Toast.LENGTH_SHORT).show();
            }
        } else {
            item.setItemQty("" + cartManager.addToCart(item));
        }
    }
    String empId;
    @SuppressLint("WrongConstant")
    private void showCustomDialog(final CustomizationModel custom, final Item item) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_menu_customization);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        assert window != null;
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        TextView text_item_name = dialog.findViewById(R.id.cust_item_name);
        text_item_name.setText("" + item.getName());

        LinearLayout layoutAddon = dialog.findViewById(R.id.layoutAddon);
        RecyclerView rc_Addon = dialog.findViewById(R.id.rc_addonList);

        LinearLayout layoutCustomType = dialog.findViewById(R.id.layoutItemType);
        RecyclerView rc_customTypeList = dialog.findViewById(R.id.rc_list_type);

        LinearLayout layoutPortion = dialog.findViewById(R.id.layoutPortion);
        RecyclerView rc_customPortionList = dialog.findViewById(R.id.rc_list_portion);

        LinearLayout layoutEmployee = dialog.findViewById(R.id.layoutEmployee);
        RecyclerView rc_employee = dialog.findViewById(R.id.rc_list_employee);

        Button btnCancel = dialog.findViewById(R.id.button_cancel);
        Button btnOrder = dialog.findViewById(R.id.button_order);
        Button button_Ncorder = dialog.findViewById(R.id.button_Ncorder);
        RecyclerView.LayoutManager layoutManager;

        AppLog.write("Customization_Values---", "---" + custom.getEmployee() + "\n" + custom.getItemsTypes() + "\n" + custom.getPortions());

        if (null != custom.getEmployee()) {
            AppLog.write("Called", "--");
            customizationEmployeeAdapter = new CustomizationEmployeeAdapter(context, custom.getEmployee());
            layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            rc_employee.setLayoutManager(layoutManager);
            rc_employee.setAdapter(customizationEmployeeAdapter);
            layoutEmployee.setVisibility(View.VISIBLE);
        }

        if (null != custom.getAddons()) {
            customizationAddonAdapter = new CustomizationAddonAdapter(context, custom.getAddons());
            layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            rc_Addon.setLayoutManager(layoutManager);
            rc_Addon.setAdapter(customizationAddonAdapter);
            layoutAddon.setVisibility(View.VISIBLE);
        }

        if (null != custom.getPortions()) {
            for (CustomizedItemsPortion customizedItemsPortion : custom.getPortions()) {
                if (customizedItemsPortion.getPortion_name().equalsIgnoreCase("Regular")) {
                    customizedItemsPortion.setSelected(true);
                    item.setCustomPortion(customizedItemsPortion);
                    break;
                }
            }
            customizationPortionAdapter = new CustomizationPortionAdapter(context, custom.getPortions(), orderType, customItem);
            layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            rc_customPortionList.setLayoutManager(layoutManager);
            rc_customPortionList.setAdapter(customizationPortionAdapter);
            layoutPortion.setVisibility(View.VISIBLE);
        }

        if (null != custom.getItemsTypes()) {
            customizationTypeAdapter = new CustomizationItemTypeAdapter(context, custom.getItemsTypes());
            layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            rc_customTypeList.setLayoutManager(layoutManager);
            rc_customTypeList.setAdapter(customizationTypeAdapter);
            layoutCustomType.setVisibility(View.VISIBLE);
        }

        rc_customPortionList.addOnItemTouchListener(new RecyclerItemClickListener(context, rc_customPortionList,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        item.setCustomPortion(custom.getPortions().get(position));
                        for (CustomizedItemsPortion customizedItemsPortion : custom.getPortions()) {
                            customizedItemsPortion.setSelected(true);
                        }
                        custom.getPortions().get(position).setSelected(true);
                        customizationPortionAdapter.MyDataChanged(custom.getPortions());
                    }

                    @Override
                    public void onItemLongClick(View view, int position) {
                    }
                }));

        rc_customTypeList.addOnItemTouchListener(new RecyclerItemClickListener(context, rc_customTypeList,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        CustomizationItemsType type = custom.getItemsTypes().get(position);
                        type.setSelected(!type.isSelected());
                        customizationTypeAdapter.MyDataChanged(custom.getItemsTypes());
                    }

                    @Override
                    public void onItemLongClick(View view, int position) {
                    }
                }));

        rc_employee.addOnItemTouchListener(new RecyclerItemClickListener(context, rc_customTypeList,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        EmployeeTypeName e_type = custom.getEmployee().get(position);
                        for (EmployeeTypeName typeName : custom.getEmployee()) {
                            typeName.setSelected(true

                            );
                        }
                        e_type.setSelected(true);
                        customizationEmployeeAdapter.MyDataChanged(custom.getEmployee());
                    }

                    @Override
                    public void onItemLongClick(View view, int position) {
                    }
                }));

        rc_Addon.addOnItemTouchListener(new RecyclerItemClickListener(context, rc_Addon,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        item.setCustomAddOn(custom.getAddons().get(position));
                    }

                    @Override
                    public void onItemLongClick(View view, int position) {
                    }
                }));
        dialog.show();


        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });


        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    ArrayList<CustomizationItemsType> types = new ArrayList<>();
                    for (CustomizationItemsType customizationItemsType : custom.getItemsTypes()) {
                        if (customizationItemsType.isSelected()) {
                            types.add(customizationItemsType);
                        }
                    }
                    item.setCustomType(types);
                    ArrayList<EmployeeTypeName> employee = new ArrayList<>();
                    for (EmployeeTypeName emp_Type : custom.getEmployee()) {
                        if (emp_Type.isSelected()) {
                            employee.add(emp_Type);
                            item.setEmp_id(emp_Type.getId());
                        }
                        else
                        {
                            employee.add(emp_Type);
                        }
                    }

                    item.setEmployeeType(employee);

                    if (null != item.getCustomAddOn() || null != item.getCustomPortion() ||
                            item.getCustomType().size() > 0 || item.getEmployeeType().size() > 0) {
                        item.setCustomId("cust_" + Utility.customCounter + "_" + item.getId());
                        item.setItemQty("0");
                        item.setEmp_id(item.getEmp_id());
                        Utility.customCounter++;
                    }
                    Log.e("normalorer", "" + new Gson().toJson(item));


                    incrementQuantity(item);
                    updateMenu(-1, customItem);
                    customItem = null;
                    dialog.dismiss();
                }
            }
        });



        button_Ncorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {
                    for (EmployeeTypeName emp_Type : custom.getEmployee()) {
                        if (emp_Type.isSelected()) {
                            isSelected=true;
                        }
                    }

                    if(isSelected)
                    {
                        ArrayList<CustomizationItemsType> types = new ArrayList<>();
                        for (CustomizationItemsType customizationItemsType : custom.getItemsTypes()) {
                            if (customizationItemsType.isSelected()) {
                                types.add(customizationItemsType);
                            }
                        }
                        item.setCustomType(types);

                        ArrayList<EmployeeTypeName> employee = new ArrayList<>();
                        for (EmployeeTypeName emp_Type : custom.getEmployee()) {
                            if (emp_Type.isSelected()) {
                                employee.add(emp_Type);
                                item.setEmp_id(emp_Type.getId());
                            }
                            else
                            {
                                employee.add(emp_Type);
                            }
                        }

                        item.setEmployeeType(employee);
                        if (null != item.getCustomAddOn() || null != item.getCustomPortion() ||
                                item.getCustomType().size() > 0 || item.getEmployeeType().size() > 0) {
                            item.setCustomId("cust_" + Utility.customCounter + "_" + item.getId());
                            item.setItemQty("0");
                            item.setEmp_id(item.getEmp_id());
                            Utility.customCounter++;
                        }

                        Log.e("NCOrder", "" + item.getEmp_id());
                        incrementQuantity(item);
                        cartManager.addNC(item);
                        updateMenu(-1, customItem);
                        customItem = null;
                        dialog.dismiss();
                        isSelected=false;
                    }
                    else
                    {
                        Toast.makeText(context, "Select Employee Category", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        updateMenu(-1, customItem);
    }

    boolean isSelected=false;
    /*
     * Method to decrement Quantity
     * */
    @Override
    public void decrementQuantity(String p_itemId, int adapterPosition) {
        for (Item decItem : menuList) {
            if (p_itemId.equals(decItem.getId())) {
                customItem = decItem;
            }
        }
        if (customItem.getCustomised_item().equals("1")) {

            Utility.customCounter--;
            customItem.setCustomId("cust_" + Utility.customCounter + "_" + customItem.getId());

            //  show_alertForDecrementItem();
            cartManager.removeFromCart(customItem);
            int qty = Utility.convertToInteger(customItem.getItemQty());
            if (qty > 0) {
                int sub_units = Utility.convertToInteger(customItem.getSub_units());


                customItem.setItemQty(String.valueOf((qty - 1)));
            } else {
                qty--;
                customItem.setItemQty(String.valueOf(qty));
            }
            updateMenu(adapterPosition, customItem);

        } else {
            cartManager.removeFromCart(customItem);
            int qty = Utility.convertToInteger(customItem.getItemQty());
            if (customItem.getLive_item().equals("1")) {
                if (qty > 0) {
                    int sub_units = Utility.convertToInteger(customItem.getSub_units());
                    AppLog.write("Sub units------>", "" + sub_units);
                    customItem.setItemQty("" + (qty - 1));
                }
            } else {
                qty--;
                customItem.setItemQty("" + qty);
            }
            updateMenu(adapterPosition, customItem);
        }
    }

    @Override
    public void show_open_customization(String p_itemId) {
    }

    @Override
    public void update_nc(String p_itemId) {

    }

    /**
     * Showing Alert Message for customization
     */

    private void show_alertForDecrementItem() {
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle("Sorry!!!");
        alertDialog.setMessage("The item has variations. Please visit your cart to remove this item");
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /**
     * Method to update layout
     *
     * @param itemPosition
     * @param customItem
     */

    void updateMenu(int itemPosition, Item customItem) {


        updateTotal();
        if (itemPosition == -1) {
            filterMenu(currentCategory);
        } else {
            AppLog.write("ItemPosition", "--" + itemPosition);
            mAdapter.notifyItemChanged(itemPosition);
            // onQueryTextChange(searchView.getQuery().toString());
        }
        //mAdapter.MyDataChanged(categoryFilterMenu);
    }

    private void updateTotal() {

        float total = (float) Math.round(cartManager.getTotalNCNew(orderType) * 100) / 100;
        int qty = cartManager.getItemCounts();

        Log.e("getItemCounts", "" + cartManager.getItemCounts());
        AppLog.write("ITEM_PRICE--", "--" + total + "--" + orderType + "-------" + qty);
        txt_totalAmount.setText("₹ " + total);
        if (qty > 0) {
            layout_goCart.setVisibility(View.VISIBLE);
        } else {
            layout_goCart.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBeverages:
                selectCategoryButton(btnBeverages);
                filterMainCategory("6");
                break;
            case R.id.btnDessert:
                selectCategoryButton(btnDessert);
                filterMainCategory("3");
                break;
            case R.id.btnNonVegetarian:
                selectCategoryButton(btnNonVegetarian);
                filterMainCategory("2");
                break;
            case R.id.btnVegetarian:
                selectCategoryButton(btnVegetarian);
                filterMainCategory("1");
                break;
            case R.id.btnAlcohol:
                selectCategoryButton(btnAlcohol);
                filterMainCategory("200");
                break;
            case R.id.btnBakery:
                selectCategoryButton(btnBakery);
                filterMainCategory("0");
                break;
            case R.id.btn_view_status:
                navigate_to_page();
                break;
            case R.id.btn_view_live_menu:
                Intent live = new Intent(context, LiveMenuItemsActivity.class);
                startActivity(live);
                //push from bottom to top
                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                break;
            default:
                break;
        }
    }

    private void selectCategoryButton(Button selectedButton) {
        AppLog.write("Selected_Button", "--" + selectedButton);
        btnVegetarian.setBackgroundResource(R.drawable.edit_background);
        btnNonVegetarian.setBackgroundResource(R.drawable.edit_background);
        btnBeverages.setBackgroundResource(R.drawable.edit_background);
        btnDessert.setBackgroundResource(R.drawable.edit_background);
        btnAlcohol.setBackgroundResource(R.drawable.edit_background);
        btnBakery.setBackgroundResource(R.drawable.edit_background);
        selectedButton.setBackgroundResource(R.drawable.bg_green_button);
    }

    @Override
    public void onBackPressed() {
        if (counterSettings.isConfigured()) {
            Intent i = new Intent(context, SelectOrderTypeActivity.class);
            startActivity(i);
        } else {
            moveToTablesActivity();
        }
    }

    private void navigate_to_page() {
        Intent i = new Intent(context, OrderStatusActivity.class);
        i.putExtra("liveMenuItems", new Gson().toJson(sList));
        startActivity(i);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final ArrayList<Item> filteredModelList = filter(menuList, newText);
        mAdapter.setFilter(filteredModelList);
        return true;
    }

    private ArrayList<Item> filter(ArrayList<Item> menu, String query) {
        query = query.toLowerCase();
        final ArrayList<Item> menuList = new ArrayList<>();
        for (Item model : menu) {
            final String text = model.getName().toLowerCase();
            if (text.contains(query)) {
                menuList.add(model);
            }
        }
        return menuList;
    }

    private class ValidateCustomization extends AsyncTask<String, Void, CustomizationModel> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading Menu...");
            dialog.show();
        }

        @Override
        protected CustomizationModel doInBackground(String... params) {
            CustomizationModel cust = new CustomizationModel();
            RestApiCalls call = new RestApiCalls();
            cust.setPortions(call.getPortion((App) getApplication(), params[0], db_name));
            cust.setAddons(call.getAddon((App) getApplication(), params[0], db_name));
            cust.setItemsTypes(call.getItemType((App) getApplication(), params[0], db_name));
            cust.setEmployee(call.getEmployeeListNC((App) getApplication(), db_name));
            return cust;
        }

        @Override
        protected void onPostExecute(CustomizationModel customizationModel) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != customizationModel) {
                showCustomDialog(customizationModel, customItem);


                Addon = new Gson().toJson(customizationModel.getAddons());
                Portion = new Gson().toJson(customizationModel.getPortions());
                Type = new Gson().toJson(customizationModel.getItemsTypes());
                Employee = new Gson().toJson(customizationModel.getEmployee());
                AppLog.write("E_values---", Employee);
                AppLog.write("Values---", Addon + "\n" + Portion + "\n" + Type);
            }
        }
    }

    private class UnLockSelectedTable extends AsyncTask<String, Void, UnLockTable> {

        String tableID = settings.getTableID();
        String userName = settings.getUSER_NAME();
        String password = settings.getPASSWORD();

        @Override
        protected UnLockTable doInBackground(String... params) {
            RestApiCalls restApiCalls = new RestApiCalls();
            AppLog.write("Values---->>", "" + tableID + "--" + userName + "--" + password);
            return restApiCalls.unLockTable((App) getApplication(), userName, password, tableID, db_name);
        }

        @Override
        protected void onPostExecute(UnLockTable unLockTable) {
            if (null != unLockTable && "1".equalsIgnoreCase(unLockTable.getSuccess())) {
                Toast.makeText(context, "Table Released Successfully..!", Toast.LENGTH_SHORT).show();
                moveToTablesActivity();
            }
        }
    }

    private class Logout extends AsyncTask<String, String, String> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                final String url = App.IP_ADDRESS + "/index.php/site/logoutapi";
                AppLog.write("URL:::::::::::=>", url);
                RestTemplate restTemplate = new RestTemplate();
                List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
                messageConverters.add(new FormHttpMessageConverter());
                messageConverters.add(new StringHttpMessageConverter());
                restTemplate.setMessageConverters(messageConverters);
                MultiValueMap<String, String> part = new LinkedMultiValueMap<>();
                part.add("username", userName);
                part.add("selected_db_name", db_name);
                return restTemplate.postForObject(url, part, String.class);
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (!result.equals(null)) {
                AppLog.write("Result=========>", result);
                if (!result.equals("-1") && result.equals("1")) {
                    settings.clearSession();
                    Toast.makeText(context, "Logged out Successfully..!", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, LoginActivity.class);
                    startActivity(intent);
                }
            } else {
                Toast.makeText(context, "Failed to logout..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class GetBillTotalForOrders extends AsyncTask<String, String, GetTotalModel> {

        protected GetTotalModel doInBackground(String... params) {
            RestApiCalls calls = new RestApiCalls();
            AppLog.write("Result-table--", "--" + params[0] + "--" + configurationSettings.getBILL__ID(tableId));
            GetTotalModel total = calls.getBillTotal((App) getApplication(), configurationSettings.getBILL__ID(tableId), db_name);
            //GetTotalModel total = calls.getBillTotal((App) getApplication(), tableId, db_name);
            try {
                bill_total = total.getTotal();
                AppLog.write("Result-BillTotal----->", bill_total);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            return total;
        }

        @Override
        protected void onPostExecute(GetTotalModel getTotalModel) {
            stop();
            if (null != getTotalModel) {
                if (!TextUtils.isEmpty(bill_total)) {
                    if (!getTotalModel.getTotal().equals("0") || counterSettings.isConfigured()) {
                        btn_unLockTable.setVisibility(View.GONE);
                    } else {
                        btn_unLockTable.setVisibility(View.VISIBLE);
                    }
                } else {
                    AppLog.write("Result-BillTotal----->", "-" + new Gson().toJson(getTotalModel));
                }
            } else {
                AppLog.write("Result-BillTotal--------->", "-" + new Gson().toJson(getTotalModel));
            }
        }
    }

    private class GetMenuItems extends AsyncTask<Boolean, String, LiveMenuItem> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading Menu...");
            dialog.show();
        }

        @Override
        protected LiveMenuItem doInBackground(Boolean... params) {
            LiveMenuItem liveMenuItem = new LiveMenuItem();
            RestApiCalls call = new RestApiCalls();
            if (!params[0]) {
                liveMenuItem.setItems(call.getMenuList((App) getApplication(), db_name));
            }
            liveMenuItem.setLiveMenuItems(call.getStockList((App) getApplication(), db_name));
            return liveMenuItem;
        }

        @Override
        protected void onPostExecute(LiveMenuItem liveMenuItem) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("MENU--", "" + new Gson().toJson(liveMenuItem));
            menuList = new ArrayList<>();
            sList = liveMenuItem.getLiveMenuItems();
            ArrayList<Item> items = liveMenuItem.getItems();
            if (items.size() > 0) {
                for (Item item : liveMenuItem.getItems()) {
                    if (!item.getDisable().equalsIgnoreCase("1")
                            && !item.getRemove_from_pos().equalsIgnoreCase("1"))
                        menuList.add(item);
                }
                settings.setMenu(menuList);
            } else {
                items = settings.getMenu();
                menuList = items;
            }
            Log.e("MenuItems",""+new Gson().toJson(menuList));
            if (null != items && null != sList) {
                for (Item item : menuList) {
                    for (StockItem stockItem : sList) {
                        AppLog.write("Item name---", item.getName() + ":" + item.getLive_item() + "--->" + stockItem.getStockName());
                        if (item.getLive_item().equals("1") && item.getName().trim().equalsIgnoreCase(stockItem.getStockName().trim())) {
                            AppLog.write("Item name condition---", item.getName() + ":" + item.getLive_item() + "--->" + stockItem.getStockName());
                            item.setStockQuantity(stockItem.getStockQuantity());
                        }
                    }
                }

                for (Item item : menuList) {
                    for (Item cartItem : cartItems) {
                        if (cartItem.getId().equalsIgnoreCase(item.getId())) {
                            item.setItemQty("" + cartManager.getQuantity(cartItem));
                        }
                    }
                }
                updateTotal();
                filterMainCategory("1");
                selectCategoryButton(btnVegetarian);
            }
        }
    }

    private class GetFilterTypes extends AsyncTask<Void, Void, ArrayList<FilterType>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<FilterType> doInBackground(Void... params) {
            ArrayList<FilterType> filterList = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            filterList = call.getItemFilterType((App) getApplication(), db_name);
            return filterList;
        }

        @Override
        protected void onPostExecute(ArrayList<FilterType> filterList) {
            if (null != dialog && dialog.isShowing()) {
                dialog.dismiss();
            }
            AppLog.write("FilterList", "" + new Gson().toJson(filterList));
            filterTypeAdapter.MyDataChanged(filterList);
        }
    }
}
    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_search, menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setOnQueryTextListener(this);
        return true;
    }*/
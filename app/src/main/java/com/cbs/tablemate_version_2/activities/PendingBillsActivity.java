package com.cbs.tablemate_version_2.activities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.UIWidgets.MyCustomDialog;
import com.cbs.tablemate_version_2.adapter.PendingBillsAdapter;
import com.cbs.tablemate_version_2.configuration.App;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.configuration.ConfigurationSettings;
import com.cbs.tablemate_version_2.configuration.RestApiCalls;
import com.cbs.tablemate_version_2.configuration.Settings;
import com.cbs.tablemate_version_2.helper.RecyclerItemClickListener;
import com.cbs.tablemate_version_2.models.GetBillStatus;
import com.cbs.tablemate_version_2.utilities.Utility;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/*********************************************************************
 * Created by Barani on 22-12-2016 in TableMateNew
 ***********************************************************************/
public class PendingBillsActivity extends AppCompatActivity {
    private PendingBillsAdapter billsAdapter;
    private ArrayList<GetBillStatus> billArrayList = new ArrayList<>();
    private LinearLayoutManager mLayoutManager;
    private RecyclerView billDetailsView;
    private Context context;
    private Settings settings;
    private ConfigurationSettings configurationSettings;
    private String db_name;
    private String bill_number, bill_amount, display_billID, tableName, tableID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_bills);
        context = PendingBillsActivity.this;
        settings = new Settings(context);
        configurationSettings = new ConfigurationSettings(context);
        db_name = configurationSettings.getDb_Name();
        billDetailsView = findViewById(R.id.rvBillDetailsList);
        billDetailsView.addOnItemTouchListener(new RecyclerItemClickListener(context, billDetailsView,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        GetBillStatus bill = billArrayList.get(position);
                        bill_number = bill.getId();
                        display_billID = bill.getBill_id();
                        bill_amount = bill.getBillGrandTotal();
                        tableName = bill.getTable_name();
                        tableID = bill.getTable_id();
                        settings.setTABLE_NAME(tableName);
                        settings.setORDER_TYPE(bill.getOrder_type());
                        AppLog.write("bill values-----", bill_number + "---" + settings.getTABLE_NAME() + "---" + bill.getTable_name());
                        callPaymentPage();
                    }

                    @Override
                    public void onItemLongClick(View view, int position) {
                    }
                }));

        AppLog.write("Pending Bills..", "" + new Gson().toJson(billArrayList));
        billsAdapter = new PendingBillsAdapter(context, billArrayList);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        billDetailsView.setLayoutManager(mLayoutManager);
        billDetailsView.setAdapter(billsAdapter);
        //billDetailsView.addItemDecoration(new DividerItemDecoration(context, null));
        new getBillStatus_for_billID().execute();
    }

    private void callPaymentPage() {
        AppLog.write("bill values-----", bill_number + "---" + bill_amount);
        Intent go_to_payment = new Intent(context, BillPreviewActivity.class);
        go_to_payment.putExtra("BILL_ID", bill_number);
        go_to_payment.putExtra("DISPLAY_BILL", display_billID);
        go_to_payment.putExtra("BILL_AMOUNT", bill_amount);
        go_to_payment.putExtra("TABLE_NAME", tableName);
        go_to_payment.putExtra("TABLE_ID", tableID);
        startActivity(go_to_payment);
    }

    private class getBillStatus_for_billID extends AsyncTask<String, String, ArrayList<GetBillStatus>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<GetBillStatus> doInBackground(String... params) {
            RestApiCalls call = new RestApiCalls();
            ArrayList<GetBillStatus> status = new ArrayList<>();
            status = call.getBillStatus((App) getApplication(), db_name);
            return status;
        }

        @Override
        protected void onPostExecute(ArrayList<GetBillStatus> getPendingBillList) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (null != getPendingBillList) {
                AppLog.write("Result----------=>", "" + new Gson().toJson(getPendingBillList));

                Collections.sort(getPendingBillList, new BillComparator());
                for (GetBillStatus status : getPendingBillList) {
                    System.out.println("POS_Bill values" + ": " + status.getId());
                }
                billArrayList = getPendingBillList;
                billsAdapter.MyDataChanged(billArrayList);
            } else {
                AppLog.write("Result----------=>", "" + new Gson().toJson(getPendingBillList));
            }
        }
    }

    private class BillComparator implements Comparator<GetBillStatus> {
        @Override
        public int compare(GetBillStatus g1, GetBillStatus g2) {
            if (Utility.convertToInteger(g1.getId()) > Utility.convertToInteger(g2.getId())) {
                return -1;
            } else if (Utility.convertToInteger(g1.getId()) < Utility.convertToInteger(g2.getId())) {
                return 1;
            }
            return 0;
        }
    }
}
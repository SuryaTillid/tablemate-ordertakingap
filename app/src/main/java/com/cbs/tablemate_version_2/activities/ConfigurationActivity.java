package com.cbs.tablemate_version_2.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.configuration.App;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.configuration.ConfigurationSettings;
import com.cbs.tablemate_version_2.configuration.CounterSaleSettings;
import com.cbs.tablemate_version_2.configuration.Settings;
import com.cbs.tablemate_version_2.helper.ConnectivityReceiver;
import com.cbs.tablemate_version_2.models.DbNameModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*********************************************************************
 * Created by Baraneeswari on 06-04-2016.
 ***********************************************************************/
public class ConfigurationActivity extends AppCompatActivity implements View.OnClickListener, ConnectivityReceiver.ConnectivityReceiverListener {
    private EditText ed_hotelCode, ed_IpAddress;
    private Button btn_Submit, btn_Cancel;
    private String hotelCode, IpAddress;
    private Context context;
    private Settings settings;
    private ConfigurationSettings configurationSettings;
    private CounterSaleSettings counterSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        try {
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getSupportActionBar().hide();
        } catch (Exception e) {
            e.printStackTrace();
        }
        setContentView(R.layout.activity_configuration);
        context = ConfigurationActivity.this;
        settings = new Settings(context);
        configurationSettings = new ConfigurationSettings(context);
        counterSettings = new CounterSaleSettings(context);
        IpAddress = configurationSettings.getIP_ADDRESS();
        hotelCode = configurationSettings.getHOTEL_CODE();

        ed_hotelCode = findViewById(R.id.ed_hotelCode);
        btn_Submit = findViewById(R.id.btn_Submit);
        btn_Cancel = findViewById(R.id.btn_Cancel);
        ed_IpAddress = findViewById(R.id.ed_IpAddress);
        btn_Cancel.setOnClickListener(this);
        btn_Submit.setOnClickListener(this);

        if (configurationSettings.isConfigured()) {
            AppLog.write("Ip Address", "----" + configurationSettings.getIP_ADDRESS());
            ed_IpAddress.setText(IpAddress);
            ed_hotelCode.setText(hotelCode);
        } else {
            AppLog.write("Empty", "----");
        }
    }

    @Override
    public void onClick(View v) {
        boolean isConnected = ConnectivityReceiver.isConnected();
        if (isConnected) {
            switch (v.getId()) {
                case R.id.btn_Cancel:
                    exit_app();
                    break;
                case R.id.btn_Submit:
                    if (DetectConnection.isOnline(context)) {
                        if (ed_hotelCode.getText().toString().equals("")) {
                            ed_hotelCode.setError("Field cannot be left blank..!");
                        } else if (ed_IpAddress.getText().toString().equals("")) {
                            ed_IpAddress.setError("Field cannot be left blank..!");
                        } else {
                            IpAddress = ed_IpAddress.getText().toString().trim();
                            IpAddress = IpAddress.replaceAll("\\s", "");
                            settings.setIP_ADDRESS(IpAddress);
                            configurationSettings.setIP_ADDRESS(IpAddress);
                            IpAddress = "http://" + IpAddress;
                            AppLog.write("IP Address--", "---" + IpAddress);
                            App.IP_ADDRESS = IpAddress;
                            hotelCode = ed_hotelCode.getText().toString().trim();
                            AppLog.write("Hotel Code--", "---" + hotelCode);
                            hotelCode = hotelCode.replaceAll("\\s", "");
                            getDBName(hotelCode);
                        }
                        break;
                    } else {
                        showInternetError();
                    }
            }
        } else {
            showInternetError();
        }
    }

    private void showInternetError() {
        Intent i = new Intent(context, Show_NoInternetMessage_Activity.class);
        startActivity(i);
    }

    private void getDBName(String hotelCode) {
        App app = new App();
        app.createRestAdaptor().getDB_Name(hotelCode).enqueue(new Callback<DbNameModel>() {
            @Override
            public void onResponse(Call<DbNameModel> call, Response<DbNameModel> response) {
                AppLog.write("Response---------", "---" + response.code());
                DbNameModel dbNameModel = response.body();
                if (null != dbNameModel) {
                    if ("1".equals(dbNameModel.getSuccess())) {
                        settings.setHOTEL_NAME(dbNameModel.getHotel_name());
                        configurationSettings.setHOTEL_NAME(dbNameModel.getHotel_name());
                        saveDBName(dbNameModel.getDb_name());
                        AppLog.write("Correct hotel code", "----" + dbNameModel);
                    } else if ("0".equals(dbNameModel.getSuccess())) {
                        Toast.makeText(context, "Incorrect hotel code..!", Toast.LENGTH_SHORT).show();
                    }
                }
                if (response.code() == 404) {
                    Toast.makeText(context, "Please check the IP Address..!", Toast.LENGTH_SHORT).show();
                } else if (response.code() == 500) {
                    Toast.makeText(context, "Internal Server Error..!", Toast.LENGTH_SHORT).show();
                } else if (response.code() != 200) {
                    Toast.makeText(context, "Failed to connect..!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DbNameModel> call, Throwable t) {

                Log.e("dsdfhdf", "" + t.toString());
            }
        });
    }

    private void saveDBName(String p_dbName) {
        configurationSettings.setDb_Name(p_dbName);
        settings.setDb_Name(p_dbName);
        counterSettings.setDb_Name(p_dbName);
        String hotel_code = ed_hotelCode.getText().toString().trim();
        configurationSettings.setHOTEL_CODE(hotel_code);
        configurationSettings.isConfigured();
        AppLog.write("DBName====", "\n" + configurationSettings.getDb_Name());
        Intent in = new Intent(ConfigurationActivity.this, LoginActivity.class);
        startActivity(in);
    }

    private void exit_app() {
        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
        homeIntent.addCategory(Intent.CATEGORY_HOME);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
    }

    @Override
    public void onBackPressed() {
        confirmExit();
    }

    private void confirmExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage("Are you sure want to exit?");
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                exit_app();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
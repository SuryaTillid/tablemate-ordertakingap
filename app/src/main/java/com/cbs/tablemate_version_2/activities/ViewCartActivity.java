package com.cbs.tablemate_version_2.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.UIWidgets.DividerItemDecoration;
import com.cbs.tablemate_version_2.UIWidgets.MyCustomDialog;
import com.cbs.tablemate_version_2.adapter.ViewCartAdapter;
import com.cbs.tablemate_version_2.configuration.App;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.configuration.ConfigurationSettings;
import com.cbs.tablemate_version_2.configuration.RestApi;
import com.cbs.tablemate_version_2.configuration.RestApiCalls;
import com.cbs.tablemate_version_2.configuration.Settings;
import com.cbs.tablemate_version_2.helper.CartManager;
import com.cbs.tablemate_version_2.helper.ConnectivityReceiver;
import com.cbs.tablemate_version_2.helper.ExceptionHandler;
import com.cbs.tablemate_version_2.interfaces.MenuItemQtyChangeListener;
import com.cbs.tablemate_version_2.models.CustomizationItemAddon;
import com.cbs.tablemate_version_2.models.CustomizationItemsType;
import com.cbs.tablemate_version_2.models.CustomizedItemsPortion;
import com.cbs.tablemate_version_2.models.Employee_Data;
import com.cbs.tablemate_version_2.models.Item;
import com.cbs.tablemate_version_2.models.Order;
import com.cbs.tablemate_version_2.models.OrderDetails;
import com.cbs.tablemate_version_2.models.OrderResponse;
import com.cbs.tablemate_version_2.models.PortionOrder;
import com.cbs.tablemate_version_2.models.StockItem;
import com.cbs.tablemate_version_2.utilities.Utility;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*********************************************************************
 * Created by Baraneeswari on 02-05-2016 in TableMateNew
 ***********************************************************************/
public class ViewCartActivity extends AppCompatActivity implements MenuItemQtyChangeListener, View.OnClickListener {

    CartManager cartManager;
    OrderDetails orderDetails = new OrderDetails();
    MyCustomDialog dialog;
    private ViewCartAdapter cartAdapter;
    private Context context;
    private RecyclerView rv_CartList;
    private int totalAmount;
    private String occupancy;
    private FloatingActionButton fab_OrderItems;
    private ArrayList<Item> menuList = new ArrayList<>();
    private ArrayList<CustomizedItemsPortion> portions = new ArrayList<>();
    private TextView txtGrandTotal, txtTableName;
    private String db_name, user_name, user_id;
    private String tableID, tableName;
    private Settings settings;
    private ConfigurationSettings configurationSettings;
    private String orderType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.fab_layout);

        context = ViewCartActivity.this;
        settings = new Settings(context);
        orderType = settings.getORDER_TYPE();
        configurationSettings = new ConfigurationSettings(context);
        db_name = configurationSettings.getDb_Name();
        user_name = settings.getUSER_NAME();
        cartManager = new CartManager(context);
        tableID = settings.getTableID();
        tableName = settings.getTABLE_NAME();
        occupancy = settings.getTOTAL_NUMBER_OF_GUESTS(tableID);
        AppLog.write("Occupancy-------=>", occupancy);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(settings.getHOTEL_NAME() + " - Cart");

        fab_OrderItems = findViewById(R.id.fab_order);
        rv_CartList = findViewById(R.id.rvCartList);
        txtGrandTotal = findViewById(R.id.txtGrandTotal);
        txtTableName = findViewById(R.id.txtCurrentTableName);

        fab_OrderItems.setOnClickListener(this);
        rv_CartList.addItemDecoration(new DividerItemDecoration(context, null));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rv_CartList.setLayoutManager(linearLayoutManager);
        menuList = cartManager.getItemList();
        txtTableName.setText(tableName);
        addLiveMenuItems();
        cartAdapter = new ViewCartAdapter(context, menuList, orderType);
        cartAdapter.setMenuListener(this);
        rv_CartList.setAdapter(cartAdapter);

        if (DetectConnection.isOnline(context)) {
            getQtyTotal();
            new GetUserIdDetails().execute();
        } else {
            showNoInternet();
        }
    }

    private void addLiveMenuItems() {
        Intent in = getIntent();
        String sListString = in.getStringExtra("liveMenuItems");
        if (!TextUtils.isEmpty(sListString) && null != menuList) {
            ArrayList<StockItem> sList = new ArrayList<>(Arrays.asList(new Gson().
                    fromJson(sListString, StockItem[].class)));
            for (Item item : menuList) {
                for (StockItem stockItem : sList) {
                    AppLog.write("Item name---", item.getName() + ":" + item.getLive_item() + "-->" + stockItem.getStockName());
                    if (item.getLive_item().equals("1") && item.getName().trim().equalsIgnoreCase(stockItem.getStockName().trim())) {
                        AppLog.write("Item name condition---", item.getName() + ":" + item.getLive_item() + "-->" + stockItem.getStockName());
                        item.setStockQuantity(stockItem.getStockQuantity());
                    }
                }
            }
        }
    }

    public void OrderProcess(String waiter_Id, String orders_modified) {

        Log.e("orders_modified", "" + orders_modified);
        boolean isConnected = ConnectivityReceiver.isConnected();
        if (isConnected) {
            PlaceOrder((App) getApplication(), orders_modified);
            //        new OrderConfirmed().execute(orders_modified);
        } else {
            showNoInternet();
        }
    }

    private void showNoInternet() {
        Intent i = new Intent(context, Show_NoInternetMessage_Activity.class);
        startActivity(i);
    }

    private OrderDetails collect_Details_for_order() {
        String delivery_partner = settings.getDELIVERY_PARTNER(tableID);
        orderDetails.setOrders(getOrderDetails());
        orderDetails.setUname(user_name);
        orderDetails.setBill_id(configurationSettings.getBILL__ID(tableID));
        if (orderType.equalsIgnoreCase("3") && !TextUtils.isEmpty(delivery_partner)) {
            orderDetails.setDelivery_partner(delivery_partner);
        } else {
            orderDetails.setDelivery_partner("None");
        }
        AppLog.write("value~~~~~~", "" + settings.getUSER_ID() + "--" + user_name);

        Log.e("orders", "" + getOrderDetails().size());
        return orderDetails;
    }

    /*
     * Order Array List
     * */
    private ArrayList<Order> getOrderDetails() {
        ArrayList<Order> ordersList = new ArrayList<>();
        try {
            AppLog.write("ItemSize", "menu" + menuList.size());

            for (Item item : menuList) {
                Order o_list = new Order();
                o_list.setMenu_item_id(item.getId());
                o_list.setWaiter_id(settings.getUSER_ID());

                o_list.setEmployeeId(item.getEmp_id());
                o_list.setQuantity(item.getItemQty());
                o_list.setCustom_data(constructCustomString(item));
                o_list.setCustomisation_comments(getCustomisationComment(item));
                o_list.setFilter_type_id(item.getFilter_type_name());
                String portion_size = getPortionSize(item);
                String item_price;
                if (!TextUtils.isEmpty(portion_size) && !(portions.get(0).getPortion_name().
                        equalsIgnoreCase("Regular"))) {
                    item_price = getPortionPrice(item);
                } else if (orderType.equals("2")) {

                    item_price = item.getAlternate_price();
                } else {
                    item_price = item.getPrice();
                }
                AppLog.write("PRICE_2", "" + item.getPrice_1() + "----" + item.getPrice());
                o_list.setSale_price(item_price);
                o_list.setDiscount("0.00");
                o_list.setDiscount_type("");
                o_list.setNc_order("0");
                o_list.setMrp_price(item_price);
                if (item.getEnable_NC().equalsIgnoreCase("1")) {
                    o_list.setDiscount(item.getInternal_costing());
                    o_list.setDiscount_type("NC");
                    o_list.setSale_price("0.00");
                    o_list.setNc_order("1");
                    o_list.setMrp_price(item.getInternal_costing());
                }
                o_list.setPortion_size(portion_size);
                ordersList.add(o_list);
            }
            int OrderSize = ordersList.size();
            AppLog.write("ordersList", "--" + new Gson().toJson(ordersList));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ordersList;
    }

    private String getPortionPrice(Item item) {
        String customPortion = "";
        CustomizedItemsPortion cust_Portion = item.getCustomPortion();
        if (null == cust_Portion) {
            customPortion += "";
        } else {
            PortionOrder portion = new PortionOrder();
            portion.setData_price(cust_Portion.getPortion_price());
            customPortion += cust_Portion.getPortion_price();
        }
        return customPortion;
    }

    private void getQtyTotal() {
        try {
            txtGrandTotal.setText(String.format("₹ %s", (float) Math.round(cartManager.getTotalNCNew(orderType) * 100) / 100));

        } catch (NumberFormatException n) {
            AppLog.write("Exception:::::", "--" + n);
        }
    }

    @Override
    public void incrementQuantity(String p_itemId, int adapterPosition) {
        for (Item item : menuList) {
            if (p_itemId.equals(item.getCustomId())) {
                if (item.getLive_item().equals("1")) {
                    Double stockQty = Utility.convertToDouble(item.getStockQuantity());
                    int itemQty = Utility.convertToInteger(item.getItemQty());
                    AppLog.write("Stock quantity---", "" + stockQty);
                    if (stockQty > itemQty) {
                        item.setItemQty("" + cartManager.addToCart(item));
                    } else {
                        Toast.makeText(context, "Item not available", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    item.setItemQty("" + cartManager.addToCart(item));
                }
                float total = (float) Math.round(cartManager.getTotalNCNew(orderType) * 100) / 100;
                txtGrandTotal.setText("₹ " + total);


            }
        }
        menuList.clear();
        menuList = cartManager.getItemList();
        cartAdapter.MyDataChanged(menuList);
    }

    @Override
    public void decrementQuantity(String p_itemId, int adapterPosition) {
        for (Item item : menuList) {
            if (p_itemId.equals(item.getCustomId())) {
                int qty = Utility.convertToInteger(item.getItemQty());
                if (qty > 0) {
                    cartManager.removeFromCart(item);
                    AppLog.write("Quantity---1", "---" + qty);
                    qty--;
                    item.setItemQty("" + qty);
                    String itemPrice;
                    if (orderType.equalsIgnoreCase("rooms")) {
                        itemPrice = item.getAlternate_price();
                    } else {
                        itemPrice = item.getPrice();
                    }
                    totalAmount -= Utility.convertToFloat(itemPrice);
                    float total = (float) Math.round(cartManager.getTotalNCNew(orderType) * 100) / 100;
                    txtGrandTotal.setText("₹ " + total);
                }
                cartAdapter.notifyDataSetChanged();
            }
        }
        menuList.clear();
        menuList = cartManager.getItemList();
        cartAdapter.MyDataChanged(menuList);
    }

    @Override
    public void show_open_customization(final String p_itemId) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_user_customization);
        final EditText edit_customizationComments = dialog.findViewById(R.id.ed_specification);
        Button btnCancel = dialog.findViewById(R.id.btn_Cancel);
        Button btnOrder = dialog.findViewById(R.id.btn_Submit);
        dialog.show();

        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (Item item : menuList) {
                    if (p_itemId.equals(item.getCustomId())) {
                        String customizationComments = edit_customizationComments.getText().toString();
                        if (!TextUtils.isEmpty(customizationComments))
                            cartManager.addCustomizationComment(item,
                                    TextUtils.isEmpty(customizationComments) ? "NA" : customizationComments);
                        dialog.dismiss();
                        menuList = cartManager.getItemList();
                        AppLog.write("customList", new Gson().toJson(menuList));
                        cartAdapter.MyDataChanged(menuList);
                        //String s = menuList.get(0).getCustomisation_comments();
                        //AppLog.write("@@@@@@@@@@@@", "--" + s);
                    }
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void update_nc(String p_itemId) {
        for (Item item : menuList) {
            if (p_itemId.equals(item.getCustomId())) {
                cartManager.addNC(item);
                menuList = cartManager.getItemList();
                AppLog.write("customList_1", new Gson().toJson(menuList));
                txtGrandTotal.setText("₹ " + (float) Math.round(cartManager.getTotal(orderType) * 100) / 100);
                cartAdapter.MyDataChanged(menuList);
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent menu = new Intent(context, MenuActivityWithFilters.class);
        menu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(menu);
        finish();
    }

    @Override
    public void onClick(View v) {

        if (v == fab_OrderItems) {
            AppLog.write("Orders list------*****", "---" + new Gson().toJson(menuList) + "\n" + menuList.size());
            if (menuList.size() != 0) {
                fab_OrderItems.setEnabled(false);
                String default_waiter_id = "1";
                String orders_modified = new Gson().toJson(collect_Details_for_order());


                Log.e("collect_Details", "" + new Gson().toJson(collect_Details_for_order()));
                OrderProcess(default_waiter_id, orders_modified);
            } else {
                Intent live = new Intent(context, MenuActivityWithFilters.class);
                startActivity(live);
            }
        }

    }

    private String getPortionSize(Item item) {
        String customPortion = "";
        String c_string = constructCustomString(item);
        AppLog.write("TAG", "---" + c_string);
        CustomizedItemsPortion cust_Portion = item.getCustomPortion();

        if (null == cust_Portion) {
            customPortion += "";
        } else {
            PortionOrder portion = new PortionOrder();
            if (cust_Portion.getPortion_name().equalsIgnoreCase("Regular") &&
                    TextUtils.isEmpty(c_string)) {
                customPortion += "";
            } else {
                portion.setData_size(cust_Portion.getPortion_name());
                customPortion += cust_Portion.getPortion_name();
            }
        }
        return customPortion;
    }

    private String getCustomisationComment(Item item) {
        String customComments;
        if (null == item.getCustomisation_comments()) {
            customComments = "";
        } else {
            customComments = item.getCustomisation_comments();
        }
        return customComments;
    }

    /**
     * method to construct string for the custom extra
     *
     * @param item -
     * @return -
     */
    private String constructCustomString(Item item) {
        AppLog.write("value----", "--" + new Gson().toJson(item));
        String customExtra = "";
        //String employeeId = "";

        /*//Employee Data

        employeeId += (null == item.getEmployeeType() ? "null" : new Gson().toJson(item.getEmployeeType()));

        if (null == item.getEmployeeType() || item.getEmployeeType().size() == 0) {
            employeeId += " null";
        } else {
            ArrayList<String> employee = new ArrayList<>();
            employee.clear();
            for (EmployeeTypeName e_type : item.getEmployeeType()) {
                employee.add(e_type.getId());
            }
            employeeId += " " + new Gson().toJson(employee);
        }*/

        customExtra += (null == item.getCustomAddOn() ? "null" : new Gson().toJson(item.getCustomAddOn()));

        if (null == item.getCustomType() || item.getCustomType().size() == 0) {
            customExtra += " null";
        } else {
            ArrayList<String> types = new ArrayList<>();
            types.clear();
            for (CustomizationItemsType type : item.getCustomType()) {
                types.add(type.getCustomisation_name());
            }
            customExtra += " " + new Gson().toJson(types);
        }

        CustomizedItemsPortion custPortion = item.getCustomPortion();
        if (null == custPortion) {
            customExtra += " null";
        } else {
            String portionRegular = checkOnlyRegular(item.getCustomType(), item.getCustomAddOn(), item.getCustomPortion());
            AppLog.write("CUSTOM_EXTRA_PORTION_REGULAR--", portionRegular);
            if (TextUtils.isEmpty(portionRegular) || portionRegular.equalsIgnoreCase(null)) {
                customExtra += " null";
            } else if (portionRegular.equalsIgnoreCase("[null]")) {
                customExtra += " [null]";
            } else {
                ArrayList<PortionOrder> portionOrders = new ArrayList<>();
                PortionOrder portion = new PortionOrder();
                portion.setData_attr(custPortion.getPortion_name());
                portion.setData_size(custPortion.getPortion_size());
                double data_price = Utility.convertToDouble(custPortion.getPortion_price());
                String price = String.format("%.2f", data_price);
                portion.setData_price(price);
                portionOrders.add(portion);
                customExtra += " " + new Gson().toJson(portionOrders);
                AppLog.write("T3--", "--" + customExtra);
            }
        }
        if (customExtra.equalsIgnoreCase("null null null") || customExtra.equalsIgnoreCase("null null [null]")) {
            String s = "";
            return s;
        } else {
            AppLog.write("CUSTOM_EXTRA---", "-" + customExtra.trim());
            return customExtra.trim();
        }
    }

    private String checkOnlyRegular(ArrayList<CustomizationItemsType> customType, CustomizationItemAddon customAddOn, CustomizedItemsPortion customPortion) {
        portions.clear();
        portions.add(customPortion);
        String s = null;
        AppLog.write("NULL", "---" + customType.size() + "--" + portions.size() + "----" + customAddOn + "---" + customPortion.getPortion_name());
        if (portions.size() == 1 && customPortion.getPortion_name().equalsIgnoreCase("Regular")) {
            String portion = "[null]";
            return portion;
        } else if (customType.size() == 0 && customAddOn == null && customPortion.getPortion_name().equalsIgnoreCase("Regular")) {
            return s;
        } else {
            return customPortion.getPortion_name();
        }
    }

    public void PlaceOrder(App app, String orderdeatils) {
        dialog = new MyCustomDialog(context, "Processing Order...");
        dialog.show();
        RestApi apiService = app.createRestAdaptor();
        Call<OrderResponse> call = apiService.submitOrder(orderdeatils, db_name);
        call.enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(@NonNull Call<OrderResponse> call, @NonNull Response<OrderResponse> response) {
                OrderResponse orderResponse = response.body();
                Log.e("responePlace", "" + response.code());
                dialog.dismiss();

                if (orderResponse != null) {
                    String responsenew = orderResponse.getSuccess();

                    if (!responsenew.equals("-1")) {
                        if (responsenew.equals("1") || responsenew.equals("true")) {
                            Toast.makeText(context, "Order Confirmed..!", Toast.LENGTH_SHORT).show();
                            fab_OrderItems.setEnabled(true);
                       /* Intent i = new Intent(context,PrintKOTActivity.class);
                        i.putExtra("KOT",generated_KOT);
                        startActivity(i);*/
                            startActivity(new Intent(context, TableActivity_1.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                            cartManager.clearCart();
                        } else {
                            Toast.makeText(context, "Failed to place the order..!", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, "Please lock table to order items", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(context, TableActivity_1.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                    }
                } else {
                    AppLog.showToast(context, "Failed to place the order..!");
                }

            }

            @Override
            public void onFailure(@NonNull Call<OrderResponse> call, @NonNull Throwable t) {
                dialog.dismiss();
            }
        });
    }

    private class OrderConfirmed extends AsyncTask<String, String, OrderResponse> {


        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Processing Order...");
            dialog.show();
        }

        @Override
        protected OrderResponse doInBackground(String... params) {

            Log.e("params", "" + params[0]);
            RestApiCalls call = new RestApiCalls();
            return call.submitOrder((App) getApplication(), String.valueOf(params[0]), db_name);
        }

        @Override
        protected void onPostExecute(OrderResponse orderResponse) {
            //  Log.e("responseOrder----", "" + orderResponse);
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            if (orderResponse != null) {
                String response = orderResponse.getSuccess();

                if (!response.equals("-1")) {
                    if (response.equals("1") || response.equals("true")) {
                        Toast.makeText(context, "Order Confirmed..!", Toast.LENGTH_SHORT).show();
                        fab_OrderItems.setEnabled(true);
                       /* Intent i = new Intent(context,PrintKOTActivity.class);
                        i.putExtra("KOT",generated_KOT);
                        startActivity(i);*/
                        startActivity(new Intent(context, TableActivity_1.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                        cartManager.clearCart();
                    } else {
                        Toast.makeText(context, "Failed to place the order..!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "Please lock table to order items", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(context, TableActivity_1.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                }
            } else {
                AppLog.showToast(context, "Failed to place the order..!");
            }
        }
    }

    private class GetUserIdDetails extends AsyncTask<Void, Void, ArrayList<Employee_Data>> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<Employee_Data> doInBackground(Void... params) {
            ArrayList<Employee_Data> employeeList = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            employeeList = call.getEmployeeDetailsList((App) getApplication(), db_name);
            return employeeList;
        }

        @Override
        protected void onPostExecute(ArrayList<Employee_Data> employee_dataArrayList) {
            if (null != dialog && dialog.isShowing()) ;
            dialog.dismiss();
            if (null != employee_dataArrayList) {
                for (Employee_Data t : employee_dataArrayList) {
                    if (user_name.equalsIgnoreCase(t.getName())) {
                        user_id = t.getId();
                        settings.setUSER_ID(user_id);
                        AppLog.write("-----------", "--" + user_id);
                    }
                }
            }
        }
    }
}
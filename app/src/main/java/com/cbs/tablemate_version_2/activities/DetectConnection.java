package com.cbs.tablemate_version_2.activities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/*********************************************************************
 * Created by Baraneeswari on 11-05-2016 in TableMateNew
 ***********************************************************************/
public class DetectConnection {
    private static Context context;

    public static boolean isConnectedInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}

package com.cbs.tablemate_version_2.activities;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.configuration.Settings;
import com.cbs.tablemate_version_2.helper.CartManager;
import com.cbs.tablemate_version_2.models.Item;
import com.cbs.tablemate_version_2.models.OrdersForTable;
import com.cbs.tablemate_version_2.utilities.ConnectionException;
import com.cbs.tablemate_version_2.utilities.DateUtil;
import com.cbs.tablemate_version_2.utilities.MyBluetoothConnector;
import com.cbs.tablemate_version_2.utilities.StringUtils;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/*********************************************************************
 * Created by Barani on 28-06-2017 in TableMateNew
 ***********************************************************************/
public class PrintKOTActivity extends AppCompatActivity {
    private Button mConnectBtn;
    private Button mEnableBtn;
    private Button btn_Print;
    private Spinner spinner_deviceList;
    private ProgressDialog myProgressDialog;
    private ProgressDialog connectivityDialog;
    private Context context;
    private String db_name, table_id, generated_KOT;
    private Settings settings;
    private ArrayList<OrdersForTable> orderList = new ArrayList<>();
    private ArrayList<Item> menuList = new ArrayList<>();
    private BluetoothAdapter bluetoothAdapter;
    private MyBluetoothConnector bluetoothConnector;
    private CartManager cartManager;
    private ArrayList<BluetoothDevice> myDeviceList = new ArrayList<>();
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                if (state == BluetoothAdapter.STATE_ON) {
                    showEnabled();
                } else if (state == BluetoothAdapter.STATE_OFF) {
                    showDisabled();
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                myDeviceList = new ArrayList<>();
                myProgressDialog.show();
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                myProgressDialog.dismiss();
                updateDeviceList();
            } else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                myDeviceList.add(device);
                showToast("Found device..!" + device.getName());
            } else if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);
                if (state == BluetoothDevice.BOND_BONDED) {
                    showToast("Paired..!");
                    connectDevice();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_printer);

        context = PrintKOTActivity.this;
        settings = new Settings(context);
        cartManager = new CartManager(context);
        db_name = settings.getDbName();
        table_id = settings.getTableID();
        AppLog.write("TABLE.ID", "-" + table_id);
        Intent i = getIntent();
        generated_KOT = i.getStringExtra("KOT");

        mConnectBtn = findViewById(R.id.btn_connect);
        mEnableBtn = findViewById(R.id.btn_enable);
        btn_Print = findViewById(R.id.btn_print);
        spinner_deviceList = findViewById(R.id.spinner_deviceList);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        menuList = cartManager.getItemList();

        //enable bluetooth
        mEnableBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(intent, 1000);
            }
        });

        //connect/disconnect
        mConnectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                connect();
                printContent(menuList);
            }
        });

        //print
        btn_Print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        IntentFilter filter = new IntentFilter();

        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);

        registerReceiver(mReceiver, filter);

        if (bluetoothAdapter == null) {
            showDeviceNotSupports();
        } else {
            if (!bluetoothAdapter.isEnabled()) {
                showDisabled();
            } else {
                showEnabled();
                Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
                if (pairedDevices != null) {
                    myDeviceList.addAll(pairedDevices);
                    updateDeviceList();
                }
                myProgressDialog = new ProgressDialog(this);
                myProgressDialog.setMessage("Scanning...");
                myProgressDialog.setCancelable(false);
                myProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        bluetoothAdapter.cancelDiscovery();
                    }
                });

                connectivityDialog = new ProgressDialog(this);
                connectivityDialog.setMessage("Connecting...");
                connectivityDialog.setCancelable(false);
                callConnector();
            }
        }
    }

    @Override
    public void onPause() {
        if (bluetoothAdapter != null) {
            if (bluetoothAdapter.isDiscovering()) {
                bluetoothAdapter.cancelDiscovery();
            }
        }

        if (bluetoothConnector != null) {
            try {
                bluetoothConnector.disconnect();
            } catch (ConnectionException e) {
                e.printStackTrace();
            }
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    private void printContent(ArrayList<Item> ordersForTable) {

        String formatFirstLine = "%1$-26s %2$4s";
        String formatThirdLine = "%1$-15s %2$15s";
        int maxChars = 32;
        String newLine = "\r\n";
        StringBuilder content = new StringBuilder();
        content.append(StringUtils.center("KOT" + generated_KOT, maxChars)).append(newLine);
        content.append(StringUtils.center("Date : " + DateUtil.format(System.currentTimeMillis()), maxChars)).append(newLine);
        content.append(String.format(formatThirdLine, StringUtils.center("Table : " + settings.getTABLE_NAME(), 14),
                StringUtils.center("Waiter : " + settings.getUSER_NAME(), 14))).append(newLine);
        content.append(StringUtils.center("--------------------------------", maxChars));
        content.append(String.format(formatThirdLine, StringUtils.center("Item Name", 15), StringUtils.center("Qty", 15)));
        content.append(StringUtils.center("--------------------------------", maxChars)).append(newLine);
        for (Item orders : ordersForTable) {
            String itemName = orders.getName();
            List<String> itemNames = StringUtils.splitEqually(itemName, 24);
            content.append(String.format(formatFirstLine, itemNames.get(0),
                    orders.getItemQty()) + newLine);
            for (int i = 1; i < itemNames.size(); i++) {
                content.append(String.format(formatFirstLine, itemNames.get(i), "") + newLine);
            }
        }
        content.append(StringUtils.center("--------------------------------", maxChars) + newLine);
        AppLog.write("CONTENT---", "" + content + "--" + db_name);
        sendData(content);
    }

    private void sendData(StringBuilder s) {
        try {
            bluetoothConnector.sendData(s);
            startActivity(new Intent(context, TableActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
            cartManager.clearCart();
        } catch (ConnectionException e) {
            e.printStackTrace();
        }
    }

    private void connect() {
        if (myDeviceList == null || myDeviceList.size() == 0) {
            return;
        }
        BluetoothDevice device = myDeviceList.get(spinner_deviceList.getSelectedItemPosition());
        if (device.getBondState() == BluetoothDevice.BOND_NONE) {
            try {
                createBond(device);
            } catch (Exception e) {
                showToast("Failed to pair device");
                return;
            }
        }

        try {
            if (!bluetoothConnector.isConnected()) {
                bluetoothConnector.connect(device);
            } else {
                bluetoothConnector.disconnect();
                showDisconnected();
            }
        } catch (ConnectionException e) {
            e.printStackTrace();
        }
    }

    private void showDeviceNotSupports() {
        showToast("Bluetooth is unsupported by this device");
        mConnectBtn.setEnabled(false);
        btn_Print.setEnabled(false);
        spinner_deviceList.setEnabled(false);
    }

    private void callConnector() {
        bluetoothConnector = new MyBluetoothConnector(new MyBluetoothConnector.BluetoothConnectionListener() {

            @Override
            public void onStartConnecting() {
                connectivityDialog.show();
            }

            @Override
            public void onConnectionSuccess() {
                connectivityDialog.dismiss();
                showConnected();
            }

            @Override
            public void onConnectionFailed(String error) {
                connectivityDialog.dismiss();
            }

            @Override
            public void onConnectionCancelled() {
                connectivityDialog.dismiss();
            }

            @Override
            public void onDisconnected() {
                showDisconnected();
            }
        });
    }

    private void showConnected() {
        showToast("Connected..!");
        mConnectBtn.setText(R.string.text_connect);
        btn_Print.setEnabled(true);
        spinner_deviceList.setEnabled(false);
    }

    private void showDisconnected() {
        showToast("Disconnected..!");
        mConnectBtn.setText(R.string.text_disconnect);
        btn_Print.setEnabled(false);
        spinner_deviceList.setEnabled(true);
    }

    private void showDisabled() {
        showToast("Bluetooth disabled..!");
        mEnableBtn.setVisibility(View.VISIBLE);
        mConnectBtn.setVisibility(View.GONE);
        spinner_deviceList.setVisibility(View.GONE);
    }

    private void showToast(String s) {
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
    }

    private void showEnabled() {
        showToast("Bluetooth enabled..!");
        mEnableBtn.setVisibility(View.GONE);
        mConnectBtn.setVisibility(View.VISIBLE);
        spinner_deviceList.setVisibility(View.VISIBLE);
    }

    private void updateDeviceList() {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.simple_spinner_item, getArray(myDeviceList));

        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);

        spinner_deviceList.setAdapter(adapter);
        spinner_deviceList.setSelection(0);
    }

    private String[] getArray(ArrayList<BluetoothDevice> myDeviceList) {
        String[] list = new String[0];

        if (myDeviceList == null)
            return list;

        int size = myDeviceList.size();
        list = new String[size];

        for (int i = 0; i < size; i++) {
            list[i] = myDeviceList.get(i).getName();
        }

        return list;
    }

    private void connectDevice() {
        if (myDeviceList == null || myDeviceList.size() == 0) {
            return;
        } else {
            BluetoothDevice device = myDeviceList.get(spinner_deviceList.getSelectedItemPosition());
            try {
                if (!bluetoothConnector.isConnected()) {
                    bluetoothConnector.connect(device);
                } else {
                    bluetoothConnector.disconnect();
                    showDisconnected();
                }
            } catch (ConnectionException e) {
                e.printStackTrace();
            }
            if (device.getBondState() == BluetoothDevice.BOND_NONE) {
                try {
                    createBond(device);
                } catch (Exception e) {
                    showToast("Failed to pair device..!");
                }
            }
        }
    }

    private void createBond(BluetoothDevice device) throws Exception {

        try {
            Class<?> cl = Class.forName("android.bluetooth.BluetoothDevice");
            Class<?>[] par = {};

            Method method = cl.getMethod("createBond", par);

            method.invoke(device);

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, TableActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
        cartManager.clearCart();
    }
}
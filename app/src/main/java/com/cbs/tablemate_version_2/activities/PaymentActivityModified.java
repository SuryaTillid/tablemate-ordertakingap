package com.cbs.tablemate_version_2.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.UIWidgets.DividerItemDecoration;
import com.cbs.tablemate_version_2.UIWidgets.MyCustomDialog;
import com.cbs.tablemate_version_2.adapter.GetInputForPaymentDetailsAdapter;
import com.cbs.tablemate_version_2.configuration.App;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.configuration.ConfigurationSettings;
import com.cbs.tablemate_version_2.configuration.CounterSaleSettings;
import com.cbs.tablemate_version_2.configuration.RestApiCalls;
import com.cbs.tablemate_version_2.configuration.Settings;
import com.cbs.tablemate_version_2.helper.PendingBillsDB;
import com.cbs.tablemate_version_2.interfaces.AddPaymentValuesListener;
import com.cbs.tablemate_version_2.models.BillCalcModel;
import com.cbs.tablemate_version_2.models.BillPay;
import com.cbs.tablemate_version_2.models.GetPaymentAmount;
import com.cbs.tablemate_version_2.models.GetPaymentOptions;
import com.cbs.tablemate_version_2.models.POS_Bill;
import com.cbs.tablemate_version_2.models.PaymentOption;
import com.cbs.tablemate_version_2.utilities.Utility;
import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/*********************************************************************
 * Created by Barani on 26-09-2017 in TableMateNew
 ***********************************************************************/
public class PaymentActivityModified extends AppCompatActivity implements AddPaymentValuesListener {

    private Settings settings;
    private CounterSaleSettings counterSettings;
    private String billId;
    private String db_name;
    private String storeID;
    private String tableId;
    private Context context;
    private String tableName;
    private String billAmount;
    private String pay_option;
    private String billDetails;
    private String display_bill_id;
    private RecyclerView rc_pay_type;
    private LinearLayoutManager layoutManager;
    private String amount_received, mobile_number;
    private double balance_amount, amountReceived;
    private GetInputForPaymentDetailsAdapter payAdapter;
    private ConfigurationSettings configurationSettings;
    private ArrayList<PaymentOption> options = new ArrayList<>();
    private Button add_option, remove_option, btn_pay, btnAddPay;
    private ArrayList<GetPaymentOptions> pay_options = new ArrayList<>();
    private ArrayList<POS_Bill> billList = new ArrayList<>();
    private EditText et_GrandTotal, et_amount_received, et_balance_due, et_tip;
    private double balance;

    @SuppressLint({"DefaultLocale", "WrongConstant"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_redesign);

        Intent i = getIntent();
        context = PaymentActivityModified.this;
        settings = new Settings(context);
        configurationSettings = new ConfigurationSettings(context);
        counterSettings = new CounterSaleSettings(context);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(settings.getHOTEL_NAME() + " - Pay");

        db_name = configurationSettings.getDb_Name();
        tableId = i.getStringExtra("TABLE_ID");
        tableName = i.getStringExtra("TABLE_NAME");
        mobile_number = settings.getMOBILE_NUMBER(tableId);
        storeID = settings.getSTORE_ID();
        billId = i.getStringExtra("BILL_ID");
        display_bill_id = i.getStringExtra("DISPLAY_BILL");
        billAmount = i.getStringExtra("BILL_AMOUNT");
        AppLog.write("Bill Values are---->", display_bill_id + "---" +
                billAmount + "---" + billId + "---" + storeID + "---" + tableId + "---" + tableName);
        initialize();

        billAmount = String.format("%.2f", Utility.convertToDouble(billAmount));
        et_GrandTotal.setText("₹ " + billAmount);

        btn_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new BillCalculation().execute(billId);
            }
        });
        btnAddPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (balance > 0 && options.size() < 1) {
                    getPaymentDetailsDialog(getPayOptions(pay_options), -1, null, null, null);
                    btnAddPay.setVisibility(View.GONE);
                }
            }
        });
        add_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppLog.write("Options_size--", "--" + options.size());
                if (balance > 0 && options.size() < 5) {
                    getPaymentDetailsDialog(getPayOptions(pay_options), -1, null, null, null);
                }
            }
        });
        remove_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removePaymentOptions();
                if (options.size() == 0) {
                    btnAddPay.setVisibility(View.VISIBLE);
                }
            }
        });

        et_tip.addTextChangedListener((new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable edt) {
                try {
                    String temp = edt.toString();
                    int posDot = temp.indexOf(".");
                    AppLog.write("POS DOT---", "" + posDot);
                    if (posDot <= 0) {
                        return;
                    }
                    if (temp.length() - posDot - 1 > 2) {
                        edt.delete(posDot + 2, posDot + 3);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }));

        rc_pay_type.addItemDecoration(new DividerItemDecoration(context, null));
        payAdapter = new GetInputForPaymentDetailsAdapter(context, options);
        payAdapter.setPaymentOptionsSelectionListener(this);
        layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rc_pay_type.setLayoutManager(layoutManager);
        rc_pay_type.setAdapter(payAdapter);

        if (DetectConnection.isOnline(context)) {
            //new BillCalculation().execute(billId);
            new SendPrintRequestForBillPreview().execute(billId, "1");
            new GetPaymentType().execute();
        } else {
            showNoInternet();
        }
        updateBalance();
    }

    private void validateBillStatus(ArrayList<POS_Bill> billList) {
        for (POS_Bill bill : billList) {
            if (bill.getBill_status().equalsIgnoreCase("4")) {
                Toast.makeText(context, "Bill cancelled.!", Toast.LENGTH_SHORT).show();
                redirectPage();
            } else if (bill.getBill_status().equalsIgnoreCase("2")) {
                Toast.makeText(context, "Bill already paid.!", Toast.LENGTH_SHORT).show();
                redirectPage();
            } else {
                amount_received = String.valueOf(amountReceived);
                roundValue(amount_received, billAmount);
                String amount = String.valueOf(balance_amount);
                AppLog.write("Balance Amount-----", "--" + amount);
                callPaymentFunction(options);
            }
        }
    }

    private double roundValue(String amount_received, String billAmount) {
        balance_amount = (Utility.convertToFloat(amount_received) - Utility.convertToFloat(billAmount));
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        balance_amount = Double.valueOf(twoDForm.format(balance_amount));
        AppLog.write("Balance--", "" + balance_amount);
        return balance_amount;
    }

    private void callPaymentFunction(ArrayList<PaymentOption> pay_options) {
        String get_pay_amount = new Gson().toJson(getPaymentAmount(pay_options));
        String get_pay_option = new Gson().toJson(getPaymentMode(pay_options));
        AppLog.write("OPTION------", "--" + get_pay_amount + "--" + get_pay_option);
        new BillPayment().execute(amount_received, "0", get_pay_amount, get_pay_option);
    }

    private ArrayList<GetPaymentAmount> getPaymentMode(ArrayList<PaymentOption> pay_option) {
        ArrayList<GetPaymentAmount> pay_mode_list = new ArrayList<>();
        for (PaymentOption getPaymentOptions : pay_option) {
            GetPaymentAmount payment_mode = new GetPaymentAmount();
            payment_mode.setMode(getPaymentOptions.getPaymentType());
            pay_mode_list.add(payment_mode);
        }
        return pay_mode_list;
    }

    private ArrayList<GetPaymentAmount> getPaymentAmount(ArrayList<PaymentOption> amount_received) {
        ArrayList<GetPaymentAmount> pay_list = new ArrayList<>();
        for (PaymentOption getPaymentOptions : amount_received) {
            GetPaymentAmount payment = new GetPaymentAmount();
            payment.setAmount(getPaymentOptions.getAmount());
            pay_list.add(payment);
        }
        return pay_list;
    }

    private List<String> getPayOptions(ArrayList<GetPaymentOptions> pay_options) {
        List<String> list = new ArrayList<>();
        for (GetPaymentOptions payOption : pay_options) {
            list.add(payOption.getPaymentmode());
        }
        return list;
    }

    private void initialize() {
        et_GrandTotal = findViewById(R.id.et_GrandTotal);
        et_amount_received = findViewById(R.id.et_amount_received);
        et_balance_due = findViewById(R.id.et_balance_due);
        et_tip = findViewById(R.id.et_tip);
        rc_pay_type = findViewById(R.id.rv_pay_option);
        btn_pay = findViewById(R.id.btn_pay_bill);
        btnAddPay = findViewById(R.id.btnAddPay);
        add_option = findViewById(R.id.btnAdd);
        remove_option = findViewById(R.id.btnRemove);
    }

    private void showNoInternet() {
        Intent i = new Intent(context, Show_NoInternetMessage_Activity.class);
        startActivity(i);
    }

    /*
     * Listener - payment type
     * */
    @Override
    public void setPaymentValues(int position, String amount, String card_num, String payType, boolean selected) {
        getPaymentDetailsDialog(getPayOptions(pay_options), position, payType, amount, card_num);
        AppLog.write("Pay_Values----", "" + amount + "---" + card_num + "---" + payType + "---" + selected);
    }

    private void redirectPage() {
        //Intent i = new Intent(context, SelectOrderTypeActivity.class);
        //startActivity(i);
        checkCounterSaleType();
    }

    private void checkCounterSaleType() {
        if (counterSettings.isConfigured()) {
            Intent intent = new Intent(context, PrintForCounterSaleBill.class);
            intent.putExtra("BILL_ID", billId);
            intent.putExtra("PAY_MODE", pay_option);
            intent.putExtra("TABLE_ID", tableId);
            intent.putExtra("TABLE_NAME", tableName);
            intent.putExtra("CUSTOMER_ID", settings.getCUSTOMER_ID(tableId));
            startActivity(intent);
        } else {
            Intent intent = new Intent(context, BluetoothPrint_for_Payment.class);
            intent.putExtra("BILL_ID", billId);
            intent.putExtra("PAY_MODE", pay_option);
            intent.putExtra("TABLE_ID", tableId);
            intent.putExtra("TABLE_NAME", tableName);
            intent.putExtra("CUSTOMER_ID", settings.getCUSTOMER_ID(tableId));
            startActivity(intent);
        }
    }

    private void addPaymentOptions(int i, int position, String amount, String pin) {
        PaymentOption option = i != -1 ? options.get(i) : new PaymentOption();
        if (null == option) {
            option = new PaymentOption();
        }
        option.setAmount(TextUtils.isEmpty(amount) ? "" : amount);
        option.setPin(TextUtils.isEmpty(pin) ? "" : pin);
        option.setPaymentType(pay_options.get(position).getPaymentmode());
        if (i == -1) {
            options.add(option);
        }
        AppLog.write("Pay options---", "---" + new Gson().toJson(options));
        payAdapter.MyDataChanged(options);
        updateBalance();
    }

    private void removePaymentOptions() {
        if (options.size() > 0) {
            options.remove(options.size() - 1);
        }
        payAdapter.MyDataChanged(options);
        updateBalance();
    }

    private void getPaymentDetailsDialog(final List<String> payOptions, final int position, final String paymentType, String amount, String pin) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_pay_option);

        final Button btn_proceed_pay = dialog.findViewById(R.id.btnProceed_pay);
        final Button btn_cancel_pay = dialog.findViewById(R.id.btnCancel_pay);
        final Spinner spinner_option = dialog.findViewById(R.id.pay_option);
        spinner_option.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, payOptions));
        final EditText edit_amount = dialog.findViewById(R.id.edit_amount);
        final EditText edit_card_num = dialog.findViewById(R.id.edit_card_num);

        edit_amount.setText("" + balance);
        edit_amount.setSelection(edit_amount.getText().length());

        spinner_option.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    String s = payOptions.get(position);
                    AppLog.write("Position_spinner--", "--" + s);
                    if (s.equalsIgnoreCase("Cash")) {
                        edit_card_num.setVisibility(View.GONE);
                    } else {
                        edit_card_num.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        int ptPosition = -1;
        if (position != -1) {
            for (String payOption : payOptions) {
                ptPosition++;
                if (payOption.equalsIgnoreCase(paymentType)) {
                    break;
                }
            }
            spinner_option.setSelection(ptPosition);
            edit_amount.setText(amount);
            edit_card_num.setText(pin);
        }

        edit_amount.addTextChangedListener((new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable edt) {
                try {
                    String temp = edt.toString();
                    int posDot = temp.indexOf(".");
                    AppLog.write("POS_DOT---", "" + posDot);
                    if (posDot <= 0) {
                        return;
                    }
                    if (temp.length() - posDot - 1 > 2) {
                        edt.delete(posDot + 2, posDot + 3);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }));

        btn_proceed_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double amount = Utility.convertToDouble(edit_amount.getText().toString());
                if (validate(amount)) {
                    AppLog.write("Pay_option--", paymentType + "--" + spinner_option.getSelectedItem());
                    addPaymentOptions(position, spinner_option.getSelectedItemPosition(), edit_amount.getText().toString(), edit_card_num.getText().toString());
                    dialog.dismiss();
                }
            }
        });

        btn_cancel_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (options.size() == 0) {
                    btnAddPay.setVisibility(View.VISIBLE);
                }
            }
        });
        dialog.show();
    }

    private boolean validate(double amount) {
        if (amount > 0) {
            return true;
        } else {
            Toast.makeText(context, "Please enter valid amount..!", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    private void updateBalance() {
        double amountPaid = 0;
        for (PaymentOption option : options) {
            amountPaid += Utility.convertToDouble(option.getAmount());
        }
        balance = Utility.convertToDouble(billAmount) - amountPaid;
        et_balance_due.setText("₹ " + balance);
        amountReceived = amountPaid;
        et_amount_received.setText("₹ " + amountPaid);
        if (balance <= 0) {
            btn_pay.setBackgroundResource(R.drawable.add_btn);
            btn_pay.setTextColor(Color.WHITE);
            btn_pay.setAlpha((float) 1);
            btn_pay.setFocusable(true);
        } else {
            btn_pay.setBackgroundResource(R.drawable.edit_background_disabled);
            btn_pay.setTextColor(Color.BLACK);
            btn_pay.setAlpha((float) 0.5);
            btn_pay.setFocusable(false);
        }
    }

    private class GetPaymentType extends AsyncTask<Boolean, Void, ArrayList<GetPaymentOptions>> {

        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Loading...");
            dialog.show();
        }

        @Override
        protected ArrayList<GetPaymentOptions> doInBackground(Boolean... params) {
            ArrayList<GetPaymentOptions> list = new ArrayList<>();
            RestApiCalls call = new RestApiCalls();
            list = call.get_paymentOptions((App) getApplication(), db_name);
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<GetPaymentOptions> options) {
            if (null != dialog && dialog.isShowing()) {
                dialog.dismiss();
            }
            AppLog.write("Options------>", "--" + new Gson().toJson(options));
            if (null != options && options.size() > 0) {
                pay_options = options;
            }
        }
    }

    //Pay Bill
    private class BillPayment extends AsyncTask<String, Void, BillPay> {
        MyCustomDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new MyCustomDialog(context, "Submitting...");
            dialog.show();
        }

        @Override
        protected BillPay doInBackground(String... params) {

            RestApiCalls calls = new RestApiCalls();
            BillPay submit = calls.billPaymentModified((App) getApplication(), "1", "0", billId, "0.00", params[1], "0.00", "", tableId,
                    "", "", "", params[0], params[2], params[3], billDetails, tableName, "None", db_name);
            AppLog.write("Value------", "---" + new Gson().toJson(submit));
            pay_option = params[3];
            return submit;
        }

        @Override
        protected void onPostExecute(BillPay billPay) {
            if (null != dialog && dialog.isShowing())
                dialog.dismiss();
            AppLog.write("BillPay---", "---" + new Gson().toJson(billPay));
            if (null != billPay) {
                if (billPay.getSuccess().equals("1")) {
                    Toast.makeText(context, "Bill paid successfully..!", Toast.LENGTH_SHORT).show();
                    new SendPrintRequestForBillPreview().execute(billId, "2");
                    PendingBillsDB db = new PendingBillsDB(context);
                    db.updateBillDetails(billId);
                    redirectPage();
                    //checkCounterSaleType();

                } else {
                    Toast.makeText(context, "Error occurred in bill paying..!", Toast.LENGTH_SHORT).show();
                }
                AppLog.write("BillPay--", "" + new Gson().toJson(billPay));
            } else {
                Toast.makeText(context, "Error occurred in bill paying..!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class SendPrintRequestForBillPreview extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            RestApiCalls calls = new RestApiCalls();
            AppLog.write("Bill_request_bill_paying---->", "" + params[0] + "\n" + params[1]);
            return calls.sendPrintRequest_to_ServerForBill((App) getApplication(), params[0], params[1], db_name);
        }

        @Override
        protected void onPostExecute(String s) {
            AppLog.write("Bill preview result--->", s);
        }
    }

    private class BillCalculation extends AsyncTask<String, String, BillCalcModel> {
        MyCustomDialog my_dialog;

        @Override
        protected void onPreExecute() {
            my_dialog = new MyCustomDialog(context, "Loading...");
            my_dialog.show();
        }

        @Override
        protected BillCalcModel doInBackground(String... params) {
            BillCalcModel billCalcModel = new BillCalcModel();
            RestApiCalls call = new RestApiCalls();
            AppLog.write("POS_Bill_ID--------", billId);
            AppLog.write("Order_Type---------", settings.getORDER_TYPE());
            billCalcModel = call.billCalculation((App) getApplication(), billId, settings.getORDER_TYPE(), db_name);
            AppLog.write("Bill------", "--" + new Gson().toJson(billCalcModel));
            return billCalcModel;
        }

        @Override
        protected void onPostExecute(BillCalcModel billCalcModel) {
            if (null != my_dialog && my_dialog.isShowing())
                my_dialog.dismiss();
            if (null != billCalcModel) {
                billDetails = new Gson().toJson(billCalcModel);
                billList.addAll(billCalcModel.getBill_api());
                if (balance <= 0) {
                    validateBillStatus(billList);
                } else {
                    Toast.makeText(context, "Please enter a valid amount,cant proceed for payment.!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Error occurred in bill paying..!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
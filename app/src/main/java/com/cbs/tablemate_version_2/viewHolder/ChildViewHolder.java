package com.cbs.tablemate_version_2.viewHolder;

/*********************************************************************
 * Created by Barani on 10-08-2018 in TableMateNew
 ***********************************************************************/

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

/**
 * ViewHolder for
 */
public class ChildViewHolder extends RecyclerView.ViewHolder {

    public ChildViewHolder(View itemView) {
        super(itemView);
    }
}

package com.cbs.tablemate_version_2.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.cbs.tablemate_version_2.models.Item;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 14-02-2017 in TableMateNew-2.0
 ***********************************************************************/
public class MenuItemsDB extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;

    private static final String DATABASE_NAME = "MENU_ITEMS_DB";

    private static final String TABLE_NAME = "GET_MENU_ITEMS_TABLE";

    private static final String KEY_PRIMARY_ID = "key_id";

    private static final String KEY_ID = "id";

    private static final String KEY_NAME = "name";

    private static final String KEY_PRICE = "price";

    private static final String KEY_PROCESS_TIME = "process_time";

    private static final String KEY_TYPE = "type";

    private static final String KEY_FOOD_TYPE_NAME = "food_type_name";

    private static final String KEY_DISABLE = "disable";

    private static final String KEY_C_AVAILABLE = "customisation_available";

    private static final String KEY_C_MANDATORY = "customisation_mandatory";

    private static final String KEY_LIVE_ITEM = "live_item";

    private static final String KEY_FILTER_TYPE_NAME = "filter_type_name";

    private static final String KEY_FILTER_NAME = "filter_name";

    private static final String KEY_CUSTOMISED_ITEM = "customised_item";

    public MenuItemsDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + KEY_PRIMARY_ID + " INTEGER PRIMARY KEY," + KEY_ID + " TEXT," + KEY_NAME + " TEXT,"
                + KEY_PRICE + " TEXT," + KEY_PROCESS_TIME + " TEXT," + KEY_TYPE + " TEXT," + KEY_FOOD_TYPE_NAME
                + " TEXT," + KEY_DISABLE + " TEXT," + KEY_C_AVAILABLE + " TEXT," + KEY_C_MANDATORY + " TEXT," +
                KEY_LIVE_ITEM + " TEXT," + KEY_FILTER_TYPE_NAME + " TEXT," + KEY_FILTER_NAME + " TEXT," + KEY_CUSTOMISED_ITEM + " TEXT" + ")";

        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        onCreate(db);
    }

    public void saveMenuItems(Item menuItems) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_ID, menuItems.getId());

        values.put(KEY_NAME, menuItems.getName());

        values.put(KEY_PRICE, menuItems.getPrice());

        values.put(KEY_PROCESS_TIME, menuItems.getProcess_time());

        values.put(KEY_TYPE, menuItems.getType());

        values.put(KEY_FOOD_TYPE_NAME, menuItems.getFood_type_name());

        values.put(KEY_DISABLE, menuItems.getDisable());

        values.put(KEY_C_AVAILABLE, menuItems.getCust_aval());

        values.put(KEY_C_MANDATORY, menuItems.getCust_mandatory());

        values.put(KEY_LIVE_ITEM, menuItems.getLive_item());

        values.put(KEY_FILTER_TYPE_NAME, menuItems.getFilter_type_name());

        values.put(KEY_FILTER_NAME, menuItems.getFilter_name());

        values.put(KEY_CUSTOMISED_ITEM, menuItems.getCustomised_item());

        db.insert(TABLE_NAME, null, values);

        db.close();
    }

    public void deleteTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        String DeleteQuery = "DELETE FROM " + TABLE_NAME;
        db.execSQL(DeleteQuery);
        db.close();
    }

    public ArrayList<Item> getMenuItems() {
        ArrayList<Item> list = new ArrayList<Item>();

        String selectQuery = "SELECT * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Item details = new Item();
                details.setId(cursor.getString(1));
                details.setName(cursor.getString(2));
                details.setPrice(cursor.getString(3));
                details.setFood_type_name(cursor.getString(4));
                details.setDisable(cursor.getString(5));
                details.setCust_aval(cursor.getString(6));
                details.setCust_mandatory(cursor.getString(7));
                details.setLive_item(cursor.getString(8));
                details.setFilter_type_name(cursor.getString(9));
                details.setFilter_name(cursor.getString(10));
                details.setCustomised_item(cursor.getString(11));
                list.add(details);
            } while (cursor.moveToNext());
        }
        db.close();
        return list;
    }

}

package com.cbs.tablemate_version_2.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.cbs.tablemate_version_2.models.LoginDetails;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 14-02-2017 in TableMateNew-2.0
 ***********************************************************************/
public class LoginDB extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;

    private static final String DATABASE_NAME = "LOGIN_DB";

    private static final String TABLE_NAME = "LOGIN_TABLE";

    private static final String KEY_ID = "id";

    private static final String KEY_HOTEL_CODE = "hotel_code";

    private static final String KEY_NAME = "user_name";

    private static final String KEY_PASSWORD = "password";

    private static final String KEY_EMPLOYEE_ID = "employee_id";

    private static final String KEY_IN_TIME = "login_time";

    private static final String KEY_OUT_TIME = "logout_time";

    private static final String KEY_REF_ID = "ref_id";

    public LoginDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_HOTEL_CODE + " TEXT," + KEY_NAME + " TEXT,"
                + KEY_PASSWORD + " TEXT," + KEY_EMPLOYEE_ID + " TEXT," + KEY_IN_TIME + " TEXT," + KEY_OUT_TIME + " TEXT," + KEY_REF_ID + " TEXT" + ")";

        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void saveLoginDetails(LoginDetails details) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_HOTEL_CODE, details.getHotel_code());

        values.put(KEY_NAME, details.getName());

        values.put(KEY_PASSWORD, details.getPassword());

        values.put(KEY_EMPLOYEE_ID, details.getEmployee_id());

        values.put(KEY_IN_TIME, details.getInTime());

        values.put(KEY_OUT_TIME, details.getOutTime());

        values.put(KEY_REF_ID, details.getRef_id());

        db.insert(TABLE_NAME, null, values);

        db.close();
    }

    public void deleteTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        String DeleteQuery = "DELETE FROM " + TABLE_NAME;
        db.execSQL(DeleteQuery);
        db.close();

        /* db.delete(TABLE_NAME, KEY_EMPLOYEE_ID + " = ?",
                new String[]{String.valueOf(1)});*/
        /*db.update(TABLE_NAME, values, COLUMN_ID + " = ?",
                new String[]{String.valueOf(value)});*/
    }

    public ArrayList<LoginDetails> getLoginDetails() {
        ArrayList<LoginDetails> list = new ArrayList<LoginDetails>();

        String selectQuery = "SELECT * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                LoginDetails details = new LoginDetails();
                details.setHotel_code(cursor.getString(1));
                details.setName(cursor.getString(2));
                details.setPassword(cursor.getString(3));
                details.setEmployee_id(cursor.getString(4));
                details.setInTime(cursor.getString(5));
                details.setOutTime(cursor.getString(6));
                details.setRef_id(cursor.getString(7));
                list.add(details);
            } while (cursor.moveToNext());
        }
        db.close();
        return list;
    }
}
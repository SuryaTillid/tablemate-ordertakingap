package com.cbs.tablemate_version_2.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.cbs.tablemate_version_2.models.Item;
import com.cbs.tablemate_version_2.utilities.Utility;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/*********************************************************************
 * Created by Barani on 08-06-2016 in TableMateNew
 ***********************************************************************/
public class CartManager {

    private final String PREFERENCE_NAME = "TableMate_Cart_Pref";
    private final int MODE = 0;
    private Context context;
    private SharedPreferences sPref;
    private SharedPreferences.Editor editor;
    private Gson gson;

    public CartManager(Context context) {
        this.context = context;
        sPref = context.getSharedPreferences(PREFERENCE_NAME, MODE);
        editor = sPref.edit();
        gson = new Gson();
    }

    public int addToCart(Item item) {
        int itemQty = 0;
        addItemsToCart(item);
        itemQty = getQuantity(item);
        return itemQty;
    }

    public int getQuantity(Item p_item) {
        int qty = 0;
        for (Item item : getItemList()) {
            if (item.getId().equalsIgnoreCase(p_item.getId()))
                qty += Utility.convertToInteger(item.getItemQty());
        }
        return qty;
    }

    public ArrayList<Item> getCartItems() {
        ArrayList<Item> list = new ArrayList<>();
        list.addAll(getItemList());
        return list;
    }

    public void addItemsToCart(Item item) {
        item.setItemQty("1");
        String itemId = item.getCustomId();
        String alreadyExists = sPref.getString("" + itemId, "");
        if (alreadyExists.equals("")) {
            String json = gson.toJson(item);
            editor.putString("" + itemId, json);
        } else {
            Item item1 = gson.fromJson(alreadyExists, Item.class);
            if (item1 != null) {
                item1.setItemQty("" + (Utility.convertToInteger(item1.getItemQty()) + 1));
                String json1 = gson.toJson(item1);
                editor.putString("" + itemId, json1);
            }
        }
        editor.commit();
    }

    public void addCustomizationComment(Item item, String comment) {
       /* String itemId = item.getCustomId();
        String alreadyExists = sPref.getString("" + itemId, "");
        if (!alreadyExists.equals("")) {
            Item item1 = gson.fromJson(alreadyExists, Item.class);
            if (Utility.convertToInteger(item1.getItemQty()) > 1) {
                Random r = new Random();
                Item newItem = new Item();
                newItem.setId(item1.getId());
                newItem.setCustomId(item1.getCustomId() + r.nextInt());
                newItem.setItemQty("1");
                newItem.setAlternate_price(item1.getAlternate_price());
                newItem.setAlternate_price1(item1.getAlternate_price2());
                newItem.setAlternate_price2(item1.getAlternate_price2());
                newItem.setItemName(item1.getItemName());
                newItem.setCust_aval(item1.getCust_aval());
                newItem.setCust_mandatory(item1.getCust_mandatory());
                newItem.setCustomAddOn(item1.getCustomAddOn());
                newItem.setCustomised_item(item1.getCustomised_item());
                newItem.setCustomPortion(item1.getCustomPortion());
                newItem.setCustomType(item1.getCustomType());
                newItem.setDelivery(item1.getDelivery());
                newItem.setDelivery_cost(item1.getDelivery_cost());
                newItem.setDescription(item1.getDescription());
                newItem.setDinein(item1.getDinein());
                newItem.setDisable(item1.getDisable());
                newItem.setFilter(item1.getFilter_name());
                newItem.setFilter_type_name(item1.getFilter_type_name());
                newItem.setFood_type_disable(item1.getFood_type_disable());
                newItem.setFood_type_name(item1.getFood_type_name());
                newItem.setFor_ck(item1.getFor_ck());
                newItem.setLive_item(item1.getLive_item());
                newItem.setName(item1.getName());
                newItem.setPrice(item1.getPrice());
                newItem.setPrice_1(item1.getPrice());
                newItem.setPrice_tax1(item1.getPrice_tax1());
                newItem.setPrice_tax2(item1.getPrice_tax2());
                newItem.setProcess_time(item1.getProcess_time());
                newItem.setRemove_from_pos(item1.getRemove_from_pos());
                newItem.setSearch_id(item1.getSearch_id());
                newItem.setShort_name(item1.getShort_name());
                newItem.setTakeaway(item1.getTakeaway());
                newItem.setTakeaway_cost(item1.getTakeaway_cost());
                newItem.setType(item1.getType());
                newItem.setCustomisation_comments(comment);
                newItem.setEnable_NC(item1.getEnable_NC());
                addItemsToCart(newItem);
                removeFromCart(item1);
            } else {
                item1.setPrice(item1.getPrice());
                item1.setPrice_1(item1.getPrice());
                item1.setCustomisation_comments(comment);
                item1.setEnable_NC(item1.getEnable_NC());
                String json1 = gson.toJson(item1);
                editor.putString("" + itemId, json1);
                editor.commit();
            }
        }*/

        String itemId = item.getCustomId();
        String alreadyExists = sPref.getString("" + itemId, "");
        if (!alreadyExists.equals("")) {
            Item item1 = gson.fromJson(alreadyExists, Item.class);
            item1.setCustomisation_comments(comment);
            String json1 = gson.toJson(item1);
            editor.putString("" + itemId, json1);
        }
        editor.commit();
    }

    public void removeFromCart(Item item) {
        String itemId = item.getCustomId();

        String alreadyExists = sPref.getString("" + itemId, "");

        if (!alreadyExists.equals("")) {
            Item item1 = gson.fromJson(alreadyExists, Item.class);
            if (item1 != null) {

                int qty = Utility.convertToInteger(item1.getItemQty());
                if (qty > 1) {
                    item1.setItemQty("" + (qty - 1));
                    String json1 = gson.toJson(item1);
                    editor.putString("" + itemId, json1);
                } else {
                    editor.remove("" + itemId);
                }
            }
        }
        editor.commit();
        editor.apply();
    }

    public float getTotal(String orderType) {
        float amount = 0;
        List<Item> l = getItemList();
        for (Item item : l) {
            int qty = Utility.convertToInteger(item.getItemQty());
            float price = (float) Utility.getItemPrice(item, orderType);
            amount += (qty * price);
        }
        return amount;
    }

    public float getTotalNC(String orderType, String NcOrder) {

        float amount = 0;
        List<Item> l = getItemList();
        for (Item item : l) {
            int qty = Utility.convertToInteger(item.getItemQty());
            float price = (float) Utility.getItemPriceNC(item, orderType, NcOrder);
            amount += (qty * price);
        }
        return amount;
    }

    public float getTotalNCNew(String orderType) {
        float amount = 0;
        List<Item> l = getItemList();
        for (Item item : l) {

            if (item.getEnable_NC().equals("1")) {
                int qty = Utility.convertToInteger(item.getItemQty());
                float price = (float) Utility.getItemPriceNC(item, orderType, "1");
                amount += (qty * price);
            } else {
                int qty = Utility.convertToInteger(item.getItemQty());
                float price = (float) Utility.getItemPriceNC(item, orderType, "");
                amount += (qty * price);
            }

        }
        return amount;
    }

    public int getItemCounts() {
        int count = 0;
        List<Item> items = getItemList();

        for (Item item : items) {


            count += Integer.parseInt(item.getItemQty());

            Log.e("count", "" + count);
        }
        return count;
    }


    public ArrayList<Item> getItemList() {
        ArrayList<Item> itemList = new ArrayList<>();
        Map<String, ?> items = sPref.getAll();
        for (String key : items.keySet()) {
            String json = (String) items.get(key);
            Gson gson = new Gson();
            Item item = gson.fromJson(json, Item.class);
            itemList.add(item);
        }
        return itemList;
    }

    public void clearCart() {
        editor.clear();
        editor.commit();
    }

    public void addNC(Item item) {
        String itemId = item.getCustomId();
        String alreadyExists = sPref.getString("" + itemId, "");
        if (!alreadyExists.equals("")) {
            Item item1 = gson.fromJson(alreadyExists, Item.class);
            if (Utility.convertToInteger(item1.getItemQty()) > 1) {
                Random r = new Random();
                Item newItem = new Item();
                newItem.setId(item1.getId());
                newItem.setCustomId(item1.getCustomId() + r.nextInt());
                newItem.setItemQty("1");
                newItem.setAlternate_price(item1.getAlternate_price());
                newItem.setAlternate_price1(item1.getAlternate_price2());
                newItem.setAlternate_price2(item1.getAlternate_price2());
                newItem.setItemName(item1.getItemName());
                newItem.setCust_aval(item1.getCust_aval());
                newItem.setCust_mandatory(item1.getCust_mandatory());
                newItem.setCustomAddOn(item1.getCustomAddOn());
                newItem.setCustomised_item(item1.getCustomised_item());
                newItem.setCustomPortion(item1.getCustomPortion());
                newItem.setCustomType(item1.getCustomType());
                newItem.setDelivery(item1.getDelivery());
                newItem.setDelivery_cost(item1.getDelivery_cost());
                newItem.setDescription(item1.getDescription());
                newItem.setDinein(item1.getDinein());
                newItem.setDisable(item1.getDisable());
                newItem.setFilter(item1.getFilter_name());
                newItem.setFilter_type_name(item1.getFilter_type_name());
                newItem.setFood_type_disable(item1.getFood_type_disable());
                newItem.setFood_type_name(item1.getFood_type_name());
                newItem.setFor_ck(item1.getFor_ck());
                newItem.setLive_item(item1.getLive_item());
                newItem.setName(item1.getName());
                newItem.setPrice_1(item1.getPrice());
                newItem.setPrice("0.00");
                newItem.setPrice_tax1(item1.getPrice_tax1());
                newItem.setPrice_tax2(item1.getPrice_tax2());
                newItem.setProcess_time(item1.getProcess_time());
                newItem.setRemove_from_pos(item1.getRemove_from_pos());
                newItem.setSearch_id(item1.getSearch_id());
                newItem.setShort_name(item1.getShort_name());
                newItem.setTakeaway(item1.getTakeaway());
                newItem.setTakeaway_cost(item1.getTakeaway_cost());
                newItem.setType(item1.getType());
                newItem.setCustomisation_comments("NC" + item1.getCustomisation_comments());
                newItem.setEnable_NC("1");
                newItem.setInternal_costing(item1.getInternal_costing());
                newItem.setEmp_id(item1.getEmp_id());
                addItemsToCart(newItem);
                removeFromCart(item1);
            } else {
                item1.setCustomisation_comments("NC" + item1.getCustomisation_comments());
                item1.setPrice_1(item1.getPrice());
                item1.setInternal_costing(item1.getInternal_costing());
                item1.setPrice("0.00");
                item1.setEnable_NC("1");
                item1.setEmp_id(item1.getEmp_id());
                String json1 = gson.toJson(item1);
                editor.putString("" + itemId, json1);
                editor.commit();
            }
        }
    }


}
package com.cbs.tablemate_version_2.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.models.POS_Bill;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 14-02-2017 in TableMateNew-2.0
 ***********************************************************************/
public class PendingBillsDB extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;

    private static final String DATABASE_NAME = "PENDING_BILLS_DB_1";

    private static final String TABLE_NAME = "PENDING_BILL_TABLE";

    private static final String KEY_PRIMARY_ID = "key_id";

    private static final String KEY_BILL_ID = "bill_id";

    private static final String KEY_TOTAL_AMOUNT = "amount";

    private static final String KEY_BILL_STATUS = "bill_status";

    public PendingBillsDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + KEY_PRIMARY_ID + " INTEGER PRIMARY KEY," + KEY_BILL_ID + " TEXT," + KEY_TOTAL_AMOUNT + " TEXT," + KEY_BILL_STATUS + " TEXT" + ")";

        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        onCreate(db);
    }

    public void saveBillDetails(POS_Bill bill) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_BILL_ID, bill.getBill_id());

        values.put(KEY_TOTAL_AMOUNT, bill.getBillGrandTotal());

        values.put(KEY_BILL_STATUS, bill.getBill_status());

        db.insert(TABLE_NAME, null, values);

        db.close();
    }

    public void deleteTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        String DeleteQuery = "DELETE FROM " + TABLE_NAME;
        db.execSQL(DeleteQuery);
        db.close();
    }

    public ArrayList<POS_Bill> updateBillDetails(String billId) {
        ArrayList<POS_Bill> list = new ArrayList<POS_Bill>();

        String updateQuery = "UPDATE " + TABLE_NAME + " SET " + KEY_BILL_STATUS + "=" + "'cleared'" + " WHERE " + KEY_BILL_ID + "=" + billId;
        AppLog.write("UPDATE QUERY---", updateQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(updateQuery);
        db.close();
        return list;
    }

    public ArrayList<POS_Bill> getBillDetails() {
        ArrayList<POS_Bill> list = new ArrayList<POS_Bill>();
        String selectQuery = "SELECT DISTINCT * FROM " + TABLE_NAME + " WHERE " + KEY_BILL_STATUS + "=" + "'pending'" + " GROUP BY " + KEY_BILL_ID;
        AppLog.write("SELECT QUERY---", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                POS_Bill bill = new POS_Bill();
                bill.setBill_id(cursor.getString(1));
                bill.setBillGrandTotal(cursor.getString(2));
                bill.setBill_status(cursor.getString(3));
                list.add(bill);
            } while (cursor.moveToNext());
        }
        db.close();
        return list;
    }
}

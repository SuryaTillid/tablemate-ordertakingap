package com.cbs.tablemate_version_2.helper;

import android.os.Parcel;
import android.os.Parcelable;

import com.cbs.tablemate_version_2.models.Item;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 14-02-2017 in TableMateNew-2.0
 ***********************************************************************/
public class OrderListArray extends ArrayList<Item> implements Parcelable {

    public static final Creator CREATOR = new Creator() {

        public OrderListArray createFromParcel(Parcel in) {

            return new OrderListArray(in);

        }

        public Object[] newArray(int arg0) {
            return null;
        }
    };

    public OrderListArray() {
    }

    OrderListArray(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        int size = this.size();

        //We have to write the list size, we need him recreating the list

        dest.writeInt(size);

        //We decided arbitrarily to write first the Name and later the Phone Number.

        for (int i = 0; i < size; i++) {

            Item itemList = this.get(i);

            dest.writeString(itemList.getItemName());
            dest.writeString(itemList.getItemQty());
            dest.writeString(itemList.getItemPrice());
            dest.writeString(itemList.getItemId());
            dest.writeString(itemList.getItemProcessTime());
        }
    }

    private void readFromParcel(Parcel in) {

        this.clear();

        // First we have to read the list size

        int size = in.readInt();

        // Reading remember that we wrote first the itemName,itemPrice,itemQty
        // and itemTotal.

        for (int i = 0; i < size; i++) {

            Item itemsList = new Item();

            itemsList.setItemName(in.readString());
            itemsList.setItemQty(in.readString());
            itemsList.setItemPrice(in.readString());
            itemsList.setItemId(in.readString());
            itemsList.setItemProcessTime(in.readString());
            this.add(itemsList);

        }
    }
}


package com.cbs.tablemate_version_2.helper;

import android.content.Context;

import com.cbs.tablemate_version_2.configuration.Settings;
import com.cbs.tablemate_version_2.models.Item;
import com.cbs.tablemate_version_2.utilities.Utility;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 14-02-2017 in TableMateNew-2.0
 ***********************************************************************/
public class ShoppingCart {

    private static ShoppingCart ourInstance;
    ArrayList<Item> items = new ArrayList<>();
    ArrayList<Item> cartItems = null;

    private ShoppingCart() {
    }

    public static ShoppingCart getInstance() {
        if (null == ourInstance) {
            ourInstance = new ShoppingCart();
        }
        return ourInstance;
    }

    public ArrayList<Item> loadMenuItems(Context context) {
        if (items.size() == 0)
            items = loadMenu(context);
        return items;
    }

    private ArrayList<Item> loadMenu(Context context) {
        ArrayList<Item> menuItems = new ArrayList<>();
        Settings settings = new Settings(context);
        menuItems = settings.getMenu();
        return menuItems;
    }

    public ArrayList<Item> loadOrderedMenuItems(Context context) {
        if (cartItems == null)
            cartItems = loadOrderedMenuItems(context);
        return cartItems;
    }

    private ArrayList<Item> loadOrdersList(Context context) {
        ArrayList<Item> orderedItems = new ArrayList<>();
        CartManager cartManager = new CartManager(context);
        orderedItems = cartManager.getItemList();
        return orderedItems;
    }


    public ArrayList<Item> addMenuItems(Item item) {
        ArrayList<Item> temp = cartItems;
        if (temp.size() > 0) {
            for (Item items : temp) {
                if (items.equals(item)) {
                    int qty = Utility.convertToInteger(items.getItemQty()) + 1;
                    items.setItemQty("" + qty);
                } else {
                    cartItems.add(item);
                }
            }
        } else {
            cartItems.add(item);
        }
        return cartItems;
    }

    public ArrayList<Item> removeMenuItems(Item item) {
        if (cartItems.size() > 0) {
            for (Item items : cartItems) {
                if (items.equals(item)) {
                    int qty = Utility.convertToInteger(items.getItemQty()) - 1;
                    items.setItemQty("" + qty);
                    if (qty == 0)
                        cartItems.remove(item);
                }
            }
        }
        return cartItems;
    }

    public ArrayList<Item> addCustomizationComments(Item item, String comment) {
        for (Item cartItem : cartItems) {
            if (cartItem.equals(item)) {
                cartItem.setCustomisation_comments(comment);
            }
        }
        return cartItems;
    }

    /*
    1. Load menu
    2. Add menu items to the cart
    3. Remove items from the cart
    4. add customization comments and display
    5. Load ordered items
     */
}

package com.cbs.tablemate_version_2.helper;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.cbs.tablemate_version_2.R;

/*********************************************************************
 * Created by Barani on 07-09-2017 in TableMateNew
 ***********************************************************************/
public class DisplayErrorMessage extends Activity {

    TextView error;
    Button btn_ok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        setContentView(R.layout.activity_display_error);

        error = findViewById(R.id.error);
        btn_ok = findViewById(R.id.btn_ok);

        error.setText(getIntent().getStringExtra("error"));
    }
}

package com.cbs.tablemate_version_2.helper;

/*********************************************************************
 * Created by Baraneeswari on 30-04-2016 in TableMateNew
 ***********************************************************************/
public class Billing {
    private static Billing billing;

    private Billing() {
    }

    public static Billing getBilling() {
        if (null == billing)
            billing = new Billing();
        return billing;
    }
}

package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.cbs.tablemate_version_2.models.DefaultCustomerDetails;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 30-05-2016 in TableMateNew
 ***********************************************************************/
public class CustomerDataAdapter extends BaseAdapter {

    private ArrayList<DefaultCustomerDetails> defaultCustomerDetailsArrayList;
    private Context context;
    private LayoutInflater inflater;

    public CustomerDataAdapter(Context contexts, ArrayList<DefaultCustomerDetails> defaultCustomerDetailses) {
        this.context = contexts;
        this.defaultCustomerDetailsArrayList = defaultCustomerDetailses;
    }

    public void MyDataChanged(ArrayList<DefaultCustomerDetails> defaultCustomerDetailses) {
        defaultCustomerDetailsArrayList = defaultCustomerDetailses;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return defaultCustomerDetailsArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return defaultCustomerDetailsArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        DefaultCustomerDetails defaultCustomerDetails = new DefaultCustomerDetails();

        defaultCustomerDetails.getCustomer_list().get(0).getId();
        defaultCustomerDetails.getCustomer_list().get(0).getName();
        defaultCustomerDetails.getCustomer_list().get(0).getPhone();
        defaultCustomerDetails.getCustomer_list().get(0).getAddress();
        defaultCustomerDetails.getCustomer_list().get(0).getEmail();

        return null;
    }
}

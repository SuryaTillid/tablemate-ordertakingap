package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.models.GetPaymentOptions;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 20-04-2018 in TableMateNew
 ***********************************************************************/
public class PayAdapter extends BaseAdapter {
    private ArrayList<GetPaymentOptions> list;
    private Context context;
    private LayoutInflater inflater;

    public PayAdapter(Context contexts, ArrayList<GetPaymentOptions> p_list) {
        this.context = contexts;
        this.list = p_list;
    }

    public void MyDataChanged(ArrayList<GetPaymentOptions> p_list) {
        list = p_list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int p) {
        return list.get(p);
    }

    @Override
    public long getItemId(int p) {
        return p;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.pos_payment_row, null);
        Button b_PaymentType = convertView.findViewById(R.id.pay_option);
        EditText edit_amount = convertView.findViewById(R.id.edit_amount);
        EditText edit_card = convertView.findViewById(R.id.edit_card_num);

        GetPaymentOptions options = list.get(position);
        b_PaymentType.setText(options.getPaymentmode());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //amount = edit_amount.getText().toString();
                //card_digit = edit_card.getText().toString();
                //payType = b_PaymentType.getText().toString();
                //selected = true;
                //listener.setPaymentValues(getAdapterPosition(),amount, card_digit, payType, selected);
            }
        });
        return convertView;
    }
}

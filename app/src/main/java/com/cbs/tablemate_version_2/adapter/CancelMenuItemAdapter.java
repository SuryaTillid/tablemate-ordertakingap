package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.interfaces.CancelItem_after_BillingListener;
import com.cbs.tablemate_version_2.models.DiscountOrder;

import java.util.ArrayList;
import java.util.List;

/*********************************************************************
 * Created by Barani on 02-05-2017 in TableMateNew
 ***********************************************************************/
public class CancelMenuItemAdapter extends RecyclerView.Adapter<CancelMenuItemAdapter.MyHolder> {
    CancelItem_after_BillingListener listener;
    private Context context;
    private ArrayList<DiscountOrder> billItemsList;
    private LayoutInflater inflater;
    private boolean checked;

    public CancelMenuItemAdapter(Context context, ArrayList<DiscountOrder> billItemsList) {
        this.context = context;
        this.billItemsList = billItemsList;
        inflater = LayoutInflater.from(context);
    }

    public void setCancelItemListener(CancelItem_after_BillingListener cancelItemListener) {
        this.listener = cancelItemListener;
    }

    @Override
    public CancelMenuItemAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.activity_delete_popup_row_modified, parent, false);
        return new CancelMenuItemAdapter.MyHolder(v);
    }

    @Override
    public int getItemCount() {
        return billItemsList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void MyDataChanged(ArrayList<DiscountOrder> bill) {
        billItemsList = bill;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(CancelMenuItemAdapter.MyHolder holder, int position) {
        DiscountOrder bills = billItemsList.get(position);

        holder.t_qty.setText("1");
    }

    class MyHolder extends RecyclerView.ViewHolder {

        TextView t_qty;
        Spinner spinner_comments;
        CheckBox check_menu_value;
        EditText other_comment;
        LinearLayout layout;
        String comment, other_reason = "";

        public MyHolder(View itemView) {
            super(itemView);

            t_qty = itemView.findViewById(R.id.t_quantity);
            spinner_comments = itemView.findViewById(R.id.spinnerComments);
            check_menu_value = itemView.findViewById(R.id.check_menu);
            other_comment = itemView.findViewById(R.id.edit_other_comment);

            check_menu_value.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    other_reason = other_comment.getText().toString().trim();
                    checked = isChecked;
                    listener.deleteItem_fromListListener(getAdapterPosition(), isChecked, comment, other_reason);
                    AppLog.write("TAG--", "Position--" + getAdapterPosition() + "--" + other_reason);
                }
            });

            check_menu_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

            // Spinner Drop down elements
            List<String> commentsList = new ArrayList<String>();
            commentsList.add("Select Comment");
            commentsList.add("Cancelled by Customer");
            commentsList.add("Did not Like");
            commentsList.add("Wrong order by Captain");
            commentsList.add("Delayed by Kitchen");
            commentsList.add("Did not Serve");
            commentsList.add("Dish not Available");
            commentsList.add("POS Error");
            commentsList.add("Double Entry");
            commentsList.add("Other");

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, commentsList);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_comments.setAdapter(dataAdapter);

            // Spinner click listener
            spinner_comments.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String s = parent.getItemAtPosition(position).toString();
                    if (!s.equalsIgnoreCase("Select Comment") && !s.equalsIgnoreCase("Other")) {
                        comment = s;
                        other_comment.setVisibility(View.GONE);
                    } else if (s.equalsIgnoreCase("Other")) {
                        comment = s;
                        other_comment.setVisibility(View.VISIBLE);
                    } else {
                        Toast.makeText(parent.getContext(), "Please select a comment..!", Toast.LENGTH_SHORT).show();
                        other_comment.setVisibility(View.GONE);
                    }

                    listener.deleteItem_fromListListener(getAdapterPosition(), checked, comment, other_reason);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }
    }
}
package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.interfaces.ModifyItemListener;
import com.cbs.tablemate_version_2.models.BillPreview;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 08-10-2018 in TableMateNew
 ***********************************************************************/
public class ModifyItemPrimaryAdapter extends RecyclerView.Adapter<ModifyItemPrimaryAdapter.MyHolder> {
    ModifyItemListener listener;
    private Context context;
    private ArrayList<BillPreview> ordersList = new ArrayList<>();
    private LayoutInflater inflater;
    private boolean checked;

    public ModifyItemPrimaryAdapter(Context context, ArrayList<BillPreview> orders) {
        this.context = context;
        this.ordersList = orders;
        inflater = LayoutInflater.from(context);
    }

    public void setClickMenuItem_Listener(ModifyItemListener listener) {
        this.listener = listener;
    }

    @Override
    public ModifyItemPrimaryAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.activity_modify_price_row, parent, false);
        return new ModifyItemPrimaryAdapter.MyHolder(v);
    }

    @Override
    public int getItemCount() {
        return ordersList.size();
    }

    public void myDataSetChanged(ArrayList<BillPreview> o_list) {
        this.ordersList = o_list;
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(ModifyItemPrimaryAdapter.MyHolder holder, int position) {
        BillPreview orders = ordersList.get(position);

        holder.t_qty.setText(orders.getOrderQuantity());
        holder.t_actual_price.setText(orders.getMrp_price());
        holder.t_item_name.setText(orders.getMenu_item_name());
    }

    class MyHolder extends RecyclerView.ViewHolder {

        TextView t_qty;
        TextView t_actual_price;
        TextView t_item_name;

        public MyHolder(View itemView) {
            super(itemView);

            t_qty = itemView.findViewById(R.id.txtItemQty);
            t_actual_price = itemView.findViewById(R.id.txtActualPrice);
            t_item_name = itemView.findViewById(R.id.txtItemName);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.modifyItem_clickListener(getAdapterPosition());
                }
            });
        }
    }
}
package com.cbs.tablemate_version_2.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.models.GetBillStatus;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 22-12-2016 in TableMateNew
 ***********************************************************************/
public class PendingBillsAdapter extends RecyclerView.Adapter<PendingBillsAdapter.MyHolder> {
    private ArrayList<GetBillStatus> billList;
    private Context context;
    private LayoutInflater inflater;


    public PendingBillsAdapter(Context context, ArrayList<GetBillStatus> billsList) {
        this.context = context;
        this.billList = billsList;
        inflater = LayoutInflater.from(context);
    }

    public void MyDataChanged(ArrayList<GetBillStatus> billsList) {
        billList = billsList;
        notifyDataSetChanged();
    }

    @Override
    public PendingBillsAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.activity_redesign_pending_bills_row, parent, false);
        return new PendingBillsAdapter.MyHolder(v);
    }

    @Override
    public void onBindViewHolder(PendingBillsAdapter.MyHolder holder, int position) {
        GetBillStatus bill = billList.get(position);
        holder.bill_id.setText(bill.getBill_id());
        holder.bill_amount.setText(bill.getBillGrandTotal());
        holder.table_name.setText(bill.getTable_name());
        holder.date.setText(bill.getDate());
    }

    @Override
    public int getItemCount() {
        return billList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        TextView bill_id;
        TextView bill_amount;
        TextView table_name;
        TextView date;

        public MyHolder(View itemView) {
            super(itemView);
            bill_id = itemView.findViewById(R.id.txtBillId);
            bill_amount = itemView.findViewById(R.id.txtTotal);
            table_name = itemView.findViewById(R.id.txtTableId);
            date = itemView.findViewById(R.id.txt_date);
        }
    }
}
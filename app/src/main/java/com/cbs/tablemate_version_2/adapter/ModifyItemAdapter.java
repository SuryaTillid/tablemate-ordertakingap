package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.interfaces.ModifyItemListener;
import com.cbs.tablemate_version_2.models.DiscountOrder;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 08-10-2018 in TableMateNew
 ***********************************************************************/
public class ModifyItemAdapter extends RecyclerView.Adapter<ModifyItemAdapter.MyHolder> {
    ModifyItemListener listener;
    private Context context;
    private ArrayList<DiscountOrder> ordersList = new ArrayList<>();
    private LayoutInflater inflater;
    private boolean checked;

    public ModifyItemAdapter(Context context, ArrayList<DiscountOrder> orders) {
        this.context = context;
        this.ordersList = orders;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ModifyItemAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.activity_modify_item_popup, parent, false);
        return new MyHolder(v);
    }

    public void setModifyUpdateListener(ModifyItemListener listener) {
        this.listener = listener;
    }


    @Override
    public int getItemCount() {
        return ordersList.size();
    }

    public void myDataSetChanged(ArrayList<DiscountOrder> o_list, int position) {
        this.ordersList = o_list;
        notifyItemChanged(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(ModifyItemAdapter.MyHolder holder, int position) {
        DiscountOrder orders = ordersList.get(position);

        holder.t_qty.setText("1");
        holder.updated_value.setText(orders.getMrp_price());
    }

    class MyHolder extends RecyclerView.ViewHolder {

        TextView t_qty;
        CheckBox check_menu_value;
        EditText updated_value;
        LinearLayout layout;

        public MyHolder(View itemView) {
            super(itemView);

            t_qty = itemView.findViewById(R.id.t_quantity);
            check_menu_value = itemView.findViewById(R.id.check_menu);
            updated_value = itemView.findViewById(R.id.edit_updated_value);
            layout = itemView.findViewById(R.id.l1);
            check_menu_value.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    String updated_price = updated_value.getText().toString();
                    checked = isChecked;
                    listener.update_ModifyItem_fromListListener(getAdapterPosition(), isChecked, updated_price);
                    AppLog.write("TAG--", "Position--" + getAdapterPosition() + "--" + updated_price);
                }
            });

            check_menu_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
        }
    }
}


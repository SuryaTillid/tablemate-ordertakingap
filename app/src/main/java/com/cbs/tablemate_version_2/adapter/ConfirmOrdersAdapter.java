package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.interfaces.CancelItemListener;
import com.cbs.tablemate_version_2.models.OrdersForTable;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 21-06-2016 in TableMateNew
 ***********************************************************************/
public class ConfirmOrdersAdapter extends RecyclerView.Adapter<ConfirmOrdersAdapter.MyHolder> {
    CancelItemListener listener;
    private ArrayList<OrdersForTable> ordersForTableArrayList;
    private LayoutInflater inflater;
    private String cNotes;

    public ConfirmOrdersAdapter(Context context, ArrayList<OrdersForTable> ordersForTables) {
        Context c_context = context;
        this.ordersForTableArrayList = ordersForTables;
        inflater = LayoutInflater.from(context);
    }

    public void setCancelItemListener(CancelItemListener cancelItemListener) {
        this.listener = cancelItemListener;
    }

    public void MyDataChanged(ArrayList<OrdersForTable> ordersForTables) {
        ordersForTableArrayList = ordersForTables;
        notifyDataSetChanged();
    }

    @Override
    public ConfirmOrdersAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.activity_order_status_row, parent, false);
        return new MyHolder(v);
    }

    @Override
    public void onBindViewHolder(ConfirmOrdersAdapter.MyHolder holder, int position) {
        OrdersForTable ordersForTable = ordersForTableArrayList.get(position);
        holder.itemName.setText(ordersForTable.getMenu_item_name());
        holder.itemQuantity.setText(ordersForTable.getOrderQuantity());
        holder.txtOrderStatus.setText(checkOrderStatus(ordersForTable));
        String cust = ordersForTable.getCustom_details();
        String portion = ordersForTable.getPortion_details();
        String nc = ordersForTable.getNc_order();
        checkNC_Status(nc, holder);

        if (!TextUtils.isEmpty(cust)) {
            holder.c_note_layout.setVisibility(View.VISIBLE);
            holder.txt_customizationNotes.setText(getCustomizationNotes(ordersForTable));
        } else if (!TextUtils.isEmpty(portion)) {
            holder.c_note_layout.setVisibility(View.VISIBLE);
            holder.txt_customizationNotes.setText(getPortionInfo(ordersForTable));
        } else {
            holder.c_note_layout.setVisibility(View.GONE);
        }
    }

    private void checkNC_Status(String nc, MyHolder holder) {
        if (nc.equalsIgnoreCase("1"))
            holder.txtNC.setVisibility(View.VISIBLE);
        else {
            holder.txtNC.setVisibility(View.GONE);
        }
    }

    private String checkOrderStatus(OrdersForTable ordersForTable) {
        String orderStatus = ordersForTable.getStatus();
        switch (orderStatus) {
            case "0":
                orderStatus = "Ordered";
                break;
            case "1":
                orderStatus = "Processing";
                break;
            case "2":
                orderStatus = "Finished";
                break;
            case "3":
                orderStatus = "Cancelled";
                break;
            case "4":
                orderStatus = "Unable to process";
                break;
            case "5":
                orderStatus = "Split";
                break;
        }
        return orderStatus;
    }

    private String getPortionInfo(OrdersForTable ordersForTable) {
        String c_note = "";
        String portion = ordersForTable.getPortion_details();
        if (!TextUtils.isEmpty(portion)) {
            c_note = ordersForTable.getPortion_details();
        }
        return c_note;
    }

    private String getCustomizationNotes(OrdersForTable ordersForTable) {
        String portion = ordersForTable.getPortion_details();
        if (TextUtils.isEmpty(portion)) {
            portion = "Regular";
        }
        cNotes = "C: " + ordersForTable.getCustom_details() + " - " + portion;
        return cNotes;
    }

    @Override
    public int getItemCount() {
        return ordersForTableArrayList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView itemName;
        TextView itemQuantity;
        TextView txtOrderStatus;
        TextView txt_customizationNotes;
        TextView txtNC;
        ProgressBar progressBar;
        Button btnOrderCancel;
        LinearLayout c_note_layout;

        public MyHolder(View itemView) {
            super(itemView);
            itemName = itemView.findViewById(R.id.txtItemName);
            itemQuantity = itemView.findViewById(R.id.txtItemQty);
            txtOrderStatus = itemView.findViewById(R.id.txtOrderStatus);
            txt_customizationNotes = itemView.findViewById(R.id.txtCustomNotes);
            txtNC = itemView.findViewById(R.id.txtNC);
            progressBar = itemView.findViewById(R.id.itemProgress);
            btnOrderCancel = itemView.findViewById(R.id.btnOrderCancel);
            c_note_layout = itemView.findViewById(R.id.c_note_layout);
            btnOrderCancel.setOnClickListener(this);

           /* itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.cancelItem_fromListListenerModified(ordersForTableArrayList.get(getAdapterPosition()).getName(),
                            cNotes,ordersForTableArrayList.get(getAdapterPosition()).getMenu_item_id(),
                            ordersForTableArrayList.get(getAdapterPosition()).getCustom_data());
                }
            });*/
        }

        @Override
        public void onClick(View v) {
            listener.cancelItem_fromListListener(ordersForTableArrayList.get(getAdapterPosition()).getOrder_id());
            listener.cancelItem_fromListListenerModified(ordersForTableArrayList.get(getAdapterPosition()).getMenu_item_name(),
                    cNotes, ordersForTableArrayList.get(getAdapterPosition()).getMenu_item_id(),
                    ordersForTableArrayList.get(getAdapterPosition()).getCustom_data(),
                    ordersForTableArrayList.get(getAdapterPosition()).getNc_order());
            notifyDataSetChanged();
        }
    }
}

 /* private void splitValues(String s_1) {
        String[] separated = s_1.split(" ");
        String customization_comment = Arrays.toString(separated);
        AppLog.write("@@@@@@@@@@@","-"+separated[0].trim()+separated[1].trim()+separated[2].trim());
    }*/
package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.models.GetPaymentOptions;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 26-09-2017 in TableMateNew
 ***********************************************************************/
public class PaymentOptionAdapter extends RecyclerView.Adapter<PaymentOptionAdapter.Holder> {
    private Context context;
    private ArrayList<GetPaymentOptions> optionsList;
    private LayoutInflater inflater;

    public PaymentOptionAdapter(Context context, ArrayList<GetPaymentOptions> optionsList) {
        this.context = context;
        this.optionsList = optionsList;
        inflater = LayoutInflater.from(context);
    }

    public void MyDataChanged(ArrayList<GetPaymentOptions> options) {
        optionsList = options;
        notifyDataSetChanged();
    }

    @Override
    public PaymentOptionAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.get_payment_other_details, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(PaymentOptionAdapter.Holder holder, int position) {
        GetPaymentOptions options = optionsList.get(position);
        holder.btn_pay_option.setText(options.getPaymentmode());
    }

    @Override
    public int getItemCount() {
        return optionsList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView btn_pay_option;

        public Holder(View itemView) {
            super(itemView);
            btn_pay_option = itemView.findViewById(R.id.pay_option);
        }
    }
}


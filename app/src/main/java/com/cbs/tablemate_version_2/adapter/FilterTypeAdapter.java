package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.interfaces.MenuListener;
import com.cbs.tablemate_version_2.models.FilterType;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 26-11-2018 in TableMateNew
 ***********************************************************************/
public class FilterTypeAdapter extends RecyclerView.Adapter<FilterTypeAdapter.MyHolder> {
    MenuListener listener;
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<FilterType> filterList;
    private int selectedPos = 0;

    public FilterTypeAdapter(Context context, ArrayList<FilterType> filterList) {
        this.context = context;
        this.filterList = filterList;
        inflater = LayoutInflater.from(context);
    }

    public void MyDataChanged(ArrayList<FilterType> filters) {
        filterList = filters;
        notifyDataSetChanged();
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.activity_filter_type, parent, false);
        return new MyHolder(v);
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        FilterType filter = filterList.get(position);
        holder.btnFilterType.setText(filter.getName());
        holder.itemView.setSelected(selectedPos == position);

        if (selectedPos == position) {
            // view is selected
            holder.btnFilterType.setBackgroundResource(R.drawable.bg_green_button);
            holder.btnFilterType.setTextColor(Color.WHITE);
            holder.btnFilterType.setFocusable(true);
        } else
            // view is not selected
            holder.btnFilterType.setBackgroundResource(R.drawable.edit_background);
        holder.btnFilterType.setTextColor(Color.BLACK);
    }

    @Override
    public int getItemCount() {
        return filterList.size();
    }

    public void setFilterListener(MenuListener menuListener) {
        this.listener = menuListener;
    }

    public class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Button btnFilterType;

        public MyHolder(View itemView) {
            super(itemView);
            btnFilterType = itemView.findViewById(R.id.btnFilter);
            btnFilterType.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            notifyItemChanged(selectedPos);
            selectedPos = getLayoutPosition();
            notifyItemChanged(selectedPos);
            listener.onFilterClick(filterList.get(getAdapterPosition()).getName(), filterList.get(getAdapterPosition()).getId(),
                    filterList.get(getAdapterPosition()).getColor_value());
        }
    }
}


package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.interfaces.AddPaymentValuesListener;
import com.cbs.tablemate_version_2.models.PaymentOption;
import com.cbs.tablemate_version_2.utilities.Utility;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 26-09-2017 in TableMateNew
 ***********************************************************************/
public class GetInputForPaymentDetailsAdapter extends RecyclerView.Adapter<GetInputForPaymentDetailsAdapter.Holder> {
    AddPaymentValuesListener listener;
    private Context mContext;
    private ArrayList<PaymentOption> optionsList;
    private LayoutInflater inflater;

    public GetInputForPaymentDetailsAdapter(Context context, ArrayList<PaymentOption> optionsList) {
        this.mContext = context;
        this.optionsList = optionsList;
        inflater = LayoutInflater.from(context);
    }

    public void setPaymentOptionsSelectionListener(AddPaymentValuesListener paymentValuesListener) {
        this.listener = paymentValuesListener;
    }

    public void MyDataChanged(ArrayList<PaymentOption> options) {
        optionsList = options;
        notifyDataSetChanged();
    }

    @Override
    public GetInputForPaymentDetailsAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.pos_payment_row, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(GetInputForPaymentDetailsAdapter.Holder holder, int position) {
        holder.bind(optionsList.get(position));

    }

    @Override
    public int getItemCount() {
        return optionsList.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        Button b_PaymentType;
        TextView edit_amount, edit_card;
        boolean selected = false;
        String amount, card_digit, payType;

        Holder(View itemView) {
            super(itemView);
            b_PaymentType = itemView.findViewById(R.id.pay_option);
            edit_amount = itemView.findViewById(R.id.edit_amount);
            edit_card = itemView.findViewById(R.id.edit_card_num);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    amount = edit_amount.getText().toString();
                    card_digit = edit_card.getText().toString();
                    payType = b_PaymentType.getText().toString();
                    selected = true;
                    listener.setPaymentValues(getAdapterPosition(), amount, card_digit, payType, selected);
                }
            });
        }

        public void bind(PaymentOption paymentOption) {
            b_PaymentType.setText(paymentOption.getPaymentType());
            double amount = Utility.convertToDouble(paymentOption.getAmount());
            edit_amount.setText(String.format("%.2f", amount));
            if (paymentOption.getPaymentType().equalsIgnoreCase("Cash")) {
                edit_card.setVisibility(View.GONE);
            } else {
                edit_card.setVisibility(View.VISIBLE);
                edit_card.setText(paymentOption.getPin());
            }
        }
    }
}
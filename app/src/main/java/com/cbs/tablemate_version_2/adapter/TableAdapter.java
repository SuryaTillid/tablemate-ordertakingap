package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.interfaces.TableClickListener;
import com.cbs.tablemate_version_2.models.Table;

import java.util.List;


/*********************************************************************
 * Created by Baraneeswari on 04-05-2016 in TableMateNew
 ***********************************************************************/
public class TableAdapter extends RecyclerView.Adapter<TableAdapter.MyHolder> {

    private List<Table> table_list;
    private Context context;
    private LayoutInflater inflater;
    private TableClickListener listener;
    private Table t_obj;

    public TableAdapter(Context contexts, List<Table> table_list, TableClickListener tableClickListener) {
        this.context = contexts;
        this.table_list = table_list;
        this.listener = tableClickListener;
        inflater = LayoutInflater.from(context);
    }

    public void MyDataChanged(List<Table> tables) {
        table_list = tables;
        notifyDataSetChanged();
    }

    public void setTableOnClickListener(TableClickListener tableClickListener) {
        this.listener = tableClickListener;
    }

    @Override
    public TableAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.singletable, parent, false);
        return new MyHolder(v);
    }

    @Override
    public void onBindViewHolder(TableAdapter.MyHolder holder, int position) {
        t_obj = table_list.get(position);
        holder.txt_Table_message.setVisibility(View.GONE);
        if (t_obj != null) {
            checkHoldTable(t_obj, holder.img, holder.txt_Table_message, holder);
            holder.tableId.setText(t_obj.getTable_name());
            holder.tableSize.setText("Seats:" + t_obj.getSize());
            if (t_obj.isSelected()) {
                AppLog.write("TAG", "" + t_obj.isSelected());
                //holder.itemView.setBackground(context.getResources().getDrawable(R.drawable.edit_background));
                holder.frameGridView.setBackgroundColor(Color.parseColor("#cccccc"));
            } else {
                holder.itemView.setBackground(null);
            }
        } else {
            holder.itemView.setVisibility(View.GONE);
        }
    }

    private void checkHoldTable(Table t_obj, ImageView img, TextView txt_table_message, MyHolder holder) {
        String bill_id = t_obj.getBill_id();
        AppLog.write("BILL--", "--" + bill_id);
        AppLog.write("TableStatus--", "--" + t_obj.getStatus());

       /* if(!TextUtils.isEmpty(bill_id)&&order_type.equalsIgnoreCase("dine-in")&&!("Joined".equalsIgnoreCase(t_obj.getStatus())))
        {
            img.setImageResource(R.drawable.ic_locked_table);
            ColorFilter filter = new LightingColorFilter(Color.parseColor("#ffc002"),Color.parseColor("#ffc002"));
            img.setColorFilter(filter);
        }
        else
        {*/
        if ("0".equalsIgnoreCase(t_obj.getStatus())) {
            img.setImageResource(R.drawable.ic_free_table);
            ColorFilter filter = new LightingColorFilter(Color.parseColor("#008000"),
                    Color.parseColor("#008000"));
            img.setColorFilter(filter);
            holder.txt_Table_message.setVisibility(View.GONE);
        } else if ("1".equalsIgnoreCase(t_obj.getStatus())) {
            img.setImageResource(R.drawable.ic_locked_table);
            ColorFilter filter = new LightingColorFilter(Color.RED, Color.RED);
            img.setColorFilter(filter);
            holder.txt_Table_message.setVisibility(View.GONE);
        } else if ("3".equalsIgnoreCase(t_obj.getStatus())) {
            img.setImageResource(R.drawable.ic_locked_table);
            ColorFilter filter = new LightingColorFilter(Color.parseColor("#ffc002"), Color.parseColor("#ffc002"));
            img.setColorFilter(filter);
            holder.txt_Table_message.setVisibility(View.GONE);
        } else if ("2".equalsIgnoreCase(t_obj.getStatus())) {
            img.setImageResource(R.drawable.ic_join_table);
            ColorFilter filter = new LightingColorFilter(Color.BLUE, Color.BLUE);
            img.setColorFilter(filter);
            img.setEnabled(false);
            t_obj.setSelected(false);
            holder.txt_Table_message.setVisibility(View.VISIBLE);
            holder.txt_Table_message.setText(t_obj.getRoot_table_id());
        }
        //}
    }

    @Override
    public int getItemCount() {
        return table_list.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView tableId;
        TextView tableSize;
        FrameLayout frameGridView;
        TextView txt_Table_message;

        public MyHolder(final View view) {
            super(view);

            img = view.findViewById(R.id.imgTable);
            tableId = view.findViewById(R.id.txtTableId);
            tableSize = view.findViewById(R.id.txtTableSize);
            frameGridView = view.findViewById(R.id.frameGridView);
            txt_Table_message = view.findViewById(R.id.txtTableText);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppLog.write("REQ.ID", "---" + table_list.get(getAdapterPosition()).getId());
                    listener.OnTableClick(getAdapterPosition());
                    //view.setBackgroundColor(Color.parseColor("#cccccc"));
                }
            });
        }
    }
}
package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.models.Table;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 28-08-2018 in TableMateNew
 ***********************************************************************/
public class TableAdapter_1 extends BaseAdapter {

    private ArrayList<Table> table_list;
    private Context context;
    private LayoutInflater inflater;
    private String order_type;

    public TableAdapter_1(Context contexts, ArrayList<Table> table_list, String orderType) {
        this.context = contexts;
        this.table_list = table_list;
        this.order_type = orderType;
    }

    public void MyDataChanged(ArrayList<Table> tables) {
        table_list = tables;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return table_list.size();
    }

    @Override
    public Object getItem(int p) {
        return table_list.get(p);
    }

    @Override
    public long getItemId(int p) {
        return p;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.singletable, null);

        ImageView img = convertView.findViewById(R.id.imgTable);
        TextView tableId = convertView.findViewById(R.id.txtTableId);
        TextView tableSize = convertView.findViewById(R.id.txtTableSize);
        FrameLayout frameGridView = convertView.findViewById(R.id.frameGridView);
        TextView txt_Table_message = convertView.findViewById(R.id.txtTableText);

        Table t_obj = table_list.get(position);
        txt_Table_message.setVisibility(View.GONE);
        if (t_obj != null) {
            checkHoldTable(t_obj, img, txt_Table_message);
            tableId.setText(t_obj.getTable_name());
            tableSize.setText("Seats:" + t_obj.getSize());
            if (t_obj.isSelected()) {
                convertView.setBackground(context.getResources().getDrawable(R.drawable.edit_background));
            } else {
                convertView.setBackground(null);
            }
        } else {
            frameGridView.setVisibility(View.GONE);
        }
        return convertView;
    }

    private void checkHoldTable(Table t_obj, ImageView img, TextView txt_Table_message) {

        String bill_id = t_obj.getBill_id();
        AppLog.write("BILL--", "--" + bill_id);
        AppLog.write("TableStatus--", "--" + t_obj.getStatus());

       /* if(!TextUtils.isEmpty(bill_id)&&order_type.equalsIgnoreCase("dine-in")&&!("Joined".equalsIgnoreCase(t_obj.getStatus())))
        {
            img.setImageResource(R.drawable.ic_locked_table);
            ColorFilter filter = new LightingColorFilter(Color.parseColor("#ffc002"),Color.parseColor("#ffc002"));
            img.setColorFilter(filter);
        }
        else
        {*/
        if ("0".equalsIgnoreCase(t_obj.getStatus())) {
            img.setImageResource(R.drawable.ic_free_table);
            ColorFilter filter = new LightingColorFilter(Color.parseColor("#008000"),
                    Color.parseColor("#008000"));
            img.setColorFilter(filter);
            txt_Table_message.setVisibility(View.GONE);
        } else if ("1".equalsIgnoreCase(t_obj.getStatus())) {
            img.setImageResource(R.drawable.ic_locked_table);
            ColorFilter filter = new LightingColorFilter(Color.RED, Color.RED);
            img.setColorFilter(filter);
            txt_Table_message.setVisibility(View.GONE);
        } else if ("3".equalsIgnoreCase(t_obj.getStatus())) {
            img.setImageResource(R.drawable.ic_locked_table);
            ColorFilter filter = new LightingColorFilter(Color.parseColor("#ffc002"), Color.parseColor("#ffc002"));
            img.setColorFilter(filter);
            txt_Table_message.setVisibility(View.GONE);
        } else if ("2".equalsIgnoreCase(t_obj.getStatus())) {
            img.setImageResource(R.drawable.ic_join_table);
            ColorFilter filter = new LightingColorFilter(Color.BLUE, Color.BLUE);
            img.setColorFilter(filter);
            img.setEnabled(false);
            t_obj.setSelected(false);
            txt_Table_message.setVisibility(View.VISIBLE);
            txt_Table_message.setText(t_obj.getRoot_table_name());
            //}
        }
    }
}
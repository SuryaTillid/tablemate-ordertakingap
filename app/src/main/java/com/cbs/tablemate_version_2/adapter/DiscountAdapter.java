package com.cbs.tablemate_version_2.adapter;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.interfaces.DiscountListener;
import com.cbs.tablemate_version_2.models.BillPreview;
import com.cbs.tablemate_version_2.utilities.Utility;

import java.util.ArrayList;

/*********************************************************************
 * Created by Baraneeswari on 28-04-2016 in TableMateNew
 ***********************************************************************/

public class DiscountAdapter extends RecyclerView.Adapter<DiscountAdapter.MyHolder> {
    ArrayList<BillPreview> discountList;
    DiscountListener listener;
    private String[] discount = {"%", "₹", "NC"};
    private boolean onBind = true;

    public DiscountAdapter(ArrayList<BillPreview> billList, DiscountListener listener) {
        this.discountList = billList;
        this.listener = listener;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_discount_row, parent, false);
        return new MyHolder(v, new MyCustomEditTextListener());
    }

    public void myDataSetChanged(ArrayList<BillPreview> discountList) {
        this.discountList = discountList;
        notifyDataSetChanged();

    }

    @Override
    public void onBindViewHolder(DiscountAdapter.MyHolder holder, int position) {

        onBind = true;
        holder.customEditTextListener.updatePosition(holder.getAdapterPosition());
        BillPreview item = discountList.get(position);
        holder.tvQuantity.setText("" + item.getOrderQuantity());
        holder.tvItem.setText(item.getMenu_item_name());
        holder.tvPrice.setText("" + item.getPrice());
        String dType = item.getDiscountType();
        Double discountAmount = 0.0;
        Double amount = item.getPrice();
        if (!TextUtils.isEmpty(dType)) {
            if ("%".equalsIgnoreCase(item.getDiscountType())) {
                discountAmount = amount * (Utility.convertToDouble(item.getDiscount()) / 100);
            } else if ("₹".equalsIgnoreCase(item.getDiscountType())) {
                discountAmount = Utility.convertToDouble(item.getDiscount());
            } else if ("NC".equalsIgnoreCase(item.getDiscountType())) {
                discountAmount = amount;
            }
        }

        holder.tvAmount.setText("" + (amount - discountAmount));
        holder.etDiscount.setText("" + item.getDiscountAmount());
        holder.spDiscountType.setSelection(getSelectedItemPosition(dType));
        onBind = false;
    }

    public void updatePosition(int position) {
        notifyItemChanged(position);
    }

    private int getSelectedItemPosition(String dType) {
        int position = 0;
        if (!TextUtils.isEmpty(dType)) {
            for (String s : discount) {
                if (s.equalsIgnoreCase(dType)) {
                    return position;
                }
                position++;
            }
        }
        return 0;
    }

    @Override
    public int getItemCount() {
        return discountList.size();
    }

    class MyHolder extends RecyclerView.ViewHolder {

        private TextView tvQuantity;
        private TextView tvItem;
        private TextView tvPrice;
        private Spinner spDiscountType;
        private EditText etDiscount;
        private TextView tvAmount;
        private MyCustomEditTextListener customEditTextListener;


        public MyHolder(View vi, MyCustomEditTextListener customEditTextListener) {
            super(vi);

            this.customEditTextListener = customEditTextListener;

            tvQuantity = vi.findViewById(R.id.tvQuantity);
            tvItem = vi.findViewById(R.id.tvItem);
            tvPrice = vi.findViewById(R.id.tvPrice);
            spDiscountType = vi.findViewById(R.id.spDiscountType);
            etDiscount = vi.findViewById(R.id.etDiscount);
            tvAmount = vi.findViewById(R.id.tvAmount);

            vi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onDiscountClick(getAdapterPosition());
                }
            });

            spDiscountType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (!onBind) {
                        discountList.get(getAdapterPosition()).setDiscountType(discount[position]);
                        listener.onDiscountTypeChanged(position, getAdapterPosition());
                        notifyItemChanged(getAdapterPosition());
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            etDiscount.addTextChangedListener(customEditTextListener);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(spDiscountType.getContext(), android.R.layout.simple_dropdown_item_1line, spDiscountType.getContext().getResources().getStringArray(R.array.discount_types));
            spDiscountType.setAdapter(adapter);
        }
    }

    private class MyCustomEditTextListener implements TextWatcher {
        private int position;

        public void updatePosition(int position) {
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            // no op
        }

        @Override
        public void onTextChanged(CharSequence s, int i, int i2, int i3) {
            if (!onBind) {
                discountList.get(position).setDiscount(s.toString());
                //listener.onDiscountAdded("", s.toString(), position,false);
                // TODO take this in dialog and pass three values and update adapter

            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
           /* if (!onBind) {
                discountList.get(position).setDiscount(editable.toString());
                listener.onDiscountAdded(editable.toString(), position);
                notifyItemChanged(position);
            }*/
        }
    }
}




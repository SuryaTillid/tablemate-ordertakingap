package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.configuration.Settings;
import com.cbs.tablemate_version_2.interfaces.GetCustomizedUserData;
import com.cbs.tablemate_version_2.interfaces.MenuItemQtyChangeListener;
import com.cbs.tablemate_version_2.models.Item;
import com.cbs.tablemate_version_2.utilities.ImageLoader;
import com.cbs.tablemate_version_2.utilities.Utility;

import java.util.ArrayList;
import java.util.List;

/*********************************************************************
 * Created by Baraneeswari on 27-04-2016 in TableMateNew
 *********************************************************************/

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MyHolder> {
    public ImageLoader imageLoader;
    MenuItemQtyChangeListener listener;
    GetCustomizedUserData customizedUserDataListener;
    String s = "#006600";
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<Item> menuItemsList;
    private Settings settings;

    public MenuAdapter(Context contexts, ArrayList<Item> menuItemsList) {
        this.context = contexts;
        this.menuItemsList = menuItemsList;
        inflater = LayoutInflater.from(context);
        settings = new Settings(context);
        imageLoader = new ImageLoader(context);
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.activity_menu_row, parent, false);
        return new MyHolder(v);
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position, List<Object> payloads) {
        // TODO call OnBindViewHolder
        //callBinderFunctionality(holder, position);
        super.onBindViewHolder(holder, position, payloads);
    }

    /*private void callBinderFunctionality(MyHolder holder, int position) {
        Item menuItem = menuItemsList.get(position);
        holder.txtItemName.setText(menuItem.getName());
        holder.txtItemPrice.setText(menuItem.getPrice());

        String s = menuItem.getFilter_type_name();

        if ("1".equals(s))
        {
            holder.imgFilter.setImageResource(R.drawable.ic_veg);
        }
        else if ("2".equals(s)) {
            holder.imgFilter.setImageResource(R.drawable.ic_non_veg);
        }
        else if ("3".equals(s)) {
            holder.imgFilter.setImageResource(R.drawable.ic_dessert);
        }
        else if ("6".equals(s)) {
            holder.imgFilter.setImageResource(R.drawable.ic_a);
        }
        if ("0".equals(s)||"200".equals(s))
        {
            holder.imgFilter.setImageResource(R.drawable.ic_a);
        }

        if (menuItem.getCustomised_item().equals("0")) {
            holder.txtDescription.setVisibility(View.GONE);
        } else {
            holder.txtDescription.setVisibility(View.VISIBLE);
        }

        int qty = Integer.parseInt(TextUtils.isEmpty(menuItem.getItemQty()) ? "0" : menuItem.getItemQty());
        if (qty > 0) {
            holder.btn_add.setVisibility(View.VISIBLE);
            holder.btn_deduct.setVisibility(View.VISIBLE);
            holder.btn_show_qty.setBackgroundColor(Color.parseColor("#006600"));
            holder.btn_show_qty.setTextColor(Color.parseColor("#ffffff"));
            holder.btn_show_qty.setText("" + qty);
        } else {
            holder.btn_show_qty.setText("ADD");
            holder.btn_show_qty.setBackgroundResource(R.drawable.bg_cust1);
            holder.btn_show_qty.setTextColor(Color.parseColor("#006600"));
            holder.btn_add.setVisibility(View.GONE);
            holder.btn_deduct.setVisibility(View.GONE);
        }

        if (menuItem.getLive_item().equals("1")) {
            holder.layout_live_quantity.setVisibility(View.VISIBLE);
            double count = Utility.convertToDouble(menuItem.getStockQuantity()) - Utility.convertToInteger(menuItem.getItemQty());
            holder.txtLiveMenuItemQuantity.setText("" + count);
        } else {
            holder.layout_live_quantity.setVisibility(View.GONE);
        }
    }*/

    public void setCustomizedUserDataListener(GetCustomizedUserData listener) {
        this.customizedUserDataListener = listener;
    }

    public void setMenuListener(MenuItemQtyChangeListener menuListener) {
        this.listener = menuListener;
    }

    public void MyDataChanged(ArrayList<Item> menus) {
        menuItemsList = menus;
        notifyDataSetChanged();
    }

    public void setFilter(ArrayList<Item> menuList) {
        menuItemsList = new ArrayList<>();
        menuItemsList.addAll(menuList);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        Item menuItem = menuItemsList.get(position);
        holder.txtItemName.setText(menuItem.getName());

        /*
         * Load Image - decode and convert to bitmap
         * */

        //imageLoader.DisplayImage("http://192.168.10.31:9000/images/Icon_png/loyalty.png",holder.thumb_image);

        try {
            String food_path_string = menuItem.getPhoto();
            String food_image_path_after_split = food_path_string.split(",")[1];
            byte[] image_byte_string = Base64.decode(food_image_path_after_split, Base64.DEFAULT);
            Bitmap decodedImage = BitmapFactory.decodeByteArray(image_byte_string, 0, image_byte_string.length);
            holder.thumb_image.setImageBitmap(decodedImage);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Load Values---

        if (settings.getORDER_TYPE().equalsIgnoreCase("2")) {
            AppLog.write("Price--alertnate", "--" + menuItem.getAlternate_price());
            holder.txtItemPrice.setText(menuItem.getAlternate_price());
        } else {
            AppLog.write("Price--", "--" + menuItem.getPrice());
            holder.txtItemPrice.setText(menuItem.getPrice());
        }

        s = menuItem.getColor_filter_type();
        AppLog.write("TAG--", "-" + s);

        if (!TextUtils.isEmpty(s)) {
            try {
                holder.imgFilter.setColorFilter(Color.parseColor(s));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

       /* if ("1".equals(s)) {
            holder.imgFilter.setImageResource(R.drawable.ic_veg);
        }
        else if ("2".equals(s)) {
            holder.imgFilter.setImageResource(R.drawable.ic_non_veg);
        }
        else if ("3".equals(s)) {
            holder.imgFilter.setImageResource(R.drawable.ic_dessert);
        }
        else if ("6".equals(s)) {
            holder.imgFilter.setImageResource(R.drawable.ic_a);
        }
        if ("0".equals(s)||"200".equals(s)) {

            holder.imgFilter.setImageResource(R.drawable.ic_a);
        }*/

        if (menuItem.getCustomised_item().equals("0")) {
            holder.txtDescription.setVisibility(View.GONE);
        } else {
            holder.txtDescription.setVisibility(View.VISIBLE);
        }

        int qty = Integer.parseInt(TextUtils.isEmpty(menuItem.getItemQty()) ? "0" : menuItem.getItemQty());
        if (qty > 0) {
            holder.btn_add.setVisibility(View.VISIBLE);
            holder.btn_deduct.setVisibility(View.VISIBLE);
            holder.btn_show_qty.setBackgroundColor(Color.parseColor("#006600"));
            holder.btn_show_qty.setTextColor(Color.parseColor("#ffffff"));
            holder.btn_show_qty.setText("" + qty);
        } else {
            holder.btn_show_qty.setText("ADD");
            holder.btn_show_qty.setBackgroundResource(R.drawable.bg_cust1);
            holder.btn_show_qty.setTextColor(Color.parseColor("#006600"));
            holder.btn_add.setVisibility(View.GONE);
            holder.btn_deduct.setVisibility(View.GONE);
        }

        if (menuItem.getLive_item().equals("1")) {
            holder.layout_live_quantity.setVisibility(View.VISIBLE);
            double count = Utility.convertToDouble(menuItem.getStockQuantity()) - Utility.convertToInteger(menuItem.getItemQty());
            holder.txtLiveMenuItemQuantity.setText("" + count);
        } else {
            holder.layout_live_quantity.setVisibility(View.GONE);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return menuItemsList.size();
    }

    class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtItemName;
        TextView txtDescription;
        TextView txtItemPrice;
        TextView txtItemQty;
        TextView txtLiveMenuItemQuantity;
        ImageView thumb_image;
        ImageView imgFilter;
        LinearLayout layout_live_quantity;
        Button btn_add, btn_deduct, btn_show_qty;
        //ViewGroup transitionsContainer;

        public MyHolder(View itemView) {
            super(itemView);
            //transitionsContainer = itemView.findViewById(R.id.transitions_container);
            txtItemName = itemView.findViewById(R.id.txtItemName);
            txtDescription = itemView.findViewById(R.id.description);
            txtItemPrice = itemView.findViewById(R.id.txtItemPrice);
            txtItemQty = itemView.findViewById(R.id.txtItemQty);
            txtLiveMenuItemQuantity = itemView.findViewById(R.id.txt_live_menuQuantity);
            thumb_image = itemView.findViewById(R.id.list_image);
            imgFilter = itemView.findViewById(R.id.imageFilter);
            btn_add = itemView.findViewById(R.id.btn_add);
            btn_deduct = itemView.findViewById(R.id.btn_deduct);
            btn_show_qty = itemView.findViewById(R.id.btn_show_qty);
            layout_live_quantity = itemView.findViewById(R.id.layout_live_quantity);

            btn_deduct.setOnClickListener(this);
            btn_add.setOnClickListener(this);
            btn_show_qty.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            try {
                switch (v.getId()) {
                    case R.id.btn_add:
                        listener.incrementQuantity(menuItemsList.get(getAdapterPosition()).getId(), getAdapterPosition());
                        break;
                    case R.id.btn_show_qty:
                        listener.incrementQuantity(menuItemsList.get(getAdapterPosition()).getId(), getAdapterPosition());
                        break;
                    case R.id.btn_deduct:
                        listener.decrementQuantity(menuItemsList.get(getAdapterPosition()).getId(), getAdapterPosition());
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
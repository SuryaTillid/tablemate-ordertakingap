package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.models.EmployeeTypeName;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 09-10-2019 in TableMateNew
 ***********************************************************************/
public class CustomizationEmployeeAdapter extends RecyclerView.Adapter<CustomizationEmployeeAdapter.Holder> {

    ItemTypeListener listener;
    private ArrayList<EmployeeTypeName> employeeTypeName;
    private Context contexts;
    private LayoutInflater inflater;

    public CustomizationEmployeeAdapter(Context context, ArrayList<EmployeeTypeName> name) {
        this.contexts = context;
        this.employeeTypeName = name;
        inflater = LayoutInflater.from(context);
    }

    public void MyDataChanged(ArrayList<EmployeeTypeName> e_name) {
        employeeTypeName = e_name;
        notifyDataSetChanged();
    }

    public void setListener(ItemTypeListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public CustomizationEmployeeAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.row_for_employee, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(CustomizationEmployeeAdapter.Holder holder, int position) {
        EmployeeTypeName name = employeeTypeName.get(position);
        holder.t_name.setText(name.getName());
        if (name.isSelected()) {
            holder.itemView.setBackgroundResource(R.drawable.bg_green_button);
            holder.t_name.setTextColor(Color.WHITE);
            holder.t_name.setTextColor(Color.WHITE);
        } else {
            holder.itemView.setBackgroundResource(R.drawable.bg_cust1);
            holder.t_name.setTextColor(Color.BLACK);
            holder.t_name.setTextColor(Color.BLACK);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return employeeTypeName.size();
    }

    public interface ItemTypeListener {
        void selectItemType(int position);
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView t_name;

        public Holder(View itemView) {
            super(itemView);

            t_name = itemView.findViewById(R.id.item_EmployeeName);
        }
    }
}


package com.cbs.tablemate_version_2.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.interfaces.CancelItemListener;
import com.cbs.tablemate_version_2.interfaces.GetDeliveryStatusListener;
import com.cbs.tablemate_version_2.models.OrdersForTable;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 20-08-2016 in TableMateNew
 ***********************************************************************/
public class ViewDeliveryTrackingAdapter extends RecyclerView.Adapter<ViewDeliveryTrackingAdapter.MyViewHolder> {
    CancelItemListener listener;
    GetDeliveryStatusListener deliveryStatusListener;
    private ArrayList<OrdersForTable> trackingList;
    private Context context;
    private LayoutInflater inflater;

    public ViewDeliveryTrackingAdapter(Context context, ArrayList<OrdersForTable> trackings) {
        this.trackingList = trackings;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public void MyDataChanged(ArrayList<OrdersForTable> ordersForTables) {
        trackingList = ordersForTables;
        notifyDataSetChanged();
    }

    public void setCancelItemListener(CancelItemListener cancelItemListener) {
        this.listener = cancelItemListener;
    }

    public void setDeliveryStatusListener(GetDeliveryStatusListener deliveryStatusListener) {
        this.deliveryStatusListener = deliveryStatusListener;
    }

    @Override
    public ViewDeliveryTrackingAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_redesign_delivery_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewDeliveryTrackingAdapter.MyViewHolder holder, int position) {
        OrdersForTable tracking = trackingList.get(position);
        holder.menuName.setText(tracking.getName());
        holder.progress.setText(tracking.getStatus());
        holder.status.setText(tracking.getStatus());

        AppLog.write("Tracking Status---1--", "" + tracking.getStatus());

        if (tracking.getStatus().equals("finished")) {
            AppLog.write("Tracking Status---1.", "" + tracking.getStatus());
            holder.status.setBackgroundResource(R.drawable.bg_green_button);
            holder.status.setTextColor(Color.WHITE);
        } else if (tracking.getStatus().equals("Unable to Process")) {
            holder.status.setBackgroundResource(0);
            holder.status.setTextColor(Color.RED);
            holder.progress.setVisibility(View.GONE);
            holder.imgButton_cancel.setVisibility(View.GONE);
            holder.imgButton_ok.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return trackingList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView menuName, progress;
        ImageButton imgButton_ok, imgButton_cancel;
        Button status;

        public MyViewHolder(View view) {
            super(view);
            menuName = view.findViewById(R.id.txtItemName);
            progress = view.findViewById(R.id.txtItemProgress);
            status = view.findViewById(R.id.btnDeliveryStatus);
            imgButton_ok = view.findViewById(R.id.btnOk);
            imgButton_cancel = view.findViewById(R.id.btnCancel);
            imgButton_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog dialog = new Dialog(context);
                    dialog.setCancelable(false);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.order_cancel_conform);
                    dialog.show();
                    Button btnOk = dialog.findViewById(R.id.btnOk);
                    Button btnCancel = dialog.findViewById(R.id.btnCancel);

                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AppLog.write("Order-----Id", "---" + trackingList.get(getPosition()).getOrder_id());
                            listener.cancelItem_fromListListener(trackingList.get(getPosition()).getOrder_id());
                            notifyDataSetChanged();
                            dialog.dismiss();
                        }
                    });
                }
            });

            imgButton_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deliveryStatusListener.getDeliveryStatus_for_menuItemListener(trackingList.get(getPosition()).getOrder_id());
                    notifyDataSetChanged();
                }
            });
        }
    }
}

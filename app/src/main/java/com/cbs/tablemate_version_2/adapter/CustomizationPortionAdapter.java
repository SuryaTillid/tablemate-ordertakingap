package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.models.CustomizedItemsPortion;
import com.cbs.tablemate_version_2.models.Item;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 29-07-2016 in TableMateNew
 ***********************************************************************/
public class CustomizationPortionAdapter extends RecyclerView.Adapter<CustomizationPortionAdapter.Holder> {
    private Context context;
    private ArrayList<CustomizedItemsPortion> itemsPortionsList;
    private Item menuList = new Item();
    private LayoutInflater inflater;
    private PortionListener listener;
    private String orderType;

    public CustomizationPortionAdapter(Context context, ArrayList<CustomizedItemsPortion> itemsPortionsList, String orderType, Item menu_list) {
        this.context = context;
        this.itemsPortionsList = itemsPortionsList;
        inflater = LayoutInflater.from(context);
        this.orderType = orderType;
        this.menuList = menu_list;
    }

    public void MyDataChanged(ArrayList<CustomizedItemsPortion> portions) {
        itemsPortionsList = portions;
        notifyDataSetChanged();
    }

    public void setListener(PortionListener listener) {
        this.listener = listener;
    }

    @Override
    public CustomizationPortionAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.row_for_portion2, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(CustomizationPortionAdapter.Holder holder, int position) {
        CustomizedItemsPortion portion = itemsPortionsList.get(position);
        holder.itemPortionName.setText(portion.getPortion_name());
        if (orderType.equalsIgnoreCase("4")) {
            if (portion.getPortion_name().equalsIgnoreCase("Regular")) {
                holder.itemPortionPrice.setText(menuList.getAlternate_price());
            } else {
                holder.itemPortionPrice.setText(portion.getPortion_price());
            }
        } else {
            holder.itemPortionPrice.setText(portion.getPortion_price());
        }

        if (portion.isSelected()) {
            holder.itemView.setBackgroundResource(R.drawable.bg_green_button);
            holder.itemPortionName.setTextColor(Color.WHITE);
            holder.itemPortionPrice.setTextColor(Color.WHITE);
        } else {
            holder.itemView.setBackgroundResource(R.drawable.bg_cust1);
            holder.itemPortionName.setTextColor(Color.BLACK);
            holder.itemPortionPrice.setTextColor(Color.BLACK);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return itemsPortionsList.size();
    }

    public interface PortionListener {
        void selectPortion(int position);
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView itemPortionName;
        TextView itemPortionPrice;

        public Holder(final View itemView) {
            super(itemView);
            itemPortionName = itemView.findViewById(R.id.item_PortionName);
            itemPortionPrice = itemView.findViewById(R.id.item_PortionPrice);
        }
    }
}

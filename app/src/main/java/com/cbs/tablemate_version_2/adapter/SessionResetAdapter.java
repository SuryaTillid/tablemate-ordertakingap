package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.interfaces.ChangeColorListener;
import com.cbs.tablemate_version_2.models.Employee_Data;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 14-12-2017 in TableMateNew
 ***********************************************************************/
public class SessionResetAdapter extends RecyclerView.Adapter<SessionResetAdapter.MyHolder> {
    private ArrayList<Employee_Data> employee_dataList;
    private Context context;
    private LayoutInflater inflater;
    private ChangeColorListener listener;

    public SessionResetAdapter(Context context, ArrayList<Employee_Data> employee_List, ChangeColorListener listener) {
        this.context = context;
        this.employee_dataList = employee_List;
        this.listener = listener;
        inflater = LayoutInflater.from(context);
    }

    public void MyDataChanged(ArrayList<Employee_Data> employeeDataList) {
        employee_dataList = employeeDataList;
        notifyDataSetChanged();
    }

    @Override
    public SessionResetAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.activity_emp_details_row, parent, false);
        return new MyHolder(v);
    }

    @Override
    public void onBindViewHolder(SessionResetAdapter.MyHolder holder, int position) {
        Employee_Data employee_data = employee_dataList.get(position);

        holder.empName.setText(employee_data.getName().trim());
        String login_status = employee_data.getLogin_status().trim();
        if (login_status.equalsIgnoreCase("1")) {
            holder.img_active_status.setBackgroundResource(R.drawable.ic_active);
        } else {
            holder.img_active_status.setBackgroundResource(R.drawable.ic_inactive);
        }
        holder.itemView.setBackgroundColor(employee_data.isSelected() ? Color.parseColor("#cccccc") : Color.WHITE);
    }

    @Override
    public int getItemCount() {
        return employee_dataList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        TextView empName;
        ImageView img_active_status;

        public MyHolder(final View itemView) {
            super(itemView);
            empName = itemView.findViewById(R.id.txtName);
            img_active_status = itemView.findViewById(R.id.active_status);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.changeColor(getAdapterPosition());
                }
            });
        }
    }
}

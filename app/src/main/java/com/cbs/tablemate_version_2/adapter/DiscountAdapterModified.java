package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.interfaces.DiscountListener;
import com.cbs.tablemate_version_2.models.BillPreview;
import com.cbs.tablemate_version_2.models.CustomizationPreview;
import com.cbs.tablemate_version_2.utilities.Utility;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 12-02-2018 in TableMateNew
 ***********************************************************************/
public class DiscountAdapterModified extends RecyclerView.Adapter<DiscountAdapterModified.MyHolder> {
    private ArrayList<BillPreview> discountList = new ArrayList<>();
    private ArrayList<CustomizationPreview> customizationPreviewArrayList = new ArrayList<>();
    private DiscountListener listener;
    private String[] discount = {"%", "₹", "NC"};
    private boolean onBind = true;
    private Context context;
    private LayoutInflater inflater;

    public DiscountAdapterModified(Context context, ArrayList<BillPreview> list) {
        this.context = context;
        this.discountList = list;
        inflater = LayoutInflater.from(context);
    }

    public DiscountAdapterModified(ArrayList<BillPreview> billList, ArrayList<CustomizationPreview> customize_values_list, DiscountListener listener) {
        this.discountList = billList;
        this.listener = listener;
        this.customizationPreviewArrayList = customize_values_list;
    }

    @Override
    public DiscountAdapterModified.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.pos_activity_discount_row, parent, false);
        return new DiscountAdapterModified.MyHolder(v);
    }

    public void myDataSetChanged(ArrayList<BillPreview> discountList) {
        this.discountList = discountList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(DiscountAdapterModified.MyHolder holder, int position) {

        onBind = true;
        BillPreview item = discountList.get(position);

        holder.tvQuantity.setText("" + item.getOrderQuantity());
        if (isExist(customizationPreviewArrayList, item.getOrder_id())) {
            String name = "";
            double price = 0;

            for (CustomizationPreview cPreview : customizationPreviewArrayList) {
                if (item.getOrder_id().equals(cPreview.getOrder_id())) {
                    name += TextUtils.isEmpty(name) ? cPreview.getCustomisation_name() : "," + cPreview.getCustomisation_name();
                    price += Utility.convertToDouble(cPreview.getPrice());
                }
            }

            AppLog.write("Custom--", "--" + name);
            holder.tv_C_Item.setVisibility(View.VISIBLE);
            holder.tv_C_Item.setText("C: " + name);
            holder.tvCustPrice.setVisibility(View.VISIBLE);
            holder.tvCustPrice.setText(String.format("%.2f", price));
            holder.tvCustAmount.setVisibility(View.VISIBLE);
            holder.tvCustAmount.setText(String.format("%.2f", price));
        } else {
            holder.tv_C_Item.setVisibility(View.GONE);
            holder.tvCustPrice.setVisibility(View.GONE);
            holder.tvCustAmount.setVisibility(View.GONE);
        }
        holder.tvItem.setText(item.getMenu_item_name());
        holder.tvPrice.setText(String.format("%.2f", Utility.convertToDouble(item.getMrp_price())));
        String dType = item.getDiscountType();
        Double discountAmount = 0.0;
        Double amount = Utility.convertToDouble(item.getMrp_price());
        if (!TextUtils.isEmpty(dType)) {
            if ("%".equalsIgnoreCase(item.getDiscountType())) {
                //Todo changed - billDiscount
                discountAmount = amount * (Utility.convertToDouble(item.getBillDiscount()) / 100);
            } else if ("₹".equalsIgnoreCase(item.getDiscountType())) {
                discountAmount = Utility.convertToDouble(item.getBillDiscount());
            } else if ("NC".equalsIgnoreCase(item.getDiscountType())) {
                discountAmount = amount;
                holder.tvCustAmount.setText("0.00");
            }
        }
        AppLog.write("After_Discounted", "-" + discountAmount);
        holder.tvDiscount.setText(String.format("%.2f", discountAmount));
        double total_amt = amount * Utility.convertToDouble(item.getOrderQuantity());
        holder.tvAmount.setText(String.format("%.2f", (total_amt - discountAmount)));
        holder.etDiscount.setText(String.format("%.2f", item.getDiscountAmount()));
        onBind = false;
    }

    private boolean isExist(ArrayList<CustomizationPreview> customizationPreviewArrayList, String order_id) {
        for (CustomizationPreview customizationPreview : customizationPreviewArrayList) {
            if (customizationPreview.getOrder_id().equalsIgnoreCase(order_id)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int getItemCount() {
        return discountList.size();
    }

    class MyHolder extends RecyclerView.ViewHolder {

        private TextView tvQuantity;
        private TextView tvItem;
        private TextView tvPrice;
        private TextView tv_C_Item;
        private EditText etDiscount;
        private TextView tvAmount;
        private TextView tvDiscount;
        private TextView tvCustPrice;
        private TextView tvCustAmount;

        public MyHolder(View vi) {
            super(vi);
            tvQuantity = vi.findViewById(R.id.tvQuantity);
            tvItem = vi.findViewById(R.id.tvItem);
            tvPrice = vi.findViewById(R.id.tvPrice);
            etDiscount = vi.findViewById(R.id.etDiscount);
            tvAmount = vi.findViewById(R.id.tvAmount);
            tvDiscount = vi.findViewById(R.id.tvDiscount);
            tv_C_Item = vi.findViewById(R.id.custItem);
            tvCustPrice = vi.findViewById(R.id.tvCustPrice);
            tvCustAmount = vi.findViewById(R.id.custAmount);

            vi.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    listener.onDiscountClick(getAdapterPosition());
                }
            });
        }
    }
}
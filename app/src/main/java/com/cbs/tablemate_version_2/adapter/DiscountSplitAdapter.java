package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.interfaces.DiscountSplitListener;
import com.cbs.tablemate_version_2.models.DiscountOrder;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 04-10-2018 in TableMateNew
 ***********************************************************************/
public class DiscountSplitAdapter extends RecyclerView.Adapter<DiscountSplitAdapter.MyHolder> {
    DiscountSplitListener d_listener;
    private Context context;
    private ArrayList<DiscountOrder> ordersList = new ArrayList<>();
    private LayoutInflater inflater;
    private boolean checked;

    public DiscountSplitAdapter(Context context, ArrayList<DiscountOrder> orders) {
        this.context = context;
        this.ordersList = orders;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public DiscountSplitAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.activity_discount_dialog, parent, false);
        return new MyHolder(v);
    }

    public void setDiscountListener(DiscountSplitListener listener) {
        this.d_listener = listener;
    }


    @Override
    public int getItemCount() {
        return ordersList.size();
    }

    public void myDataSetChanged(ArrayList<DiscountOrder> o_list, int position) {
        this.ordersList = o_list;
        notifyItemChanged(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(DiscountSplitAdapter.MyHolder holder, int position) {
        DiscountOrder orders = ordersList.get(position);

        holder.t_qty.setText("1");
        holder.t_price.setText(orders.getMrp_price());
    }

    private String validateType(String type) {
        switch (type) {
            case "%":
                type = "percentage";
                break;
            case "₹":
                type = "rupee";
                break;
            case "NC":
                type = "NC";
                break;
        }
        return type;
    }

    private void set_discount_type_editable_values(EditText edit_discountAmount) {
        edit_discountAmount.setBackgroundResource(R.drawable.editext_style_normal);
        edit_discountAmount.setHint("Enter discount amount..");
        edit_discountAmount.setEnabled(true);
    }

    class MyHolder extends RecyclerView.ViewHolder {

        TextView t_qty, t_price;
        CheckBox check_menu_value;
        EditText edit_discountAmount;
        Spinner spinnerDiscount;
        private String[] discount = {"₹", "%", "NC"};

        public MyHolder(View itemView) {
            super(itemView);

            t_qty = itemView.findViewById(R.id.tvQuantity);
            t_price = itemView.findViewById(R.id.tvPrice);
            check_menu_value = itemView.findViewById(R.id.check_menu);
            edit_discountAmount = itemView.findViewById(R.id.et_Discount);
            spinnerDiscount = itemView.findViewById(R.id.spinnerDiscountType);


            TextWatcher tWatcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    String discount_type = validateType(spinnerDiscount.getSelectedItem().toString().trim());
                    String updated_price = edit_discountAmount.getText().toString().trim();
                    AppLog.write("TAG----@--", "called--" + discount_type);
                    d_listener.onDiscountAdded(discount_type, updated_price, getAdapterPosition(), checked);
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            };
            edit_discountAmount.addTextChangedListener(tWatcher);

            check_menu_value.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    String discount_type = validateType(spinnerDiscount.getSelectedItem().toString().trim());
                    String updated_price = edit_discountAmount.getText().toString().trim();
                    checked = isChecked;
                    AppLog.write("TAG----@", "called--" + discount_type);
                    d_listener.onDiscountAdded(discount_type, updated_price, getAdapterPosition(), isChecked);
                }
            });

            check_menu_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

            //Spinner
            ArrayAdapter<String> disAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, discount);
            spinnerDiscount.setAdapter(disAdapter);

            spinnerDiscount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String spinner_type = spinnerDiscount.getSelectedItem().toString();
                    if ("%".equalsIgnoreCase(spinner_type)) {
                        set_discount_type_editable_values(edit_discountAmount);
                    } else if ("₹".equalsIgnoreCase(spinner_type)) {
                        set_discount_type_editable_values(edit_discountAmount);
                    } else if ("NC".equalsIgnoreCase(spinner_type)) {
                        edit_discountAmount.setBackgroundResource(R.drawable.edit_background_disabled);
                        edit_discountAmount.setText(t_price.getText().toString());
                        edit_discountAmount.setEnabled(false);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }
    }
}



package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.models.BillPreview;
import com.cbs.tablemate_version_2.models.CustomizationPreview;
import com.cbs.tablemate_version_2.utilities.Utility;
import com.google.gson.Gson;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 14-02-2017 in TableMateNew
 ***********************************************************************/

public class BillCalculationAdapter extends RecyclerView.Adapter<BillCalculationAdapter.MyHolder> {
    private Context context;
    private ArrayList<BillPreview> billItemsList;
    private LayoutInflater inflater;
    private ArrayList<CustomizationPreview> customizationPreviewArrayList;

    public BillCalculationAdapter(Context context, ArrayList<CustomizationPreview> customization_valuesList, ArrayList<BillPreview> billItemsList) {
        this.context = context;
        this.billItemsList = billItemsList;
        this.customizationPreviewArrayList = customization_valuesList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public BillCalculationAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.activity_bill_row_redesign, parent, false);
        return new MyHolder(v);
    }

    @Override
    public int getItemCount() {
        return billItemsList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void MyDataChanged(ArrayList<CustomizationPreview> customization_valuesList, ArrayList<BillPreview> bill) {
        this.customizationPreviewArrayList = customization_valuesList;
        billItemsList = bill;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(BillCalculationAdapter.MyHolder holder, int position) {
        BillPreview bills = billItemsList.get(position);
        if (null != bills) {
            AppLog.write("Custom--", "--" + new Gson().toJson(customizationPreviewArrayList));
            if (isExist(customizationPreviewArrayList, bills.getOrder_id())) {
                String name = "";
                double price = 0;
                double amount = 0;
                for (CustomizationPreview cPreview : customizationPreviewArrayList) {
                    if (bills.getOrder_id().equals(cPreview.getOrder_id())) {
                        name += TextUtils.isEmpty(name) ? cPreview.getCustomisation_name() : "," + cPreview.getCustomisation_name();
                        price += Utility.convertToDouble(cPreview.getPrice());
                    }
                }
                holder.tv_C_Item.setVisibility(View.VISIBLE);
                holder.tv_C_Item.setText("C: " + name);
                holder.tv_C_Price.setVisibility(View.VISIBLE);
                holder.tv_C_Price.setText(String.format("%.2f", price));
                holder.tv_C_Amount.setVisibility(View.VISIBLE);
                amount = Utility.convertToDouble(bills.getOrderQuantity()) * price;
                holder.tv_C_Amount.setText(String.format("%.2f", amount));
            } else {
                holder.tv_C_Item.setVisibility(View.GONE);
                holder.tv_C_Price.setVisibility(View.GONE);
                holder.tv_C_Amount.setVisibility(View.GONE);
            }
            holder.ItemName.setText("" + bills.getMenu_item_name());
            Double item_Price = Utility.convertToDouble(bills.getMrp_price());
            holder.ItemPrice.setText(String.format("%.2f", item_Price));
            holder.ItemQuantity.setText("" + bills.getOrderQuantity());
            double total_amt = bills.getPrice();
            holder.ItemTotal.setText(String.format("%.2f", total_amt));
        }
    }

    private boolean isExist(ArrayList<CustomizationPreview> customizationPreviewArrayList, String order_id) {
        for (CustomizationPreview customizationPreview : customizationPreviewArrayList) {
            if (customizationPreview.getOrder_id().equalsIgnoreCase(order_id)) {
                return true;
            }
        }
        return false;
    }

    class MyHolder extends RecyclerView.ViewHolder {

        private TextView ItemName;
        private TextView ItemPrice;
        private TextView ItemQuantity;
        private TextView ItemTotal;
        private TextView tv_C_Item;
        private TextView tv_C_Price;
        private TextView tv_C_Amount;

        public MyHolder(View itemView) {
            super(itemView);

            ItemName = itemView.findViewById(R.id.txtItemName);
            ItemPrice = itemView.findViewById(R.id.txtItemPrice);
            ItemQuantity = itemView.findViewById(R.id.txtBillQty);
            ItemTotal = itemView.findViewById(R.id.txtBillTotal);
            tv_C_Item = itemView.findViewById(R.id.cItemName);
            tv_C_Price = itemView.findViewById(R.id.cItemPrice);
            tv_C_Amount = itemView.findViewById(R.id.custAmount);
        }
    }
}
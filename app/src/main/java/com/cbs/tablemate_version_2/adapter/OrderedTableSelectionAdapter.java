package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.configuration.Settings;
import com.cbs.tablemate_version_2.models.Table;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 20-08-2016 in TableMateNew
 ***********************************************************************/
public class OrderedTableSelectionAdapter extends BaseAdapter {

    private ArrayList<Table> table_list;
    private Context context;
    private LayoutInflater inflater;
    private Settings settings;

    public OrderedTableSelectionAdapter(Context contexts, ArrayList<Table> table_list) {
        this.context = contexts;
        this.table_list = table_list;

    }

    public void MyDataChanged(ArrayList<Table> tables) {
        table_list = tables;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return table_list.size();
    }

    @Override
    public Object getItem(int p) {
        return table_list.get(p);
    }

    @Override
    public long getItemId(int p) {
        return p;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.singletable, null);

        ImageView img = convertView.findViewById(R.id.imgTable);
        TextView tableId = convertView.findViewById(R.id.txtTableId);
        TextView tableSize = convertView.findViewById(R.id.txtTableSize);
        FrameLayout frameGridView = convertView.findViewById(R.id.frameGridView);

        Table t_obj = table_list.get(position);

        if (t_obj != null) {
            tableId.setText(t_obj.getId());

            tableSize.setText("Seats:" + t_obj.getSize());
        }

        return convertView;
    }
}


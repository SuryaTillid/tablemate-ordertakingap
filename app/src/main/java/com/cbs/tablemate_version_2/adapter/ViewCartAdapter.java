package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.interfaces.MenuItemQtyChangeListener;
import com.cbs.tablemate_version_2.models.Item;
import com.cbs.tablemate_version_2.utilities.Utility;

import java.util.ArrayList;

/**
 * Created by Barani on 18-05-2016.
 */
public class ViewCartAdapter extends RecyclerView.Adapter<ViewCartAdapter.MyHolder> {

    ArrayList<Item> cart_list = new ArrayList<>();
    Context context;
    MenuItemQtyChangeListener listener;
    private LayoutInflater inflater;
    private String orderType;

    public ViewCartAdapter(Context context, ArrayList<Item> cart_list, String orderType) {
        this.context = context;
        this.cart_list = cart_list;
        inflater = LayoutInflater.from(context);
        this.orderType = orderType;
    }

    public void MyDataChanged(ArrayList<Item> cart_list) {
        this.cart_list = cart_list;
        notifyDataSetChanged();
    }

    public void setMenuListener(MenuItemQtyChangeListener menuListener) {
        this.listener = menuListener;
    }

    @NonNull
    @Override
    public ViewCartAdapter.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.activity_cart_row, parent, false);
        return new MyHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        Item item = cart_list.get(position);
        double price;


        if (item.getEnable_NC().equals("1")) {
            price = Utility.getItemPriceNC(item, orderType, "1");
        } else {
            price = Utility.getItemPriceNC(item, orderType, "");

        }
        float totalPrice = (float) (price * Utility.convertToFloat(item.getItemQty()));
        String user_comment = item.getCustomisation_comments().trim();
        String nc = getColoredSpanned("NC : ", "#ff0000");

        holder.itemQty.setText(item.getItemQty());


        holder.txtItemPrice.setText(String.format("%.2f", price));
        holder.txtItemName.setText(Utility.getItemName(item));
        holder.txtItemTotal.setText(String.format("%.2f", totalPrice));

        // changeNCContent(item, holder);


        Log.e("CustomerEnableNC", "" + item.getEnable_NC());
        if (!TextUtils.isEmpty(user_comment)) {
            user_comment = getColoredSpanned(user_comment, "#000000");
            holder.txt_user_comment.setVisibility(View.VISIBLE);
            if (item.getEnable_NC().equalsIgnoreCase("1")) {
                holder.txt_user_comment.setText(Html.fromHtml(nc + " " + user_comment));
            } else {
                holder.txt_user_comment.setText(Html.fromHtml("(" + user_comment + ")"));
            }
        } else {
            if (item.getEnable_NC().equalsIgnoreCase("1")) {
                holder.txt_user_comment.setVisibility(View.VISIBLE);
                holder.txt_user_comment.setText("( NC )");
                holder.txt_user_comment.setTextColor(Color.parseColor("#ff0000"));
            } else {
                holder.txt_user_comment.setVisibility(View.INVISIBLE);
            }
        }

        String s = item.getColor_filter_type();

        if (!TextUtils.isEmpty(s)) {
            try {
                holder.img_veg_nv_icon.setColorFilter(Color.parseColor(s));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /*if ("1".equals(s)) {
            holder.img_veg_nv_icon.setImageResource(R.drawable.ic_veg);

        } else if ("2".equals(s)) {
            holder.img_veg_nv_icon.setImageResource(R.drawable.ic_non_veg);
        }
        else if ("4".equals(s)) {
            holder.img_veg_nv_icon.setImageResource(R.drawable.ic_dessert);
        }
        else if ("5".equals(s)) {
            holder.img_veg_nv_icon.setImageResource(R.drawable.ic_a);
        }*/
    }

    private void changeNCContent(Item item, MyHolder holder) {
        if (item.getEnable_NC().equalsIgnoreCase("1")) {
            holder.btnNC.setText("Remove");
        }
//        else {
//            holder.btnNC.setText("NC");
//        }
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public int getItemCount() {
        return cart_list.size();
    }

    private void decrement(ArrayList<Item> cart_list, int adapterPosition, Button itemQty) {
        listener.decrementQuantity(cart_list.get(adapterPosition).getCustomId(), adapterPosition);
        if (itemQty.getText().toString().equals("0")) {
            cart_list.remove(adapterPosition);
            notifyItemRemoved(adapterPosition);
            notifyDataSetChanged();
        }
    }

    class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Button itemAdd;
        Button itemDetect;
        Button itemQty;
        TextView txtItemName;
        TextView txtItemPrice;
        TextView txtItemTotal;
        ImageView img_veg_nv_icon;
        ImageButton img_btn_customize;
        TextView txt_user_comment;
        Button btnNC, btnRemove;

        public MyHolder(View vi) {
            super(vi);
            itemAdd = vi.findViewById(R.id.btn_add);
            itemDetect = vi.findViewById(R.id.btn_deduct);
            txt_user_comment = vi.findViewById(R.id.txt_user_comment);
            itemQty = vi.findViewById(R.id.btn_show_qty);
            txtItemName = vi.findViewById(R.id.txt_food_name);
            txtItemPrice = vi.findViewById(R.id.txtItemPrice);
            txtItemTotal = vi.findViewById(R.id.txtItemTotal);
            img_veg_nv_icon = vi.findViewById(R.id.veg_nv_icon);
            img_btn_customize = vi.findViewById(R.id.img_customize);
            btnNC = vi.findViewById(R.id.btn_NC);
            btnRemove = vi.findViewById(R.id.btn_remove);

            itemAdd.setOnClickListener(this);
            itemDetect.setOnClickListener(this);
            img_btn_customize.setOnClickListener(this);
            btnNC.setOnClickListener(this);
            btnRemove.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            try {
                switch (v.getId()) {
                    case R.id.btn_add:
                        String id = cart_list.get(getAdapterPosition()).getCustomId();
                        listener.incrementQuantity(id, getAdapterPosition());
                        break;
                    case R.id.btn_deduct:
                        decrement(cart_list, getAdapterPosition(), itemQty);
                        break;
                    case R.id.img_customize:
                        listener.show_open_customization(cart_list.get(getAdapterPosition()).getCustomId());
                        break;
                    case R.id.btn_NC:
                        if (!cart_list.get(getAdapterPosition()).getEnable_NC().equalsIgnoreCase("1")) {
                            listener.update_nc(cart_list.get(getAdapterPosition()).getCustomId());
                        }
                    case R.id.btn_remove:

                        decrement(cart_list, getAdapterPosition(), itemQty);

                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

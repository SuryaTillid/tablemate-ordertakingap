package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.models.TaxSplit;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 06-11-2017 in TableMateNew
 ***********************************************************************/
public class TaxListAdapter extends RecyclerView.Adapter<TaxListAdapter.MyHolder> {

    ArrayList<TaxSplit> bill_list = new ArrayList<>();
    Context context;
    private LayoutInflater inflater;

    public TaxListAdapter(Context context, ArrayList<TaxSplit> b_list) {
        this.context = context;
        this.bill_list = b_list;
        inflater = LayoutInflater.from(context);
    }

    public void MyDataChanged(ArrayList<TaxSplit> bill_list) {
        this.bill_list = bill_list;
        notifyDataSetChanged();
    }

    @Override
    public TaxListAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.activity_tax_row, parent, false);
        return new MyHolder(v);
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        TaxSplit bill = bill_list.get(position);

        holder.txtTaxName.setText(bill.getTax_name());
        holder.txtTaxAmt.setText(bill.getTax_sum_api());
    }

    @Override
    public int getItemCount() {
        return bill_list.size();
    }

    class MyHolder extends RecyclerView.ViewHolder {

        TextView txtTaxName;
        TextView txtTaxAmt;

        public MyHolder(View v) {
            super(v);

            txtTaxName = v.findViewById(R.id.txt_tax_name);
            txtTaxAmt = v.findViewById(R.id.txt_tax_value);
        }
    }
}



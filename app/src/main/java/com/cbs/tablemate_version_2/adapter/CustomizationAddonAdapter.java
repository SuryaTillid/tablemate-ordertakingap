package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.models.CustomizationItemAddon;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 29-07-2016 in TableMateNew
 ***********************************************************************/

public class CustomizationAddonAdapter extends RecyclerView.Adapter<CustomizationAddonAdapter.Holder> {
    private Context context;
    private ArrayList<CustomizationItemAddon> addonsList;
    private LayoutInflater inflater;
    private AddOnListener listener;

    public CustomizationAddonAdapter(Context context, ArrayList<CustomizationItemAddon> addonArrayList) {
        this.context = context;
        this.addonsList = addonArrayList;
        inflater = LayoutInflater.from(context);
    }

    public void MyDataChanged(ArrayList<CustomizationItemAddon> addons) {
        addonsList = addons;
        notifyDataSetChanged();
    }

    public void setListener(AddOnListener listener) {
        this.listener = listener;
    }

    @Override
    public CustomizationAddonAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.row_for_addon, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(CustomizationAddonAdapter.Holder holder, int position) {
        CustomizationItemAddon addon = addonsList.get(position);
        holder.txt_name.setText(addon.getStockName());
        holder.txt_unit.setText(addon.getUnit_master_id());
        holder.txt_quantity.setText(addon.getAddon_quantity() + addon.getSub_unit_name());
    }

    @Override
    public int getItemCount() {
        return addonsList.size();
    }

    public interface AddOnListener {
        void selectAddOn(int position);
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView txt_name;
        TextView txt_unit;
        TextView txt_quantity;
        Button btn_add;
        Button btn_deduct;


        public Holder(View itemView) {
            super(itemView);

            txt_name = itemView.findViewById(R.id.txtAddonName);
            txt_unit = itemView.findViewById(R.id.txtUnit);
            txt_quantity = itemView.findViewById(R.id.txtAddonQty);
            btn_add = itemView.findViewById(R.id.btnAdd);
            btn_deduct = itemView.findViewById(R.id.btnDeduct);

        }
    }
}

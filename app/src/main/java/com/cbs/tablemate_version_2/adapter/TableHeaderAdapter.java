package com.cbs.tablemate_version_2.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.expand.TablesViewHolder;
import com.cbs.tablemate_version_2.expand.TitleViewHolder;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/*********************************************************************
 * Created by Barani on 10-08-2018 in TableMateNew
 ********************************************************************/
public class TableHeaderAdapter extends ExpandableRecyclerViewAdapter<TitleViewHolder, TablesViewHolder> {

    public TableHeaderAdapter(List<? extends ExpandableGroup> groups) {
        super(groups);
    }

    @Override
    public TitleViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_tier, parent, false);
        return new TitleViewHolder(view);
    }

    @Override
    public TablesViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.singletable, parent, false);
        return new TablesViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(TablesViewHolder holder, int flatPosition, ExpandableGroup group,
                                      int childIndex) {
        // final Table artist = ((Tier) group).getItems().get(childIndex);
        // holder.setTierName(artist);

    }

    @Override
    public void onBindGroupViewHolder(TitleViewHolder holder, int flatPosition,
                                      ExpandableGroup group) {
        holder.setTierTitle(group);
    }
}
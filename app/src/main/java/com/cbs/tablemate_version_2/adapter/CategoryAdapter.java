package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.interfaces.MenuListener;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 21-05-2016 in TableMateNew
 ***********************************************************************/
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyHolder> {
    MenuListener listener;
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<String> menuItemsList;
    private int selectedPos = 0;

    public CategoryAdapter(Context context, ArrayList<String> menuItemsList) {
        this.context = context;
        this.menuItemsList = menuItemsList;
        inflater = LayoutInflater.from(context);
    }

    public void MyDataChanged(ArrayList<String> menus) {
        menuItemsList = menus;
        notifyDataSetChanged();
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.category_row, parent, false);
        return new MyHolder(v);
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        holder.btnCategory.setText(menuItemsList.get(position));
        holder.itemView.setSelected(selectedPos == position);

        if (selectedPos == position) {
            // view is selected
            holder.btnCategory.setBackgroundResource(R.drawable.category_filter_bg);
            holder.btnCategory.setFocusable(true);
        } else
            // view is not selected
            holder.btnCategory.setBackgroundColor(Color.WHITE);
    }

    @Override
    public int getItemCount() {
        return menuItemsList.size();
    }

    public void setMenuListener(MenuListener menuListener) {
        this.listener = menuListener;
    }

    public class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Button btnCategory;

        public MyHolder(View itemView) {
            super(itemView);
            btnCategory = itemView.findViewById(R.id.btnCategory);
            btnCategory.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            notifyItemChanged(selectedPos);
            selectedPos = getLayoutPosition();
            notifyItemChanged(selectedPos);
            String category = btnCategory.getText().toString();
            AppLog.write("Item clicked-----", category);
            listener.onMenuClick(category);
        }
    }
}

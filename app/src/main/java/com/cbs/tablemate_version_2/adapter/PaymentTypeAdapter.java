package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.models.GetPaymentOptions;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 05-08-2016 in TableMateNew
 *********************************************************************/
public class PaymentTypeAdapter extends RecyclerView.Adapter<PaymentTypeAdapter.Holder> {
    private Context context;
    private ArrayList<GetPaymentOptions> optionsList;
    private LayoutInflater inflater;

    public PaymentTypeAdapter(Context context, ArrayList<GetPaymentOptions> optionsList) {
        this.context = context;
        this.optionsList = optionsList;
        inflater = LayoutInflater.from(context);
    }

    public void MyDataChanged(ArrayList<GetPaymentOptions> options) {
        optionsList = options;
        notifyDataSetChanged();
    }

    @Override
    public PaymentTypeAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.card_type_row, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(PaymentTypeAdapter.Holder holder, int position) {
        GetPaymentOptions options = optionsList.get(position);
        holder.txtPaymentType.setText(options.getPaymentmode());
    }

    @Override
    public int getItemCount() {
        return optionsList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView txtPaymentType;

        public Holder(View itemView) {
            super(itemView);
            txtPaymentType = itemView.findViewById(R.id.txt_payment_type);
        }
    }
}

package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.models.OrdersForTable;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 23-11-2016 in TableMateNew
 ***********************************************************************/
public class OrderStatusAdapter extends RecyclerView.Adapter<OrderStatusAdapter.MyHolder> {
    private ArrayList<OrdersForTable> ordersForTableArrayList;
    private Context context;
    private LayoutInflater inflater;


    public OrderStatusAdapter(Context context, ArrayList<OrdersForTable> ordersForTables) {
        this.context = context;
        this.ordersForTableArrayList = ordersForTables;
        inflater = LayoutInflater.from(context);
    }

    public void MyDataChanged(ArrayList<OrdersForTable> ordersForTables) {
        ordersForTableArrayList = ordersForTables;
        notifyDataSetChanged();
    }

    @Override
    public OrderStatusAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.activity_order_progress_row, parent, false);
        return new MyHolder(v);
    }

    @Override
    public void onBindViewHolder(OrderStatusAdapter.MyHolder holder, int position) {
        OrdersForTable ordersForTable = ordersForTableArrayList.get(position);

        holder.itemName.setText(ordersForTable.getMenu_item_name());
        holder.txtItemProgress.setText(checkOrderStatus(ordersForTable));
        if (ordersForTable.getStatus().equalsIgnoreCase("4")) {
            holder.txtItemProgress.setTextColor(Color.RED);
        }
    }

    private String checkOrderStatus(OrdersForTable ordersForTable) {
        String orderStatus = ordersForTable.getStatus();
        switch (orderStatus) {
            case "0":
                orderStatus = "Ordered";
                break;
            case "1":
                orderStatus = "Processing";
                break;
            case "2":
                orderStatus = "Finished";
                break;
            case "3":
                orderStatus = "Cancelled";
                break;
            case "4":
                orderStatus = "Unable to process";
                break;
            case "5":
                orderStatus = "Split";
                break;
        }
        return orderStatus;
    }

    @Override
    public int getItemCount() {
        return ordersForTableArrayList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        TextView itemName;
        TextView txtItemProgress;

        public MyHolder(View itemView) {
            super(itemView);
            itemName = itemView.findViewById(R.id.txtItemName);
            txtItemProgress = itemView.findViewById(R.id.txtItemProgress);
        }
    }
}

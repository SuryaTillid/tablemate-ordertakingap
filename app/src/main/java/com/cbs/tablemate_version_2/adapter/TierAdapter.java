package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.interfaces.TableClickListener;
import com.cbs.tablemate_version_2.interfaces.TableGroupClickListener;
import com.cbs.tablemate_version_2.models.TierValues;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 20-08-2018 in TableMateNew
 ***********************************************************************/
public class TierAdapter extends RecyclerView.Adapter<TierAdapter.MyHolder> {
    private ArrayList<TierValues> s_list = new ArrayList<>();
    private Context context;
    private LayoutInflater inflater;
    private TableGroupClickListener listener;

    public TierAdapter(Context context, ArrayList<TierValues> list, TableGroupClickListener listener) {
        this.context = context;
        this.s_list = list;
        this.listener = listener;
        inflater = LayoutInflater.from(context);
    }

    public void MyDataChanged(ArrayList<TierValues> list) {
        this.s_list = list;
        notifyDataSetChanged();
    }

    @Override
    public TierAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.list_item_tier, parent, false);
        return new TierAdapter.MyHolder(v);
    }

    @Override
    public void onBindViewHolder(TierAdapter.MyHolder holder, int position) {
        holder.bind(s_list.get(position));
    }

    @Override
    public int getItemCount() {
        return s_list.size();
    }

    class MyHolder extends RecyclerView.ViewHolder implements TableClickListener {
        private RecyclerView rv_table_grid;
        private TextView list_item_tier_name;

        public MyHolder(View itemView) {
            super(itemView);
            list_item_tier_name = itemView.findViewById(R.id.list_item_tier_name);
            rv_table_grid = itemView.findViewById(R.id.rv_table_grid);
        }

        void bind(TierValues tierValues) {
            list_item_tier_name.setText(tierValues.getTier());
            TableAdapter adapter = new TableAdapter(rv_table_grid.getContext(), tierValues.getTableList(), this);
            GridLayoutManager layoutManager = new GridLayoutManager(rv_table_grid.getContext(), 3);
            rv_table_grid.setLayoutManager(layoutManager);
            rv_table_grid.setAdapter(adapter);
        }

        @Override
        public void OnTableClick(int childPosition) {
            listener.OnTableClick(getAdapterPosition(), childPosition);
        }
    }
}
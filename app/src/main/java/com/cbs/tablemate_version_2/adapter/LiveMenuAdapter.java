package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.configuration.AppLog;
import com.cbs.tablemate_version_2.models.StockItem;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 01-11-2016 in TableMateNew
 ***********************************************************************/
public class LiveMenuAdapter extends RecyclerView.Adapter<LiveMenuAdapter.MyHolder> {
    private ArrayList<StockItem> s_list = new ArrayList<>();
    private Context context;
    private LayoutInflater inflater;

    public LiveMenuAdapter(Context context, ArrayList<StockItem> list) {
        this.context = context;
        this.s_list = list;
        inflater = LayoutInflater.from(context);
    }

    public LiveMenuAdapter(ArrayList<StockItem> liveMenuList, Context context) {
        this.context = context;
        this.s_list = liveMenuList;
        inflater = LayoutInflater.from(context);
    }

    public void MyDataChanged(ArrayList<StockItem> list) {
        this.s_list = list;
        notifyDataSetChanged();
    }

    public void setFilter(ArrayList<StockItem> stockItems) {
        s_list = new ArrayList<>();
        s_list.addAll(stockItems);
        notifyDataSetChanged();
    }

    @Override
    public LiveMenuAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.activity_live_menu_item_row, parent, false);
        return new LiveMenuAdapter.MyHolder(v);
    }

    @Override
    public void onBindViewHolder(LiveMenuAdapter.MyHolder holder, int position) {
        StockItem stocks = s_list.get(position);
        String stock_quantity = stocks.getStockQuantity();
        Double s = Double.parseDouble(stock_quantity);
        s = Math.round(s * 100.0) / 100.0;
        AppLog.write("Double value-----", "" + s);

        holder.itemName.setText(stocks.getStockName());
        holder.itemQuantity.setText(stocks.getStockQuantity());
    }

    @Override
    public int getItemCount() {
        return s_list.size();
    }

    class MyHolder extends RecyclerView.ViewHolder {

        TextView itemName;
        TextView itemQuantity;

        public MyHolder(View itemView) {
            super(itemView);

            itemName = itemView.findViewById(R.id.txtItemName);
            itemQuantity = itemView.findViewById(R.id.txtItemQty);
        }
    }
}
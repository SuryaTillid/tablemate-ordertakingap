package com.cbs.tablemate_version_2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cbs.tablemate_version_2.R;
import com.cbs.tablemate_version_2.models.CustomizationItemsType;

import java.util.ArrayList;

/*********************************************************************
 * Created by Barani on 29-07-2016 in TableMateNew
 ***********************************************************************/
public class CustomizationItemTypeAdapter extends RecyclerView.Adapter<CustomizationItemTypeAdapter.Holder> {

    ItemTypeListener listener;
    private ArrayList<CustomizationItemsType> customizationItemsTypes;
    private Context contexts;
    private LayoutInflater inflater;

    public CustomizationItemTypeAdapter(Context context, ArrayList<CustomizationItemsType> customizationItemsTypes) {
        this.contexts = context;
        this.customizationItemsTypes = customizationItemsTypes;
        inflater = LayoutInflater.from(context);
    }

    public void MyDataChanged(ArrayList<CustomizationItemsType> itemsTypes) {
        customizationItemsTypes = itemsTypes;
        notifyDataSetChanged();
    }

    public void setListener(ItemTypeListener listener) {
        this.listener = listener;
    }

    @Override
    public CustomizationItemTypeAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.row_for_item_type, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(CustomizationItemTypeAdapter.Holder holder, int position) {
        CustomizationItemsType type = customizationItemsTypes.get(position);
        holder.item_name.setText(type.getCustomisation_name());
        if (type.isSelected()) {
            holder.item_name.setBackgroundResource(R.drawable.bg_green_button);
        } else {
            holder.item_name.setBackgroundResource(R.drawable.editext_style_normal);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return customizationItemsTypes.size();
    }

    public interface ItemTypeListener {
        void selectItemType(int position);
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView item_name;

        public Holder(View itemView) {
            super(itemView);

            item_name = itemView.findViewById(R.id.item_typeName);
            item_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //listener.selectItemType(getAdapterPosition());
                }
            });
        }
    }
}
